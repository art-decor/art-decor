const plugins = []
if (process.env.NODE_ENV === 'test') {
  plugins.push('transform-require-context')
}

module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset'
  ],
  plugins
}
