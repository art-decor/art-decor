require('@rushstack/eslint-patch/modern-module-resolution')

const path = require('node:path')

module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/essential',
    '@vue/eslint-config-standard'
  ],
  parserOptions: {
    parser: '@babel/eslint-parser'
  },
  rules: {
    'import/no-unresolved': 'error',
    'vue/valid-v-slot': ['error', {
      allowModifiers: true
    }],
    'vue/no-v-text-v-html-on-component': [
      'error',
      {
        allow: [
          'v-icon',
          'v-card-text',
          'v-card-title',
          'v-chip'
        ]
      }
    ],
    'vue/no-mutating-props': ['error', {
      shallowOnly: true
    }]
  },
  settings: {
    'import/resolver': {
      [require.resolve('eslint-import-resolver-custom-alias')]: {
        alias: {
          '@': `${path.resolve(__dirname, './src')}`
        },
        extensions: ['.mjs', '.js', '.jsx', '.json', '.node', '.vue']
      }

    }

  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)'
      ],
      env: {
        jest: true
      }
    }
  ]
}
