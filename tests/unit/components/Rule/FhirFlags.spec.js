import { mount, createLocalVue } from '@vue/test-utils'
import Vuetify from 'vuetify/lib'
import FhirFlags from '@/components/Rule/FhirFlags'
import AdButton from '@/components/Utilities/AdButton'

const localVue = createLocalVue()
localVue.component('AdButton', AdButton)

describe('FhirFlags', () => {
  let vuetify
  beforeEach(() => {
    vuetify = new Vuetify()
  })

  it('renders no chips when prop value has no flags set to true', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: {
        value: {
        }
      }
    })
    expect(wrapper.find('[data-test="chip"]').exists()).toBe(false)
  })

  it('renders one chip when prop value has one flag set to true', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: {
        value: {
          isModifier: 'true',
          isSummary: 'false'
        }
      }
    })
    expect(wrapper.find('[data-test="chip"]').exists()).toBe(true)
    expect(wrapper.find('[data-test="chip"]').isVisible()).toBe(true)
  })

  it('renders n chips when prop value has n flags set to true', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: {
        value: {
          isModifier: 'true',
          isSummary: 'true',
          hasConstraint: 'true',
          mustSupport: 'true'
        }
      }
    })
    const chips = wrapper.findAll('[data-test="chip"]')
    expect(chips.length).toBe(4)
    chips.wrappers.forEach(chip => {
      expect(chip.isVisible()).toBe(true)
    })
  })

  it('shows an add button when prop editable is true', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: { editable: true, value: {} }
    })
    expect(wrapper.find('[data-test="add-btn"]').exists()).toBe(true)
    expect(wrapper.find('[data-test="add-btn"]').isVisible()).toBe(true)
  })

  it('shows no add button when prop editable is false', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: { editable: false, value: {} }
    })
    expect(wrapper.find('[data-test="add-btn"]').exists()).toBe(false)
  })

  it('shows a tooltip when a chip recieves a mouseenter event', (done) => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: { value: { isModifier: 'true' } }
    })
    const chip = wrapper.find('[data-test="chip"]')
    const tooltip = wrapper.find('[data-test="tooltip"]')
    const openDelay = tooltip.vm.openDelay || 0
    chip.trigger('mouseenter')
    setTimeout(() => {
      expect(chip.exists()).toBe(true)
      expect(tooltip.exists()).toBe(true)
      expect(tooltip.vm.isActive).toBe(true)
      done()
    }, openDelay)
  })

  it('has disabled chips when prop disabled is true', () => {
    const wrapper = mount(FhirFlags, {
      localVue,
      vuetify,
      propsData: { disabled: true, value: { isModifier: 'true' } }
    })
    const chip = wrapper.find('[data-test="chip"]')
    expect(chip.vm.disabled).toBe(true)
    expect(chip.classes()).toContain('v-chip--disabled')
  })
})
