import getCardinality from '@/filters/getCardinality.js'

describe('getCardinality', () => {
  it('returns null when minimumMultiplicity or maximumMultiplicity is missing', () => {
    const item = { minimumMultiplicity: '0' }
    expect(getCardinality(item)).toBeNull()
  })
  it('returns the formatted cardinality when both minimumMultiplicity and maximumMultiplicity are provided', () => {
    const item = { minimumMultiplicity: '0', maximumMultiplicity: '1' }
    expect(getCardinality(item)).toBe('0..1')
  })
})
