import getShortId from '@/filters/getShortId.js'

describe('getShortId', () => {
  it('returns a shortened version of the passed id', () => {
    const id = '2.16.840.1.113883.2.4.3.11.60.20'
    expect(getShortId(id)).toMatch('20')
  })
})
