import store from '@/store'
import getMatchingTranslation from '@/filters/getMatchingTranslation.js'

describe('getMatchingTranslation', () => {
  beforeEach(() => {
    store.commit('app/setProjectLanguage', null)
    store.commit('project/setProjectInFocus', null)
  })

  it('returns #text if no selector is passed', () => {
    const resultForObject = getMatchingTranslation({
      value: {
        '#text': 'a',
        language: 'nl-NL'
      }
    })
    expect(resultForObject).toBe('a')
    const resultForArray = getMatchingTranslation({
      value: [{
        '#text': 'a',
        language: 'nl-NL'
      }]
    })
    expect(resultForArray).toBe('a')
  })

  it('returns [selector] if specified', () => {
    const resultForObject = getMatchingTranslation({
      value: {
        mySelector: 'a',
        language: 'nl-NL'
      },
      selector: 'mySelector'
    })
    expect(resultForObject).toBe('a')

    const resultForArray = getMatchingTranslation({
      value: [{
        mySelector: 'a',
        language: 'nl-NL'
      }],
      selector: 'mySelector'
    })
    expect(resultForArray).toBe('a')
  })

  it('always returns a string if returnObject is not true', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'en-US'
      }
    ]
    expect(typeof getMatchingTranslation({})).toBe('string')
    expect(typeof getMatchingTranslation({ value: {} })).toBe('string')
    expect(typeof getMatchingTranslation({ value: null })).toBe('string')
    expect(typeof getMatchingTranslation({ value: example })).toBe('string')
  })

  it('returns the selected projectLanguage translation if present', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'fr-FR'
      }
    ]

    store.commit('app/setProjectLanguage', 'nl-NL')
    expect(getMatchingTranslation({ value: example })).toBe('a')
    store.commit('app/setProjectLanguage', 'fr-FR')
    expect(getMatchingTranslation({ value: example })).toBe('b')
  })

  it('returns the project default language translation if selected language is not available', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'fr-FR'
      }
    ]
    store.commit('project/setProjectInFocus', { defaultLanguage: 'fr-FR' })
    store.commit('app/setProjectLanguage', 'pl-PL')
    expect(getMatchingTranslation({ value: example })).toBe('b')
  })

  it('falls back to English if the project default language is not available', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'en-US'
      }
    ]
    store.commit('project/setProjectInFocus', { defaultLanguage: 'fr-FR' })
    store.commit('app/setProjectLanguage', 'pl-PL')
    expect(getMatchingTranslation({ value: example })).toBe('b')
  })

  it('returns the first translation if selected projectLanguage and project default are not found and English is not present', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'fr-FR'
      }
    ]

    expect(getMatchingTranslation({ value: example })).toBe('a')
  })

  it('returns an empty string when "disableFallback" is true and the requested translation is not found', () => {
    const example = [
      {
        '#text': 'a',
        language: 'nl-NL'
      },
      {
        '#text': 'b',
        language: 'en-US'
      }
    ]
    store.commit('app/setProjectLanguage', 'de-DE')
    expect(getMatchingTranslation({ value: example, disableFallback: true })).toBe('')
  })
})
