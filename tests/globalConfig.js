import Vue from 'vue'
import vuetify from 'vuetify'
Vue.use(vuetify)

const VueTestUtils = require('@vue/test-utils')

// Mock vue-i18n
VueTestUtils.config.mocks.$t = (i18nKey) => i18nKey

// Prevent console errors for some Vuetify components, like v-tooltip.
// (Unable to locate target [data-app])
const el = document.createElement('div')
el.setAttribute('data-app', true)
document.body.appendChild(el)
