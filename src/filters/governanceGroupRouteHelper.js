export default {
  isCurrentGovernanceGroupRoute (name) {
    return (this?.readonly || false)
      ? this.$route.name === name.replace(/^server-/, '')
      : this.$route.name === name
  },

  getGovernanceGroupRoute (name) {
    return (this?.readonly || false)
      ? name.replace(/^server-/, '')
      : name
  }
}
