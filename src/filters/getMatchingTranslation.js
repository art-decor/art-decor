// import i18n from '../plugins/i18n'
import store from '@/store'

/**
 * Get translation for project content
 *
 * @param value
 * @param selector
 * @param locale
 * @param returnObject
 * @param disableFallback
 * @return {string}
 */
export default ({ value, selector, locale, returnObject, disableFallback }) => {
  if (!(value instanceof Array)) {
    return String(value?.[selector || '#text'] || value || '')
  }

  const preferredProjectLanguage = locale || store.getters['app/projectLanguage']
  const projectDefault = store.getters['project/getProjectInFocus']?.defaultLanguage

  const matchingLabel = value.find(item => item.language === preferredProjectLanguage)
  const fallbackLabel = value.find(item => item.language === projectDefault)
  const englishLabel = value.find(item => item.language?.substr(0, 2) === 'en')
  const firstLabel = value[0]

  const parsedSelector = selector || '#text'

  // Check if matching label is available.
  if (matchingLabel) {
    // Return matching label.
    return returnObject !== true
      ? String(matchingLabel?.[parsedSelector] || '')
      : matchingLabel
  }

  // Check if fallbacks are allowed
  if (disableFallback) {
    return returnObject ? value : ''
  }

  // Check if fallback label is available.
  if (fallbackLabel) {
    // Return fallback label.
    return returnObject !== true
      ? String(fallbackLabel?.[parsedSelector] || '')
      : fallbackLabel
  }

  // Check if english label is available.
  if (englishLabel) {
    // Return fallback label.
    return returnObject !== true
      ? String(englishLabel?.[parsedSelector] || '')
      : englishLabel
  }

  // Check if first label is available.
  if (firstLabel) {
    // Return first label.
    return returnObject !== true
      ? typeof firstLabel === 'string'
        ? firstLabel
        : String(firstLabel?.[parsedSelector] || '')
      : firstLabel
  }

  // Return value.
  return returnObject !== true
    ? String(value?.value || '')
    : value
}
