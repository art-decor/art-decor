export default (type) => {
  if (type === 'project') {
    return 'mdi-briefcase'
  }
  if (type === 'dataset') {
    return 'mdi-book-open-blank-variant'
  }
  if (type === 'scenario') {
    return 'mdi-movie'
  }
  if (type === 'transaction') {
    return 'mdi-swap-horizontal-bold'
  }
  if (type === 'questionnaire') {
    return 'mdi-comment-question'
  }
  if (type === 'conceptMap') {
    return 'mdi-map-marker-distance'
  }
  if (type === 'valueSet') {
    return 'mdi-heart'
  }
  if (type === 'codeSystem') {
    return 'mdi-format-list-bulleted-type'
  }
  if (type === 'browsableCodeSystem') {
    return 'mdi-format-list-bulleted-type'
  }
  if (type === 'template') {
    return 'mdi-recycle'
  }
  if (type === 'profile') {
    return 'mdi-fire'
  }
  return 'mdi-origin'
}
