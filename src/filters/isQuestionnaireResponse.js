// export default (q) => !!(q?.relationship?.some(rel => rel.type === 'ANSW') || q?.qrInvalidRelationship)
export default (q) => !!(q?.questionnaire || q?.qrInvalidRelationship)
