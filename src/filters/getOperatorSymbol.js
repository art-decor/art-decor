export default (o) => {
  if (o === '<') return 'mdi-code-less-than'
  if (o === '<=') return 'mdi-code-less-than-or-equal'
  if (o === '>') return 'mdi-code-greater-than'
  if (o === '>=') return 'mdi-code-greater-than-or-equal'
  if (o === '=') return 'mdi-code-equal'
  if (o === '!=') return 'mdi-code-not-equal'
  if (o === 'exist') return 'mdi-checkbox-multiple-marked-circle'
  return 'mdi-map-marker-question-outline'
}
