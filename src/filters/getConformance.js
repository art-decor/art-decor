const $t = (input) => {
  return window.Vue?.$t(input) || input
}
/**
 * Get display name for conformance / mandatory if isMandatory === true
 * from item with elements conformance and optional isMandatory
 *
 * @param item
 * @return {string|*}
 */
export default (item) => {
  if (item?.isMandatory === 'true') return $t('mandatory')
  if (item?.conformance === 'O') return $t('optional')
  if (item?.conformance === 'R') return $t('required')
  if (item?.conformance === 'C') return $t('conditional')
  if (item?.conformance === 'NP') return $t('not-permitted')
  return ''
}
