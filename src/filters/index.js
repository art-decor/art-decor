import clone from './clone'
import get from './get'
import getArtefactColor from './getArtefactColor'
import getActorIcon from './getActorIcon'
import getCardinality from './getCardinality'
import getConformance from './getConformance'
import getConceptTypeStyling from './getConceptTypeStyling'
import getCurrentDate from './getCurrentDate'
import getDarkOrLightFontColor from './getDarkOrLightFontColor'
import getDatasetIcon from './getDatasetIcon'
import getDesignation from './getDesignation'
import getExampleBackgroundColor from './getExampleBackgroundColor'
import getIntention from './getIntention'
import getMatchingTranslation from './getMatchingTranslation'
import getMatchingDesignations from './getMatchingDesignations'
import getShortId from './getShortId'
import getTemplateIcon from './getTemplateIcon'
import getConceptMapIcon from './getConceptMapIcon'
import getValueDomainTypeIcon from './getValueDomainTypeIcon'
import getValuesetIcon from './getValuesetIcon'
import nonBreakable from './nonBreakable'
import getQuestionnaireIcon from './getQuestionnaireIcon'
import getSessionStorage from './getSessionStorage'
import getStatusColor from './getStatusColor'
import getScenarioIcon from './getScenarioIcon'
import getUrgentNewsColor from './getUrgentNewsColor'
import formatDate from './formatDate'
import formatDateShort from './formatDateShort'
import isExpired from './isExpired'
import formatDateSeparator from './formatDateSeparator'
import htmlHasTextContent from './htmlHasTextContent'
import isQuestionnaireResponse from './isQuestionnaireResponse'
import isTrue from './isTrue'
import listHelper from './listHelper'
import updateMatchingTranslation from './updateMatchingTranslation'
import numericStringComparator from './numericStringComparator'
import getOperatorSymbol from './getOperatorSymbol'
import createChildParentMap from './createChildParentMap'
import getCascadedArtifactIcon from './getCascadedArtifactIcon'

export default {
  clone,
  createChildParentMap,
  get,
  getActorIcon,
  getArtefactColor,
  getCardinality,
  getCascadedArtifactIcon,
  getConformance,
  getConceptTypeStyling,
  getCurrentDate,
  getDarkOrLightFontColor,
  getDatasetIcon,
  getDesignation,
  getExampleBackgroundColor,
  getIntention,
  getMatchingTranslation,
  getMatchingDesignations,
  getQuestionnaireIcon,
  getScenarioIcon,
  getSessionStorage,
  getShortId,
  getTemplateIcon,
  getConceptMapIcon,
  getValueDomainTypeIcon,
  getValuesetIcon,
  nonBreakable,
  getStatusColor,
  getUrgentNewsColor,
  formatDate,
  formatDateShort,
  isExpired,
  isQuestionnaireResponse,
  formatDateSeparator,
  htmlHasTextContent,
  isTrue,
  listHelper,
  updateMatchingTranslation,
  numericStringComparator,
  getOperatorSymbol,

  /**
   * ##################################################
   * ## DEPRECATED FILTER FUNCTIONS.
   * ##################################################
   */

  /** @deprecated */
  helper: {
    /** @deprecated */
    get (object, path, defaultValue) {
      console.error('$filters.helper.get() is deprecated use $filters.get() instead!')
      return get(object, path, defaultValue)
    }
  },
  /** @deprecated */
  moment: {
    /** @deprecated */
    datetime (value) {
      console.error('$filters.moment.datetime() is deprecated use $filters.formatDate() instead!')
      return formatDate(value)
    },
    /** @deprecated */
    joined (value) {
      console.error('$filters.moment.joined() is deprecated use $filters.formatDate() instead!')
      return formatDate(value)
    }
  },
  /** @deprecated */
  text: {
    /** @deprecated */
    darkLightFontColor (value) {
      console.error('$filters.text.darkLightFontColor() is deprecated use $filters.getDarkLightFontColor() instead!')
      return getDarkOrLightFontColor(value)
    },
    /** @deprecated */
    matchingTranslation ({ value, selector, locale, returnObject }) {
      console.error('$filters.text.matchingTranslation() is deprecated use $filters.getMatchingTranslation() instead!')
      return getMatchingTranslation({ value, selector, locale, returnObject })
    },
    /** @deprecated */
    updateMatchingTranslation ({ value, text, locale }) {
      console.error('$filters.text.updateMatchingTranslation() is deprecated use $filters.updateMatchingTranslation() instead!')
      return updateMatchingTranslation({ value, text, locale })
    }
  }
}
