/**
 * Check if light or dark font color is best choice.
 *
 * In base of given background color this function will
 * check if your should use light or dark font color
 * on it.
 *
 * @param value
 * @return {string}
 */
export default (value) => {
  // Define variables.
  let r, g, b, color

  // Check if color code is starting with rgb syntax.
  if (value.match(/^rgb/)) {
    // Get matching color values.
    color = value.match(/^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/)
    // Get red.
    r = color[1]
    // Get green.
    g = color[2]
    // Get blue.
    b = color[3]
  } else {
    // Get matching color values.
    color = +('0x' + value.slice(1).replace(value.length < 5 && /./g, '$&$&'))
    // Get red.
    r = color >> 16
    // Get green.
    g = color >> 8 & 255
    // Get blue.
    b = color & 255
  }

  // Calculate hsp.
  const hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b))

  // Return black or white in base of if color is dark or light.
  return hsp > 125.5
    ? 'black'
    : 'white'
}
