const valueDomainTypes = [
  {
    value: 'string',
    icon: 'mdi-alphabet-latin'
  },
  {
    value: 'text',
    icon: 'mdi-subtitles-outline'
  },
  {
    value: 'code',
    icon: 'mdi-human-male-female'
  },
  {
    value: 'identifier',
    icon: 'mdi-account-multiple-outline'
  },
  {
    value: 'quantity',
    icon: 'mdi-beaker-outline'
  },
  {
    value: 'duration',
    icon: 'mdi-timelapse'
  },
  {
    value: 'currency',
    icon: 'mdi-currency-eur'
  },
  {
    value: 'date',
    icon: 'mdi-calendar-outline'
  },
  {
    value: 'datetime',
    icon: 'mdi-calendar-clock'
  },
  {
    value: 'time',
    icon: 'mdi-clock-outline'
  },
  {
    value: 'count',
    icon: 'mdi-counter'
  },
  {
    value: 'boolean',
    icon: 'mdi-toggle-switch-outline'
  },
  {
    value: 'decimal',
    icon: 'mdi-decimal'
  },
  {
    value: 'ordinal',
    icon: 'mdi-format-list-numbered'
  },
  {
    value: 'blob',
    icon: 'mdi-image-outline'
  },
  {
    value: 'complex',
    icon: 'mdi-head-question-outline'
  },
  /* the parts below are all from and for Questionnaire Item Type */
  {
    value: 'dateTime',
    icon: 'mdi-calendar-clock'
  },
  {
    value: 'url',
    icon: 'mdi-network'
  },
  {
    value: 'choice',
    icon: 'mdi-arrow-decision'
  },
  {
    value: 'open-choice',
    icon: 'mdi-arrow-decision-auto'
  },
  {
    value: 'attachment',
    icon: 'mdi-attachment'
  },
  {
    value: 'reference',
    icon: 'mdi-link'
  },
  {
    value: 'integer',
    icon: 'mdi-numeric'
  }
]

export default (type) => {
  return valueDomainTypes.find(t => t.value === type)?.icon || null
}
