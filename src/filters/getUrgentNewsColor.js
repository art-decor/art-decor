/**
 * Get urgent news color.
 *
 * @param type
 * @return {string|*}
 */
export default (type) => {
  if (type === 'info') return 'info'
  if (type === 'warning') return 'warning'
  if (type === 'error') return 'error'
  return 'success'
}
