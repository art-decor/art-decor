/**
 * Get status color.
 *
 * @param status
 * @param darker
 * @return {string|*}
 */
export default (status, darker) => {
  if (status === 'draft') return 'amber ' + (darker ? 'darker-2' : 'lighten-2')
  if (status === 'deprecated') return 'blue ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'final') return 'green ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'active') return 'green ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'rejected') return 'pink ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'pending') return 'orange ' + (darker ? '' : 'lighten-2')
  if (status === 'cancelled') return 'blue ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'retired') return 'blue ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'closed') return 'green ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'open') return 'red ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'inprogress') return 'orange ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'feedback') return 'orange ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'closed') return 'green ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'rejected') return 'orange ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'deferred') return 'purple ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'cancelled') return 'orange ' + (darker ? 'lighten-1' : 'lighten-3')
  if (status === 'failed') return 'red ' + (darker ? 'lighten-1' : 'lighten-3')

  // Fhir QuestionnaireResponse (http://hl7.org/fhir/questionnaire-answers-status)
  // if (status === 'in-progress') return 'amber ' + (darker ? 'darker-2' : 'lighten-2')
  if (status === 'completed') return 'green ' + (darker ? 'lighten-1' : 'lighten-3')
  // if (status === 'amended') return 'orange ' + (darker ? '' : 'lighten-2')
  // if (status === 'entered-in-error') return 'red ' + (darker ? 'lighten-1' : 'lighten-3')
  // if (status === 'stopped') return 'purple ' + (darker ? 'lighten-1' : 'lighten-3')

  return 'grey lighten-1 black--text'
}
