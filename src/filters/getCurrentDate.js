import { format } from 'date-fns'

/**
 * Get current date.
 *
 * @return {string|*}
 */
export default () => {
  // Return current date.
  return format(new Date(), 'yyyy-MM-dd\'T\'HH:mm:ss')
}
