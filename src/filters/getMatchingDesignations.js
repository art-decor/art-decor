// import i18n from '../plugins/i18n'
import store from '@/store'

/**
 * Get all matching designations
 *
 * @param value
 * @param selector
 * @param locale
 * @param returnObject
 * @param disableFallback
 * @return {string}
 */
export default ({ value }) => {
  const preferredProjectLanguage = store.getters['app/projectLanguage']
  const projectDefault = store.getters['project/getProjectInFocus']?.defaultLanguage

  // CAVE: for expansion sets the returned language is in property "lang" not "language" as anywhere else
  const matchingLabel = value?.filter(item => item.language === preferredProjectLanguage)
  const fallbackLabel = value?.filter(item => item.language === projectDefault)
  const englishLabel = value?.filter(item => item.language.substr(0, 2) === 'en')
  const firstLabel = value?.[0]

  // Check if matching label is available.
  if (matchingLabel) {
    // Return matching label.
    return matchingLabel
  }

  // Check if fallback label is available.
  if (fallbackLabel) {
    // Return fallback label.
    return fallbackLabel
  }

  // Check if english label is available.
  if (englishLabel) {
    // Return fallback label.
    return englishLabel
  }

  // Check if first label is available.
  if (firstLabel) {
    // Return first label.
    return firstLabel
  }

  // Return value.
  return value
}
