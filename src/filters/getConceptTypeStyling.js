export default (type) => {
  switch (type) {
    case 'D':
      return 'text-decoration-line-through grey--text font-italic'
    case 'A':
      return 'grey--text font-italic'
    case 'S':
      return 'font-italic'
    default:
      return ''
  }
}
