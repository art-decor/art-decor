// Checks wether a string contains only empty HTML tags
export default (string) => {
  if (typeof string !== 'string' && !(string instanceof String)) {
    return false
  }
  const regex = /(((<\w+>)+[ \n(<br>)]*(<\/\w+>)+)+)|<br>/g
  return string.replace(regex, '').trim() !== ''
}
