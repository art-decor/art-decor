import { format } from 'date-fns'

/**
 * Format date.
 *
 * @param value
 * @return {string|*}
 */
export default (value) => {
  // Return format iso 9075.
  return format(new Date(value), "yyyy-MM-dd '–' HH:mm")
}
