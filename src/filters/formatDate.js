import { formatISO9075 } from 'date-fns'
const $t = (input) => {
  return window.Vue?.$t(input) || input
}

/**
 * Format date.
 *
 * @param value
 * @return {string|*}
 */
export default (value) => {
  // Return format iso 9075.
  try {
    return formatISO9075(new Date(value), 'yyyy-MM-dd HH:mm')
  } catch (error) {
    return $t('invalid-value')
  }
}
