export default (o) => {
  if (o?.contains) return 'mdi-share-all'
  if (o?.inheritedContains) return 'mdi-share-all-outline'
  if (o?.recycleBin) return 'mdi-trash-can'
  if (o?.type === 'group') return o?.inherit ? 'mdi-folder-outline' : 'mdi-folder'
  if (o?.type === 'item') return o?.inherit ? 'mdi-link-variant' : 'mdi-cube'
  return 'mdi-book-open-blank-variant'
}
