import i18n from '../plugins/i18n'

/**
 * Check if light or dark font color is best choice.
 *
 * In base of given background color this function will
 * check if your should use light or dark font color
 * on it.
 *
 * @param value
 * @param text
 * @param locale
 * @return {array|null}
 */
export default ({ value, text, locale }) => {
  // Check if value is not instance of array.
  if (!(value instanceof Array)) {
    // Return null.
    return null
  }

  // Get current locale.
  const currentLocale = locale || i18n.locale
  // Try to get matching description in base of current locale.
  const matchingLabel = value.find(item => item.language === currentLocale)
  // Check if matching label is not null.
  if (!!matchingLabel === true) {
    // Update text.
    matchingLabel['#text'] = text
  } else {
    // Push new translation item.
    value.push({
      language: currentLocale,
      '#text': text
    })
  }

  // Return value object.
  return value
}
