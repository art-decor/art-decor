import { formatISO9075 } from 'date-fns'
const $t = (input) => {
  return window.Vue?.$t(input) || input
}

/**
 * Format date short.
 * Incoming date can be short or long (with time) format
 * @param value
 * @return {string|*}
 */
export default (value) => {
  // Return format iso 9075 in short mode.
  try {
    return formatISO9075(new Date(value), {
      representation: 'date',
      format: 'extended'
    })
  } catch (error) {
    return $t('invalid-value')
  }
}
