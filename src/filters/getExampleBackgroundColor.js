/**
 * Get example background color.
 *
 * @param type
 * @return {string|*}
 */
export default (type) => {
  if (type === 'neutral') return 'blue lighten-5 mt-3'
  if (type === 'valid') return 'green lighten-5 mt-3'
  if (type === 'error') return 'red lighten-5 mt-3'
  return ''
}
