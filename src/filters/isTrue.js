/**
 * Helper which checks if the value is true.
 *
 * The value it self can also be a function. If
 * the value is a function the result is checked.
 *
 * @param value
 * @return {*}
 */
export default (value) => {
  return (
    (!(value instanceof Function) && !!value === true) ||
    ((value instanceof Function) && !!value() === true)
  )
}
