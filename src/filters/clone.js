/**
 * Helper for clone object.
 *
 * @param object
 * @return {*}
 */
export default function clone (object) {
  return JSON.parse(JSON.stringify(object))
}
