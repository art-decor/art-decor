export default (item) => {
  const { minimumMultiplicity, maximumMultiplicity } = item
  if (minimumMultiplicity != null && maximumMultiplicity != null) {
    return `${minimumMultiplicity}..${maximumMultiplicity}`
  }
  return null
}
