const $t = (input) => {
  return window.Vue?.$t(input) || input
}

export default (intention) => {
  if (intention === 'patch') return $t('intention-patch')
  if (intention === 'version') return $t('intention-version')
  if (intention === 'delete') return $t('intention-delete')
  if (intention === 'move/version') return $t('intention-move-version')
  if (intention === 'move') return $t('intention-move')
  return '?'
}
