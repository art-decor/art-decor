export default (type) => {
  if (type === 'group') return 'mdi-folder'
  if (type === 'initial') return 'mdi-arrow-right-bold'
  if (type === 'back') return 'mdi-arrow-left-bold'
  if (type === 'stationary') return 'mdi-file-document'
  return 'mdi-movie'
}
