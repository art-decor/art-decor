/**
 * Create a map with parent-child relationships
 * @description This function creates a map with parent-child relationships from an array of items with a hierarchical structure.
 * @param {*} options
 * @param {Array} options.list - The list of items
 * @param {String} options.childrenKey - The key which contains the children
 * @param {String} [options.uniqueKey] - The key which contains the unique identifier
 * @returns {Map<string, Object>} - A map with parent-child relationships
 */
const createChildParentMap = ({ list, childrenKey, uniqueKey = 'id' }) => {
  const childParentMap = new Map()

  const addToMap = (list, parent) => {
    list.forEach(item => {
      const itemDetails = {
        id: item[uniqueKey],
        parentId: parent ? parent[uniqueKey] : null,
        ref: item,
        /**
         * Get all the parents of the item
         * @returns {Array<Object>} - An array of all the parents, starting from the direct parent to the root parent
         */
        getAllParents () {
          const parents = []
          let currentParent = childParentMap.get(this.parentId)
          while (currentParent) {
            parents.push(currentParent.ref)
            currentParent = childParentMap.get(currentParent.parentId)
          }
          return parents
        },
        /**
         * Get the nth parent of the item
         * @param {number} n The level of the parent. 1 is the direct parent, 2 is the grandparent, and so on.
         * @returns {Object|null} - The nth parent of the item or null if the parent doesn't exist
         */
        getNthParent (n) {
          return this.getAllParents()[n - 1] ?? null
        },
        /**
         * Get the root parent of the item
         * @returns {Object|null} - The root parent of the item or null if the parent doesn't exist
         */
        getRootParent () {
          return this.getAllParents().pop() ?? null
        }
      }
      if (parent) {
        itemDetails.parentRef = parent
      }
      childParentMap.set(item[uniqueKey], itemDetails)

      if (item[childrenKey]) {
        addToMap(item[childrenKey], item)
      }
    })
  }
  addToMap(list, null)

  return childParentMap
}

export default createChildParentMap
