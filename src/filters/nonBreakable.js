/**
 * Replace hyphen and spaces into nonbreakable ones.
 *
 * @param id
 * @return {string|*}
 */
export default (nbs) => {
  // char constants
  const NONBREAKABLESPACE = '\u00a0'
  const NONBREAKABLEHYPHEN = '\u2011'

  // Return nonbreakable string.
  const r = String(nbs).replace(/-/g, NONBREAKABLEHYPHEN)
  return r.replace(/ /g, NONBREAKABLESPACE)
}
