/**
 * Get cloned items.
 * On json parse error just return empty array.
 * @param items
 * @returns {*[]|any}
 */
const getClonedItems = (items) => {
  try {
    return JSON.parse(JSON.stringify(items))
  } catch (error) {
    return []
  }
}

/**
 * Add item after list item.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const addItemAfterListItem = ({ items, item, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Replace item.
    data.items.splice((data.index + 1), 0, item)
  }

  // Return cloned items.
  return clonedItems
}

/**
 * Add item before list item.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const addItemBeforeListItem = ({ items, item, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Replace item.
    data.items.splice(data.index, 0, item)
  }

  // Return cloned items.
  return clonedItems
}

/**
 * Push item inside list item.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const pushItemInsideListItem = ({ items, item, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Push item.
    data.item[children] = Array.isArray(data.item[children]) === true
      ? [...data.item[children], item]
      : [item]
  }

  // Return cloned items.
  return clonedItems
}

/**
 * Unshift item inside list item.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const unshiftItemInsideListItem = ({ items, item, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Push item.
    data.item[children] = Array.isArray(data.item[children]) === true
      ? [item, ...data.item[children]]
      : [item]
  }

  // Return cloned items.
  return clonedItems
}

/**
 * Replace item in list.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const replaceItemInList = ({ items, item, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Get parsed item.
    const parsedItem = item instanceof Function ? item(data.item) : item
    // Replace item.
    data.items.splice(data.index, 1, parsedItem)
  }

  // Return cloned items.
  return clonedItems
}

// Remove item from list
const removeItemFromList = ({ items, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Check if status is true.
  if (data.status === true) {
    // Replace item.
    data.items.splice(data.index, 1)
  }

  // Return cloned items.
  return clonedItems
}

/**
 * Find item in list.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
const findItemInList = ({ items, children, handler }) => {
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Call find recursive helper.
  const data = listHelper({ items: clonedItems, children, handler })
  // Return matching item.
  return data.status === true
    ? data.item || null
    : null
}

const getRootElementFromListItem = ({ items, children, handler }) => {
  let parent = null
  // Get cloned items.
  const clonedItems = getClonedItems(items)
  // Loop all items.
  for (const clonedItem of Object.values(clonedItems)) {
    // Call find recursive helper.
    const data = listHelper({ items: [clonedItem], children, handler })
    // Check if status is true.
    if (data.status === true) {
      // Set cloned item to parent object.
      parent = clonedItem
      // Break the loop.
      break
    }
  }

  // Return matching item.
  return parent || null
}

const filterList = ({ items, children, handler }) => {
  return items
    .filter((item) => handler(item))
    .map((item) => ({
      ...item,
      [children]: Array.isArray(item?.[children])
        ? filterList({ items: item[children], children, handler })
        : []
    }))
}

/**
 * Replace item in list helper.
 * @param items
 * @param item
 * @param children
 * @param handler
 * @returns {{}}
 */
function listHelper ({ items, item, children, handler, level = 0 }) {
  // Get parsed items.
  const parsedItems = items || []
  // Loop all items.
  for (const [index, currentItem] of Object.entries(parsedItems)) {
    // Check if id or item through handler is matching.
    if (handler(currentItem) === true) {
      // Return items, item, index and parent.
      return {
        status: true,
        items: parsedItems,
        item: currentItem,
        index: parseInt(index),
        level
      }
    } else if (!!children && !!currentItem?.[children] === true) {
      // Find matching item on children.
      const data = listHelper({
        items: currentItem?.[children],
        children,
        handler,
        level: level + 1
      })

      // Check if status is true.
      if (data.status === true) {
        // Return data.
        return data
      }
    }
  }

  // Return empty object.
  return {
    status: false,
    items: [],
    item: null,
    index: null,
    level: null
  }
}

export default {
  addItemAfterListItem,
  addItemBeforeListItem,
  pushItemInsideListItem,
  unshiftItemInsideListItem,
  replaceItemInList,
  removeItemFromList,
  findItemInList,
  getRootElementFromListItem,
  filterList,
  listHelper
}
