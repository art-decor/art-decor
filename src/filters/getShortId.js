/**
 * Get short id.
 *
 * @param id
 * @return {string|*}
 */
export default (id) => {
  // Return short id.
  return String(id).replace(/(\S+\.)+?/g, '')
}
