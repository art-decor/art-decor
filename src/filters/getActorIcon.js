export default (type) => {
  if (type === 'organization') {
    return 'mdi-hospital-building'
  }
  if (type === 'person') {
    return 'mdi-doctor'
  }
  if (type === 'device') {
    return 'mdi-monitor'
  }
}
