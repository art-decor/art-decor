/**
 * Get artefact color.
 *
 * @param type
 * @return {string|*}
 */
export default (type) => {
  if (type === 'DS') return 'green lighten-5'
  if (type === 'DE') return 'green lighten-5'
  if (type === 'SC') return 'green lighten-5'
  if (type === 'TR') return 'green lighten-5'
  if (type === 'VS') return 'blue lighten-5'
  if (type === 'CS') return 'green lighten-5'
  if (type === 'TM') return 'red lighten-5'
  if (type === 'QQ') return 'red lighten-5'
  if (type === 'QR') return 'red lighten-5'
  return ''
}
