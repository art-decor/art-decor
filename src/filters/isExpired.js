import { isBefore } from 'date-fns'

/**
 * Format date.
 *
 * @param value
 * @return {string|*}
 */
export default (value) => {
  // return true if date is before current date (=expired), otherwise false
  return isBefore(new Date(value), new Date())
}
