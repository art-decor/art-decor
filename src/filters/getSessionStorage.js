export default (key = 'adstate') => {
  try {
    return JSON.parse(sessionStorage.getItem(key))
  } catch (error) {
    return null
  }
}
