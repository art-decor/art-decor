export default (o) => {
  if (o?.ref) return 'mdi-link-variant'
  return 'mdi-recycle'
}
