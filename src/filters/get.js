/**
 * Helper for get path from object.
 *
 * @param object
 * @param path
 * @param defaultValue
 * @return {*}
 */
export default (object, path, defaultValue) => {
  const travel = regexp =>
    String.prototype.split
      .call(path, regexp)
      .filter(Boolean)
      .reduce((res, key) => (res !== null && res !== undefined ? res[key] : res), object)

  const result = path !== undefined
    ? travel(/[,[\]]+?/) || travel(/[,[\].]+?/)
    : undefined

  return result === undefined || result === object ? defaultValue : result
}
