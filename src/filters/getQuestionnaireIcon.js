import isQuestionnaireResponse from './isQuestionnaireResponse'

export default (o) => {
  if (o?.enhanced === 'true') return 'mdi-comment-edit'
  if (isQuestionnaireResponse(o)) return 'mdi-comment-check'
  return 'mdi-comment-question'
}
