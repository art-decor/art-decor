import store from '@/store'

export default ({ value, locale }) => {
  if (!Array.isArray(value)) {
    return
  }
  const language = locale || store.getters['app/projectLanguage']
  return (
    value.find(d => d.type === 'fsn' && d.language === language) ||
    value.find(d => d.type === 'preferred' && d.language === language) ||
    value.find(d => d.language === language) ||
    value[0] ||
    null
  )
}
