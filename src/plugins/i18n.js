import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Vuetify from './vuetify'
import enUS from './../locales/en-US.json'
import deDE from './../locales/de-DE.json'
import frFR from './../locales/fr-FR.json'
import itIT from './../locales/it-IT.json'
import nlNL from './../locales/nl-NL.json'
import plPL from './../locales/pl-PL.json'

// Use vue i18n.
Vue.use(VueI18n)

const getBrowserLanguage = () => {
  const preferredLanguage = (navigator.language || navigator.userLanguage)?.substr(0, 2)
  const supportedLocales = ['en-US', 'nl-NL', 'de-DE', 'fr-FR', 'pl-PL', 'it-IT']

  return supportedLocales.find(locale => locale.substring(0, 2) === preferredLanguage) || null
}

// Get new instance of vue i18n.
const vueI18n = new VueI18n({
  dateTimeFormats: {
    'en-US': {
      short: 'yyyy-MM-DD',
      long: 'yyyy-MM-DD HH:mm:ss'
    },
    'de-DE': {
      short: 'yyyy-MM-DD',
      long: 'yyyy-MM-DD HH:mm:ss'
    }
  },
  locale: getBrowserLanguage() || process.env.VUE_APP_I18N_LOCALE || 'en-US',
  fallbackLocale: process.env.VUE_APP_I18N_FALLBACK_LOCALE || 'en-US',
  silentTranslationWarn: true, // could locally turn on by the developer
  messages: {
    'en-US': enUS,
    'de-DE': deDE,
    'fr-FR': frFR,
    'it-IT': itIT,
    'nl-NL': nlNL,
    'pl-PL': plPL
  }
})

// Add set locale method.
vueI18n.setLocale = (locale) => {
  // Tell vuetify new set locale.
  Vuetify.framework.lang.current = String(locale).substr(0, 2)
  // Set locale on i18n.
  vueI18n.locale = locale
}

// Export vue i18n.
export default vueI18n
