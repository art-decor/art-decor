import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/lib/util/colors'
import '@mdi/font/css/materialdesignicons.css'
// Use vuetify.
Vue.use(Vuetify)

/**
 * Load locale messages.
 *
 * @returns {{}}
 */
function loadLocaleMessages () {
  // Define messages.
  const messages = {}
  // Get all vuetify locales.
  const locales = require.context('vuetify/es5/locale', true, /\/[a-z]{2}\.js$/i)

  // Loop each locale.
  locales.keys().forEach(key => {
    // Get matching locale without js suffix.
    const matched = key.match(/\/([a-z]{2})\./i)
    // Check if match was successfully.
    if (matched && matched.length > 1) {
      // Get locale.
      const locale = matched[1]
      // Assign locale messages.
      messages[locale] = locales(key).default
    }
  })

  // Return messages.
  return messages
}

// Export new instance of vuetify.
export default new Vuetify({
  theme: {
    options: {
      customProperties: true
    },
    themes: {
      light: {
        primary: colors.orange.darken2, // #E53935
        secondary: colors.orange.lighten2, // #FFCDD2
        accent: colors.indigo.base, // #3F51B5
        background: '#f7f5f3', // Not automatically applied #f7f5f3 #f0ebe4 #f4f3ef #f6f3ee
        topbottomstrip: '#f0ebe4',
        dialogtopstrip: colors.grey.lighten3,
        dialogbottomstrip: colors.grey.lighten4
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  },
  lang: {
    locales: loadLocaleMessages()
  }
})
