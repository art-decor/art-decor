import Api from '@/services/Api'

export default {
  getImplementationGuide ({ id, effectiveDate, params, abortController } = {}) {
    let path = `/api/implementationguide/${id}`
    if (effectiveDate) {
      path += `/${encodeURIComponent(effectiveDate)}`
    }
    return Api().get(path, {
      params,
      signal: abortController?.signal
    })
  },

  getTemplateHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },

  getImplementationGuideMetaData ({ id, effectiveDate, params, abortController } = {}) {
    return Api().get(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/metadata`, {
      params,
      signal: abortController?.signal
    })
  },

  getImplementationGuidePage ({ id, effectiveDate, pageId, params, abortController } = {}) {
    return Api().get(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/page/${pageId}`, {
      params,
      signal: abortController?.signal
    })
  },

  getImplementationGuidePageTree ({ id, effectiveDate, params, abortController } = {}) {
    return Api().get(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/pages`, {
      params,
      signal: abortController?.signal
    })
  },

  getImplementationGuideList ({ params, abortController } = {}) {
    return Api().get('/api/implementationguide', {
      params,
      signal: abortController?.signal
    })
  },

  getImplementationGuideResourceList ({ id, effectiveDate, params, abortController } = {}) {
    return Api().get(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/resources`, {
      params,
      signal: abortController?.signal
    })
  },

  patchImplementationGuide ({ id, effectiveDate, data, params, broadcast, abortController } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}`, data, {
        params,
        signal: abortController?.signal
      })
  },

  postImplementationGuide ({ data, params, broadcast, abortController } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post('/api/implementationguide', data, {
        params,
        signal: abortController?.signal
      })
  },

  postImplementationGuideRefresh ({ id, effectiveDate, data, params, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/implementationguide/${id}/${encodeURIComponent(effectiveDate)}/$refresh-resources`, data, {
        params
      })
  }
}
