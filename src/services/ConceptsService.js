import Api from '@/services/Api'

export default {
  searchDesignation (search, context, loincSearchContext, ancestor, isa, refset) {
    return Api().get('/api/terminology/codesystem', {
      params: {
        string: search,
        context,
        loincContext: loincSearchContext,
        ancestor,
        isa,
        refset
      }
    })
  },

  searchCode (search, context) {
    return Api().get('/api/terminology/codesystem', {
      params: {
        code: search,
        context
      }
    })
  },

  getCodeSystemInfo (codeSystemId) {
    return Api().get('/api/terminology/codesystem/' + codeSystemId + '/info')
  },

  getCodeSystemLanguages () {
    return Api().get('/api/terminology/codesystem/languages')
  },

  getSnomedFilters () {
    return Api().get('/api/terminology/codesystem/snomed-filters')
  },
  getConcept (codeSystemId, conceptCode, preferredLanguage, selectedLanguages) {
    return Api().get('/api/terminology/codesystem/' + codeSystemId + '/concept/' + conceptCode, {
      params: {
        language: selectedLanguages.toString().replace(/,/g, ' '),
        preferred: preferredLanguage,
        children: '10'
      }
    })
  },

  getConceptChildren (codeSystemId, conceptCode, preferredLanguage, start, children) {
    return Api().get('/api/terminology/codesystem/' + codeSystemId + '/concept/' + conceptCode + '/children', {
      params: {
        preferred: preferredLanguage,
        start,
        children
      }
    })
  },

  validateCode (data) {
    return Api().post('/api/terminology/codesystem/$validate-code', data)
  },

  /**
   * Retrieves DECOR valueSet based on id (oid) and effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
   * and expands it if it is an intensional value set
   *
   * @param {string} id - Parameter denoting the id of the template.
   * @param {string} effectiveDate - parameter denoting the effectiveDate of the valueSet. Example: 2012-07-25T15:22:56.
   * @param {string} expand - if 'true' the expanded value set is returned, if 'false' only the count of codes after expansion
   * @param {string} prefix - Determines search scope. null is full server (+ cache), pfx- limits scope to this project only
   * @param {string} language - Parameter to select from a specific compiled language.
   * @return {Promise<AxiosResponse<any>>}
   */
  getExpandedComposition (idOrRef, effectiveDate, expand, prefix, language) {
    const apip = idOrRef
    return Api().get(`/api/terminology/composition/${apip}/${encodeURIComponent(effectiveDate)}/$expand`, {
      params: {
        expand,
        prefix,
        language
      }
    })
  },

  post (testEdit) {
    return Api().post('/terminology/modules/edit-test.xquery', testEdit, { withCredentials: true })
  },

  postTwo (testEdit) {
    return Api().post('/terminology/modules/edit-test-two.xquery', testEdit, { withCredentials: true })
  }
}
