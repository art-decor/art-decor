import Api from '@/services/Api'

export default {

  /**
   * Get all urgent news
   *
   * @returns {promise<AxiosResponse<any></any>>}
   */
  getNews () {
    return Api().get('/api/news', {
      params: {}
    })
  },

  /**
   * Get a specific urgent news by id
   *
   * @param id
   * @returns {promise<AxiosResponse<any></any>>}
   */
  getNewsById ({
    id
  }) {
    return Api().get('/api/news/' + id, {
      params: {}
    })
  },

  /**
   * Create new urgent news
   *
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  postNewsItem (data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/news', data, {
      params: {}
    })
  },

  /**
   * Update existing urgent news
   *
   * @param id
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  putNewsItem (id, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).put('/api/news/' + id, data, {
      params: {}
    })
  },

  /**
   * Delete existing urgent news
   *
   * @param id
   * @returns {Promise<AxiosResponse<any>>}
   */
  deleteNewsItem (id) {
    return Api().delete('/api/news/' + id, {
      params: {}
    })
  }
}
