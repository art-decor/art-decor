import Api from '@/services/Api'

export default {
  /**
   * Get issue list.
   *
   * @param project
   * @param search
   * @param type
   * @param priority
   * @param status
   * @param assignee
   * @param label
   * @param changedAfter
   * @param sort
   * @param order
   * @param max
   * @returns {Promise<AxiosResponse<any>>}
   */
  getIssueList ({ project, search, type, priority, status, assignee, label, changedAfter, sort, order, max, objectId, objectEffectiveDate }, abortController) {
    return Api().get('/api/issue', {
      params: Object.entries({
        prefix: project,
        search: search ?? null,
        type: type ?? null,
        priority: priority ?? null,
        status: status ?? null,
        assignee: assignee ?? null,
        label: label ?? null,
        changedafter: changedAfter ?? null,
        sort: sort ?? null,
        sortorder: order ?? null,
        max: max ?? null,
        objectId: objectId ?? null,
        objectEffectiveDate: objectEffectiveDate ?? null
      }).filter(param => param[1] !== null)
        .map(param => ({ [param[0]]: param[1] }))
        .reduce((obj, item) => ({ ...obj, ...item })),
      signal: abortController?.signal
    })
  },

  /**
   * Get issue.
   *
   * @param id
   * @returns {Promise<AxiosResponse<any>>}
   */
  getIssue (id) {
    return Api().get('/api/issue/' + id)
  },

  /**
   * Create issue.
   *
   * @param project
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  createIssue (project, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/issue', data, {
      params: {
        prefix: project
      }
    })
  },

  /**
   * Patch/update issue.
   *
   * @param project
   * @param id
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  patchIssue (project, id, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch('/api/issue/' + id, data, {
      params: {
        prefix: project
      }
    })
  },
  subscribeIssue (project, id, value) {
    return Api().post(`/api/issue/${id}/$subscribe`, null, {
      params: {
        prefix: project,
        value
      }
    })
  },

  /**
   * Get issue label list.
   *
   * @param project
   * @returns {Promise<AxiosResponse<any>>}
   */
  getIssueLabelList (project) {
    return Api().get('/api/issue/label', {
      params: {
        prefix: project
      }
    })
  },

  /**
   * Create issue label.
   *
   * @param project
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  createIssueLabel (project, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/issue/label', data, {
      params: {
        prefix: project
      }
    })
  },

  /**
   * Put/update issue label.
   *
   * @param project
   * @param id
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  putIssueLabel (project, id, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).put(`/api/issue/label/${id}`, data, {
      params: {
        prefix: project
      }
    })
  },

  /**
   * Put/update issue label.
   *
   * @param project
   * @param id
   * @returns {Promise<AxiosResponse<any>>}
   */
  deleteIssueLabel (project, id, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/issue/label/${id}`, {
      params: {
        prefix: project
      }
    })
  }
}
