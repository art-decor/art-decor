import Api from '@/services/Api'

export default {
  deleteTemplate (id, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/template/${id}`, {
      params
    })
  },
  getTemplateList (params = {}, abortController) {
    return Api().get('/api/template', {
      params: {
        prefix: params.prefix,
        release: params.release,
        search: params.search
      },
      signal: abortController?.signal
    })
  },
  getTemplate (id, effectiveDate, prefix, language, abortController) {
    let endpoint = `/api/template/${id}`
    if (effectiveDate) {
      endpoint += `/${encodeURIComponent(effectiveDate)}`
    }
    return Api().get(endpoint, {
      params: {
        prefix,
        language
      },
      signal: abortController?.signal
    })
  },
  getTemplateDatatypes (params) {
    return Api().get('/api/template/$datatypes', {
      params
    })
  },
  getTemplateUsage (id, effectiveDate, params) {
    return Api().get(`/api/template/${id}/${encodeURIComponent(effectiveDate)}/$usage`, {
      params
    })
  },
  getTemplateHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/template/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },
  getTemplateForEdit (id, effectiveDate, params) {
    return Api().post(`/api/template/${id}/${encodeURIComponent(effectiveDate)}/$edit`, null, {
      params
    })
  },

  patchTemplate (id, effectiveDate, data, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/template/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  },

  postTemplate (data, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/template/', data, {
      params
    })
  },

  postTemplateExample (data, params) {
    return Api().post('/api/template/$example', data, {
      params
    })
  },

  putTemplate (id, effectiveDate, data, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).put(`/api/template/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  },

  getTemplateAssociations (prefix) {
    return Api().get('/api/template/$templateAssociation', {
      params: {
        prefix
      }
    })
  },

  patchTemplateAssociation (id, effectiveDate, prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/template/${id}/${encodeURIComponent(effectiveDate)}/$templateAssociation`, data, {
        params: {
          project: prefix
        }
      })
  }
}
