import axios from 'axios'
import Api from '@/services/Api'

export default {
  /**
   * Get server info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getServerInfo () {
    return Api().get('/api/server', {
      params: {}
    })
  },

  /**
   * Get users list info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getUsersList (par) {
    return Api().get('/api/server/user', {
      params: {
        active: par?.active,
        groups: par?.groups
      }
    })
  },

  /**
   * Get users list info for users in group groups only
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getActiveUsersListInGroups (groups) {
    return Api().get('/api/server/user?active=true&groups=' + groups, {
      params: {}
    })
  },

  /**
   * Get user info.
   *
   * @param name
   * @return {Promise<AxiosResponse<any>>}
   */
  getUser (name) {
    return Api().get('/api/server/user/' + name, {
      params: {}
    })
  },
  /**
   * Update user info.
   *
   * @param name
   * @param data
   * @return {Promise<AxiosResponse<any>>}
   */
  updateUser (name, data) {
    return Api().put('/api/server/user/' + name, data, {
      params: {}
    })
  },
  /**
   * Update user password.
   *
   * @param data
   * @return {Promise<AxiosResponse<any>>}
   */
  updateUserPassword (data) {
    return Api().post('/api/server/user/$password', data, {
      params: {}
    })
  },
  /**
   * Add new user.
   *
   * @param name
   * @param data
   * @return {Promise<AxiosResponse<any>>}
   */
  addUser (data) {
    return Api().post('/api/server/user', data, {
      params: {}
    })
  },
  /**
   * Signal notify user.
   *
   * @param data
   * @return {Promise<AxiosResponse<any>>}
   */
  notifyUser (name) {
    return Api().post('/api/server/user/' + name + '/$notify', {
      params: {}
    })
  },
  /**
   * Retrieves DECOR status code trasitions map
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getDecorStatusMaps () {
    return Api().get('/api/server/$statusmaps', {
      params: {}
    })
  },
  /**
   * Get decor types.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getDecorTypes () {
    return Api().get('/api/server/$decortypes', {
      params: {}
    })
  },

  /**
   * Get group info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getGroupsList () {
    return Api().get('/api/server/group', {
      params: {}
    })
  },

  /**
   * Get build server infos.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getServerUptimes (url) {
    const config = {
      method: 'get',
      url,
      headers: {
        'Content-Type': 'application/json'
      },
      data: {}
    }
    return await axios(config)
  },

  /**
   * Get adadwib service infos.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getAdawibInfo (url, token) {
    const config = {
      method: 'get',
      url: url + '?token=' + token,
      headers: {
        'Content-Type': 'application/json'
      }
    }
    return await axios(config)
  },
  /**
   * Get server settings.
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  getServerSettings () {
    return Api().get('/api/server/settings', {
      params: {}
    })
  },
  /**
   * Update DECOR server settings parts
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  async updateServerSettings (data) {
    return await Api().patch('/api/server/settings', data, {
      params: {}
    })
  },
  /**
   * Retrieves server list of governance groups
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  getGovernancegroupList () {
    return Api().get('/api/server/governancegroup', {
      params: {}
    })
  },
  /**
   * Retrieves server governance group details
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  getGovernancegroup (id, details) {
    return Api().get(`/api/server/governancegroup/${id}`, {
      params: {
        details
      }
    })
  },
  /**
   * Create a new governance group
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  addGovernancegroup (data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/server/governancegroup', data, {
      params: {}
    })
  },
  /**
   * Create a new governance group
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  patchGovernancegroup (id, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/server/governancegroup/${id}`, data, {
      params: {}
    })
  },
  /**
   * Delete a governance group
   *
   * @return {Promise<AxiosResponse<any>>}
   */
  deleteGovernancegroup (id, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/server/governancegroup/${id}`, {
      params: {}
    })
  },
  getBuildingBlockRepositories (uri, type) {
    return Api().get('/api/server/$bbrlist', {
      params: {
        uri,
        type
      }
    })
  }
}
