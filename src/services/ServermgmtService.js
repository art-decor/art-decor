import Api from '@/services/Api'

export default {
  /**
   * Recreates the codesystem index aka CLAML index
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async codesystemGenerateIndex () {
    return await Api().post('/api/server-mgmt/codesystem/$generate-index')
      .catch(() => {})
  },
  /**
   * Repairs permissions in ART related data
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async resetArtPermissions () {
    return await Api().post('/api/server-mgmt/db/$reset-art-permissions')
      .catch(() => {})
  },
  /**
   * Cleans up DECOR tmp and older development releases
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async cleanUpDecor () {
    return await Api().post('/api/server-mgmt/db/$clean-up-decor')
      .catch(() => {})
  },
  /**
   * Update permissions related to Terminology package
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async resetTerminologyPermissions () {
    return await Api().post('/api/server-mgmt/db/$reset-terminology-permissions')
      .catch(() => {})
  },
  /**
   * Update OID Registries lookup files. Note that the package may not be installed.
   * Note that this function may need to move to a separate set of OID functions.
   * Requires dba authentication.
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async oidsGenerateLookups () {
    return await Api().post('/api/server-mgmt/oids/$generate-lookups')
      .catch(() => {})
  },
  /**
   * Repairs permissions in FHIR related data
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async resetFhirPermissions (data) {
    const query = (data) ? Object.values(data).filter(a => a !== '').join('&') : ''
    return await Api().post(`/api/server-mgmt/db/$reset-fhir-permissions${(query) ? `?${query}` : ''}`)
      .catch(() => {})
  },
  /**
   * Update one or all OID Registries lookup files.
   * Note that the package may not be installed.
   * Require dba authentication
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async decorCacheUpdate (data) {
    const query = (data) ? Object.values(data).filter(a => a !== '').join('&') : ''
    return await Api().post(`/api/server-mgmt/decor-cache/$update${(query) ? `?${query}` : ''}`)
      .catch(() => {})
  },
  /**
   * Retrieves CRUD permissions for server functionality
   * No frontend for maintaining configuration
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async getServerFunctions (data) {
    return await Api().get('/api/server-mgmt/server-functions')
      .catch(() => {})
  },
  /**
   * gets all server schedulers
   * @returns {Promise<AxiosResponse<any>>}
   *
   */
  async getServerSchedulers () {
    return await Api().get('/api/server-mgmt/schedulers')
      .catch(() => {})
  }
}
