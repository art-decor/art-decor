import Api from '@/services/Api'

export default {
  getQuestionnaireList (params, abortController) {
    return Api().get('/api/questionnaire', {
      params,
      signal: abortController?.signal
    })
  },

  getQuestionnaire (id, effectiveDate, params, xml = false) {
    const headers = xml
      ? {
          Accept: 'application/xml'
        }
      : null
    return Api(headers).get(`/api/questionnaire/${id}/${encodeURIComponent(effectiveDate)}`, {
      params
    })
  },
  getQuestionnaireResponse (id, _, params, xml = false) {
    const headers = xml
      ? {
          Accept: 'application/xml'
        }
      : null
    return Api(headers).get(`/api/questionnaireresponse/${id}`, {
      params
    })
  },

  patchQuestionnaire ({ id, effectiveDate, data, params, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/questionnaire/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  },

  patchQuestionnaireResponse ({ id, effectiveDate, data, params, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/questionnaireresponse/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  }
}
