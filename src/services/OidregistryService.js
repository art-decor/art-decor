import Api from '@/services/Api'

export default {
  getOIDList (params) {
    return Api().get('api/oidregistry/oids', { params })
  },
  getOIDRegistries () {
    return Api().get('api/oidregistry/registries')
  }
}
