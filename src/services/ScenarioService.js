import Api from '@/services/Api'

export default {
  getScenarioList (params = {}) {
    return Api().get('/api/scenario', {
      params
    })
  },

  getScenarioItem (id, effectiveDate) {
    return Api().get(`/api/scenario/${id}/${encodeURIComponent(effectiveDate)}`)
  },

  getScenarioHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/scenario/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },

  createScenarioItem (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/scenario?prefix=${prefix}`, data)
  },

  cloneScenarioItem (prefix, sourceId, sourceEffectiveDate, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/scenario?prefix=${prefix}&sourceId=${sourceId}&effectiveDate=${encodeURIComponent(sourceEffectiveDate)}`, null)
  },

  patchScenarioItem (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/scenario/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },

  updateScenarioItem (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .put(`/api/scenario/${id}/${encodeURIComponent(effectiveDate)}?deletelock=true`, data)
  },

  getTransactionItem (id, effectiveDate) {
    return Api().get(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}`)
  },

  getTransactionHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },

  cloneScenarioTransactionItem (id, effectiveDate, sourceId, sourceEffectiveDate, transactionType, insertMode, insertRef) {
    return Api().post(
      `/api/scenario/${id}/${encodeURIComponent(effectiveDate)}/$transaction` +
      `?sourceId=${sourceId}` +
      `&sourceEffectiveDate=${sourceEffectiveDate}` +
      `&transactionType=${transactionType}` +
      `&insertMode=${insertMode}` +
      (insertRef ? `&insertRef=${insertRef}` : ''),
      null
    )
  },

  createTransactionItem (id, effectiveDate, data) {
    return Api()
      .patch(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },

  patchTransactionItem (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },

  getActorList (project) {
    return Api().get('/api/scenario/actor', {
      params: {
        prefix: project
      }
    })
  },
  /**
   * Creates an actor definition to be called from scenarios
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  postScenarioActor (prefix, actor, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/scenario/actor', actor, {
      params: {
        prefix
      }
    })
  },
  /**
   * Expect as input array of parameter objects,
   * each containing RFC 6902 compliant contents
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  patchScenarioActor (prefix, actor, { broadcast } = {}) {
    // Define data.
    const data = {
      parameter: []
    }

    // // update the actor
    // const attributes = Object.keys(modelActor).filter((key) => key !== 'id')

    for (const [type, value] of Object.entries(actor)) {
      // Skip id type.
      if (type === 'id') {
        continue
      }

      // Check if value is not an array.
      if (Array.isArray(value) === false) {
        // Push parameter.
        data.parameter.push({
          op: 'replace',
          path: `/${type}`,
          value
        })
      }

      // Check if value is an array.
      if (Array.isArray(value) === true) {
        // Push parameter.
        data.parameter.push(
          ...Object.values(value)
            .filter((currentValue) => currentValue?.['#text'] && currentValue?.language)
            .map((currentValue) => {
              return {
                op: (actor?.[type] || []).find(
                  (value) => value.language === currentValue.language && !!value.lastTranslated
                ) !== undefined
                  ? 'replace'
                  : 'add',
                path: `/${type}`,
                value: { [type]: currentValue }
              }
            })
        )
      }
    }

    return Api({ 'X-Broadcast': broadcast }).patch(`/api/scenario/actor/${actor.id}`, data, {
      params: {
        prefix
      }
    })
  },
  /**
   * Delete scenario actor. For dba only.
   * If the actor does not exist: no error is returned.
   * The actor is only deleted if it is not in use.
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  deleteActor (prefix, actor, { broadcast } = {}) {
    // delete the actor
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/scenario/actor/${actor.id}`, {
      params: {
        prefix
      }
    })
  },

  postScenarioStatus (id, effectiveDate, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/scenario/${id}/${encodeURIComponent(effectiveDate)}/$status`, null, {
        params
      })
  },
  postTransactionStatus (id, effectiveDate, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}/$status`, null, {
        params
      })
  },

  postTransactionQuestionnaire ({ params, data, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/transaction/$questionnaire', data, {
      params
    })
  },

  getRepresentingTemplate (id, effectiveDate, fulltree, params = {}, abortController) {
    return Api().get(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}`, {
      params: {
        fulltree: !!fulltree,
        treeonly: true,
        ...params
      },
      signal: abortController?.signal
    })
  },

  smartCloneRepresentingTemplate ({ transactionId, transactionEffectiveDate, datasetId, datasetEffectiveDate, params = {} } = {}) {
    return Api().get(`/api/transaction/${transactionId}/${encodeURIComponent(transactionEffectiveDate)}/representingTemplate/${datasetId}/${encodeURIComponent(datasetEffectiveDate)}/$smartclone`, {
      params
    })
  },

  patchRepresentingTemplate ({ id, effectiveDate, data, params = {}, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}/representingTemplate`, data, {
      params
    })
  },

  updateRepresentingTemplate (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).put(`/api/transaction/${id}/${encodeURIComponent(effectiveDate)}/representingTemplate?deletelock=true`, data, {
      params: {
        fulltree: true,
        treeonly: true
      }
    })
  }
}
