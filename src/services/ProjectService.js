import Api from '@/services/Api'

export default {
  getProject (project, language, projectId, checkadram) {
    return Api().get('/api/project/' + project, {
      params: {
        language,
        id: projectId,
        checkadram
      }
    })
  },
  getProjectAuthorList (prefix) {
    return Api().get(`/api/project/${prefix}/author`)
  },
  getProjectPublicationList (project, language) {
    return Api().get('/api/project/' + project + '/$publication', {
      params: {
        language
      }
    })
  },
  getProjectHistory (project) {
    return Api().get('/api/project/' + project + '/history', {
      params: {
      }
    })
  },
  getProjectIcon (project, logo) {
    return Api().defaults.baseURL + 'api/project/' + project + '/logo?logo=' + logo
  },

  /**
   * Patch project.
   *
   * @param project
   * @param data
   * @returns {Promise<AxiosResponse<any>>}
   */
  patchProject (project, data, checkadram, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch('/api/project/' + project, data, {
      params: {
        checkadram
      }
    })
  },
  /**
   * Get projects info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  getProjectsList () {
    return Api().get('/api/project', {
      params: {}
    })
  },
  /**
   * Get projects info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  checkProjectAttribute (data) {
    return Api().get(`/api/project/${data}`, {
      params: {}
    })
  },
  /**
   * Add new project.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  addProject (data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/project', data, {
      params: {}
    })
  },
  getProjectLastCheck (prefix, datesonly) {
    return Api().get(`/api/project/${prefix}/$check`, {
      params: {
        datesonly
      }
    })
  },
  checkProject (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/project/${prefix}/$check`, data, {
      params: {}
    })
  },
  postProjectPublication (prefix, data, { broadcast } = {}) {
    /**
     * Creates DECOR project releases or versions (publications) based on
     * its project id (oid) or prefix (\S-) and release/version effectiveDate.
     */
    return Api({ 'X-Broadcast': broadcast }).post(`/api/project/${prefix}/$publication`, data, {
      params: {}
    })
  },
  /**
   * Expect as input array of parameter objects,
   * each containing RFC 6902 compliant contents
   */
  patchProjectPublication (prefix, parameter, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/project/${prefix}/$publication`, {
      parameter
    })
  },
  getProjectExtraChecks (prefix) {
    return Api().get(`/api/project/${prefix}/$check/extra-params`)
  },
  updateProjectExtraChecks (prefix, data, { broadcast }) {
    return Api({ 'X-Broadcast': broadcast }).put(`/api/project/${prefix}/$check/extra-params`, data)
  },
  getRuntimeCompilation (prefix, params = {}) {
    return Api().get(`/api/project/${prefix}/$runtime`, {
      params: {
        compilationId: params.compilationId,
        download: params.download
      },
      responseType: params.download ? 'arraybuffer' : undefined
    })
  },
  createRuntimeCompilation (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/project/${prefix}/$runtime`, data)
  },
  deleteRuntimeCompilation (prefix, compilationId, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/project/${prefix}/$runtime`, {
      params: {
        compilationId
      }
    })
  },
  getCompilationFilters (prefix) {
    return Api().get(`/api/project/${prefix}/$filters`)
  },
  patchProjectFilters (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/project/${prefix}/$filters`, data)
  },
  /**
   * Validate single instance against project transaction
   */
  projectInstanceValidation (prefix, data) {
    return Api().post(`/api/project/${prefix}/$validate-instance`, data, {
      params: {}
    })
  },
  postDecorProjectRelease (prefix, data) {
    return Api().post(`/api/project/${prefix}/releases`, data, {
      params: {}
    })
  },
  updateDecorProjectRelease (prefix, effectiveDate, data) {
    return Api().put(`/api/project/${prefix}/releases/${encodeURIComponent(effectiveDate)}`, data, {
      params: {}
    })
  },
  /**
   * Get publication parameters
   * prapi:getProjectPublicationParameters
   */
  getProjectPublicationParameters (prefix) {
    return Api().get(`/api/project/${prefix}/$publication-parameters`)
  },
  /**
   * Expect as input array of parameter objects, each containing RFC 6902 compliant contents
   * prapi:patchProjectPublicationParameters
   */
  patchProjectPublicationParameters (prefix, parameter, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/project/${prefix}/$publication-parameters`, {
      parameter
    })
  },
  /**
   * Retrieves decor-parameters object with DECOR project release or version notes skeleton
   * prapi:getProjectPublicationNotes
   */
  getProjectPublicationNotes (prefix, effectiveDate) {
    return Api().get(`/api/project/${prefix}/$publication-notes/${encodeURIComponent(effectiveDate)}`)
  },
  // ADA
  getADAList (prefix) {
    return Api().get(`/api/project/${prefix}/$ada`)
  },
  // Adds element and attribute ids to all templates of a project.
  addTemplateIds (prefix, params = {}) {
    return Api().post(`/api/project/${prefix}/$add-template-ids`, null, { params })
  },

  getProjectScheduledTasks (prefix, params) {
    return Api().get(`/api/project/${prefix}/$scheduled-task`, {
      params
    })
  },
  deleteProjectScheduledTasks (prefix, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/project/${prefix}/$scheduled-task`, {
      params
    })
  },
  getDecorProjectIds (prefix, params, abortController) {
    return Api().get(`/api/project/${prefix}/id`, {
      params,
      signal: abortController?.signal
    })
  },
  /**
   * Terminology report
   */
  getTerminologyReport (prefix, params = {}) {
    return Api().get(`/api/project/${prefix}/$terminology-report`, {
      params: {
        reportId: params.reportId,
        retrieve: params.retrieve
      }
    })
  },
  createTerminologyReport (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/project/${prefix}/$terminology-report`, data)
  },
  deleteTerminologyReport (prefix, reportId, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/project/${prefix}/$terminology-report`, {
      params: {
        reportId
      }
    })
  },
  getChangeProjectArtifactIdEffects ({ project, artefact, id, effectiveDate, newId, params, abortController } = {}) {
    return Api().get(`/api/project/${project}/${artefact}/${id}/${encodeURIComponent(effectiveDate)}/$change-id/${newId}`, {
      params,
      signal: abortController?.signal
    })
  },
  patchChangeProjectArtifactIdEffects ({ project, artefact, id, effectiveDate, newId, params, abortController, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/project/${project}/${artefact}/${id}/${encodeURIComponent(effectiveDate)}/$change-id/${newId}`, null, {
      params,
      signal: abortController?.signal
    })
  },
  /**
   * Generic and Sanity report
   */
  getGenericAndSanityReport (prefix, params = {}) {
    return Api().get(`/api/project/${prefix}/$generic-report`, {
      params: {
        reportId: params.reportId,
        retrieve: params.retrieve
      }
    })
  },
  createGenericAndSanityReport (prefix, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/project/${prefix}/$generic-report`, data)
  },
  deleteGenericAndSanityReport (prefix, reportId, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/project/${prefix}/$generic-report`, {
      params: {
        reportId
      }
    })
  },
  /**
   * Import LOINC panel
   */
  getImportLoincPanel (params = {}) {
    // get preview of items that will be imported
    return Api().get('/api/project/$import-loinc-panel', {
      params: {
        datasetId: params.datasetId,
        effectiveDate: params.effectiveDate,
        panelCode: params.panelCode,
        baseId: params.baseId,
        insertMode: params.insertMode,
        insertRef: params.insertRef,
        insertFlexibility: params.insertFlexibility
      }
    })
  },
  createImportLoincPanel (data) {
    // perform import
    return Api().post('/api/project/$import-loinc-panel', data, {
      params: {}
    })
  }
}
