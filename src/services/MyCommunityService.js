import Api from '@/services/Api'

export default {
  getMyCommunityList ({ project, params = {}, abortController }) {
    return Api().get(`/api/mycommunity/${project}`, {
      params,
      signal: abortController?.signal
    })
  },
  postMyCommunity ({ project, data, params = {} }) {
    return Api().post(`/api/mycommunity/${project}`, data, {
      params
    })
  },
  getMyCommunity ({ project, name, params = {} }) {
    return Api().get(`/api/mycommunity/${project}/${name}`, {
      params
    })
  },
  putMyCommunity ({ project, name, data, params = {} }) {
    return Api().put(`/api/mycommunity/${project}/${name}`, data, {
      params
    })
  }
}
