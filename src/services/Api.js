import axios from 'axios'
import store from '@/store'

/**
 * Get Api axios instance.
 * @returns {AxiosInstance}
 */
export default (customHeaders = {}) => {
  // Define headers.
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    ...customHeaders
  }

  // Check if authentication token is available.
  if (store.state.authentication.token !== null) {
    // Add x auth token.
    headers['X-Auth-Token'] = store.state.authentication.token
  }
  headers['X-Socket-Id'] = store.state.app.socketId

  // Create new axios instance.
  const $axios = axios.create({
    baseURL: '/exist/apps/',
    // timeout: 1000,
    headers
  })

  $axios.interceptors.request.use(function (request) {
    // Commit increment active requests mutation.
    store.commit('axios/incrementActiveRequests')
    return request
  })

  // Add a response interceptor
  $axios.interceptors.response.use(function (response) {
    // Dispatch decrement active requests action.
    store.dispatch('axios/decrementActiveRequests')
    // Return response.
    return response
  }, function (error) {
    // Dispatch decrement active requests action.
    store.dispatch('axios/decrementActiveRequests')
    // Check if http status code is 401.
    if (parseInt(error?.response?.status) === 401) {
      // Force user logout.
      store.dispatch('authentication/clearTokenData')
    }

    // Return error.
    return Promise.reject(error)
  })

  // Return axios.
  return $axios
}
