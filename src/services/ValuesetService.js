import Api from '@/services/Api'

export default {
  /**
   * Retrieves DECOR valueSet list. All by default.
   * Use query parameters for a more targeted list
   *
   * Notes:
   * - You cannot mix governanceGroupId and prefix : it is either/or
   * - You cannot use governanceGroupId and resolve=true.
   *   This is too expensive for larger governance groups
   */
  getValueset (params) {
    return Api().get('/api/valueset', {
      params
    })
  },
  putValueSet (id, effectiveDate, data) {
    return Api().put(`/api/valueset/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },
  patchValueSet (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/valueset/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  },
  postValueSet (data, queryParams = {}, { broadcast } = {}) {
    /**
     * Creates a DECOR valueSet for the project denoted in parameter prefix.
     * A new valueSet MAY be created from an existing valueSet using parameters sourceId and
     * optionally sourceEffectiveDate. If an input valueSet is provided, its id and
     * effectiveDate are respected, but if a valueSet with that combination already exists,
     * the valueSet cannot added and an error is returned.
     * If any of id or effectiveDate is missing, it will be created.
     *
     * @param {string} prefix - project to create this valueSet in
     * @param {string} targetDate - If true invokes effectiveDate of the new valueSet as [DATE]T00:00:00.
     * If false invokes date + time [DATE]T[TIME].
     * @param {string} sourceId - parameter denoting the id of a valueSet to use as a basis for creating
     * the new valueSet
     * @param {string} sourceEffectiveDate - parameter denoting the effectiveDate of a valueSet to use
     * as a basis for creating the new valueSet
     * @param {string} keepIds - Only relevant if source valueSet is specified. If true, the new valueSet
     * will keep the same ids for the new valueSet, and only update the effectiveDate
     * @param {string} baseId - Only relevant when a source valueSet is specified and keepIds is false.
     * This overrides the default base id for valueSet in the project. The value SHALL match one of the
     * projects base ids for valueSet
     */
    return Api({ 'X-Broadcast': broadcast })
      .post('/api/valueset', data, {
        params: {
          prefix: queryParams.prefix,
          targetDate: (queryParams.targetDate != null) ? encodeURIComponent(queryParams.targetDate) : null,
          sourceId: queryParams.sourceId,
          sourceEffectiveDate: (queryParams.sourceEffectiveDate != null) ? encodeURIComponent(queryParams.sourceEffectiveDate) : null,
          keepIds: queryParams.keepIds,
          baseId: queryParams.baseId,
          refOnly: queryParams.refOnly
        }
      })
  },
  postValueSetStatus (id, effectiveDate, params = {}) {
    /**
     * Change the status of the valueSet.
     * You may include setting the versionLabel, expirationDate, and officialReleaseDate.
     * Not sending the parameter versionLabel, expirationDate, and officialReleaseDate leaves
     * this property as-is. This operation always does one pass in list mode. If in list mode
     * no issues are encountered like an illegal state change or a pre-existing lock by someone
     * else, and if list=false, then all is applied in a second pass. If list=false, the list is
     * returned. If list=true the list is returned only if successful, otherwise an HTTP error.
     *
     * @param {boolean} list - if set to true, then you receive a list of 'to be affected particles'
     * and if that would succeed. Note that this is partly point-in-time information. Locks could be
     * obtained/statuses could be changed by someone after retrieving the list.
     * @param {string} statusCode - new statusCode to be set from the ItemStatusCodeLifeCycle set.
     * Allowable status transitions may be obtained from /server/$statusmaps
     * Available values : new, draft, pending, final, rejected, cancelled, deprecated
     * @param {string} versionLabel - new versionLabel string. This is an informal free text human
     * readable string. The formal version indicator is the effectiveDate. You are encouraged to
     * use semantic versioning, commonly seen as major.minor[.patch], e.g. 1.0
     * @param {string} expirationDate - new expirationDate. Expected format: empty, yyyy-mm-dd
     * (T00:00:00 is assumed) or yyyy-mm-ddThh:mm:ss. Sending the empty string, removes this
     * property (recursively). Omitting this parameter, leaves this property as-is (recursively)
     * @param {string} officialReleaseDate - new officialReleaseDate. Expected format: empty, yyyy-mm-dd
     * (T00:00:00 is assumed) or yyyy-mm-ddThh:mm:ss. Sending the empty string, removes this
     * property (recursively). Omitting this parameter, leaves this property as-is (recursively)
     */
    return Api().post(`/api/valueset/${id}/${encodeURIComponent(effectiveDate)}/$status`, null, {
      params: {
        list: params.list,
        statusCode: params.statusCode,
        versionLabel: params.versionLabel,
        expirationDate: params.expirationDate,
        officialReleaseDate: params.officialReleaseDate
      }
    })
  }
}
