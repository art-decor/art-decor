import Api from '@/services/Api'

export default {
  /**
   * Create lock on object id.
   * @param {Object} params Object containing the parameters.
   * @param {string} params.effectiveDate
   * @param {string} params.objectId
   * @param {string} params.projectId
   * @returns {Promise<AxiosResponse<any>>}
   */
  async lockObjectId ({ objectId, effectiveDate, projectId, ...rest }) {
    /**
     * If a lock can be made on the required object,
     * the object is free to edit (for that user).
     * In all other cases a lock is in place.
     */
    return await Api().post('/api/lock/', null,
      {
        params: {
          objectId,
          objectEffectiveDate: effectiveDate ? encodeURIComponent(effectiveDate) : null,
          projectId: projectId ?? null,
          ...rest
        }
      }
    )
  },

  /**
   * Remove lock on object id.
   * @param {Object} params Object containing the parameters.
   * @param {string} params.effectiveDate
   * @param {string} params.objectId
   * @param {string} params.projectId
   * @returns {Promise<AxiosResponse<any>>}
   */
  async unlockObjectId ({ objectId, effectiveDate, projectId, ...rest }) {
    return await Api().delete('/api/lock', {
      params: {
        objectId,
        objectEffectiveDate: effectiveDate ? encodeURIComponent(effectiveDate) : null,
        projectId: projectId ?? null,
        ...rest
      }
    })
  },
  /**
   * Get locks info.
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getLocksList () {
    return await Api().get('/api/lock', {
      params: {}
    })
  },
  /**
   * Get locks info for a project
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async getLocksListForProject (prefix) {
    return await Api().get(`/api/lock/?prefix=${prefix}`, {
      params: {}
    })
  },
  /**
   * Remove lock on username
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async unlockUsername (username) {
    return await Api().delete(`/api/lock/?username=${username}`)
      .catch(() => {})
  },
  /**
   * Remove lock on prefix
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async unlockPrefix (data) {
    const { ref, user, prefix, effectiveDate } = data
    if (prefix) {
      if (effectiveDate !== undefined) {
        return await Api().delete(`/api/lock/?objectId=${ref}&prefix=${prefix}&username=${user}&objectEffectiveDate=${encodeURIComponent(effectiveDate)}`)
          .catch(() => {})
      } else {
        return await Api().delete(`/api/lock/?prefix=${prefix}`)
          .catch(() => {})
      }
    }
  },
  /**
   * Remove lock on prefix and username
   *
   * @returns {Promise<AxiosResponse<any>>}
   */
  async unlockUsernameAndPrefix (username, prefix) {
    return await Api().delete(`/api/lock/?username=${username}&prefix=${prefix}`)
      .catch(() => {})
  }
}
