import Api from '@/services/Api'

export default {
  getDataSetList (params, abortController) {
    return Api().get('/api/dataset', {
      params: {
        prefix: params.prefix,
        release: params.release,
        includebbr: params.includebbr,
        search: params.search
      },
      signal: abortController?.signal
    })
  },

  getDataset (id, effectiveDate) {
    return Api().get(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}`)
  },

  getDatasetHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },

  createDataset (queryParams = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post('/api/dataset', null, {
        params: {
          prefix: queryParams.prefix,
          targetDate: queryParams.targetDate,
          sourceId: queryParams.sourceId,
          sourceEffectiveDate: queryParams.sourceEffectiveDate,
          keepIds: queryParams.keepIds,
          baseDatasetId: queryParams.baseDatasetId,
          baseConceptId: queryParams.baseConceptId,
          templateAssociations: queryParams.templateAssociations,
          skipDeprecated: queryParams.skipDeprecated,
          testMode: queryParams.testMode
        }
      })
  },

  updateDataset (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .put(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}`, data, {
        params
      })
  },

  updateDatasetStatus (id, effectiveDate, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}/$status`, null, {
      params: {
        recurse: params.recurse,
        list: params.list,
        statusCode: params.statusCode,
        versionLabel: params.versionLabel,
        expirationDate: params.expirationDate,
        officialReleaseDate: params.officialReleaseDate
      }
    })
  },

  patchDataset (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}`, data, {
        params
      })
  },

  getDataSetTree (id, effectiveDate, abortController, params = { treeonly: true, conceptproperty: 'inheritedcontains' }) {
    const apip = id + (effectiveDate.length ? ('/' + encodeURIComponent(effectiveDate)) : '')
    return Api().get(`/api/dataset/${apip}`, {
      params,
      signal: abortController?.signal
    })
  },

  getDatasetUsage (id, effectiveDate, params = {}) {
    return Api().get(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}/$usage`, {
      params: {
        prefix: params.prefix,
        release: params.release,
        language: params.language
      }
    })
  },

  getDatasetConceptList (queryParams = {}, abortController) {
    return Api().get('/api/concept', {
      params: {
        prefix: queryParams.prefix || null,
        search: queryParams.search || null,
        type: queryParams.type || null,
        status: queryParams.status || null,
        datasetId: queryParams.datasetId || null,
        datasetEffectiveDate: queryParams.datasetEffectiveDate || null,
        max: queryParams.max || null,
        originalonly: queryParams.originalonly || null,
        localconceptsonly: queryParams.localconceptsonly || null,
        conceptId: queryParams.conceptId || null,
        conceptEffectiveDate: queryParams.conceptEffectiveDate || null
      },
      signal: abortController?.signal
    })
  },

  getDatasetConceptWithAssociations (id, effectiveDate) {
    return Api().get(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}?associations=true`)
  },

  getDatasetConcept (id, effectiveDate, associations, params = {}, abortController) {
    return Api().get(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}${(associations) ? `?associations=${associations}` : ''}`, {
      params,
      signal: abortController?.signal
    })
  },

  getConceptHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },

  getDatasetConceptUsage (id, effectiveDate, params = {}) {
    return Api().get(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}/$usage`, {
      params: {
        transactionId: params.transactionId,
        transactionEffectiveDate: params.transactionEffectiveDate,
        prefix: params.prefix,
        release: params.release,
        language: params.language
      }
    })
  },

  getEditableDatasetConcept (id, effectiveDate, data, queryParams = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}/$edit`, data, {
        params: {
          targetId: queryParams.targetId,
          targetEffectiveDate: queryParams.targetEffectiveDate,
          targetType: queryParams.targetType,
          generateConceptListIds: queryParams.generateConceptListIds,
          breakLock: queryParams.breakLock,
          associations: queryParams.associations
        }
      })
  },

  clearDatasetLocks (id, effectiveDate, { params = {}, broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}/$clear-locks`, null, {
      params
    })
  },

  createDatasetConcept (id, effectiveDate, conceptType, insertMode, insertRef, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post(`/api/dataset/${id}/${encodeURIComponent(effectiveDate)}/$concept?conceptType=${conceptType}&insertMode=${insertMode}&insertRef=${insertRef}`)
  },

  patchDatasetConcept (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}`, data, {
        params: {
          associations: params.associations
        }
      })
  },

  patchDatasetConceptMap (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}/$associations`, data, {
        params
      })
  },
  postConceptStatus (id, effectiveDate, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post(`/api/concept/${id}/${encodeURIComponent(effectiveDate)}/$status`, null, {
      params
    })
  }
}
