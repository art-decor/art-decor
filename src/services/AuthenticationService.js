import Api from '@/services/Api'

export default {
  login (credentials) {
    return Api().post('/api/token', credentials)
  },
  test () {
    return Api().get('/api/token/test')
  }
}
