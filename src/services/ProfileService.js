import Api from '@/services/Api'

export default {
  getProfileList (params = {}, abortController) {
    return Api().get('/api/profile', {
      params,
      signal: abortController?.signal
    })
  }
}
