import Api from '@/services/Api'

export default {
  deleteValueSet (id, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/valueset/${id}`, {
      params
    })
  },
  /**
   * Retrieves DECOR valueSet list
   *
   * @param {string} prefix - Determines search scope. null is full server (+ cache), pfx- limits scope to this project only
   * @param {string} resolve - If value is 'true' (default) tries to resolve any valueSet references in the project terminology. Default value: true.
   * @param {string} sort - Sort ascending or descending, where ascending is the default. Available values: ascending, descending.
   * @param {string} language - Parameter to select from a specific compiled language.
   * @return {Promise<AxiosResponse<any>>}
   */
  getValueSetList (prefix, resolve, sort, language) {
    return Api().get('/api/valueset', {
      params: {
        prefix,
        resolve,
        sort,
        language
      }
    })
  },
  /**
   * Retrieves DECOR valueSet based on id (oid) and effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version
   *
   * @param {string} id - Parameter denoting the id of the template.
   * @param {string} effectiveDate - parameter denoting the effectiveDate of the valueSet. Example: 2012-07-25T15:22:56.
   * @param {string} prefix - Determines search scope. null is full server (+ cache), pfx- limits scope to this project only
   * @param {string} version - parameter to select from a release. Expected format yyyy-mm-ddThh:mm:ss or 'development'
   * @param {string} language - Parameter to select from a specific compiled language.
   * @return {Promise<AxiosResponse<any>>}
   */
  getValueSet (idOrRef, effectiveDate, prefix, language, abortController) {
    const apip = idOrRef
    if (effectiveDate != null) {
      return Api().get(`/api/valueset/${apip}/${encodeURIComponent(effectiveDate)}`, {
        params: {
          prefix,
          language
        },
        signal: abortController?.signal
      })
    } else {
      return Api().get(`/api/valueset/${apip}`, {
        params: {
          prefix,
          language
        },
        signal: abortController?.signal
      })
    }
  },
  getValueSetUsage (id, effectiveDate, params = {}) {
    return Api().get(`/api/valueset/${id}/${encodeURIComponent(effectiveDate)}/$usage`, {
      params
    })
  },

  getValueSetHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/valueset/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },
  deleteCodeSystem (id, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).delete(`/api/codesystem/${id}`, {
      params
    })
  },
  // Retrieve a list of DECOR codeSystems
  getCodeSystemList (params = {}) {
    return Api().get('/api/codesystem', { params })
  },
  getCodeSystem (idOrRef, effectiveDate, params = {}, abortController) {
    const endPoint = effectiveDate
      ? `/api/codesystem/${idOrRef}/${encodeURIComponent(effectiveDate)}`
      : `/api/codesystem/${idOrRef}`
    return Api().get(endPoint, { params, signal: abortController?.signal })
  },
  getCodeSystemUsage (id, effectiveDate, params = {}) {
    return Api().get(`/api/codesystem/${id}/${encodeURIComponent(effectiveDate)}/$usage`, {
      params
    })
  },
  getCodeSystemHistory ({ id, effectiveDate, abortController } = {}) {
    return Api().get(`/api/codesystem/${id}/${encodeURIComponent(effectiveDate)}/history`, {
      signal: abortController?.signal
    })
  },
  updateCodeSystemStatus (id, effectiveDate, data, params = {}) {
    return Api().post(`/api/codesystem/${id}/${encodeURIComponent(effectiveDate)}/$status`, data, {
      params
    })
  },
  patchCodeSystem (id, effectiveDate, data, params = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).patch(`/api/codesystem/${id}/${encodeURIComponent(effectiveDate)}`, data, {
      params
    })
  },
  postCodeSystem (data, params, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast }).post('/api/codesystem', data, {
      params
    })
  },
  getTerminologyGoveranceGroupLogo (org) {
    return Api().get(`/api/codesystem/logo/${org}`)
  },
  /*
  *  get value set expansion
     param: expand: true|false
  */
  getValueSetExpansion (idOrRef, effectiveDate, params = {}) {
    const endPoint = effectiveDate
      ? `/api/valueset/${idOrRef}/${encodeURIComponent(effectiveDate)}/$expand`
      : `/api/valueset/${idOrRef}/$expand`
    return Api().get(endPoint, {
      params
    })
  },

  /**
   * Retrieves DECOR concept map list.
   *
   * @param prefix Determines search scope. null is full server (+ cache), pfx- limits scope to this project only
   * @returns {Promise<AxiosResponse<any>>}
   */
  getConceptMapList (params) {
    return Api().get('/api/conceptmap', { params })
  },

  /**
   * Retrieves DECOR concept map based on id (oid) and effectiveDate (yyyy-mm-ddThh:mm:ss) denoting its version.
   *
   * @param id Id of the concept map.
   * @param effectiveDate Effective date of the concept map.
   * @param prefix Prefix of the concept map.
   * @param params Parameters.
   * @returns {Promise<AxiosResponse<any>>}
   */
  getConceptMapItem ({ id, effectiveDate, prefix, abortController, ...params }) {
    return Api().get(`/api/conceptmap/${id}/${encodeURIComponent(effectiveDate)}`, {
      params: {
        prefix
      },
      signal: abortController?.signal
    })
  },

  createConceptMapItem (data, queryParams = {}, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .post('/api/conceptmap', data, {
        params: queryParams
      })
  },

  /**
   * Put a concept map item.
   *
   * @param id Id of the concept map.
   * @param effectiveDate Effective date of the concept map.
   * @param data Data to update.
   * @param broadcast Broadcast the change to all servers.
   * @returns {Promise<AxiosResponse<any>>}
   */
  putConceptMapItem (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .put(`/api/conceptmap/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },

  /**
   * Patch a concept map item.
   *
   * @param id Id of the concept map.
   * @param effectiveDate Effective date of the concept map.
   * @param data Data to patch.
   * @param broadcast Broadcast the change to all servers.
   * @returns {Promise<AxiosResponse<any>>}
   */
  patchConceptMapItem (id, effectiveDate, data, { broadcast } = {}) {
    return Api({ 'X-Broadcast': broadcast })
      .patch(`/api/conceptmap/${id}/${encodeURIComponent(effectiveDate)}`, data)
  },

  // Expansion sets
  getValueSetExpansionSet ({ id } = {}) {
    return Api().get(`/api/valuesetexpansion/${id}`)
  },

  deleteValueSetExpansionSet ({ id } = {}) {
    return Api().delete(`/api/valuesetexpansion/${id}`)
  }
}
