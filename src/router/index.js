import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {
    path: '/home',
    name: 'home',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "art" */ '../views/Art/Home.vue')
  },
  {
    path: '/:prefix/project/overview',
    name: 'project-overview',
    props: true,
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/Overview.vue')
  },
  {
    path: '/:prefix/project/authors',
    name: 'project-authors',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/Author')
  },
  {
    path: '/:prefix/project/publication',
    name: 'project-publication',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/Publication.vue')
  },
  {
    path: '/:prefix/project/implementation-guide',
    name: 'project-implementation-guide',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/ImplementationGuide.vue'),
    children: [
      {
        path: ':igId/:igEffectiveDate',
        name: 'project-implementation-guide-pages',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ImplementationGuideDetailsCard.vue')
      },
      {
        path: ':igId/:igEffectiveDate/page/:pageId',
        name: 'project-implementation-guide-page-details',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ImplementationGuideDetailsCard.vue')
      }
    ]
  },
  {
    path: '/:prefix/project/history',
    name: 'project-history',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/History.vue')
  },
  {
    path: '/:prefix/project/identifier',
    name: 'project-identifier',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/Identifier.vue')
  },
  {
    path: '/:prefix/project/decor-locks',
    name: 'project-decor-locks-panel',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/ProjectDecorLocks.vue')
  },
  {
    path: '/:prefix/project/community',
    name: 'project-community',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/MyCommunity.vue')
  },
  {
    path: '/:prefix/project/community/:name',
    name: 'project-community-details',
    props: true,
    component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityDetails.vue'),
    children: [
      {
        path: 'associations',
        name: 'project-community-details-associations',
        props: true,
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityAssociations.vue'),
        children: [
          {
            path: 'dataset/:datasetId/:datasetVersion',
            name: 'project-community-details-associations-data-set',
            component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityAssociationsDetailsCard.vue'),
            props: true
          },
          {
            path: 'dataset/:datasetId/:datasetVersion/:conceptId/:conceptVersion',
            name: 'project-community-details-associations-data-set-concept',
            component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityAssociationsDetailsCard.vue'),
            props: true
          },
          {
            path: 'transaction/:transactionId/:transactionVersion',
            name: 'project-community-details-associations-transaction',
            component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityAssociationsDetailsCard.vue'),
            props: true
          },
          {
            path: 'transaction/:transactionId/:transactionVersion/:tConceptId/:tConceptVersion',
            name: 'project-community-details-associations-transaction-concepts',
            component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityAssociationsDetailsCard.vue'),
            props: true
          }
        ]
      },
      {
        path: 'definition',
        name: 'project-community-details-definition',
        props: true,
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/MyCommunityDefinition.vue')
      }
    ]
  },
  {
    path: '/:prefix/project/governance',
    name: 'project-governance',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/ProjectGovernancegroup.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:prefix/project/ada',
    name: 'project-ada',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/ADA.vue')
  },
  {
    path: '/:prefix/project/development',
    name: 'project-development',
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/Development.vue'),
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:prefix/project/project-index',
    name: 'project-index-panel',
    redirect: { name: 'project-index-datasets' },
    component: () => import(/* webpackChunkName: "project" */ '../views/Project/ProjectIndex.vue'),
    children: [
      {
        path: 'datasets',
        name: 'project-index-datasets',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexDatasets.vue')
      },
      {
        path: 'transactions',
        name: 'project-index-transactions',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexTransactions.vue')
      },
      {
        path: 'valuesets',
        name: 'project-index-valuesets',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexValuesets.vue')
      },
      {
        path: 'templates',
        name: 'project-index-templates',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexTemplates.vue')
      },
      {
        path: 'codesystems',
        name: 'project-index-codesystems',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexCodesystems.vue')
      },
      {
        path: 'concept-maps',
        name: 'project-index-concept-maps',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexConceptMaps.vue')
      },
      {
        path: 'questionnaires',
        name: 'project-index-questionnaires',
        component: () => import(/* webpackChunkName: "project" */ '../components/Project/ProjectIndexQuestionnaires.vue')
      }
    ]
  },
  {
    path: '/:prefix/datasets/dataset',
    name: 'dataset',
    component: () => import(/* webpackChunkName: "dataset" */ '../views/Dataset/Dataset.vue'),
    props: true,
    children: [
      {
        path: ':datasetId/:datasetVersion',
        name: 'dataset-details',
        component: () => import(/* webpackChunkName: "dataset" */ '../components/Dataset/DatasetDetailsCard.vue'),
        props: true
      },
      {
        path: ':datasetId/:datasetVersion/concept/:conceptId/:conceptVersion',
        name: 'dataset-details-version',
        component: () => import(/* webpackChunkName: "dataset" */ '../components/Dataset/DatasetDetailsCard.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/scenarios/scenarios',
    name: 'scenarios',
    component: () => import(/* webpackChunkName: "scenario" */ '../views/Scenario/Scenario.vue'),
    props: true,
    children: [
      {
        path: ':scenarioId/:scenarioVersion',
        name: 'scenarios-details',
        component: () => import(/* webpackChunkName: "scenario" */ '../components/Scenario/ScenarioDetailsCard.vue'),
        props: true
      }, {
        path: ':scenarioId/:scenarioVersion/concepts/:conceptId/:conceptVersion',
        name: 'scenarios-details-concepts',
        component: () => import(/* webpackChunkName: "scenario" */ '../components/Scenario/ScenarioDetailsCard.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/scenarios/actors',
    name: 'actors',
    component: () => import(/* webpackChunkName: "scenario" */ '../views/Scenario/Actor.vue')
  },
  {
    path: '/:prefix/rules/questionnaires',
    name: 'questionnaires',
    component: () => import(/* webpackChunkName: "scenario" */ '../views/Rule/Questionnaire.vue'),
    children: [
      {
        path: ':id',
        name: 'questionnaires-details-id',
        props: true,
        component: () => import(/* webpackChunkName: "scenario" */ '../components/Rule/QuestionnaireDetailCard.vue')
      },
      {
        path: ':id/:effectiveDate',
        name: 'questionnaires-details-id-effectiveDate',
        props: true,
        component: () => import(/* webpackChunkName: "scenario" */ '../components/Rule/QuestionnaireDetailCard.vue')
      }
    ]
  },
  {
    path: '/:prefix/rules/templates',
    name: 'templates',
    component: () => import(/* webpackChunkName: "rule" */ '../views/Rule/Template.vue'),
    children: [
      {
        path: ':id',
        name: 'templates-details-id',
        props: true,
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/TemplateDetailCard.vue')
      },
      {
        path: ':id/:effectiveDate',
        name: 'templates-details-id-effectiveDate',
        props: true,
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/TemplateDetailCard.vue')
      }
    ]
  },
  {
    path: '/:prefix/rules/profiles',
    name: 'profiles',
    component: () => import(/* webpackChunkName: "rule" */ '../views/Rule/Profile.vue'),
    children: [
      {
        path: ':id',
        name: 'profiles-details-id',
        props: true,
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/ProfileDetailCard.vue')
      },
      {
        path: ':id/:effectiveDate',
        name: 'profiles-details-id-effectiveDate',
        props: true,
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/ProfileDetailCard.vue')
      }
    ]
  },
  {
    path: '/:prefix/rules/associations',
    name: 'templateAssociation',
    component: () => import(/* webpackChunkName: "rule" */ '../views/Rule/Association.vue'),
    children: [
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate',
        name: 'templateAssociation-root',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/template/:templateId/:templateEffectiveDate',
        name: 'templateAssociation-root-template',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/concept/:datasetScenarioId/:datasetScenarioEffectiveDate',
        name: 'templateAssociation-root-concept',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/concept/:datasetScenarioId/:datasetScenarioEffectiveDate/template/:templateId/:templateEffectiveDate',
        name: 'templateAssociation-root-concept-template',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/transaction/:transactionId/:transactionEffectiveDate',
        name: 'templateAssociation-root-transaction',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/transaction/:transactionId/:transactionEffectiveDate/template/:templateId/:templateEffectiveDate',
        name: 'templateAssociation-root-transaction-template',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/transaction/:transactionId/:transactionEffectiveDate/concept/:datasetScenarioId/:datasetScenarioEffectiveDate',
        name: 'templateAssociation-root-transaction-concept',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/:rootDatasetScenarioId/:rootDatasetScenarioEffectiveDate/transaction/:transactionId/:transactionEffectiveDate/concept/:datasetScenarioId/:datasetScenarioEffectiveDate/template/:templateId/:templateEffectiveDate',
        name: 'templateAssociation-root-transaction-concept-template',
        component: () => import(/* webpackChunkName: "rule" */ '../views/Rule/Association.vue'),
        props: true
      },
      {
        path: ':rootDatasetScenarioType/template/:templateId/:templateEffectiveDate',
        name: 'templateAssociation-template',
        component: () => import(/* webpackChunkName: "art" */ '../views/Rule/Association.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/rules/identifiers',
    name: 'rule-identifiers',
    component: () => import(/* webpackChunkName: "rule" */ '../views/Rule/Identifiers.vue'),
    redirect: { name: 'rule-identifiers-template' },
    children: [
      {
        path: 'template',
        name: 'rule-identifiers-template',
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/IdentifiersTemplate.vue')
      },
      {
        path: 'profile',
        name: 'rule-identifiers-profile',
        component: () => import(/* webpackChunkName: "rule" */ '../components/Rule/IdentifiersProfile.vue')
      }
    ]
  },
  {
    path: '/:prefix/terminology/concepts',
    name: 'concepts',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex')
  },
  {
    path: '/:prefix/terminology/identifiers',
    name: 'identifiers',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/Identifiers.vue'),
    redirect: { name: 'terminology-identifiers-valueset' },
    children: [
      {
        path: 'valueset',
        name: 'terminology-identifiers-valueset',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/IdentifiersValueset.vue')
      },
      {
        path: 'concept-map',
        name: 'terminology-identifiers-concept-map',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/IdentifiersConceptMap.vue')
      },
      {
        path: 'codesystem',
        name: 'terminology-identifiers-codesystem',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/IdentifiersCodesystem.vue')
      }
    ]
  },
  {
    path: '/:prefix/terminology/valueset',
    name: 'value-set',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/Terminology.vue'),
    props: true,
    children: [
      {
        path: ':id',
        name: 'terminology-value-set',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/TerminologyDetailsCard.vue'),
        props: true
      },
      {
        path: ':id/:version',
        name: 'terminology-value-set-version',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/TerminologyDetailsCard.vue'),
        props: true
      },
      {
        path: ':id/:effectiveDate',
        name: 'value-set-details-id-effectiveDate',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/TerminologyDetailsCard.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/terminology/associations',
    name: 'terminology-associations',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/Associations.vue'),
    props: true,
    children: [
      {
        path: 'dataset/:datasetId/:datasetVersion',
        name: 'terminology-associations-data-set',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/AssociationsDetailsCard.vue'),
        props: true
      },
      {
        path: 'dataset/:datasetId/:datasetVersion/:conceptId/:conceptVersion',
        name: 'terminology-associations-data-set-concept',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/AssociationsDetailsCard.vue'),
        props: true
      },
      {
        path: 'transaction/:transactionId/:transactionVersion',
        name: 'terminology-associations-transaction',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/AssociationsDetailsCard.vue'),
        props: true
      },
      {
        path: 'transaction/:transactionId/:transactionVersion/:tConceptId/:tConceptVersion',
        name: 'terminology-associations-transaction-concepts',
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/AssociationsDetailsCard.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/terminology/concept-maps',
    name: 'concept-maps',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/ConceptMap.vue'),
    props: true,
    children: [
      {
        path: ':conceptMapId/:conceptMapVersion',
        name: 'concept-maps-details',
        component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/ConceptMap.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/terminology/concept-maps/:conceptMapId/:conceptMapVersion/editor',
    name: 'concept-maps-editor',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/ConceptMapEditor.vue'),
    props: true
  },
  {
    path: '/:prefix/terminology/codesystems',
    name: 'codesystems',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/CodeSystems'),
    children: [
      {
        path: ':id',
        name: 'codesystems-details-id',
        props: true,
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/CodeSystemsDetailsCard.vue')
      },
      {
        path: ':id/:effectiveDate',
        name: 'codesystems-details-id-effectiveDate',
        props: true,
        component: () => import(/* webpackChunkName: "terminology" */ '../components/Terminology/CodeSystemsDetailsCard.vue')
      }
    ]
  },
  {
    path: '/terminology/browser',
    name: 'browser',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex')
  },
  {
    path: '/:prefix/terminology/browser',
    name: 'browser-project',
    props: true,
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex')
  },
  {
    path: '/:prefix/terminology/browser/:codeSystemId',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex'),
    props: true
  },
  {
    path: '/terminology/browser/:codeSystemId/concept/:conceptId',
    name: 'browser-concept',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex'),
    props: true
  },
  {
    path: '/:prefix/terminology/browser/:codeSystemId/concept/:conceptId',
    name: 'browser-project-concept',
    component: () => import(/* webpackChunkName: "terminology" */ '../views/Terminology/BrowserIndex'),
    props: true
  },
  {
    path: '/:prefix/issues/issues',
    name: 'issues',
    component: () => import(/* webpackChunkName: "issue" */ '../views/Issue/Issue.vue'),
    props: true,
    children: [
      {
        path: ':issueId',
        name: 'issue',
        component: () => import(/* webpackChunkName: "issue" */ '../views/Issue/IssueDetails.vue'),
        props: true
      }
    ]
  },
  {
    path: '/:prefix/issues/labels',
    name: 'labels',
    component: () => import(/* webpackChunkName: "issue" */ '../views/Issue/Label.vue')
  },
  {
    path: '/server-admin/overview',
    name: 'server-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/ServerAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/projects',
    name: 'project-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/ProjectAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/decor-locks',
    name: 'decor-locks-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/DecorLocks.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/server-management',
    name: 'functions-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/FunctionsAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/users',
    name: 'user-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/UserAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/adawib',
    name: 'adawib-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/AdawibAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/news',
    name: 'news-admin-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/UrgentNewsAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/server-scheduler-panel',
    name: 'server-scheduler-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/ServerScheduler.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/art-settings',
    name: 'art-settings-admin',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/ArtSettingsAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/server-admin/governancegroups',
    name: 'server-governancegroup-panel',
    component: () => import(/* webpackChunkName: "server-admin" */ '../views/ServerAdmin/ServerGovernancegroupAdmin.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    },
    children: [
      {
        path: ':governancegroupId',
        name: 'server-governancegroup-details',
        component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetails.vue'),
        props: true,
        children: [
          {
            path: 'edit',
            name: 'server-governancegroup-details-edit',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetails.vue'),
            props: true
          },
          {
            path: 'projects',
            name: 'server-governancegroup-details-dashboard',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsDashboard.vue'),
            props: true
          },
          {
            path: 'projects',
            name: 'server-governancegroup-details-projects',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsProjects.vue'),
            props: true
          },
          {
            path: 'valuesets',
            name: 'server-governancegroup-details-valuesets',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsValuesets.vue'),
            props: true
          },
          {
            path: 'codesystems',
            name: 'server-governancegroup-details-codesystems',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsCodesystems.vue'),
            props: true
          },
          {
            path: 'conceptmaps',
            name: 'server-governancegroup-details-conceptmaps',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsConceptmaps.vue'),
            props: true
          },
          {
            path: 'templates',
            name: 'server-governancegroup-details-templates',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsTemplates.vue'),
            props: true
          },
          {
            path: 'questionnaires',
            name: 'server-governancegroup-details-questionnaires',
            component: () => import(/* webpackChunkName: "server-admin" */ '../components/ServerAdmin/ServerGovernancegroupAdminPanelDetailsQuestionnaires.vue'),
            props: true
          }
        ]
      }
    ]
  },
  {
    path: '/governancegroups',
    name: 'governancegroup-panel',
    component: () => import(/* webpackChunkName: "governancegroups" */ '../views/Art/Governancegroup.vue'),
    meta: {
      passWithoutProject: true
    },
    children: [
      {
        path: ':governancegroupId',
        name: 'governancegroup-details',
        component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetails.vue'),
        props: true,
        children: [
          {
            path: 'edit',
            name: 'governancegroup-details-edit',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetails.vue'),
            props: true
          },
          {
            path: 'projects',
            name: 'governancegroup-details-dashboard',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsDashboard.vue'),
            props: true
          },
          {
            path: 'projects',
            name: 'governancegroup-details-projects',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsProjects.vue'),
            props: true
          },
          {
            path: 'valuesets',
            name: 'governancegroup-details-valuesets',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsValuesets.vue'),
            props: true
          },
          {
            path: 'codesystems',
            name: 'governancegroup-details-codesystems',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsCodesystems.vue'),
            props: true
          },
          {
            path: 'conceptmaps',
            name: 'governancegroup-details-conceptmaps',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsConceptmaps.vue'),
            props: true
          },
          {
            path: 'templates',
            name: 'governancegroup-details-templates',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsTemplates.vue'),
            props: true
          },
          {
            path: 'questionnaires',
            name: 'governancegroup-details-questionnaires',
            component: () => import(/* webpackChunkName: "governancegroups" */ '../components/GovernanceGroup/GovernancegroupPanelDetailsQuestionnaires.vue'),
            props: true
          }
        ]
      }
    ]
  },
  {
    path: '/server-admin/oid-registry',
    name: 'oid-registry-list-panel',
    component: () => import(/* webpackChunkName: "designs" */ '../views/ServerAdmin/OidRegistryList.vue'),
    meta: {
      requiresAuth: true,
      passWithoutProject: true
    }
  },
  {
    path: '/design/dialogs',
    name: 'dialogs-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/DialogsDesign.vue')
  },
  {
    path: '/design/buttons',
    name: 'buttons-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/ButtonsDesign.vue')
  },
  {
    path: '/design/icons',
    name: 'icons-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/IconsDesign.vue')
  },
  {
    path: '/design/treetable',
    name: 'treetable-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/TreetableDesign.vue')
  },
  {
    path: '/design/forms',
    name: 'form-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/FormDesign.vue')
  },
  {
    path: '/design/graphics',
    name: 'graphics-design-panel',
    meta: {
      passWithoutProject: true
    },
    component: () => import(/* webpackChunkName: "designs" */ '../views/Designs/GraphicsDesign.vue')
  },
  {
    path: '*',
    redirect: 'home'
  }
]

const router = new Router({
  // mode: 'history', // removes hash from url
  linkActiveClass: 'active',
  routes
})

router.beforeEach(async (to, from, next) => {
  // Handle project fetching based on route
  const prefixFromRoute = to.params.prefix
  const isUserLoggedIn = router.app.$store.getters['authentication/isUserLoggedIn']

  if (!prefixFromRoute) {
    // Default to prefix of projectIFocus if present, else redirect to home
    const currentPrefix = router.app.$store.getters['project/getProjectInFocus']?.prefix
    const storedPrefix = localStorage.getItem('prefix')
    const passWithoutProject = to.matched.some(record => record.meta.passWithoutProject)
    if (!currentPrefix && !storedPrefix && !passWithoutProject) {
      return next({ name: 'home' })
    }
    if (!currentPrefix && storedPrefix) {
      // Load the specified project
      try {
        const getProjects = router.app.$store.dispatch('project/getProject', { prefix: storedPrefix, checkadram: true })
        const dataToFetch = [getProjects]
        if (isUserLoggedIn) {
          const getAuthors = router.app.$store.dispatch('project/getProjectAuthorList', { prefix: storedPrefix })
          dataToFetch.push(getAuthors)
        }
        await Promise.allSettled(dataToFetch)
      } catch (error) {
        localStorage.removeItem('prefix')
        return next({ name: 'home' })
      }
    }
    to.params.prefix = currentPrefix || storedPrefix
  } else {
    const correctProjectLoaded = router.app.$store.getters['project/getProjectInFocus']?.prefix === to.params.prefix
    if (!correctProjectLoaded) {
      // Clear the store for project related data
      router.app.$store.dispatch('project/init')
      router.app.$store.commit('dataset/initDataset')
      router.app.$store.commit('scenario/initScenario')
      router.app.$store.commit('questionnaire/initQuestionnaires')
      router.app.$store.commit('terminology/initTerminology')
      router.app.$store.commit('rule/template/initProfiles')
      router.app.$store.commit('rule/profile/initProfiles')
      router.app.$store.commit('issues/initIssues')

      // Set prefix in localStorage
      localStorage.setItem('prefix', prefixFromRoute)

      // Load the specified project
      try {
        const getProjects = router.app.$store.dispatch('project/getProject', {
          prefix: prefixFromRoute,
          checkadram: true
        })
        const dataToFetch = [getProjects]
        if (isUserLoggedIn) {
          const getAuthors = router.app.$store.dispatch('project/getProjectAuthorList', { prefix: prefixFromRoute })
          dataToFetch.push(getAuthors)
        }
        await Promise.allSettled(dataToFetch)
      } catch (error) {
        return next({ name: 'home' })
      }
    }
  }

  // Check if user is authenticated
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  if (requiresAuth && !isUserLoggedIn) {
    router.app.$store.dispatch('authentication/setLoginDialog', true)
    router.app.$store.dispatch('authentication/saveToForRedirect', to)
    next({ name: 'home' })
  } else if (requiresAuth && isUserLoggedIn) {
    next()
  } else {
    next()
  }
})

export default router
