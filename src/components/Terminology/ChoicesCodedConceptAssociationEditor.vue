<template>
  <v-dialog
      attach="#associations-card"
      :value="showDialog"
      @keydown.esc="$emit('cancel')"
      fullscreen
      scrollable
      hide-overlay
  >
    <MessageSnackbar v-if="showDialog" />

    <v-card>
      <AdCardTitle dialog>{{ $t('add-valueset-code-association') }}</AdCardTitle>

      <v-card-text class="mt-4">
        <v-row>
          <v-col>
            <v-card>
              <AdCardTitle class="mb-4">
                <AdStatusDot
                  :icon="$filters.getDatasetIcon(conceptItem)"
                  :status="conceptStatus"
                  larger
                  class="mr-2"
                  tooltip
                />
                {{ conceptName }}
              </AdCardTitle>
              <v-card-text class="px-0">
                <AdInfoOwl class="mt-6 mb-5 mx-4">{{ $t('choice-list-mapping-editor-hint') }}</AdInfoOwl>
                <ChoicesAssociationsTable
                  ref="mappingsTable"
                  :conceptList="conceptList"
                  :showDropzone="modal.showDropzone"
                  :dragging="modal.dragging"
                  :loading="pendingUpdate"
                  hideSynonyms
                  @createMapping="$emit('createMapping', $event)"
                  @deleteMapping="$emit('deleteMapping', $event)"
                  @restoreMapping="$emit('restoreMapping', $event)"
                />
              </v-card-text>
            </v-card>

          </v-col>

          <v-col>
            <v-card>
              <AdCardTitle class="mb-4">{{ $t('valuesets') }}</AdCardTitle>

              <v-card-text class="px-0">
                <!-- Dropdown for valueset selection -->
                <v-autocomplete
                  v-model="modal.selectedValueSet"
                  class="mb-6 mx-5"
                  :items="availableValueSets"
                  item-value="uuid"
                  :label="$t('value-set')"
                  return-object
                  clearable
                  open-on-clear
                  :filter="valueSetDropdownFilter"
                  :loading="isLoading('valueSet')"
                  :disabled="isLoading('valueSet')"
                  hide-details
                  @change="handleSelectValueSet"
                >
                  <template #item="{ item }">
                    <AdStatusDot larger tooltip class="mr-3" :status="item.statusCode" icon="mdi-heart" />
                    {{ $filters.getMatchingTranslation({ value: item.name, locale: $i18n.locale })}}
                    <span v-if="item.flexibility === 'dynamic'" style="white-space: break-spaces"> ({{ $t('dynamic') }})</span>
                    <span v-else-if="item.versionLabel || item.effectiveDate" style="white-space: break-spaces"> ({{ item.versionLabel || $filters.formatDateShort(item.effectiveDate) }})</span>
                  </template>
                  <template #selection="{ item }">
                    <AdStatusDot class="mr-2" tooltip :status="item.statusCode" icon="mdi-heart" />
                    {{ $filters.getMatchingTranslation({ value: item.name, locale: $i18n.locale })}}
                    <span v-if="item.flexibility === 'dynamic'" style="white-space: break-spaces"> ({{ $t('dynamic') }})</span>
                    <span v-else-if="item.versionLabel || item.effectiveDate" style="white-space: break-spaces"> ({{ item.versionLabel || $filters.formatDateShort(item.effectiveDate) }})</span>
                  </template>
                </v-autocomplete>

                <!-- Table with concept list of selected valueset -->
                <v-data-table
                  v-if="modal.selectedValueSet"
                  :items="filteredValueSetItemConcepts"
                  :headers="[{value: 'displayName'}]"
                  hide-default-header
                  :headers-length="5"
                  :loading="isLoading('valueSetItem')"
                  :footer-props="{ 'items-per-page-options': [5,10] }"
                  :hide-default-footer="valueSetItemConcepts <= 5"
                  :search="modal.valueSetTableSearch"
                  :custom-filter="valueSetTableSearchFilter"
                >
                  <template #top>
                    <AdTableSearch v-model="modal.valueSetTableSearch" class="mr-1">
                      <template #prepend>
                        <v-switch v-model="modal.noMappingsOnly" class="ml-6" :label="$t('no-mappings-only')"/>
                      </template>
                    </AdTableSearch>
                  </template>
                  <template #header>
                    <ValueSetConceptListHeader :items="valueSetItemConcepts" :coloredHeaders="true"/>
                  </template>

                  <template #item="{ item }">
                    <TerminologyValueSetConceptItem
                      :itemsInValueSetHasOrdinal="hasOrdinal"
                      :item="item"
                      :showEditable="false"
                      :draggable="isDraggable(item)"
                      data-vs-coded-concept
                      @mousedown="modal.dragging = item"
                    />
                  </template>
                </v-data-table>
              </v-card-text>
            </v-card>
          </v-col>
        </v-row>
      </v-card-text>

      <v-card-actions class="dialogbottomstrip">
        <v-spacer></v-spacer>
        <AdButton type="close" @click="$emit('cancel')" />
      </v-card-actions>
    </v-card>
  </v-dialog>
</template>

<script>
import { cloneDeep } from 'lodash'
import { mapGetters } from 'vuex'
import { isCancel } from 'axios'

import AdTableSearch from '../Utilities/AdTableSearch.vue'
import ChoicesAssociationsTable from '@/components/Terminology/ChoicesAssociationsTable'
import MessageSnackbar from '@/components/Utilities/MessageSnackbar'
import TerminologyValueSetConceptItem from '@/components/Terminology/TerminologyValueSetConceptItem'
import ValueSetConceptListHeader from '@/components/Terminology/ValueSetConceptListHeader'

const initialModal = {
  dragging: null,
  selectedValueSet: null,
  showDropzone: false,
  noMappingsOnly: true,
  valueSetTableSearch: ''
}

export default {
  components: {
    AdTableSearch,
    ChoicesAssociationsTable,
    MessageSnackbar,
    TerminologyValueSetConceptItem,
    ValueSetConceptListHeader
  },
  props: {
    associatedValueSets: {
      type: Array,
      default: () => ([])
    },
    conceptItem: {
      type: Object,
      default: null
    },
    // Is a mapping update (PATCH) in progress?
    pendingUpdate: {
      type: Boolean,
      default: false
    },
    // Determines the state (open/closed) of the dialiog
    showDialog: {
      type: Boolean,
      required: true
    }
  },
  data () {
    return {
      abortController: null,
      associationsDetailsCardNode: null,
      modal: cloneDeep(initialModal)
    }
  },
  computed: {
    ...mapGetters('terminology', ['getFullValueSetList', 'valueSetItem', 'isLoading']),

    availableValueSets () {
      return this.valueSets.filter(vs => {
        return this.associatedValueSets.some(ass => ass.valueSet === vs.id && (
          vs.flexibility === 'dynamic'
            ? ass.flexibility === 'dynamic'
            : ass.flexibility === vs.effectiveDate
        ))
      })
    },
    conceptName () {
      if (!this.conceptItem) {
        return ''
      }
      return this.$filters.getMatchingTranslation({ value: this.conceptItem.name })
    },
    conceptList () {
      return this.conceptItem?.valueDomain?.[0]?.conceptList?.[0]?.concept || []
    },
    conceptStatus () {
      return this.conceptItem?.statusCode
    },
    filteredValueSetItemConcepts () {
      if (this.modal.noMappingsOnly) {
        return this.unmappedValueSetItemConcepts
      }
      return this.valueSetItemConcepts
    },
    hasOrdinal () {
      return this.valueSetItemConcepts.some((item) => item.ordinal)
    },
    isDraggable () {
      return item => !['completeCodeSystem', 'include', 'exclude'].includes(item.is)
    },
    unmappedValueSetItemConcepts () {
      return this.valueSetItemConcepts.filter(item => (
        !this.conceptList.some(concept => (
          concept?.terminologyAssociation?.some(ass => ass.code === item.code)
        ))
      ))
    },
    valueSetItemConcepts () {
      return this.valueSetItem?.items || []
    },
    valueSets () {
      return this.getFullValueSetList({ includeDynamic: true }) || []
    }
  },
  methods: {
    handleDragStart (ev) {
      if (!ev.target.hasAttribute('data-vs-coded-concept')) {
        ev.preventDefault()
        return
      }
      ev.dataTransfer.setData('text/plain', JSON.stringify(this.modal.dragging))
      ev.dataTransfer.effectAllowed = 'link'
      this.modal.showDropzone = true
    },
    handleDragEnd () {
      this.modal.showDropzone = false
      this.modal.dragging = null
    },
    async handleSelectValueSet (item) {
      if (!item) {
        return
      }

      // Clear current valueSetItem
      this.$store.commit('terminology/setValueSetItem', null)

      const { id, effectiveDate } = item
      const prefix = this.projectInFocus?.prefix

      // Abort pending request
      if (this.abortController) {
        this.abortController.abort('Re-fetching issue list')
      }

      // Set up abortController
      this.abortController = new AbortController()

      try {
        await this.$store.dispatch('terminology/getValueSet', { id, effectiveDate, prefix, abortController: this.abortController })
      } catch (error) {
        if (isCancel(error)) {
          return
        }
        const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
        this.$oops(errorMessage)
      }
    },
    valueSetDropdownFilter (item, queryText, itemText) {
      const name = this.$filters.getMatchingTranslation({ value: item.name }).toLowerCase()
      const search = queryText.toLowerCase()
      return name?.includes(search)
    },
    valueSetTableSearchFilter (value, search, item) {
      console.log(item)
      const query = search.toLowerCase()

      const code = item.code?.toLowerCase()
      const displayName = item.displayName?.toLowerCase()

      return code?.includes(query) || displayName?.includes(query)
    }
  },
  watch: {
    showDialog: {
      immediate: true,
      async handler (isOpen) {
        if (isOpen) {
          if (!this.valueSets || !this.valueSets.length) {
            try {
              await this.$store.dispatch('terminology/getValueSetList', { resolveit: true, sort: 'displayName' })
            } catch (error) {
              const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
              this.$oops(errorMessage)
            }
          }
          if (this.availableValueSets.length === 1) {
            this.modal.selectedValueSet = this.availableValueSets[0]
            this.handleSelectValueSet(this.modal.selectedValueSet)
          }
        } else {
          // The modal is closed
          this.modal = cloneDeep(initialModal)
          this.$store.commit('terminology/setValueSetItem', null)

          if (this.$refs.mappingsTable) {
            this.$refs.mappingsTable.clearSearchInput()
          }
        }
      }
    }
  },
  async mounted () {
    this.associationsDetailsCardNode = document.getElementById('associations-card')

    this.associationsDetailsCardNode.addEventListener('dragstart', this.handleDragStart)
    this.associationsDetailsCardNode.addEventListener('dragend', this.handleDragEnd)
  },
  beforeDestroy () {
    this.associationsDetailsCardNode.removeEventListener('dragstart', this.handleDragStart)
    this.associationsDetailsCardNode.removeEventListener('dragend', this.handleDragEnd)
  }
}
</script>

<style lang="scss" scoped>
tr[data-vs-coded-concept]:not([draggable=false]) {
  cursor: move;
  cursor: grab;
}

tr[draggable=false] {
  cursor: not-allowed;
  user-select: none;
}
</style>
