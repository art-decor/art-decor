<template>
  <div class="d-flex align-center">
    <!-- Trackings -->
    <v-timeline-item
        v-if="event.type === 'tracking'"
        color="blue lighten-2"
        icon="mdi-text"
        :key="`${eventIndex}-tracking`"
      >
        <strong v-if="event.effectiveDate">{{$filters.formatDate(event.effectiveDate)}}</strong>
        <div>
          {{ $t("tracking") + " " + $t("action-by") }}
          {{ eventAuthor(event) }}
        </div>

        <!-- Status change trackings -->
        <div v-if="isStatusChangeTracking(eventIndex)">
          {{ $t('status') }}:
          <AdStatusDot :status="previousStatusCode(eventIndex)" /> {{ statusLabel(previousStatusCode(eventIndex)) }}
          <v-icon>mdi-arrow-right-thin</v-icon>
          <AdStatusDot :status="event.statusCode" /> {{ statusLabel(event.statusCode) }}
        </div>

        <!-- Label change trackings -->
        <div v-if="isLabelChangeTracking(eventIndex)">
          {{ $t('labels') }}:

          <!-- Old labels -->
          <template v-if="previousLabels(eventIndex)">
            <v-chip
              v-for="(label, chipIndex) in getParsedLabels(previousLabels(eventIndex))"
              :key="`${eventIndex}-${label.code}-${chipIndex}`"
              :class="{ 'mr-2': chipIndex < (getParsedLabels(previousLabels(eventIndex)).length - 1)}"
              :color="label.color"
              x-small
              label
              :text-color="$filters.getDarkOrLightFontColor(label.color)"
            >
              {{ label.code }}
            </v-chip>
          </template>
          <template v-else>{{ $t('none')}}</template>

          <v-icon>mdi-arrow-right-thin</v-icon>

          <!-- New labels -->
          <template v-if="event.labels">
            <v-chip
              v-for="(label, chipIndex) in getParsedLabels(event.labels)"
              :key="`${label.code}-${chipIndex}`"
              :class="{ 'mr-2': chipIndex < (getParsedLabels(event.labels).length - 1)}"
              :color="label.color"
              x-small
              label
              :text-color="$filters.getDarkOrLightFontColor(label.color)"
            >
              {{ label.code }}
            </v-chip>
          </template>

          <template v-else>{{ $t('none') }}</template>

        </div>

        <!-- Other changes -->
        <TextEditorRenderer v-if="event.desc">
          <div
            v-html="$filters.getMatchingTranslation({value: event.desc})"
            class="event-description"
          />
        </TextEditorRenderer>

        <template v-if="coAuthors(event).length">
          <div v-for="(author, i) in coAuthors(event)" :key="`coAuthorTracking-${i}`" class="text--disabled font-italic">
            {{ $t("edited-by") }} {{ author['#text'] }}
            <template v-if="author.effectiveDate">
              ({{ $filters.formatDate(author.effectiveDate) }})
            </template>
          </div>
        </template>

    </v-timeline-item>

    <!-- Assignments -->
    <v-timeline-item
      v-else-if="event.type === 'assignment'"
      :key="`${eventIndex}-assignment`"
      color="orange lighten-2"
      icon="mdi-account"
    >
      <strong>{{$filters.formatDate(event.effectiveDate)}}</strong>
      <div>
        {{ $t("assigned-to") }} {{ event.name }}
        {{ $t("action-by") }} {{ eventAuthor(event) }}
      </div>
      <div
        v-if="event.desc"
        v-html="$filters.getMatchingTranslation({ value: event.desc})"
        class="event-description"
      />

      <template v-if="coAuthors(event).length">
        <div v-for="(author, i) in coAuthors(event)" :key="`coAuthorAss-${i}`" class="text--disabled font-italic">
          {{ $t("edited-by") }} {{ author['#text'] }}
          <template v-if="author.effectiveDate">
            ({{ $filters.formatDate(author.effectiveDate) }})
          </template>
        </div>
      </template>
    </v-timeline-item>

    <AdButton
      v-if="showEditButton && !(isStatusChangeTracking(eventIndex) || isLabelChangeTracking(eventIndex))"
      :disabled="!eventIsEditableByAuthor"
      class="ml-4 mb-4"
      type="edit"
      list
      @click="$emit('edit', event)"
    />
  </div>
</template>

<script>
export default {
  props: {
    event: {
      type: Object,
      required: true
    },
    // List of all events, needed for comparison
    eventList: {
      type: Array,
      required: true
    },
    showEditButton: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    coAuthors () {
      return event => event.author?.slice(1) || []
    },

    eventAuthor () {
      return event => event.author?.[0]?.['#text']
    },

    eventIndex () {
      return this.eventList?.indexOf(this.event)
    },

    eventIsEditableByAuthor () {
      if (!this.$store.getters['authentication/isUserLoggedIn']) {
        return false
      }

      if (this.userIsDecorAdmin) {
        return true
      }

      const username = this.$store.getters['authentication/getUsername']
      const userId = this.$store.getters['project/authors']
        ?.find(author => author.username === username)
        ?.id
      if (userId == null) {
        return false
      }
      return this.event?.author?.some(auth => auth.id === userId)
    },

    isLabelChangeTracking () {
      return index => {
        const previousLabels = this.previousLabels(index)
        const currentLabels = this.eventList?.[index]?.labels
        return previousLabels !== currentLabels
      }
    },

    isStatusChangeTracking () {
      return index => {
        const previousStatusCode = this.previousStatusCode(index)
        const currentStatusCode = this.eventList?.[index]?.statusCode
        if (!previousStatusCode || !currentStatusCode) {
          return false
        }
        return previousStatusCode !== currentStatusCode
      }
    },

    labels () {
      return this.$store.getters['issues/labels'] || []
    },

    previousLabels () {
      return trackingIndex => this.eventList?.[trackingIndex + 1]?.labels
    },

    previousStatusCode () {
      return trackingIndex => this.eventList?.slice(trackingIndex + 1)?.find(item => item.statusCode)?.statusCode
    },

    statusLabel () {
      return status => {
        const issueStatusList = this.$store.getters['server/getIssueStatuses'] || []
        const issueStatus = issueStatusList.find(item => item.value === status)
        const matchingLabel = issueStatus?.label?.find(label => label.language === this.$i18n.locale)
        return matchingLabel?.['#text'] ?? status
      }
    }
  },

  methods: {
    getParsedLabels (labels) {
      if (typeof labels !== 'string') {
        return []
      }

      const matchingLabels = []
      const splitLabels = labels.split(' ')
      // Loop each split label.
      for (const splitLabel of splitLabels) {
        // Try to find matching label.
        const matchingLabel = (this.labels ?? [])
          .find(label => label?.code === splitLabel)
        if (!!matchingLabel === true) {
          matchingLabels.push(matchingLabel)
        }
      }
      return matchingLabels
    }
  }
}
</script>

<style lang="scss" scoped>
.event-description ::v-deep * {
  margin-bottom: 0;
}
</style>
