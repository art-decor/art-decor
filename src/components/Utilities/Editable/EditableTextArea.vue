<template>
  <div class="editable-text-area" v-bind:class="{'is-authenticated': isAuthenticated}">
    <EditableDialog
      :startEditMode="startEditMode"
      :saveEditValue="saveEditValue"
      :cancelEditMode="cancelEditMode"
      :showDialog="showDialog"
      :readonly="readonly"
      :disableSave="!inputIsValid"
    >

      <div slot="activator" slot-scope="scope" :class="{ 'editable-text-area-value': !readonly }">
        <label v-if="label && showLabelWhenReadOnly" class="text-editor-label">{{ label }}</label>
        <TextEditorRenderer>
          <truncate
            v-if="parsedValue"
            :text="parsedValue"
            :truncated="internalTruncated"
            :length="truncateAfter"
            :clamp="$t('seemore')"
            :less="$t('showless')"
            type="html"
            @toggle="internalTruncated = !$event"
          />
          <span v-else class="text--disabled">{{ $t('none') }}</span>
          {{ ' ' }}<a v-if="!isReadonly" class="proxy-event" @click="scope.proxyEvent($event, 'startEditMode')">{{ $t('edit-text') }}</a>
        </TextEditorRenderer>
      </div>

      <div slot="editable" slot-scope="scope">
        <slot name="editable:prepend" :value="editableValue" />
        <EditableTranslationEditor
          ref="input"
          v-model.lazy="editableValue"
          v-bind="{...$props, ...$attrs}"
          v-on="{...$listeners, input: () => {}, change: () => {}}"
          :required="required"
          :locales="locales"
          :info-text="infoText"
          @blur="scope.proxyEvent($event, 'saveEditValue')"
          editor
          :readonly="readonly"
          emitOnInput
        />
      </div>
    </EditableDialog>
  </div>
</template>

<script>
import EditableDialog from '@/components/Utilities/Editable/Helper/EditableDialog'
import EditableTranslationEditor from '@/components/Utilities/Editable/EditableTranslationEditor'
import truncate from '@/components/Utilities/AdTruncate' // We use out custom truncate component to work around bugs in vue-truncate-collapsed
import AbstractInput from '@/components/Utilities/Editable/Helper/AbstractInput'

export default {
  /**
   * Extends.
   */
  extends: AbstractInput,

  /**
   * Required components.
   */
  components: {
    EditableDialog,
    EditableTranslationEditor,
    truncate
  },

  /**
   * Properties.
   */
  props: {
    value: {
      type: [String, Array, Object],
      required: false,
      default: ''
    },
    locales: {
      type: Array,
      required: false,
      default () {
        return this.$root.langs
      }
    },
    showDialog: {
      type: Boolean,
      required: false,
      default: false
    },
    required: {
      type: Boolean,
      required: false,
      default: false
    },
    readonly: {
      type: Boolean,
      required: false,
      default: false
    },
    only: {
      type: String,
      required: false,
      default: '200'
    },
    label: {
      type: String,
      required: false
    },
    showLabelWhenReadOnly: {
      type: Boolean,
      required: false,
      default: false
    },
    truncated: Boolean,
    // Function used to validate editableItem
    validator: {
      type: Function,
      default: () => true
    }
  },

  /**
   * Data variables.
   */
  data () {
    return {
      edit: false,
      editableValue: null,
      inputIsValid: true,
      internalTruncated: true
    }
  },

  /**
   * Computed variables.
   */
  computed: {
    /**
     * Get parsed value.
     *
     * @return string
     */
    parsedValue () {
      // Return value.
      return this.locales.length > 0
        ? this.$filters.getMatchingTranslation({ value: this.value, returnObject: false })
        : String(this.value)
    },
    /**
     * Get truncate length
     */
    truncateAfter () {
      return Number(this.only)
    }
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Watch changes of current value.
     */
    value: {
      immediate: true,
      handler () {
        // Stop edit mode.
        this.edit = false
      }
    },
    editableValue: {
      deep: true,
      handler (value) {
        if (typeof this.validator === 'function') {
          this.inputIsValid = this.validator(this.editableValue) || false
        }
      }
    },
    internalTruncated: {
      handler (value) {
        this.$emit('update:truncated', value)
      }
    },
    truncated: {
      immediate: true,
      handler (value) {
        if (typeof this.$options.propsData.truncated !== 'boolean') return
        this.internalTruncated = value
      }
    }
  },

  /**
   * Methods.
   */
  methods: {
    /**
     * Start edit mode.
     */
    startEditMode (event) {
      // Check if target element is type of link.
      if (
        String(event?.target?.tagName).toLowerCase() === 'a' &&
        event?.target?.classList.contains('proxy-event') === false
      ) {
        // Return false.
        return false
      }

      // Check if current user is authenticated.
      if (this.isAuthenticated === true) {
        // Set editable value in base of value.
        this.editableValue = JSON.parse(JSON.stringify(this.value))
        // Set #text to empty string if not present.
        if (Array.isArray(this.editableValue)) {
          this.editableValue.forEach((translation) => {
            if (!translation['#text']) {
              this.$set(translation, '#text', '')
            }
          })
        }
        // Set edit to true.
        this.edit = true
        // Wait for the next tick.
        this.$nextTick(() => {
          // Check if input element is available.
          if (this.$refs?.input) {
            // Focus input element.
            this.$refs.input.focus()
          }
        })
      }

      // Return true if authenticated.
      return this.isAuthenticated === true
    },

    /**
     * Cancel edit mode.
     */
    cancelEditMode () {
      // Check if current user is authenticated.
      if (this.isAuthenticated === true) {
        // Set edit back to false.
        this.edit = false
      }

      // Return true if authenticated.
      return this.isAuthenticated === true
    },

    /**
     * Save edit value.
     */
    saveEditValue () {
      // Check if current user is authenticated.
      if (this.isAuthenticated === true) {
        // Emit current input value.
        this.$emit('input', this.editableValue)
        // Emit current change value.
        this.$emit('change', this.editableValue)
      }

      // Check if value has not changed.
      if (this.editableValue === this.parsedValue) {
        // Set edit back to false.
        this.edit = false
      }

      // Return true if authenticated.
      return this.isAuthenticated === true
    }
  }
}
</script>

<style lang="scss">
.editable-text-area {
  width: 100%;

  .editable-text-area-value {
    transition: background 0.25s ease-in-out;
    background: transparent;
    padding: 5px;
    margin: -5px;
    border-radius: 5px;

    .text-editor-label {
      display: block;
      font-size: 12px;
    }

    .text-editor-renderer {
      > div,
      > div > div {
        display: inline;
      }
    }
  }

  &.is-authenticated {
    .editable-text-area-value {
      &:hover {
        background-color: rgb(231, 245, 231);
        cursor: pointer;

        span {
          text-decoration: none;
        }
      }
    }
  }

  .v-input {
    padding-top: 0;
    margin-top: 0;
  }
}
</style>
