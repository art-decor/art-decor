import { wrappingInputRule, toggleBlockType } from 'tiptap-commands'
import { Node } from 'tiptap'

export default class Paragraph extends Node {
  get name () {
    return 'paragraph'
  }

  get schema () {
    return {
      content: 'inline*',
      group: 'block',
      defining: true,
      draggable: false,
      parseDOM: [{
        tag: 'p'
      }],
      toDOM: () => ['p', 0]
    }
  }

  commands ({ type }) {
    return () => toggleBlockType(type)
  }

  keys ({ type }) {
    return {
      'Alt-Mod-0': toggleBlockType(type)
    }
  }

  inputRules ({ type }) {
    return [wrappingInputRule(/^\s*>\s$/, type)]
  }
}
