import { Mark } from 'tiptap'
import { toggleMark } from 'tiptap-commands'

export default class Subscript extends Mark {
  get name () {
    return 'subscript'
  }

  get schema () {
    return {
      parseDOM: [
        {
          tag: 'sub'
        },
        {
          style: 'vertical-align',
          getAttrs: value => value === 'sub'
        }
      ],
      toDOM: () => ['sub', 0]
    }
  }

  keys ({ type }) {
    return {
      'Ctrl-,': toggleMark(type)
    }
  }

  commands ({ type }) {
    return () => toggleMark(type)
  }
}
