import { pasteRule } from 'tiptap-commands'
import { Node, Plugin } from 'tiptap'
import router from '../../../../router'

// Define click handler event.
let clickHandlerEvent = null

export default class Artefact extends Node {
  get name () {
    return 'artefact'
  }

  get schema () {
    return {
      group: 'inline',
      inline: true,
      content: 'text*',
      draggable: true,
      selectable: false,
      inclusive: false,
      attrs: {
        type: {},
        href: {},
        label: {}
      },
      parseDOM: [{
        priority: 51,
        tag: [
          '[href^="ds::"]',
          '[href^="tm::"]',
          '[href^="vs::"]',
          '[href^="cs::"]',
          '[href^="cm::"]'
        ],
        getAttrs: dom => ({
          type: dom.getAttribute('href').match(/^([^:]+)::/i)?.[1],
          href: dom.getAttribute('href'),
          label: dom.innerText

        })
      }],
      toDOM: node => ['a', {
        href: node.attrs.href,
        class: `__artdecor__a__inline__artifact __artdecor_${node.attrs.type}`,
        onClick: 'event.stopPropagation(); event.preventDefault(); return'
      }, node.attrs.label]
    }
  }

  commands ({ type }) {
    return attrs => (state, dispatch) => {
      const { selection } = state
      const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
      const node = type.create(attrs)
      const transaction = state.tr.insert(position, node)
      dispatch(transaction)
    }
  }

  pasteRules ({ type }) {
    return [pasteRule(/(ds|tm|vs)::/gi, type, url => {
      return {
        href: url
      }
    })]
  }

  get plugins () {
    return [new Plugin({
      props: {
        async handleClick (view, pos, event) {
          // Stop propagation and default behaviour.
          event.stopPropagation()
          event.preventDefault()

          const href = event.target?.getAttribute('href')
          const matching = href?.match(/^(?<type>[^:]+)::(?<id>[^/]+)(\/(?<version>.*))?/)
          const mappings = {
            ds: { route: 'dataset-details', id: 'datasetId', version: 'datasetVersion' },
            tm: { route: 'templates-details-id-effectiveDate', id: 'id', version: 'effectiveDate' },
            vs: { route: 'terminology-value-set-version', id: 'id', version: 'version' }
          }

          if (matching && mappings?.[matching.groups?.type]) {
            const type = matching.groups?.type
            const mapping = mappings?.[type]
            const route = router.resolve({
              name: mapping.route,
              params: {
                [mapping.id]: matching.groups?.id,
                [mapping.version]: matching.groups?.version
              }
            })

            // Check if editor is in edit mode.
            if (view.editable) {
              // Call click handler event.
              clickHandlerEvent({
                id: matching.groups?.id,
                version: matching.groups?.version,
                type: matching.groups?.type,
                href,
                route,
                callback (args) {
                  // Update node markup.
                  const transaction = view.state.tr.setNodeMarkup(pos - 1, view.state.schema.nodes.artefact, args)
                  // Dispatch transaction.
                  view.dispatch(transaction)
                }
              })
              // Stop execution.
              return
            }

            window.location.href = route.href
          }
        }
      }
    })]
  }

  static addClickHandler (handler) {
    // Set click handler event.
    clickHandlerEvent = handler
  }
}
