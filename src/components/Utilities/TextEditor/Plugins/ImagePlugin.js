import { Node, Plugin } from 'tiptap'
import { nodeInputRule } from 'tiptap-commands'
import { findChildren } from 'tiptap-utils'
import Api from '../../../../services/Api'
import router from '../../../../router'

// Define exist api path.
const existApiPath = '/exist/apps'
// Define image input regex.
const IMAGE_INPUT_REGEX = /!\[(.+|:?)]\((\S+)(?:(?:\s+)["'](\S+)["'])?\)/
// Define check images.
let checkImages = false

export default class Image extends Node {
  get name () {
    return 'image'
  }

  get schema () {
    return {
      inline: true,
      attrs: {
        src: {},
        alt: {
          default: null
        },
        title: {
          default: null
        }
      },
      group: 'inline',
      draggable: true,
      parseDOM: [{
        tag: 'img[src]',
        getAttrs: dom => ({
          src: dom.getAttribute('src'),
          title: dom.getAttribute('title'),
          alt: dom.getAttribute('alt')
        })
      }],
      toDOM: node => ['img', node.attrs]
    }
  }

  commands ({ type }) {
    return attrs => async (state, dispatch) => {
      // Get current selection.
      const { selection } = state
      // Fetch image.
      const imageResponse = await fetch(`/image-proxy?url=${encodeURI(attrs?.src)}`)
      // Handle image upload and get image data.
      const imageData = await Image.handleImageUpload(await imageResponse.blob())
      // Check if image data exists.
      if (imageData) {
        // Get current selection position.
        const position = selection.$cursor ? selection.$cursor.pos : selection.$to.pos
        // Create new image node.
        const node = type.create({ ...attrs, ...imageData })
        // Add image node to document.
        const transaction = state.tr.insert(position, node)
        // Dispatch transaction.
        dispatch(transaction)
      }
    }
  }

  inputRules ({ type }) {
    return [nodeInputRule(IMAGE_INPUT_REGEX, type, match => {
      const [, alt, src, title] = match
      return {
        src,
        alt,
        title
      }
    })]
  }

  get plugins () {
    return [new Plugin({
      view: () => ({
        /**
         * On update view.
         * We will hook into this method to check if images has to be checked.
         * If dom contains images that are not uploaded to the art-decor api
         * we will upload them.
         * @param view
         * @returns {Promise<void>}
         */
        update: async (view) => {
          // Check if images has to be checked.
          if (checkImages === true) {
            // Set check images to false.
            checkImages = false

            // Get all images from editor view.
            const images = findChildren(view.state.doc, (child) => {
              return child.type.name === 'image' && !(
                child?.attrs?.src?.startsWith(existApiPath) ||
                child?.attrs?.src?.startsWith(document.location.origin)
              )
            })

            // Loop through images.
            for (const image of images) {
              // Define image attributes.
              const attributes = { ...image.node.attrs }
              // Fetch image.
              const imageResponse = !image.node.attrs.src?.startsWith('data:')
                ? await (await fetch(`/image-proxy?url=${encodeURI(image.node.attrs.src)}`)).blob()
                : await Image.getDataImageAsBlob(image.node)

              // Handle image upload and get image data.
              const imageData = await Image.handleImageUpload(imageResponse)
              // Check if image data exists.
              if (imageData) {
                // Apply new uploaded image src.
                attributes.src = imageData.src
                // Set node markup and get transaction.
                const transaction = view.state.tr.setNodeMarkup(image.pos, null, attributes)
                // Dispatch transaction.
                view.dispatch(transaction)
              } else {
                // Delete image node and get transaction.
                const transaction = view.state.tr.delete(image.pos - 1, image.pos + 1)
                // Dispatch transaction.
                view.dispatch(transaction)
              }
            }
          }
        }
      }),
      props: {
        handleDOMEvents: {
          /**
           * On paste event.
           * We will hook into this method to check if images are pasted.
           * If images are pasted we will upload them to the art-decor api
           * and replace the image with the uploaded image.
           * @param view
           * @param event
           * @returns {Promise<boolean>}
           */
          async paste (view, event) {
            // Set check images to true.
            checkImages = true
            // Define has files and set it initially to false.
            let hasFiles = false

            // Loop through clipboard data files.
            Array.from(event.clipboardData.files)
              // Filter files that are images.
              .filter(item => item.type.startsWith('image'))
              // Loop through images.
              .forEach(item => {
                // Define has files and set it to true.
                hasFiles = true
                // Handle image upload and get image data.
                Image.handleImageUpload(item).then((imageData) => {
                  // Get image url.
                  const imageUrl = imageData.src
                  // Create new image node.
                  const node = view.state.schema.nodes.image.create({ src: imageUrl })
                  // Add image node to document.
                  const transaction = view.state.tr.replaceSelectionWith(node)
                  // Dispatch transaction.
                  view.dispatch(transaction)
                })
              })

            // Check if has files is true.
            if (hasFiles) {
              // Prevent default event.
              event.preventDefault()
              // Stop execution.
              return true
            }
          },
          /**
           * On drop event.
           * We will hook into this method to check if images are dropped.
           * If images are dropped we will upload them to the art-decor api
           * and replace the image with the uploaded image.
           * @param view
           * @param event
           */
          drop (view, event) {
            // Check if event is available and has files.
            const hasFiles = event.dataTransfer && event.dataTransfer.files && event.dataTransfer.files.length
            // Check if files are not available.
            if (!hasFiles) {
              // Stop execution.
              return
            }

            // Get all images.
            const images = Array.from(event.dataTransfer.files).filter(file => /image/i.test(file.type))
            // Check if images are not available.
            if (images.length === 0) {
              // Stop execution.
              return
            }

            // Prevent default event.
            event.preventDefault()
            // Get current schema.
            const { schema } = view.state
            // Get current cursor position.
            const coordinates = view.posAtCoords({
              left: event.clientX,
              top: event.clientY
            })

            // Loop through images.
            images.forEach(async (image) => {
              // Handle image upload and get image data.
              const imageData = await Image.handleImageUpload(image)
              // Check if image data is available.
              if (imageData) {
                // Create new image node.
                const node = schema.nodes.image.create(imageData)
                // Add image node to document.
                const transaction = view.state.tr.insert(coordinates.pos, node)
                // Dispatch transaction.
                view.dispatch(transaction)
              }
            })
          }
        }
      }
    })]
  }

  /**
   * Handle image upload.
   *
   * This method will upload the requested image to the art-decor api
   * and return src and alt attributes for the image node.
   *
   * @param imageData
   * @returns {Promise<unknown>}
   */
  static handleImageUpload (imageData) {
    return new Promise((resolve) => {
      // Check if image data is not available.
      if (!imageData) {
        // Stop execution.
        return resolve(null)
      }

      // Get file reader instance.
      const reader = new FileReader()

      // Handle onload event.
      reader.onload = async (readerEvent) => {
        // Get image base 64 data.
        const imageBase64 = readerEvent.target.result
        // Get current project prefix.
        const projectPrefix = router.currentRoute.params?.prefix || ''
        // Send post request to upload image.
        const response = await Api({
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }).post(`/api/project/${projectPrefix}/blob`, {
          type: imageData?.type,
          base64: imageBase64
        })

        // Check if response has image src.
        if (response?.data?.src) {
          return resolve({
            src: `${existApiPath}${response?.data?.src}`,
            alt: response?.data?.alt
          })
        }

        return resolve(null)
      }

      // Read image data as data url.
      reader.readAsDataURL(imageData)
    })
  }

  /**
   * Get data image as blob.
   * @param image
   * @returns {Promise<unknown>}
   */
  static async getDataImageAsBlob (image) {
    // Check if image src is not a data url.
    if (!image?.src?.startsWith('data:')) {
      // Stop execution.
      return
    }

    return new Promise((resolve) => {
      const canvas = document.createElement('canvas')
      const context = canvas.getContext('2d')
      const onload = () => {
        canvas.width = image.naturalWidth
        canvas.height = image.naturalHeight
        context.drawImage(image, 0, 0)
        canvas.toBlob((blob) => {
          return resolve(blob)
        })
      }

      return !image.complete
        ? (image.onload = onload)
        : onload()
    })
  }
}
