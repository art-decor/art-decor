import { markInputRule, markPasteRule, toggleMark } from 'tiptap-commands'
import { Mark } from 'tiptap'

export default class Code extends Mark {
  get name () {
    return 'code'
  }

  get schema () {
    return {
      excludes: '_',
      parseDOM: [{
        tag: 'code'
      }],
      toDOM: () => ['code', 0]
    }
  }

  keys ({ type }) {
    return {
      'Mod-e': toggleMark(type)
    }
  }

  commands ({ type }) {
    return () => toggleMark(type)
  }

  inputRules ({ type }) {
    return [markInputRule(/(?:`)([^`]+)(?:`)$/, type)]
  }

  pasteRules ({ type }) {
    return [markPasteRule(/(?:`)([^`]+)(?:`)/g, type)]
  }
}
