export default (diagram, options = {}) =>
  diagram.replace(/^(\s*click\s+[^\s]+\s*)$/gm, `$1 call mermaidClick_${options.id}()`)
