<template>
  <div class="password-strength-meter">
    <v-text-field
      ref="input"
      v-model="editableValue"
      v-bind="{...$props, ...$attrs}"
      v-on="{...$listeners, input: () => {}}"
      :loading="showProgressBar"
      :type="type"
      :rules="parsedRules"
      color="green"
      counter
      @input="startEditMode" @focus="startEditMode" @blur="saveEditValue" @keydown.enter="saveEditValue"
      @keydown.tab="saveEditValue"
      @keydown.esc="cancelEditMode"
    >
      <template #append>
        <div v-if="editableValue">
          <v-icon
            v-text="type !== 'password' ? 'mdi-eye' : 'mdi-eye-off'"
            :color="validation.color"
            @click="toggleType"
          />
          <v-icon v-text="validation.status ? 'mdi-check' : 'mdi-close'" :color="validation.color" />
        </div>
      </template>

      <template #append-outer>
        <div v-if="generation === true">
          <AdButton type="generate" @click="generate" />
        </div>
      </template>

      <template #progress>
        <v-progress-linear
          v-if="showProgressBar"
          :value="validation.progress.percentage"
          :background-color="validation.color + ' lighten-3'"
          :color="validation.color"
          absolute
          height="7"
        />
      </template>
    </v-text-field>

  </div>
</template>

<script>

import AdButton from '@/components/Utilities/AdButton'

export default {
  /**
   * Properties.
   */
  props: {
    value: {
      type: String,
      required: false,
      default: null
    },
    match: {
      type: String,
      required: false,
      default: null
    },
    generation: {
      type: Boolean,
      required: false,
      default: false
    }
  },
  components: {
    AdButton
  },

  /**
   * Data variables.
   */
  data () {
    return {
      edit: false,
      editableValue: '',
      type: 'password',
      validation: {
        status: true,
        color: 'red',
        hint: null,
        progress: {
          percentage: 0,
          current: 0,
          max: 0
        }
      },
      rules: {
        characters: {
          pattern: /.{10,}/g, // The pattern to match against
          minimum: 1, // The number of matches against the pattern. Used for calculating the strenght score.
          force: true, // If the rule is forced
          hint: this.$t('pattern-password')
        },
        lowerCharacters: {
          pattern: /[a-z]/g,
          characters: 'abcdefghijklmnopqrstuvwxyz',
          minimum: 1,
          force: true,
          hint: this.$t('pattern-password')
        },
        upperCharacters: {
          pattern: /[A-Z]/g,
          characters: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
          minimum: 1,
          force: true,
          hint: this.$t('pattern-password')
        },
        specialCharacters: {
          pattern: /[!"§$%&/()=?,.\-+#]/g,
          characters: '!"§$%&/()=?,.-+#',
          minimum: 1,
          force: true,
          hint: this.$t('pattern-password')
        },
        numbers: {
          pattern: /[0-9]/g,
          characters: '0123456789',
          minimum: 1,
          force: true,
          hint: this.$t('pattern-password')
        }
      },
      generationSettings: {
        minLength: 10, // The generated password minimum length
        maxLength: 20 // The generated password maximum length
      },
      colors: [
        { color: 'red', minimum: 0, ignoreValidation: false },
        { color: 'orange', minimum: 33, ignoreValidation: false },
        { color: 'green', minimum: 66, ignoreValidation: true }
      ]
    }
  },

  /**
   * Computed variables.
   */
  computed: {
    /**
     * Get information if progress bar should be visible.
     *
     * @return {boolean}
     */
    showProgressBar () {
      return this.edit === true && this.editableValue !== null
    },

    /**
     * Get parsed rules for vuetify validator.
     */
    parsedRules () {
      // Get rules.
      const rules = Array.isArray(this.$attrs?.rules)
        ? [...this.$attrs.rules]
        : []

      // Get all required rules.
      const requiredRules = Object.values(this.rules).filter((rule) => rule.force === true)

      // Loop each required rule.
      for (const rule of requiredRules) {
        // Push rule to rules array.
        rules.push((v) => !!String(v).match(rule.pattern) || rule.hint)
      }

      // Check if match mode is active.
      if (!!this.match === true && this.editableValue !== this.match) {
        // Push rule to rules array.
        rules.push((v) => (String(v) === String(this.match)) || this.$t('passwords-dont-match'))
      }

      // Return rules.
      return rules
    }
  },

  /**
   * Watchers.
   */
  watch: {
    /**
     * Watch changes of current value.
     */
    value: {
      immediate: true,
      handler: function (value) {
        // Stop edit mode.
        this.edit = false
        // Set editable value in base of value.
        this.editableValue = value
        // Cancel edit mode.
        this.cancelEditMode()
      }
    },

    /**
     * Watch changes of match value.
     */
    match: {
      immediate: false,
      handler: function () {
        // Call validate password strength.
        this.validatePasswordStrength()
      }
    },

    /**
     * Watch changes of editable value.
     */
    editableValue: {
      immediate: false,
      handler: function (editableValue) {
        // Set editable value.
        this.editableValue = editableValue || null
        // Call validate password strength.
        this.validatePasswordStrength()
      }
    }
  },

  methods: {
    /**
     * Toggle type.
     */
    toggleType () {
      // Toggle type between password and text.
      this.type = this.type !== 'password'
        ? 'password'
        : 'text'
    },

    /**
     * Validate password strength.
     */
    validatePasswordStrength () {
      // Reset validation.
      this.validation = {
        status: true,
        color: 'red',
        hint: null,
        progress: {
          percentage: 0,
          current: 0,
          max: 0
        }
      }

      // Check if password is empty.
      if (!this.editableValue?.length) return false

      for (const rule of Object.values(this.rules)) {
        const matchingCharacters = String(this.editableValue).match(rule?.pattern)?.length || 0
        // Calculate current score in base of matching characters.
        rule.current = matchingCharacters < rule.minimum
          ? Math.min(rule.minimum, matchingCharacters)
          : rule.minimum

        this.validation.progress.current += rule.current

        // Check if matching characters are not in minimum range but are forced.
        if (matchingCharacters < rule.minimum && rule.force === true) {
          this.validation.status = false
        }

        // Check if matching characters are not in minimum range and hint is defined.
        if (matchingCharacters < rule.minimum && !!rule?.hint === true && this.validation.hint === null) {
          this.validation.hint = rule.hint
        }
      }

      // Get max progress.
      this.validation.progress.max = Object.values(this.rules)
        .map(rule => rule.minimum)
        .reduce((accumulator, currentValue) => accumulator + Math.round(currentValue || 0))

      // Set current progress in percent.
      this.validation.progress.percentage = Math.max(0, Math.min(100, Math.round(
        this.validation.progress.current * 100 / this.validation.progress.max
      )))

      // Get matching validation color.
      this.validation.color = this.colors.filter(color => {
        return this.validation.progress.percentage >= color.minimum &&
          (this.validation.status === color.ignoreValidation || color.ignoreValidation === false)
      }).sort((a, b) => a.minimum > b.minimum ? -1 : 1)?.[0]?.color || ''

      // Return status.
      return this.validation.status === true
    },

    /**
     * Gernate a random password.
     */
    generate () {
      const randomCharacters = []
      const requiredCharacters = []
      const passwordLength = Math.floor(Math.random() * (this.generationSettings.maxLength - this.generationSettings.minLength) + this.generationSettings.minLength)
      let ruleMinLength = 0

      for (const rule of Object.values(this.rules)) {
        const minimum = rule?.minimum
        const characters = rule?.characters
        const length = characters?.length
        if (length) {
          randomCharacters.push(...(characters.split('')))
        }
        const isRequired = !!rule?.force

        if (length > 0 && minimum > 0) {
          ruleMinLength += minimum
          if (isRequired) {
            for (let i = 0; i < minimum; i++) {
              const requiredCharacter = characters.charAt(Math.floor(Math.random() * length))
              requiredCharacters.push(requiredCharacter)
            }
          }
        }
      }

      randomCharacters.sort(() => Math.random() - 0.5)
      this.type = 'text'
      const basePassword = randomCharacters.join('').substring(0, Math.max(passwordLength, ruleMinLength) - requiredCharacters.length)
      this.editableValue = [...basePassword.split(), ...requiredCharacters].sort(() => Math.random() - 0.5).join('')

      this.$emit('generated', this.editableValue)
      this.saveEditValue()
    },

    /**
     * Start edit mode.
     */
    startEditMode () {
      // Set edit to true.
      this.edit = true
    },

    /**
     * Cancel edit mode.
     */
    cancelEditMode () {
      // Set editable value in base of origin value.
      this.editableValue = this.value
      // Set edit back to false.
      this.edit = false
    },

    /**
     * Save edit value.
     */
    saveEditValue () {
      // Set edit back to false.
      this.edit = false
      // Emit input change.
      this.$emit('input', this.editableValue)
    }
  }
}
</script>

<style lang="scss">
.password-strength-meter {
  .v-text-field.v-text-field--enclosed .v-progress-linear__background {
    display: block !important;
  }
}
</style>
