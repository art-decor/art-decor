export default {
  copyright: {
    addrLine: [],
    years: null
  },
  defaultLanguage: null,
  desc: [], // contain array of translations { '#text': null, language: null }
  id: null,
  name: [] // contain array of translations { '#text': null, language: null }
}
