import copyrightProject from './copyrightProject'
import { cloneDeep } from 'lodash'

export default {
  author: [],
  collection: null, // examples/demo1
  copyright: cloneDeep(copyrightProject),
  defaultLanguage: null,
  desc: null, // [{ 'language': 'nl-NL', '#text': '' }]
  experimental: null,
  id: null, // oid
  lastmodified: null,
  name: null, // [{ 'language': 'nl-NL', '#text': '' }]
  notifier: 'on',
  package: 'projects',
  prefix: null,
  private: 'false',
  repository: 'false' // containsReusableContent
}
