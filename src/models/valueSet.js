export default {
  model: {
    displayName: null,
    statusCode: null,
    effectiveDate: null,
    versionLabel: null,
    id: null,
    canonicalUri: null,
    name: null,
    expirationDate: null,
    officialReleaseDate: null,
    desc: [],
    publishingAuthority: null,
    endorsingAuthority: null,
    purpose: null,
    copyright: null
  },
  steps: [
    ['displayName', 'effectiveDate', 'versionLabel'],
    ['id', 'canonicalUri', 'name', 'expirationDate', 'officialReleaseDate'],
    ['desc'],
    ['publishingAuthority'],
    ['purpose']
  ]
}
