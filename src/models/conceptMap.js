export default {
  effectiveDate: null,
  id: null,
  displayName: '',
  statusCode: null,
  sourceScope: [],
  targetScope: [],
  group: []
}
