export default {
  code: null, // pattern (some manual text the user has entered)
  codeSystem: null, // OID or URI
  codeSystemName: null, // not asked in add new Association
  conceptFlexibility: null, // version (effectiveDate) of the concept
  conceptId: null, // id of the concept
  displayName: null // not multilingual
}
