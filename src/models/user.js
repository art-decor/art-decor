export default {
  name: null,
  password: null,
  confirmPassword: null,
  active: 'true',
  displayName: null,
  email: null,
  organization: null,
  description: null,
  defaultLanguage: null,
  groups: [],
  primarygroup: null,
  resetnotify: false
}
