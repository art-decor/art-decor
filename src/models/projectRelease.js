export default {
  id: null,
  compileLanguage: [],
  development: null, // 'true', 'false'
  prefix: null,
  publicationRequest: null, // 'true', 'false'
  version: {
    by: null,
    date: null,
    desc: [],
    release: null, // 'true', 'false'
    statusCode: 'active' // [ draft, pending, active, retired, cancelled, failed ]
  }
}
