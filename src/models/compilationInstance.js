export default {
  file: {
    name: null,
    size: null,
    type: null,
    base64: null
  },
  transaction: null
}
