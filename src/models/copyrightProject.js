export default {
  years: null,
  by: null,
  logo: { // existing copyright: logo is a string
    base64: null, // temp attribute (only needed on create)
    name: null,
    size: null,
    type: null
  },
  type: null, // author, contributor, reviewer
  addrLine: [] // type, #text
}
