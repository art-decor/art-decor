import { cloneDeep } from 'lodash'
import otherDesignation from './otherDesignation'

export default {
  root: null,
  designation: [cloneDeep(otherDesignation)]
}
