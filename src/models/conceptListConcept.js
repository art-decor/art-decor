export default {
  desc: [], // mutilingual
  exception: null, // boolean (string)
  level: null, // positive integer (string)
  name: [], // mutilingual
  synonym: [], // mutilingual
  type: null // leaf (L), abstract (A), specializable (S), or deprecated (D) => DecorTypes.VocabType
}
