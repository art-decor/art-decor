export default {
  type: 'RFC',
  title: null,
  priority: 'N',
  labels: [],
  assignee: null,
  description: null
}
