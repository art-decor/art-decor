const config = {
  features: {
    // whether the menu offers the Design panels
    showDesignMenuItems: parse(process.env.VUE_APP_FEATURE_SHOW_DESIGN_MENU_ITEMS, false),
    // whether the home page JUST offers terminology testing as main entry (SO it will hide also specific components)
    testingTerminologyOnly: parse(process.env.VUE_APP_FEATURE_TESTING_TERMINOLOGY_ONLY, false),
    // whether to show development features of Release 3.x (true) or released features only (false)
    showDevelopmentFeature3: parse(process.env.VUE_APP_FEATURE_SHOW_DEVELOPMENT_FEATURES, false)
  }
}
function feature (name) {
  return config.features[name]
}
function parse (value, fallback) {
  if (typeof value === 'undefined') {
    return fallback
  }
  switch (typeof fallback) {
    case 'boolean' :
      return !!JSON.parse(value)
    case 'number' :
      return JSON.parse(value)
    default :
      return value
  }
}
export {
  config
}
export default {
  install (Vue) {
    Vue.appConfig = config
    Vue.feature = feature
    Vue.prototype.$appConfig = config
    Vue.prototype.$feature = feature
  }
}
