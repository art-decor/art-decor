export default {
  '#text': [
    v => !!v || 'address is required'
  ],
  requiredAttributes: [
    '#text'
  ]
}
