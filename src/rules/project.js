export default {
  defaultLanguage: [
    v => !!v || 'default language is required'
  ],
  description: [
    v => !!v || 'description is required'
  ],
  displayName: [
    v => !!v || 'display name is required'
  ],
  email: [
    v => !!v || 'e-mailaddress is required',
    v => /.+@.+\..+/.test(v) || 'e-mail must be valid'
  ],
  id: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  years: [
    v => !!v || 'years is required',
    v => /^(19|20)\d{2}(-((19|20)\d{2})?)?( (19|20)\d{2}(-((19|20)\d{2})?)?)*$/.test(v) || 'years must be valid'
  ],
  name: [
    v => !!v || 'required'
  ],
  organization: [
    v => !!v || 'required'
  ],
  private: [
    v => !!v || 'is private is required'
  ],
  package: [
    v => !!v || 'package language is required'
  ],
  prefix: [
    v => !!v || 'prefix is required',
    v => /^\S{1,79}-$/.test(v) || 'prefix must be valid'
  ],
  repository: [
    v => !!v || 'contains reusable content is required'
  ],
  url: [
    // eslint-disable-next-line
    v => /^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(v) || 'Invalid URL'
  ],
  requiredAttributes: [
    'by',
    'type',
    'years',
    'package',
    'name',
    'id',
    'prefix',
    'defaultLanguage',
    'repository',
    'private',
    'url'
  ]
}
