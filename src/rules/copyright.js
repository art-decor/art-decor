export default {
  copyright: [
    v => !!v || this.$t('required-field')
  ],
  requiredAttributes: [
    'copyright'
  ]
}
