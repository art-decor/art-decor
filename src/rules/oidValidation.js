const $t = (input) => {
  return window.Vue?.$t(input) || input
}

export default (required = false) => [
  v => (!required && v == null) || /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || $t('invalid-input')
]
