export default {
  code: [
    v => !!v || 'name is required'
  ],
  name: [
    v => !!v || 'name is required'
  ],
  color: [
    v => !!v || 'color is required'
  ],
  requiredAttributes: [
    'code', 'name', 'color'
  ]
}
