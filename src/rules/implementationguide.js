export default {
  title: [
    v => !!v || 'title is required'
  ],
  effectiveDate: [
    v => !!v || 'effective date is required'
  ],
  id: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  statusCode: [
    v => !!v || 'status code is required'
  ],
  requiredAttributes: [
    'effectiveDate',
    'id',
    'statusCode'
  ]
}
