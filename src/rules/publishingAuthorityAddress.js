export default {
  '#text': [
    v => !!v || 'address is required'
  ],
  type: [
    v => !!v || 'type is required'
  ],
  requiredAttributes: [
    '#text',
    'type'
  ]
}
