export default {
  name: [
    v => !!v || 'name language is required'
  ],
  id: [
    v => v == null || /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  requiredAttributes: [
    'name'
  ]
}
