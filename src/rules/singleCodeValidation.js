const $t = (input) => {
  return window.Vue?.$t(input) || input
}

export default [
  v => (typeof v === 'string' && v.length <= 128) || $t('invalid-input'),
  v => /^[^\s]+(\s[^\s]+)*$/.test(v) || $t('invalid-input')
]
