export default {
  name: [
    v => !!v || 'name is required'
  ],
  type: [
    v => !!v || 'type is required'
  ],
  requiredAttributes: [
    'type', 'name'
  ]
}
