export default {
  language: [
    v => !!v || 'default language is required'
  ],
  type: [
    v => !!v || 'type is required'
  ],
  displayName: [
    v => !!v || 'display name is required'
  ],
  '#text': [
    v => !!v || 'title is required'
  ],
  requiredAttributes: [
    'language',
    'type',
    'displayName',
    '#text'
  ]
}
