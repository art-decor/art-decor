export default {
  root: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  requiredAttributes: [
    'root'
  ]
}
