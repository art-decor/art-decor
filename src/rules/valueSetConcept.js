export default {
  code: [
    v => !!v || 'code is required',
    v => !/[:=]+/.test(v) || ': and = are reserved for post coordinated codes'
  ],
  codeSystem: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  codeSystemName: [
    v => !!v || 'codesystem name is required'
  ],
  displayName: [
    v => !!v || 'display name is required',
    v => !/[:=]+/.test(v) || ': and = are reserved for post coordinated codes'
  ],
  op: [
    v => !!v || 'intensional operator is required'
  ],
  level: [
    v => /^\d\d{0,5}$/.test(v) || 'level is greater than or equal to zero, with a maximum length of 6 characters'
  ],
  type: [
    v => !!v || 'type is required'
  ],
  requiredAttributes: [
    'code',
    'codeSystem',
    'codeSystemName',
    'displayName',
    'level',
    'op',
    'type'
  ]
}
