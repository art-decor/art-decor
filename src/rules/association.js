export default {
  requiredAttributes: [
    'displayName', 'code', 'codeSystem'
  ]
}
