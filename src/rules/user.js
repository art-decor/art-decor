const $t = (input) => {
  return window.Vue?.$t(input) || input
}

export default {
  requiredAttributes: ['name', 'displayName', 'email', 'password', 'confirmPassword'],
  defaultLanguage: [
    v => !!v || $t('required-field')
  ],
  displayName: [
    v => !!v || $t('required-field')
  ],
  email: [
    v => !!v || $t('required-field'),
    v => /.+@.+\..+/.test(v) || $t('invalid-input')
  ],
  name: [
    v => !!v || $t('required-field'),
    v => /^[a-z-]+$/.test(v) || $t('invalid-input')
  ],
  password: [
    v => !!v || $t('required-field')
  ],
  username: [
    v => !!v || $t('required-field'),
    v => v?.length >= 3 || $t('invalid-input'),
    v => /^[a-zA-Z]+[a-zA-Z0-9]*$/.test(v) || $t('invalid-input')
  ]
}
