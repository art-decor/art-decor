export default {
  id: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  prefix: [
    v => !!v || 'prefix is required',
    v => /^\S{1,79}-$/.test(v) || 'prefix must be valid'
  ],
  type: [
    v => !!v || 'type is required'
  ],
  requiredAttributes: [
    'id', 'prefix', 'type'
  ]
}
