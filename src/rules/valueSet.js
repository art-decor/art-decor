const $t = (input) => {
  return window.Vue?.$t(input) || input
}

export default {
  canonicalUri: [],
  copyright: [],
  desc: [],
  displayName: [],
  effectiveDate: [],
  endorsingAuthority: [],
  expirationDate: [],
  id: [
    v => v == null || /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || $t('invalid-input')
  ],
  level: [
    v => /^\d\d{0,5}$/.test(v) || $t('invalid-input') // 'level is greater than or equal to zero, with a maximum length of 6 characters'
  ],
  officialReleaseDate: [],
  name: [],
  publishingAuthority: [],
  purpose: [],
  statusCode: [],
  versionLabel: [],
  requiredAttributes: [
    'displayName',
    'name'
  ]
}
