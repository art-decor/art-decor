export default {
  name: [
    v => !!v || 'name is required'
  ],
  level: [
    v => /^\d\d{0,5}$/.test(v) || 'level is greater than or equal to zero, with a maximum length of 6 characters'
  ],
  requiredAttributes: [
    'name'
  ]
}
