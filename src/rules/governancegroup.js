export default {
  defaultLanguage: [
    v => !!v || 'default language is required'
  ],
  desc: [
    v => !!v || 'description language is required'
  ],
  id: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  copyright: [
    v => !!v || 'years is required',
    v => /^(19|20)\d{2}(-((19|20)\d{2})?)?( (19|20)\d{2}(-((19|20)\d{2})?)?)*$/.test(v) || 'years must be valid'
  ],
  name: [
    v => !!v || 'required'
  ],
  requiredAttributes: [
    'name',
    'id',
    'desc',
    'defaultLanguage',
    'copyright'
  ]
}
