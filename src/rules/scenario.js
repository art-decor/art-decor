export default {
  name: [
    v => !!v || 'name is required'
  ],
  effectiveDate: [
    v => !!v || 'effective date is required'
  ],
  id: [
    v => !!v || 'id is required',
    v => /^[0-2](\.(0|[1-9][0-9]*))*$/.test(v) || 'id must be valid'
  ],
  ident: [
    v => !!v || 'id is required',
    v => /^\S{1,79}-$/.test(v) || 'ident must be valid'
  ],
  statusCode: [
    v => !!v || 'status code is required'
  ],
  uuid: [
    v => !!v || 'uuid is required'
  ],
  requiredAttributes: [
    'effectiveDate',
    'ident',
    'id',
    'statusCode', // list of supported DECOR item statuses. Normal flow is new > draft > final > deprecated.
    'uuid'
  ]
}
