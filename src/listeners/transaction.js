// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'scenarios',
  'scenarios-details',
  'project-index-transactions'
]

const routesUsingTransactionItem = [
  'terminology-associations',
  'terminology-associations-data-set',
  'terminology-associations-data-set-concept',
  'terminology-associations-transaction',
  'terminology-associations-transaction-concepts'
]

export default {
  patchTransaction ({ silent = false, useTransactionItem = false } = {}, data) {
    const { response: updatedTransaction, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (routesUsingTransactionItem.includes(this.$route.name)) {
      useTransactionItem = true
    }

    this.$store.commit('scenario/updateScenarioItem', { item: updatedTransaction, data: updatedTransaction })
    const activeItem = this.$store.getters['scenario/activeItem']
    const scenarioItem = useTransactionItem
      ? this.$store.getters['scenario/transactionItem']
      : this.$store.getters['scenario/scenarioItem']

    if (activeItem?.id === updatedTransaction.id && activeItem?.effectiveDate === updatedTransaction.effectiveDate) {
      this.$store.commit('scenario/setActiveItem', updatedTransaction)

      const mutation = useTransactionItem ? 'setTransactionItem' : 'setScenarioItem'
      this.$store.commit(`scenario/${mutation}`, updatedTransaction)
    } else if (scenarioItem?.type === 'group' && Array.isArray(scenarioItem?.transaction)) {
      // Check if the updated transaction is a child of the active transaction group
      const currentTransactionGroupAffected = !!this.$filters.listHelper.findItemInList({
        items: [scenarioItem],
        children: 'transaction',
        handler: match => match.id === updatedTransaction.id && match.effectiveDate === updatedTransaction.effectiveDate
      })
      if (currentTransactionGroupAffected) {
        // Construct updated children list
        const updatedChildren = this.$filters.listHelper.replaceItemInList({
          items: scenarioItem.transaction,
          children: 'transaction',
          item: updatedTransaction,
          handler: match => match.id === updatedTransaction.id && match.effectiveDate === updatedTransaction.effectiveDate
        })

        // Construct updated transaction group and commit to store
        const updatedTransactionGroup = { ...scenarioItem, transaction: updatedChildren }

        const mutation = useTransactionItem ? 'setTransactionItem' : 'setScenarioItem'
        this.$store.commit(`scenario/${mutation}`, updatedTransactionGroup)

        // this.$store.commit('scenario/setScenarioItem', updatedTransactionGroup)
        this.$store.commit('scenario/setActiveItem', updatedTransactionGroup)
      }
    }
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      this.$notify(this.$t('proxy-tr-updated', {
        name: this.$filters.getMatchingTranslation({
          value: updatedTransaction.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  putTransaction ({ silent = false, useTransactionItem = false } = {}, data) {
    const { response: updatedTransaction, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (routesUsingTransactionItem.includes(this.$route.name)) {
      useTransactionItem = true
    }

    const scenarioItem = useTransactionItem
      ? this.$store.getters['scenario/transactionItem']
      : this.$store.getters['scenario/scenarioItem']
    if (scenarioItem?.id !== updatedTransaction.transactionId || scenarioItem?.effectiveDate !== updatedTransaction.transactionEffectiveDate) {
      return
    }

    if (!updatedTransaction.concept) {
      return
    }

    this.$store.commit('scenario/setTransactionItemConcepts', updatedTransaction.concept)
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      this.$notify(this.$t('proxy-rt-updated'), 'info', {
        long: true
      })
    }
  }
}
