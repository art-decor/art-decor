// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'actors'
]

export default {
  deleteScenarioActor ({ silent = false } = {}, data) {
    const { request } = data

    if (request.emitter === this.socketId) {
      return
    }

    // eslint-disable-next-line
    const actorId = request.originalUrl.match(/scenario\/actor\/([\d\.]+)/)?.[1] 
    if (!actorId) {
      return
    }
    const actorIndex = (this.$store.getters['scenario/actors'] || []).findIndex(a => a.id === actorId)
    if (actorIndex === -1) {
      return
    }

    const deletedActorName = this.$store.getters['scenario/actors'][actorIndex].name
    this.$store.commit('scenario/deleteActor', { index: actorIndex })
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('actors')
    )) {
      this.$notify(this.$t('proxy-ac-deleted', {
        name: this.$filters.getMatchingTranslation({
          value: deletedActorName
        })
      }), 'info', {
        long: true
      })
    }
  },

  patchScenarioActor ({ silent = false } = {}, data) {
    const { response: updatedActor, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const actorIndex = (this.$store.getters['scenario/actors'] || []).findIndex((a) => a.id === updatedActor.id)
    if (actorIndex === -1) {
      return
    }
    this.$store.commit('scenario/updateActor', { index: actorIndex, actor: updatedActor })
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('actors')
    )) {
      this.$notify(this.$t('proxy-ac-updated', {
        name: this.$filters.getMatchingTranslation({
          value: updatedActor.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  postScenarioActor ({ silent = false } = {}, data) {
    const { response: newActor, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('scenario/addActor', newActor)
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('actors')
    )) {
      this.$notify(this.$t('proxy-ac-added', {
        name: this.$filters.getMatchingTranslation({
          value: newActor.name
        })
      }), 'info', {
        long: true
      })
    }
  }
}
