// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'concept-maps',
  'concept-maps-details',
  'concept-maps-editor',
  'project-index-concept-maps',
  'terminology-identifiers-concept-map'
]

export default {
  patchConceptMap ({ silent = false } = {}, data) {
    const { response: updatedConceptMap, request } = data
    // Return if emitter is current user
    if (request.emitter === this.socketId) {
      return
    }

    const { conceptMapId: id, conceptMapVersion: effectiveDate } = this.$route.params

    // Determine if a notification is needed
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('conceptMap')
    )) {
      this.$notify(this.$t('proxy-cm-edited', {
        name: updatedConceptMap.displayName
      }), 'info', {
        long: true
      })
    }

    if (updatedConceptMap.id !== id || updatedConceptMap.effectiveDate !== effectiveDate) {
      this.$store.commit('terminology/patchConceptMapListMetaData', {
        updatedConceptMap,
        isActiveConceptMap: false,
        isVersionOfConceptMapActive: false
      })
      return
    }

    this.$store.commit('terminology/patchConceptMapListMetaData', { updatedConceptMap })
    this.$store.commit('terminology/setConceptMapItem', updatedConceptMap)
  },

  postConceptMap ({ silent = false } = {}, data) {
    const { response: conceptMap, request } = data
    if (request.emitter === this.socketId) {
      return
    }

    // Determine if a notification is needed
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('conceptMap')
    )) {
      this.$notify(this.$t('proxy-cm-added'), 'info', {
        long: true
      })
    }

    this.$store.commit('terminology/addConceptMapItemToList', conceptMap)
  },

  putConceptMap ({ silent = false } = {}, data) {
    const { response: updatedConceptMap, request } = data
    // Return if emitter is current user
    if (request.emitter === this.socketId) {
      return
    }

    const { conceptMapId: id, conceptMapVersion: effectiveDate } = this.$route.params

    // Determine if a notification is needed
    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('conceptMap')
    )) {
      this.$notify(this.$t('proxy-cm-edited', {
        name: updatedConceptMap.displayName
      }), 'info', {
        long: true
      })
    }

    if (updatedConceptMap.id !== id || updatedConceptMap.effectiveDate !== effectiveDate) {
      this.$store.commit('terminology/patchConceptMapListMetaData', {
        updatedConceptMap,
        isActiveConceptMap: false,
        isVersionOfConceptMapActive: false
      })
      return
    }

    this.$store.commit('terminology/patchConceptMapListMetaData', { updatedConceptMap })
    this.$store.commit('terminology/setConceptMapItem', updatedConceptMap)
  }
}
