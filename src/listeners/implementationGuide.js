import { getImplementationGuideFromUrl } from '@/listeners/helpers'

// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'project-implementation-guide',
  'project-implementation-guide-pages',
  'project-implementation-guide-page-details'
]

export default {
  patchIG ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    try {
      const originalUrl = new URL(request.originalUrl, 'http://somedomain.com')
      const returnMode = originalUrl.searchParams.get('returnmode')

      if (!returnMode) {
        return
      }

      let nameForNotification = null

      switch (returnMode) {
        case 'metadata': {
          const activeMetaData = this.$store.getters['project/ig/implementationGuideMetaData']
          if (
            activeMetaData &&
            activeMetaData.id === response.id &&
            activeMetaData.effectiveDate === response.effectiveDate
          ) {
            this.$store.commit('project/ig/setImplementationGuideMetaData', response)
          }
          this.$store.dispatch('project/ig/updateMetaDataInList', response)
          nameForNotification = response.title
          break
        }
        case 'page': {
          const [page] = response

          const activePage = this.$store.getters['project/ig/page']
          if (
            activePage && activePage.id === page.id) {
            this.$store.commit('project/ig/setPage', page)
          }

          const activePageTree = this.$store.getters['project/ig/implementationGuidePageTree']
          const pageIsInList = this.$filters.listHelper.findItemInList({
            items: activePageTree || [],
            children: 'page',
            handler: match => match.id === page.id
          })
          if (pageIsInList) {
            this.$store.dispatch('project/ig/updatePageDataInList', page)
            nameForNotification = page.title
          }

          break
        }
        case 'pages': {
          const activePageTree = this.$store.getters['project/ig/implementationGuidePageTree']

          if (activePageTree && activePageTree[0]?.id === response.id) {
            this.$store.commit('project/ig/setImplementationGuidePageTree', [response])
            nameForNotification = this.$store.getters['project/ig/implementationGuideMetaData']?.title
          }
          break
        }
        case 'resources': {
          const {
            implementationGuideId: reqId,
            implementationGuideEffectiveDate: reqEffectiveDate
          } = getImplementationGuideFromUrl(request.originalUrl)
          const activeItem = this.$store.getters['project/ig/activeItem']
          const activeMetaData = this.$store.getters['project/ig/implementationGuideMetaData']

          if (
            (
              activeItem &&
              activeItem.id === reqId &&
              activeItem.effectiveDate === reqEffectiveDate
            ) ||
            (
              activeMetaData &&
              activeMetaData.id === reqId &&
              activeMetaData.effectiveDate === reqEffectiveDate
            )
          ) {
            // Compare to metaData and activeItem. Assume that if it corresponds with one, the resource list can be updated
            this.$store.commit('project/ig/setImplementationGuideResourceList', response?.resource)
          }
          break
        }
      }

      if ((!silent || notifyRoutes.includes(this.$route.name)) && nameForNotification) {
        const msg = (
          ['metadata', 'pages', 'resources'].includes(returnMode)
            ? 'proxy-ig-edited'
            : 'proxy-ig-page-edited'
        )

        this.$notify(this.$t(msg, {
          name: this.$filters.getMatchingTranslation({
            value: nameForNotification
          })
        }), 'info', {
          long: true
        })
      }
    } catch (error) {
      console.error(error)
    }
  },

  postIG ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }
    const guides = response.implementationGuide
    this.$store.commit('project/ig/setImplementationGuides', guides)

    if (!silent || notifyRoutes.includes(this.$route.name)) {
      this.$notify(this.$t('proxy-ig-added'), 'info')
    }
  }

}
