import { getValueSetFromUrl } from './helpers'
import { cloneDeep } from 'lodash'

// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'value-set',
  'terminology-value-set',
  'terminology-value-set-version',
  'project-index-valuesets',
  'terminology-identifiers-valueset',
  'value-set-details-id-effectiveDate'
]

export default {
  async deleteValueSet ({ silent = false } = {}, data) {
    const { request } = data

    if (request.emitter === this.socketId) {
      // Don't handle events that are caused by own HTTP requests
      return
    }

    const { valueSetId: idFromUrl } = getValueSetFromUrl(request.originalUrl)
    if (!idFromUrl) {
      return
    }
    const valueSetList = this.$store.getters['terminology/getValueSet']
    if (!Array.isArray(valueSetList)) {
      return
    }

    const valueSetToDelete = valueSetList.find(vs => vs.ref === idFromUrl)
    if (!valueSetToDelete) {
      return
    }

    const activeValueSet = this.$store.getters['terminology/valueSetActive']
    if (activeValueSet?.ref === idFromUrl) {
      await this.$alert(this.$t('selected-reference-has-been-deleted'))
      this.$store.commit('terminology/setValueSetActive', null)
      this.$store.commit('terminology/setValueSetItem', null)
      this.$router.push({ name: 'value-set' })
    }

    this.$store.commit('terminology/removeValueSetFromList', valueSetToDelete)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('valueSet')
    )) {
      this.$notify(this.$t('proxy-xx-refremove', {
        name: valueSetToDelete.displayName
      }), 'info', {
        long: true
      })
    }
  },
  patchValueSet ({ silent = false } = {}, data) {
    const { response: valueSet, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const { id, version: effectiveDate } = this.$route.params

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('valueSet')
    )) {
      this.$notify(this.$t('proxy-vs-edited', {
        name: valueSet.displayName
      }), 'info', {
        long: true
      })
    }

    if (valueSet.id !== id || valueSet.effectiveDate !== effectiveDate) {
      this.$store.commit('terminology/patchValueSetListMetaData', {
        updatedValueSet: valueSet,
        isActiveValueSet: false,
        isVersionOfValueSetActive: valueSet.id === id
      })
      return
    }

    this.$store.commit('terminology/patchValueSetListMetaData', { updatedValueSet: valueSet })
    this.$store.commit('terminology/setValueSetItem', valueSet)
  },

  async postValueSet ({ silent = false } = {}, data) {
    const { response: valueSet, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('valueSet')
    )) {
      this.$notify(this.$t('proxy-vs-added'), 'info', {
        long: true
      })
    }

    const newValueSetIsReference = request.originalUrl.includes('&refOnly=true')

    if (newValueSetIsReference) {
      let list
      const oldActiveValueSet = cloneDeep(this.$store.getters['terminology/valueSetActive'])

      try {
        list = await this.$store.dispatch('terminology/getValueSetList', { resolveit: true, sort: 'displayName' })
      } catch (error) {
        const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
        this.$oops(errorMessage)
      }

      // If there was a valueSetActive, restore that active item without fetching the value set again.
      if (oldActiveValueSet) {
        const valueSetInNewList = list?.find(vs => vs.id === oldActiveValueSet.id || vs.ref === oldActiveValueSet.ref)
        if (!valueSetInNewList) {
          return
        }
        this.$store.commit('terminology/preserveValueSetItemOnSelect', true)
        this.$store.commit('terminology/setValueSetActive', valueSetInNewList)
        setTimeout(() => {
          this.$store.commit('terminology/preserveValueSetItemOnSelect', false)
        }, 0)
      }
    } else {
      this.$store.commit('terminology/addValueSetItemToList', valueSet)
    }
  }
}
