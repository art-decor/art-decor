export default {
  postNewsItem (data) {
    const { response: newsItem, request } = data
    if (request.emitter === this.socketId) {
      return
    }
    this.$store.commit('news/postNewsItem', newsItem)
  },

  putNewsItem (data) {
    const { response: newsItem, request } = data
    if (request.emitter === this.socketId) {
      return
    }
    const newsRef = `news-${newsItem.id}`
    const alert = this.$refs?.[newsRef]?.[0]
    if (alert?.isActive === false) {
      alert.toggle()
    }
    this.$store.commit('news/putNewsItem', newsItem)
  }
}
