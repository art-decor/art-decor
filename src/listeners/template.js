import { getTemplateFromUrl } from './helpers'
import { cloneDeep } from 'lodash'

// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'templates',
  'templates-details-id',
  'templates-details-id-effectiveDate',
  'project-index-templates',
  'rule-identifiers-template'
]

export default {
  async deleteTemplate ({ silent = false } = {}, data) {
    const { request } = data

    if (request.emitter === this.socketId) {
      // Don't handle events that are caused by own HTTP requests
      return
    }

    const { templateId: idFromUrl } = getTemplateFromUrl(request.originalUrl)
    if (!idFromUrl) {
      return
    }

    const templateToDelete = this.$filters.listHelper.findItemInList({
      items: this.$store.getters['rule/template/templates'] || [],
      children: 'template',
      handler: match => match.ref === idFromUrl
    })
    if (!templateToDelete) {
      return
    }

    const activeTemplate = this.$store.getters['rule/template/templateActive']
    if (activeTemplate?.ref === idFromUrl) {
      await this.$alert(this.$t('selected-reference-has-been-deleted'))
      this.$store.commit('rule/template/setTemplateActive', null)
      this.$store.commit('rule/template/setTemplateItem', null)
      this.$router.push({ name: 'templates' })
    }

    this.$store.commit('rule/template/removeTemplateFromList', templateToDelete)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['rule/template/hideRealtimeUpdateNotifications']('templates')
    )) {
      this.$notify(this.$t('proxy-xx-refremove', {
        name: templateToDelete.displayName
      }), 'info', {
        long: true
      })
    }
  },
  patchTemplate ({ silent = false } = {}, data) {
    const { response: template, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const { id, effectiveDate } = this.$route.params

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['rule/template/hideRealtimeUpdateNotifications']('templates')
    )) {
      this.$notify(this.$t('proxy-tm-edited', {
        name: template.displayName
      }), 'info', {
        long: true
      })
    }

    if (!(template.id === id && template.effectiveDate === effectiveDate)) {
      this.$store.commit('rule/template/patchTemplateListMetaData', { updatedTemplate: template, isActiveTemplate: false })
      return
    }

    this.$store.commit('rule/template/patchTemplateListMetaData', { updatedTemplate: template })
    this.$store.commit('rule/template/setTemplateItem', template)
  },

  async postTemplate ({ silent = false } = {}, data) {
    const { response: templateItem, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (this.$store.getters['rule/template/templates'] == null) {
      return
    }

    // Check if category exists, if not, create it
    const category = templateItem?.classification?.[0]?.type?.[0]
    this.$store.getters['rule/template/templates'].find(cat => cat.type === category)

    if (!this.$store.getters['rule/template/templates'].find(cat => cat.type === category)) {
      this.$store.dispatch('rule/template/createCategory', category)
    }

    const prefix = this.projectInFocus?.prefix
    const newTemplateIsReference = request.originalUrl.includes('&refOnly=true')

    // References can have multiple versions, that are not included in the response of a POST call
    if (newTemplateIsReference) {
      let list
      const oldActiveTemplate = cloneDeep(this.$store.getters['rule/template/templateActive'])

      try {
        list = await this.$store.dispatch('rule/template/setTemplateList')
      } catch (error) {
        const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
        this.$oops(errorMessage)
      }

      // If there was a templateActive, restore that active item without fetching the template again.
      if (oldActiveTemplate) {
        const category = list?.find(cat => cat.type === oldActiveTemplate.class)
        if (!category) {
          console.error('category not found')
        }

        const templateInNewList = category.template?.find(tm => tm.id === oldActiveTemplate.id || tm.ref === oldActiveTemplate.ref)

        if (!templateInNewList) {
          return
        }
        this.$store.commit('rule/template/preserveTemplateItemOnSelect', true)
        this.$store.commit('rule/template/setTemplateActive', templateInNewList)
        setTimeout(() => {
          this.$store.commit('rule/template/preserveTemplateItemOnSelect', false)
        }, 0)
      }
    } else {
      this.$store.commit('rule/template/addTemplateItemToList', { templateItem, projectPrefix: prefix })
    }

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['rule/template/hideRealtimeUpdateNotifications']('templates')
    )) {
      this.$notify(this.$t('proxy-tm-added'), 'info', {
        long: true
      })
    }
  },

  putTemplate ({ silent = false } = {}, data) {
    const { response: template, request } = data

    if (request.emitter === this.socketId) {
      // Don't handle events that are caused by own HTTP requests
      return
    }

    const { id, effectiveDate } = this.$store.getters['rule/template/templateItem'] || {}
    if (!(template.id === id && template.effectiveDate === effectiveDate)) {
      // Don't update if it doens't concern the current templateItem
      return
    }

    this.$store.commit('rule/template/setTemplateItem', template)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['rule/template/hideRealtimeUpdateNotifications']('templates')
    )) {
      this.$notify(this.$t('proxy-tm-edited', {
        name: template.displayName
      }), 'info', {
        long: true
      })
    }
  }
}
