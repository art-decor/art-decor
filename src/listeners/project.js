export default {
  deleteScheduled ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const currentTimestamp = this.$store.getters['project/scheduledTasks/currentTimestamp']
    const newTimestamp = new Date(response.lastModifiedDate).getTime()
    const updateIsObsolete = newTimestamp <= currentTimestamp

    if (updateIsObsolete) {
      return
    }
    this.$store.dispatch('project/scheduledTasks/setScheduledTasks', response)
  },

  getScheduled ({ silent = false } = {}, data) {
    const currentTimestamp = this.$store.getters['project/scheduledTasks/currentTimestamp']
    const newTimestamp = new Date(data.lastModifiedDate).getTime()
    const updateIsObsolete = newTimestamp <= currentTimestamp
    if (updateIsObsolete) {
      return
    }
    this.$store.dispatch('project/scheduledTasks/setScheduledTasks', data)
  },

  patchProject ({ silent = false } = {}, data) {
    const { response: updatedProject, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (!this.projectInFocus.prefix || updatedProject.prefix !== this.projectInFocus.prefix) {
      return
    }

    this.$store.commit('project/setProjectInFocus', updatedProject)
    if (!silent && this.$route.name === 'project-overview') {
      this.$notify(this.$t('proxy-pr-updated'), 'info', {
        long: true
      })
    }
  },

  async patchChangeProjectArtifactId ({ silent = false } = {}, data) {
    const { originalUrl } = data.request
    const { artefact, oldId, newId, effectiveDate } = originalUrl.match(/\/project\/[^/]*\/(?<artefact>[^/]+)\/(?<oldId>[^/]+)\/(?<effectiveDate>[^/]+)\/\$change-id\/(?<newId>[^/]+)/).groups

    this.$notify(this.$t('proxy-pr-id-changed', {
      oldId,
      effectiveDate: this.$filters.formatDateShort(decodeURIComponent(effectiveDate)),
      newId,
      artefact
    }), 'info', { long: true })

    if (artefact === 'valueSet' && this.$store.getters['terminology/isLoaded']('valueSet')) {
      await this.$store.dispatch('terminology/getValueSetList', { resolveit: true, sort: 'displayName' })

      const valueSetItem = this.$store.state.terminology.valueSetItem
      const currentId = valueSetItem?.id || valueSetItem?.ref
      const currentVersion = valueSetItem?.effectiveDate

      if (currentId === oldId || currentId === newId) {
        await this.$router.push({ name: 'home' }).catch(() => {}) // navigate away for quick reset
        this.$router.push({
          name: 'terminology-value-set-version',
          params: {
            id: currentVersion === decodeURIComponent(effectiveDate) ? newId : currentId,
            version: currentVersion
          }
        }).catch(() => {})
      }
    }

    if (artefact === 'template' && this.$store.getters['rule/template/isLoaded']('templates')) {
      await this.$store.dispatch('rule/template/setTemplateList')

      const templateItem = this.$store.state.rule.template.templateItem
      const currentId = templateItem?.id || templateItem?.ref
      const currentVersion = templateItem?.effectiveDate

      if (currentId === oldId || currentId === newId) {
        await this.$router.push({ name: 'home' }).catch(() => {}) // navigate away for quick reset
        this.$router.push({
          name: 'templates-details-id-effectiveDate',
          params: {
            id: currentVersion === decodeURIComponent(effectiveDate) ? newId : currentId,
            effectiveDate: currentVersion
          }
        }).catch(() => {})
      }
    }

    if (artefact === 'codeSystem' && this.$store.getters['terminology/isLoaded']('codeSystemItems')) {
      await this.$store.dispatch('terminology/getCodeSystemList', { prefix: this.projectInFocus?.prefix })

      const codeSystemItem = this.$store.state.terminology.codeSystemItem
      const currentId = codeSystemItem?.id || codeSystemItem?.ref
      const currentVersion = codeSystemItem?.effectiveDate

      if (currentId === oldId || currentId === newId) {
        await this.$router.push({ name: 'home' }).catch(() => {}) // navigate away for quick reset
        this.$router.push({
          name: 'codesystems-details-id-effectiveDate',
          params: {
            id: currentVersion === decodeURIComponent(effectiveDate) ? newId : currentId,
            effectiveDate: currentVersion
          }
        }).catch(() => {})
      }
    }

    if (artefact === 'conceptMap' && this.$store.getters['terminology/isLoaded']('conceptMapItems')) {
      await this.$store.dispatch('terminology/getConceptMapList', { prefix: this.projectInFocus?.prefix })

      const conceptMapItem = this.$store.state.terminology.conceptMapItem
      const currentId = conceptMapItem?.id || conceptMapItem?.ref
      const currentVersion = conceptMapItem?.effectiveDate

      if (currentId === oldId || currentId === newId) {
        await this.$router.push({ name: 'home' }).catch(() => {}) // navigate away for quick reset
        this.$router.push({
          name: 'concept-maps-details',
          params: {
            conceptMapId: currentVersion === decodeURIComponent(effectiveDate) ? newId : currentId,
            conceptMapVersion: currentVersion
          }
        }).catch(() => {})
      }
    }

    if (artefact === 'questionnaire' && this.$store.getters['questionnaire/isLoaded']('questionnaires')) {
      await this.$store.dispatch('questionnaire/getQuestionnaireList', { prefix: this.projectInFocus?.prefix })

      const questionnaireItem = this.$store.state.questionnaire.questionnaireItem
      const currentId = questionnaireItem?.id || questionnaireItem?.ref
      const currentVersion = questionnaireItem?.effectiveDate

      if (currentId === oldId || currentId === newId) {
        await this.$router.push({ name: 'home' }).catch(() => {}) // navigate away for quick reset
        this.$router.push({
          name: 'questionnaires-details-id-effectiveDate',
          params: {
            id: currentVersion === decodeURIComponent(effectiveDate) ? newId : currentId,
            effectiveDate: currentVersion
          }
        }).catch(() => {})
      }
    }
  },

  // This listener doesn´t recieve the regular request and response as a payload!
  async patchProjectAuthor ({ silent = false } = {}, data) {
    const { forceApiCall } = data

    // Only fetch authorList for project authors OR logged in users when forceApiCall == true
    if (!this.isUserLoggedIn || (!forceApiCall && !this.userIsEditor)) {
      return
    }

    try {
      await this.$store.dispatch('project/getProjectAuthorList', { prefix: this.projectInFocus?.prefix })

      if (!silent) {
        this.$notify(this.$t('proxy-au-updated'), 'info', {
          long: true
        })
      }
    } catch (error) {
      const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
      this.$oops(errorMessage)
    }
  },

  patchPublication ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('project/setProjectPublicationList', response)
  },

  postCheckDecor ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const scheduled = response?.report?.filter(check => check.progress) || null
    if (scheduled) {
      // Prepare data due to backend inconsistency. Temp?
      const preparedScheduled = scheduled.map(item => {
        const id = this.getUuid()
        return {
          ...item,
          uuid: id,
          as: id,
          on: this.currentDate()
        }
      })

      this.$store.commit('project/scheduledTasks/setProjectCheckTasks', preparedScheduled)
    }
  },

  postProject ({ silent = false } = {}, data) {
    const { response: newProject, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('project/addProject', newProject)
    this.$store.commit('server/addProject', newProject)

    if (!silent && this.$route.name === 'home') {
      this.$notify(this.$t('proxy-pr-added'), 'info', {
        long: true
      })
    }
  },

  postPublication ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('project/setProjectPublicationList', response)

    const scheduled = response.version?.filter(item => item.progress != null).map(item => ({
      on: item.date,
      progress: item.progress,
      'progress-percentage': item['progress-percentage']
    }))
    const currentRequests = this.$store.getters['project/scheduledTasks/publicationTasks']
    const publicationTasks = [...currentRequests, ...scheduled]
    this.$store.commit('project/scheduledTasks/setPublicationTasks', publicationTasks)
  },

  postRuntimeCompilation ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const scheduled = response['compile-request'] || []
    if (scheduled.length) {
      this.$store.commit('project/scheduledTasks/setCompileTasks', scheduled)
    }
  },

  postTerminologyReport ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }
    const scheduled = response['terminology-report-request'] || []
    if (scheduled.length) {
      this.$store.commit('project/scheduledTasks/setTerminologyReportTasks', scheduled)
    }
  },

  postGenericReport ({ silent = false } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }
    const scheduled = response['generic-report-request'] || []
    if (scheduled.length) {
      this.$store.commit('project/scheduledTasks/setGenericReportTasks', scheduled)
    }
  }
}
