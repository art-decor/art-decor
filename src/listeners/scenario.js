// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'scenarios',
  'scenarios-details',
  'project-index-transactions'
]

const routesUsingTransactionItem = [
  'terminology-associations',
  'terminology-associations-data-set',
  'terminology-associations-data-set-concept',
  'terminology-associations-transaction',
  'terminology-associations-transaction-concepts'
]

export default {
  patchScenario ({ silent = false } = {}, data) {
    const { response: updatedScenario, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const activeItem = this.$store.getters['scenario/activeItem']
    const scenarioItem = this.$store.getters['scenario/scenarioItem']

    this.$store.commit('scenario/updateScenarioItem', { item: updatedScenario, data: updatedScenario })

    if (activeItem?.id === updatedScenario.id && activeItem?.effectiveDate === updatedScenario.effectiveDate) {
      this.$store.commit('scenario/setActiveItem', updatedScenario)
    }
    if (scenarioItem?.id === updatedScenario.id && scenarioItem?.effectiveDate === updatedScenario.effectiveDate) {
      this.$store.commit('scenario/setScenarioItem', updatedScenario)
    }

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      this.$notify(this.$t('proxy-sc-updated', {
        name: this.$filters.getMatchingTranslation({
          value: updatedScenario.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  postScenario ({ silent = false } = {}, data) {
    const { response: newScenario, request } = data

    if (request.emitter === this.socketId) {
      // Don't handle events that are caused by own HTTP requests
      return
    }
    this.$store.commit('scenario/addNewScenarioItem', { item: newScenario })

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      this.$notify(this.$t('proxy-sc-added', {
        name: this.$filters.getMatchingTranslation({
          value: newScenario.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  postScenarioStatus ({ silent = false } = {}, data) {
    const { response: report, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('scenario/handleRecursiveScenarioStatusUpdate', report)

    const activeItem = this.$store.getters['scenario/activeItem']
    const scenarioItem = this.$store.getters['scenario/scenarioItem']
    const scenarioItems = this.$store.getters['scenario/scenarioItems'] || []

    if (!activeItem) {
      return
    }

    const { id: activeItemId, effectiveDate: activeItemEffectiveDate } = activeItem
    const activeItemAffected = report.success?.find(item => item.id === activeItemId && item.effectiveDate === activeItemEffectiveDate)

    // If the scenario / transaction in scope is not affected, still notify the user, as the scenarioItems list gets updated
    if (!activeItemAffected) {
      const affectedScenario = report.success?.find(item => item.itemcode === 'SC')

      if (affectedScenario) {
        // A scenario was updated recursively
        const scenario = scenarioItems.find(sc => sc.id === affectedScenario.id && sc.effectiveDate === affectedScenario.effectiveDate)

        if (!scenario) {
          return
        }
        if (!silent || (
          notifyRoutes.includes(this.$route.name) &&
          !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
        )) {
          this.$notify(this.$t('proxy-sc-statusrecurse', {
            name: this.$filters.getMatchingTranslation({
              value: scenario.name
            })
          }), 'info', {
            long: true
          })
        }
      } else {
        // A transaction group was updated recursively
        // Notify the user if this transaction group belongs to the scenarioItem in scope.
        const affectedTransactionGroup = report.success?.find(item => item.itemcode === 'TR' && item.type === 'group')
        if (!affectedTransactionGroup) {
          return
        }

        const transactionGroup = this.$filters.listHelper.findItemInList({
          items: scenarioItem?.transaction || [],
          children: 'transaction',
          handler: match => match.id === affectedTransactionGroup.id && match.effectiveDate === affectedTransactionGroup.effectiveDate
        })
        if (!transactionGroup) {
          return
        }

        if (!silent || (
          notifyRoutes.includes(this.$route.name) &&
          !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
        )) {
          this.$notify(this.$t('proxy-tr-statusrecurse', {
            name: this.$filters.getMatchingTranslation({
              value: transactionGroup.name
            })
          }), 'info', {
            long: true
          })
        }
      }
      return
    }

    if (silent && (
      !notifyRoutes.includes(this.$route.name) ||
      this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      return
    }

    if (activeItemAffected.itemcode === 'SC') {
      this.$notify(this.$t('proxy-sc-statusrecurse', {
        name: this.$filters.getMatchingTranslation({
          value: activeItem.name
        })
      }), 'info', {
        long: true
      })
    } else {
      this.$notify(this.$t('proxy-tr-screcurse', {
        name: this.$filters.getMatchingTranslation({
          value: activeItem.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  putScenario ({ silent = false, useTransactionItem = false } = {}, data) {
    const { response: updatedScenario, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (routesUsingTransactionItem.includes(this.$route.name)) {
      useTransactionItem = true
    }

    this.$store.commit('scenario/updateScenarioItem', { item: updatedScenario, data: updatedScenario })

    const activeItem = this.$store.getters['scenario/activeItem']

    const { id: activeItemId, effectiveDate: activeItemEffectiveDate } = activeItem || {}

    if (activeItemId === updatedScenario.id && activeItemEffectiveDate === updatedScenario.effectiveDate) {
      // This is needed to prevent tree collapsing. Can we change treeview behaviour to only compare key instead of stringified object?
      // (Treeview watcher for activeItem)
      this.$store.commit('scenario/setActiveItemKeepReference', updatedScenario)

      this.$store.commit('scenario/setScenarioItem', updatedScenario)
    } else {
      // The activeItem is not the updated scenario, but could still be affected (if it was a child transaction)
      const updatedTransaction = this.$filters.listHelper.findItemInList({
        items: [updatedScenario],
        children: 'transaction',
        handler: match => match.id === activeItemId && match.effectiveDate === activeItemEffectiveDate
      })
      if (updatedTransaction) {
        // This is needed to prevent tree collapsing. Can we change treeview behaviour to only compare key instead of stringified object?
        // (Treeview watcher for activeItem)
        this.$store.commit('scenario/setActiveItemKeepReference', updatedTransaction)

        const action = useTransactionItem
          ? 'setTransactionItem'
          : 'setScenarioItem'
        this.$store.commit(`scenario/${action}`, updatedTransaction)
      }
    }

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['scenario/hideRealtimeUpdateNotifications']('scenarioItems')
    )) {
      this.$notify(this.$t('proxy-sc-updated', {
        name: updatedScenario.name
      }), 'info', {
        long: true
      })
    }
  }
}
