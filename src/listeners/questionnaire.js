const notifyRoutes = [
  'questionnaires',
  'questionnaires-details-id',
  'questionnaires-details-id-effectiveDate'
]

export default {
  patchQuestionnaire ({ silent = false } = {}, data) {
    const { response: updatedQuestionnaire, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    console.log({ updatedQuestionnaire, request })

    this.$store.dispatch('questionnaire/updateQuestionnaireWrapper', { updatedQuestionnaire, patchPath: request.patchPath })
    const questionnaireItem = this.$store.getters['questionnaire/questionnaireItem']

    if (questionnaireItem?.id === updatedQuestionnaire.id && questionnaireItem?.effectiveDate === updatedQuestionnaire.effectiveDate) {
      this.$store.commit('questionnaire/setQuestionnaireItem', updatedQuestionnaire)
    }

    if (!silent || (notifyRoutes.includes(this.$route.name))) {
      this.$notify(this.$t('proxy-qq-edited', {
        name: this.$filters.getMatchingTranslation({ value: updatedQuestionnaire.name })
      }), 'info', {
        long: true
      })
    }
  }
}
