import { getDatasetFromUrl, getConceptFromUrl } from '@/listeners/helpers'
import { uniqBy } from 'lodash'
export default {
  patchConcept ({ silent = false } = {}, data) {
    const { response: updatedItem, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const conceptItems = this.$store.getters['dataset/conceptItems'] || []
    const conceptItem = this.$store.getters['dataset/conceptItem']
    const datasetItem = this.$store.getters['dataset/datasetItem']
    const activeItem = this.$store.getters['dataset/activeItem']

    // Lookup of concept item
    const oldItemInList = this.$filters.listHelper.findItemInList({
      items: conceptItems,
      children: 'concept',
      handler: (match) => match.id === updatedItem.id && match.effectiveDate === updatedItem.effectiveDate
    })
    if (!oldItemInList) {
      return
    }
    // If type of updatedItem is 'group', the concept array is not comming back in the response
    if (updatedItem.type === 'group' && !updatedItem.concept) {
      updatedItem.concept = oldItemInList.concept
    }

    // update conceptItems.conceptItem
    this.$store.commit('dataset/updateConceptItem', { item: oldItemInList, data: updatedItem })

    if (!silent) {
      this.$notify(this.$t('proxy-de-edited', {
        name: this.$filters.getMatchingTranslation({
          value: updatedItem.name
        })
      }), 'info', {
        long: true
      })
    }

    // Update datasetItem.concept
    const newDatasetItem = { ...datasetItem, concept: conceptItems }
    this.$store.commit('dataset/setDatasetItem', newDatasetItem)

    // Update conceptItem
    if (conceptItem && conceptItem.id === updatedItem.id && conceptItem.effectiveDate === updatedItem.effectiveDate) {
      this.$store.commit('dataset/setConceptItem', updatedItem)
    }

    // Update activeItem
    if (activeItem && activeItem.id === updatedItem.id && activeItem.effectiveDate === updatedItem.effectiveDate) {
      const newActiveItem = activeItem
      for (const key in newActiveItem) {
        delete newActiveItem[key]
      }
      for (const key in updatedItem) {
        newActiveItem[key] = updatedItem[key]
      }
      this.$store.commit('dataset/setActiveItem', updatedItem)
    }
  },

  patchConceptMap ({ silent = false, associationType = null } = {}, data) {
    const { response, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    // transactionContext: the context of the client
    // associationMode: the context in which the update is done
    const transactionContext = this.$route.name.includes('terminology-associations-transaction')
    const associationMode = response.mode // 'normal' for dataset context, 'all' for transaction context

    if (!transactionContext && associationMode === 'all') {
      return
    }

    const associationsList = (
      associationType === 'identifier'
        ? response.identifierAssociation
        : response.terminologyAssociation
    ) || []
    const activeItem = transactionContext
      ? this.$store.getters['scenario/activeItem']
      : this.$store.getters['dataset/activeItem']
    const conceptItem = this.$store.getters['dataset/conceptItem']

    const { conceptId } = getConceptFromUrl(request.originalUrl)

    if (!activeItem || activeItem.id !== conceptId) {
      return
    }

    // Default settings
    let mutation = 'addAssociationToConcept'
    let assocFilter = item => item.conceptId === activeItem.id
    let uniqueProp = 'code'
    let currentConceptAssociations = conceptItem?.terminologyAssociation

    // Association type specific settings that overwrite (part of) the defaults
    switch (associationType) {
      case 'valueset':
        mutation = 'addValueSetAssociationToConcept'
        assocFilter = item => item.valueSet
        uniqueProp = 'valueSet'
        currentConceptAssociations = conceptItem?.valueDomain?.[0]?.conceptList?.[0]?.terminologyAssociation
        break
      case 'identifier':
        mutation = 'addIdentifierAssociationToConcept'
        uniqueProp = 'ref'
        currentConceptAssociations = conceptItem?.identifierAssociation
        break
      case 'valuesetCode':
        mutation = 'addValueSetCodeAssociationsToConcept'
        currentConceptAssociations = conceptItem?.valueDomain?.[0]?.conceptList?.[0]?.concept
          ?.reduce((prev, curr) => {
            const associations = Array.isArray(curr.terminologyAssociation) ? curr.terminologyAssociation : []
            return [...prev, ...associations]
          }, [])

        assocFilter = () => true
        break
    }

    const newConceptAssociations = associationsList.filter(assocFilter)

    // If the user is in transaction context, but the update is done in dataset context
    if (transactionContext && associationMode === 'normal') {
      // If there is an overwrite already, mark the new associations with 'active: false'
      if (currentConceptAssociations?.some(item => item.active === 'true' && item.transactionId)) {
        const inactiveAssociations = newConceptAssociations.map(item => ({ ...item, active: 'false' }))
        const overwrites = currentConceptAssociations.filter(item => item.active === 'true')
        const mergedAssociations = uniqBy([...inactiveAssociations, ...overwrites], uniqueProp)

        // If an overwrite exists, and the same association is added in the dataset context
        const allowedDuplicate = overwrites.find(overwrite => inactiveAssociations.some(inactive => inactive[uniqueProp] === overwrite[uniqueProp]))
        if (allowedDuplicate) {
          mergedAssociations.push(allowedDuplicate)
        }

        this.$store.commit(`dataset/${mutation}`, mergedAssociations)
      } else {
        // There are no transaction overwrites, mark all new associations with 'active: true'
        const parsedNewAssociations = newConceptAssociations.map(item => ({ ...item, active: 'true' }))
        this.$store.commit(`dataset/${mutation}`, parsedNewAssociations)
      }
    } else {
      // The update is done in the same context as the user is currently in.
      this.$store.commit(`dataset/${mutation}`, newConceptAssociations)
    }

    if (!silent) {
      this.$notify(this.$t('proxy-ca-updated', {
        name: this.$filters.getMatchingTranslation({
          value: activeItem.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  postConcept ({ silent = false } = {}, data) {
    const { response: newConcept, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const datasetItem = this.$store.getters['dataset/datasetItem']
    const { datasetId, datasetEffectiveDate } = getDatasetFromUrl(request.originalUrl)

    if (!datasetId ||
          !datasetEffectiveDate ||
          datasetItem?.id !== datasetId ||
          datasetItem?.effectiveDate !== datasetEffectiveDate
    ) {
      return
    }

    try {
      const originalUrl = new URL(request.originalUrl, 'http://somedomain.com')
      const insertMode = originalUrl.searchParams.get('insertMode')
      const insertRef = originalUrl.searchParams.get('insertRef')

      if (!insertMode || !insertRef) {
        return
      }

      // Give a temp. name to the new concept skeleton
      if (Array.isArray(newConcept.name)) {
        newConcept.name.forEach((item, i, arr) => {
          const newName = {
            language: item.language,
            '#text': this.$t('concept-being-created', item.language)
          }
          arr[i] = newName
        })
      }
      // Add unique key
      newConcept.navkey = this.getUuid()

      // Add concept to conceptItems and datasetItem.concept
      const item = { id: insertRef }
      this.$store.commit('dataset/addNewConceptItem', { item, newConceptItem: newConcept, mode: insertMode })
    } catch (error) {
      console.error(error)
    }
  },

  postConceptDeInherit ({ silent = false } = {}, data) {
    const { response: newConcept, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const conceptItem = this.$store.getters['dataset/conceptItem']

    if (conceptItem?.id === newConcept.id && conceptItem?.effectiveDate === newConcept.effectiveDate) {
      // Save as a reference, so the concept can be reloaded when the dataset is actually updated.
      this.$store.commit('dataset/setListenerData', { type: 'deInherit', value: newConcept })
    }
  },

  postConceptStatus ({ silent = false } = {}, data) {
    const { response: conceptItem, request } = data

    if (request.emitter === this.socketId) {
      return
    }
    this.$store.commit('dataset/handleRecursiveConceptStatusUpdate', conceptItem)

    if (!silent) {
      this.$notify(this.$t('proxy-de-statusrecurse', {
        name: this.$filters.getMatchingTranslation({
          value: conceptItem.name
        })
      }), 'info', {
        long: true
      })
    }
  }
}
