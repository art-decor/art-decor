import { getCodeSystemFromUrl } from './helpers'
import { cloneDeep } from 'lodash'

// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'codesystems',
  'codesystems-details-id',
  'codesystems-details-id-effectiveDate',
  'project-index-codesystems',
  'terminology-identifiers-codesystem'
]

export default {
  async deleteCodeSystem ({ silent = false } = {}, data) {
    const { request } = data

    if (request.emitter === this.socketId) {
      // Don't handle events that are caused by own HTTP requests
      return
    }

    const { codeSystemId: idFromUrl } = getCodeSystemFromUrl(request.originalUrl)
    if (!idFromUrl) {
      return
    }
    const codeSystemList = this.$store.getters['terminology/codeSystemItems']
    if (!Array.isArray(codeSystemList)) {
      return
    }

    const codeSystemToDelete = codeSystemList.find(cs => cs.ref === idFromUrl)
    if (!codeSystemToDelete) {
      return
    }

    const activeCodeSystem = this.$store.getters['terminology/codeSystemActive']
    if (activeCodeSystem?.ref === idFromUrl) {
      await this.$alert(this.$t('selected-reference-has-been-deleted'))
      this.$store.commit('terminology/setCodeSystemActive', null)
      this.$store.commit('terminology/setCodeSystemItem', null)
      this.$router.push({ name: 'codesystems' })
    }

    this.$store.commit('terminology/removeCodeSystemFromList', codeSystemToDelete)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('codeSystemItems')
    )) {
      this.$notify(this.$t('proxy-xx-refremove', {
        name: codeSystemToDelete.displayName
      }), 'info', {
        long: true
      })
    }
  },
  patchCodeSystem ({ silent = false } = {}, data) {
    const { response: codeSystem, request } = data

    if (request.emitter === this.socketId) {
      return
    }
    this.$store.commit('terminology/patchCodeSystemListMetaData', codeSystem)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('codeSystemItems')
    )) {
      this.$notify(this.$t('proxy-cs-edited', {
        name: codeSystem.displayName
      }), 'info', {
        long: true
      })
    }

    const { id: activeId, effectiveDate: activeEffectiveDate } = this.$route.params
    if ((codeSystem.id === activeId || codeSystem.ref === activeId) && codeSystem.effectiveDate === activeEffectiveDate) {
      this.$store.commit('terminology/setCodeSystemItem', codeSystem)
    }
  },

  async postCodeSystem ({ silent = false } = {}, data) {
    const { response: codeSystem, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['terminology/hideRealtimeUpdateNotifications']('codeSystemItems')
    )) {
      this.$notify(this.$t('proxy-cs-added'), 'info', {
        long: true
      })
    }

    const newCodeSystemIsReference = request.originalUrl.includes('&refOnly=true')

    if (newCodeSystemIsReference) {
      let list
      const oldActiveCodeSystem = cloneDeep(this.$store.getters['terminology/codeSystemActive'])

      // Re-fetch the list of code systems
      try {
        list = await this.$store.dispatch('terminology/getCodeSystemList', { prefix: this.projectInFocus?.prefix })
      } catch (error) {
        const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
        this.$oops(errorMessage)
      }
      // If there was a codeSystemActive, restore that active item without fetching the codeSystem again.
      if (oldActiveCodeSystem) {
        const codeSystemInNewList = list?.find(cs => cs.id === oldActiveCodeSystem.id || cs.ref === oldActiveCodeSystem.ref)
        if (!codeSystemInNewList) {
          return
        }
        this.$store.commit('terminology/preserveCodeSystemItemOnSelect', true)
        this.$store.commit('terminology/setCodeSystemActive', codeSystemInNewList)
        setTimeout(() => {
          this.$store.commit('terminology/preserveCodeSystemItemOnSelect', false)
        }, 0)
      }
    } else {
      this.$store.commit('terminology/addCodeSystemItemToList', codeSystem)
    }
  }
}
