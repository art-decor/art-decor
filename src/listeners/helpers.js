const getConceptFromUrl = (url) => {
  // eslint-disable-next-line
const regex = /\/concept\/([\d\.]+)\/([^\/]+)/
  const matches = url.match(regex)
  return {
    conceptId: matches?.[1] || null,
    conceptEffectiveDate: matches?.[2] ? decodeURIComponent(matches?.[2]) : null
  }
}

const getCodeSystemFromUrl = (url) => {
  // eslint-disable-next-line
const valuesetIdRegex = /\/codesystem\/([\d\.]+)/
  const matches = url.match(valuesetIdRegex)
  return {
    codeSystemId: matches?.[1] || null
  }
}

const getDatasetFromUrl = (url) => {
  // eslint-disable-next-line
const datasetIdRegex = /\/dataset\/([\d\.]+)\/([^\/]+)/
  const matches = url.match(datasetIdRegex)
  return {
    datasetId: matches?.[1] || null,
    datasetEffectiveDate: matches?.[2] ? decodeURIComponent(matches?.[2]) : null
  }
}

const getImplementationGuideFromUrl = (url) => {
  // eslint-disable-next-line
const regex = /\/implementationguide\/([\d\.]+)\/([^\?]+)/
  const matches = url.match(regex)
  return {
    implementationGuideId: matches?.[1] || null,
    implementationGuideEffectiveDate: matches?.[2] ? decodeURIComponent(matches?.[2]) : null
  }
}

const getTemplateFromUrl = (url) => {
  // eslint-disable-next-line
const templateIdRegex = /\/template\/([\d\.]+)/
  const matches = url.match(templateIdRegex)
  return {
    templateId: matches?.[1] || null
  }
}

const getValueSetFromUrl = (url) => {
  // eslint-disable-next-line
const valuesetIdRegex = /\/valueset\/([\d\.]+)/
  const matches = url.match(valuesetIdRegex)
  return {
    valueSetId: matches?.[1] || null
  }
}

export {
  getConceptFromUrl,
  getCodeSystemFromUrl,
  getDatasetFromUrl,
  getImplementationGuideFromUrl,
  getTemplateFromUrl,
  getValueSetFromUrl
}
