// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'profiles',
  'profiles-details-id',
  'profiles-details-id-effectiveDate',
  'project-index-templates',
  'rule-identifiers-profile'
]

export default {
  patchProfile ({ silent = false } = {}, data) {
    const { response: profile, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const { id, effectiveDate } = this.$route.params

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['rule/profile/hideRealtimeUpdateNotifications']('profiles')
    )) {
      this.$notify(this.$t('proxy-tm-edited', {
        name: profile.displayName
      }), 'info', {
        long: true
      })
    }

    if (!(profile.id === id && profile.effectiveDate === effectiveDate)) {
      this.$store.commit('rule/profile/patchProfileListMetaData', { updatedProfile: profile, isActiveProfile: false })
      return
    }

    this.$store.commit('rule/profile/patchProfileListMetaData', { updatedProfile: profile })
    this.$store.commit('rule/profile/setProfileItem', profile)
  }
}
