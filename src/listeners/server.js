export default {
  deleteGovernanceGroup ({ silent = false } = {}, data) {
    const { request } = data

    if (request.emitter === this.socketId) {
      return
    }

    // eslint-disable-next-line
    const id = request.originalUrl.match(/\/governancegroup\/([\d\.]+)/)?.[1]
    if (!id) {
      return
    }
    const deletedGroup = this.$store.getters['server/getGovernancegroups']?.find(group => group.id === id)
    if (!deletedGroup) {
      return
    }

    this.$store.commit('server/deleteGovernancegroup', id)

    if (!silent) {
      this.$notify(this.$t('proxy-gg-deleted', {
        name: this.$filters.getMatchingTranslation({
          value: deletedGroup.name
        })
      }), 'info', {
        long: true
      })
    }
  },
  patchGovernanceGroup ({ silent = false } = {}, data) {
    const { response: updatedGovernanceGroup, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const newGovGroupList = this.$filters.listHelper.replaceItemInList({
      items: this.$store.getters['server/getGovernancegroups'],
      item: updatedGovernanceGroup,
      handler: match => match.id === updatedGovernanceGroup.id
    })

    this.$store.commit('server/setGovernancegroups', newGovGroupList)

    const attachedToProject = this.projectInFocus?.group?.some(gg => gg.id === updatedGovernanceGroup.id)

    if (!silent && attachedToProject) {
      this.$notify(this.$t('proxy-gg-updated', {
        name: this.$filters.getMatchingTranslation({
          value: updatedGovernanceGroup.name
        })
      }), 'info', {
        long: true
      })
    }
  },
  postGovernanceGroup ({ silent = false } = {}, data) {
    const { response: newGovernanceGroup, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('server/addGovernancegroup', newGovernanceGroup)

    if (!silent) {
      this.$notify(this.$t('proxy-gg-available', {
        name: this.$filters.getMatchingTranslation({
          value: newGovernanceGroup.name
        })
      }), 'info', {
        long: true
      })
    }
  },
  updateServerFunctions ({ silent = false } = {}, data) {
    this.$store.commit('server/setServerFunctions', data)

    if (!silent || this.$route.name === 'functions-admin-panel') {
      this.$notify(this.$t('proxy-sf-updated'), 'info', {
        long: true
      })
    }
  },
  updateServerInfo ({ silent = false } = {}, data) {
    this.$store.commit('server/setServerInfo', data)

    if (!silent || this.$route.name === 'functions-admin-panel') {
      this.$notify(this.$t('proxy-sv-updated'), 'info', {
        long: true
      })
    }
  }
}
