import { getDatasetFromUrl } from '@/listeners/helpers'

// Always notify on specified route names, even if silence = true.
const notifyRoutes = [
  'dataset',
  'dataset-details',
  'dataset-details-version',
  'project-index-datasets'
]

export default {
  patchDataset ({ silent = false } = {}, data) {
    const { response: updatedItem, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const datasetItems = this.$store.getters['dataset/datasetItems'] || []
    const datasetItem = this.$store.getters['dataset/datasetItem']
    const activeItem = this.$store.getters['dataset/activeItem']

    // Lookup of dataset item
    const currentListItem = datasetItems.find(ds => ds.id === updatedItem.id && ds.effectiveDate === updatedItem.effectiveDate)
    if (!currentListItem) {
      return
    }

    // Prepare patched item.
    if (Array.isArray(updatedItem.name)) {
      // Set selector name.
      updatedItem.name.forEach((name) => {
        name.selectorName = `${name?.['#text'] || ''} ${(updatedItem?.versionLabel != null) ? `(${updatedItem.versionLabel})` : ''}`
      })
    }

    // Update datasetItems.datasetItem
    const datasetItemForList = { ...updatedItem }
    delete datasetItemForList.concept
    datasetItemForList.uuid = currentListItem.uuid
    this.$store.commit('dataset/updateDatasetItem', { item: currentListItem, data: datasetItemForList })

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['dataset/hideRealtimeUpdateNotifications']('datasetItems')
    )
    ) {
      this.$notify(this.$t('proxy-ds-edited', {
        name: this.$filters.getMatchingTranslation({
          value: updatedItem.name,
          selector: 'selectorName'
        })
      }), 'info', {
        long: true
      })
    }

    //  Update datasetItem and conceptItems
    if (datasetItem?.id === updatedItem.id && datasetItem?.effectiveDate === updatedItem.effectiveDate) {
      this.$store.commit('dataset/setDatasetItem', updatedItem)

      const conceptItems = updatedItem?.concept || []
      this.$store.commit('dataset/setConceptItems', conceptItems)
    }

    // Update activeItem
    if (activeItem && activeItem.id === updatedItem.id && activeItem.effectiveDate === updatedItem.effectiveDate) {
      const newActiveItem = activeItem
      for (const key in newActiveItem) {
        delete newActiveItem[key]
      }
      for (const key in updatedItem) {
        newActiveItem[key] = updatedItem[key]
      }
      this.$store.commit('dataset/setActiveItem', updatedItem)
    }
  },

  postDataset ({ silent = false } = {}, data) {
    const { response: dataset, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    // Prepare the dataset for the list
    dataset.uuid = this.getUuid()
    delete dataset.concept

    if (Array.isArray(dataset.name)) {
      // Set selector name.
      dataset.name.forEach((name) => {
        name.selectorName = `${name?.['#text'] || ''} ${(dataset?.versionLabel != null) ? `(${dataset.versionLabel})` : ''}`
      })
    }

    this.$store.commit('dataset/addDatasetItemToList', dataset)

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['dataset/hideRealtimeUpdateNotifications']('datasetItems')
    )) {
      this.$notify(this.$t('proxy-ds-added'), 'info', {
        long: true
      })
    }
  },

  // Upon clearLocks, all concepts with statusCode 'new' will be deleted. Therefore, update datasetItem and conceptItems
  postDatasetClearLocks ({ silent = false } = {}, data) {
    const { response: newDataset, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const datasetItem = this.$store.getters['dataset/datasetItem']

    const { datasetId, datasetEffectiveDate } = getDatasetFromUrl(request.originalUrl)
    if (!datasetId ||
          !datasetEffectiveDate ||
          datasetItem?.id !== datasetId ||
          datasetItem?.effectiveDate !== datasetEffectiveDate
    ) {
      return
    }

    // Update datasetItem and conceptItems
    // datasetItems.datasetItem isn't needed, as it doens't include the concepts
    this.$store.commit('dataset/setDatasetItem', newDataset)
    this.$store.commit('dataset/setConceptItems', newDataset.concept)
  },

  postDatasetStatus ({ silent = false } = {}, data) {
    const { response: report, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('dataset/handleRecursiveDatasetStatusUpdate', report)

    const datasetItems = this.$store.getters['dataset/datasetItems'] || []
    const activeItem = this.$store.getters['dataset/activeItem']

    const { id: activeItemId, effectiveDate: activeItemEffectiveDate } = activeItem

    const activeItemAffected = report.success?.find(item => item.id === activeItemId && item.effectiveDate === activeItemEffectiveDate)

    // If the dataset / concept in scope is not affected, still notify the user, as the dataset list gets updated
    if (!activeItemAffected) {
      const affectedDataset = report.success?.find(item => item.itemcode === 'DS')
      if (!affectedDataset) {
        return
      }
      const dataset = datasetItems.find(ds => ds.id === affectedDataset.id && ds.effectiveDate === affectedDataset.effectiveDate)
      if (!dataset) {
        return
      }
      if (!silent || (
        notifyRoutes.includes(this.$route.name) &&
        !this.$store.getters['dataset/hideRealtimeUpdateNotifications']('datasetItems')
      )) {
        this.$notify(this.$t('proxy-ds-statusrecurse', {
          name: this.$filters.getMatchingTranslation({
            value: dataset.name
          })
        }), 'info', {
          long: true
        })
      }
      return
    }

    if (silent && (
      !notifyRoutes.includes(this.$route.name) ||
      this.$store.getters['dataset/hideRealtimeUpdateNotifications']('datasetItems')
    )) {
      return
    }

    if (activeItemAffected.itemcode === 'DS') {
      this.$notify(this.$t('proxy-ds-statusrecurse', {
        name: this.$filters.getMatchingTranslation({
          value: activeItem.name
        })
      }), 'info', {
        long: true
      })
    } else {
      this.$notify(this.$t('proxy-de-dsrecurse', {
        name: this.$filters.getMatchingTranslation({
          value: activeItem.name
        })
      }), 'info', {
        long: true
      })
    }
  },

  putDataset ({ silent = false } = {}, data) {
    const { response: updatedItem, request } = data

    if (request.emitter === this.socketId) {
      return
    }

    const datasetItems = this.$store.getters['dataset/datasetItems'] || []
    const datasetItem = this.$store.getters['dataset/datasetItem']
    const conceptItem = this.$store.getters['dataset/conceptItem']

    // Lookup of dataset item
    const currentListItem = datasetItems.find(ds => ds.id === updatedItem.id && ds.effectiveDate === updatedItem.effectiveDate)
    if (!currentListItem) {
      return
    }

    // Prepare patched item.
    if (Array.isArray(updatedItem.name)) {
      // Set selector name.
      updatedItem.name.forEach((name) => {
        name.selectorName = `${name?.['#text'] || ''} ${(updatedItem?.versionLabel != null) ? `(${updatedItem.versionLabel})` : ''}`
      })
    }

    // Update datasetItems.datasetItem
    const datasetItemForList = { ...updatedItem }
    delete datasetItemForList.concept
    datasetItemForList.uuid = currentListItem.uuid
    this.$store.commit('dataset/updateDatasetItem', { item: currentListItem, data: datasetItemForList })

    if (!silent || (
      notifyRoutes.includes(this.$route.name) &&
      !this.$store.getters['dataset/hideRealtimeUpdateNotifications']('datasetItems')
    )) {
      this.$notify(this.$t('proxy-ds-edited', {
        name: this.$filters.getMatchingTranslation({
          value: updatedItem.name,
          selector: 'selectorName'
        })
      }), 'info', {
        long: true
      })
    }

    if (datasetItem?.id !== updatedItem.id || datasetItem?.effectiveDate !== updatedItem.effectiveDate) {
      // Stop here, the update doesn't affect the currently selected dataset
      return
    }

    // Update datasetItem
    this.$store.commit('dataset/setDatasetItem', updatedItem)

    // Update conceptItems.
    const newConceptItems = updatedItem?.concept || []
    this.$store.commit('dataset/setConceptItems', newConceptItems)

    // De-inheriting changes a concept through PUT dataset. Reload conceptItem if needed
    const deInherit = this.$store.getters['dataset/listenerData']('deInherit')
    if (deInherit != null && conceptItem?.id === deInherit.id && conceptItem?.effectiveDate === deInherit.effectiveDate) {
      const newActiveItem = this.$filters.listHelper.findItemInList({
        items: newConceptItems,
        children: 'concept',
        handler: match => match.id === conceptItem?.id && match.effectiveDate === conceptItem?.effectiveDate
      })
      this.$store.commit('dataset/setActiveItem', newActiveItem) // setting activeItem implicitly calls loadConceptItem

      this.$store.commit('dataset/setListenerData', { type: 'deInherit', value: null })
    }
  }
}
