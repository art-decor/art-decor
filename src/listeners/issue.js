export default {
  deleteIssueLabel ({ silent = false } = {}, data) {
    const { request } = data
    if (request.emitter === this.socketId) {
      return
    }
    const url = request.originalUrl.split('?')[0]
    const code = url.match(/issue\/label\/(.+)/)?.[1]
    if (!code) {
      return
    }

    const label = this.$store.getters['issues/getIssueLabelByCode'](code)

    if (!label) {
      return
    }

    this.$store.commit('issues/deleteIssueLabel', label)
    if (!silent || this.$route.name === 'labels') {
      this.$notify(this.$t('proxy-lb-deleted', {
        name: label.name
      }), 'info', {
        long: true
      })
    }
  },

  patchIssue ({ silent = false } = {}, data) {
    const { response: updatedIssue, request } = data
    if (request.emitter === this.socketId) {
      return
    }

    const issueObjectName = updatedIssue.object?.[0]?.displayName

    if (['issues', 'issue'].includes(this.$route.name)) {
      this.$store.commit('issues/updateIssueAttribute', updatedIssue)

      if (!silent) {
        this.$notify(this.$t('proxy-is-updated', {
          name: issueObjectName
        }), 'info', {
          long: true
        })
      }

      if (this.$store.getters['issues/issue']?.id === updatedIssue.id) {
        this.$store.commit('issues/setIssue', updatedIssue)
      }
    }
  },

  postIssue ({ silent = false } = {}, data) {
    const { response, request } = data
    if (request.emitter === this.socketId) {
      return
    }
    const issueObjectType = response.object?.[0]?.type
    const issueObjectId = response.object?.[0]?.id
    const issueObjectEffectiveDate = response.object?.[0]?.effectiveDate
    const issueObjectName = response.object?.[0]?.displayName

    if (['issues', 'issue'].includes(this.$route.name)) {
      // Add to issues list
      this.$store.commit('issues/addIssue', response)

      if (!silent) {
        this.$notify(this.$t('proxy-is-added', {
          name: issueObjectName
        }), 'info', {
          long: true
        })
      }
      return
    }

    // Handle issue count on artefacts
    let allowedRoutes = []
    let objectInScope = null
    let mutationString = null
    let mutationPayload
    let notificationTag = null

    switch (issueObjectType) {
      case 'TM':
        allowedRoutes = ['templates', 'templates-details-id', 'templates-details-id-effectiveDate']
        objectInScope = this.$store.getters['rule/template/templateItem']
        mutationString = 'rule/template/incrementTemplateIssueAssociationCount'
        notificationTag = 'proxy-is-tmadded'
        break
      case 'DS':
        allowedRoutes = ['dataset-details']
        objectInScope = this.$store.getters['dataset/datasetItem']
        mutationString = 'dataset/incrementIssueAssociation'
        mutationPayload = 'datasetItem'
        notificationTag = 'proxy-is-dsadded'
        break
      case 'DE':
        allowedRoutes = ['dataset-details-version']
        objectInScope = this.$store.getters['dataset/conceptItem']
        mutationString = 'dataset/incrementIssueAssociation'
        mutationPayload = 'conceptItem'
        notificationTag = 'proxy-is-deadded'
        break
      case 'SC':
        allowedRoutes = ['scenarios-details']
        objectInScope = this.$store.getters['scenario/scenarioItem']
        notificationTag = 'proxy-is-scadded'
        break
      case 'TR':
        allowedRoutes = ['scenarios-details']
        objectInScope = this.$store.getters['scenario/scenarioItem']
        notificationTag = 'proxy-is-tradded'
        break
      case 'VS':
        allowedRoutes = ['terminology-value-set', 'terminology-value-set-version', 'value-set-details-id-effectiveDate']
        objectInScope = this.$store.getters['terminology/valueSetItem']
        mutationString = 'terminology/incrementValueSetIssueAssociationCount'
        notificationTag = 'proxy-is-vsadded'
        break
      case 'CS':
        allowedRoutes = ['codesystems-details-id', 'codesystems-details-id-effectiveDate']
        objectInScope = this.$store.getters['terminology/codeSystemItem']
        mutationString = 'terminology/incrementCodeSystemIssueAssociationCount'
        notificationTag = 'proxy-is-csadded'
        break
    }

    if (objectInScope && allowedRoutes.includes(this.$route.name)) {
      // Check if issue concerns the object in scope
      if (!((objectInScope.id === issueObjectId && objectInScope.effectiveDate === issueObjectEffectiveDate) || objectInScope.ref === issueObjectId)) {
        return
      }

      if (mutationString) {
        this.$store.commit(mutationString, mutationPayload)
      }

      if (!silent) {
        this.$notify(this.$t(notificationTag, {
          name: issueObjectName
        }), 'info', {
          long: true
        })
      }
    }
  },

  postIssueLabel ({ silent = false } = {}, data) {
    const { response: newLabel, request } = data
    if (request.emitter === this.socketId) {
      return
    }

    this.$store.commit('issues/addIssueLabel', newLabel)

    if (!silent || this.$route.name === 'labels') {
      this.$notify(this.$t('proxy-lb-added', {
        name: newLabel.name
      }), 'info', {
        long: true
      })
    }
  },

  putIssueLabel ({ silent = false } = {}, data) {
    const { response: updatedLabel, request } = data
    if (request.emitter === this.socketId) {
      return
    }

    const oldLabel = this.$store.getters['issues/getIssueLabelByCode'](updatedLabel.code)
    if (!oldLabel) {
      return
    }

    this.$store.commit('issues/updateIssueLabel', { initialItem: oldLabel, label: updatedLabel })

    if (!silent || this.$route.name === 'labels') {
      this.$notify(this.$t('proxy-lb-updated', {
        name: updatedLabel.code,
        old: oldLabel.name
      }), 'info', {
        long: true
      })
    }
  }
}
