import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import vuetify from './plugins/vuetify'
import i18n from './plugins/i18n'
import filters from './filters'
import FlagIcon from 'vue-flag-icon'
import {
  formatISO9075,
  format,
  isBefore
} from 'date-fns'
import configPlugin from '@/config'
import { mapGetters } from 'vuex'
import { Buffer } from 'buffer/'

import AdAlertConfirm from '@/components/Utilities/AdAlertConfirm'
import {
  v4 as uuidv4
} from 'uuid'
import VueSocketIO from 'vue-socket.io'
import { io } from 'socket.io-client'

import supportedtx from './assets/supportedthirdpartyterminologypackages.json'

Vue.component('AdAlertConfirm', AdAlertConfirm)

Vue.component('PanelLoading', () => import('@/components/Utilities/PanelLoading'))
Vue.component('AdCardTitle', () => import('@/components/Utilities/AdCardTitle'))
Vue.component('AdButton', () => import('@/components/Utilities/AdButton'))
Vue.component('AdGoTo', () => import('@/components/Utilities/AdGoTo'))
Vue.component('AdExample', () => import('@/components/Utilities/AdExample'))
Vue.component('AdShortId', () => import('@/components/Utilities/AdShortId'))
Vue.component('AdBreadcrumbs', () => import('@/components/Utilities/AdBreadcrumbs'))
Vue.component('AdRefChip', () => import('@/components/Utilities/AdRefChip'))
Vue.component('AdClipboardClip', () => import('@/components/Utilities/AdClipboardClip'))
Vue.component('AdStatusDot', () => import('@/components/Utilities/AdStatusDot'))
Vue.component('AdCardTitle', () => import('@/components/Utilities/AdCardTitle'))
Vue.component('AdInfoOwl', () => import('@/components/Utilities/AdInfoOwl'))
Vue.component('AdDocs', () => import('@/components/Utilities/AdDocs'))
Vue.component('AdPageUnavailable', () => import('@/components/Utilities/AdPageUnavailable'))
Vue.component('AdApiLoadingSpinner', () => import('@/components/Utilities/AdApiLoadingSpinner'))
Vue.component('AdSpeedDial', () => import('@/components/Utilities/AdSpeedDial'))
Vue.component('AdSpeedOption', () => import('@/components/Utilities/AdSpeedOption'))
Vue.component('AdArtefactDetailsCard', () => import('@/components/Utilities/AdArtefactDetailsCard'))
Vue.component('PasswordStrengthMeter', () => import('@/components/Utilities/PasswordStrengthMeter'))
Vue.component('EditableString', () => import('@/components/Utilities/Editable/EditableString/EditableString'))
Vue.component('EditableSwitch', () => import('@/components/Utilities/Editable/EditableSwitch'))
Vue.component('EditableTextArea', () => import('@/components/Utilities/Editable/EditableTextArea'))
Vue.component('EditableOID', () => import('@/components/Utilities/Editable/EditableString/EditableOID'))
Vue.component('EditableCount', () => import('@/components/Utilities/Editable/EditableString/EditableCount'))
Vue.component('EditableDecimal', () => import('@/components/Utilities/Editable/EditableString/EditableDecimal'))
Vue.component('EditableItemStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableItemStatusCode'))
Vue.component('EditableTranslationEditor', () => import('@/components/Utilities/Editable/EditableTranslationEditor'))
Vue.component('EditableValueDomain', () => import('@/components/Utilities/Editable/EditableDropdown/EditableValueDomain'))
Vue.component('EditableDropdown', () => import('@/components/Utilities/Editable/EditableDropdown/EditableDropdown'))
Vue.component('EditableShortDescriptiveName', () => import('@/components/Utilities/Editable/EditableString/EditableShortDescriptiveName'))
Vue.component('EditableEffectiveDate', () => import('@/components/Utilities/Editable/EditableString/EditableEffectiveDate'))

Vue.component('EditableShortFormalName', () => import('@/components/Utilities/Editable/EditableString/EditableShortFormalName'))
Vue.component('EditableURI', () => import('@/components/Utilities/Editable/EditableString/EditableURI'))
Vue.component('EditableIdentifier', () => import('@/components/Utilities/Editable/EditableString/EditableIdentifier'))
Vue.component('EditableProjectPrefix', () => import('@/components/Utilities/Editable/EditableString/EditableProjectPrefix'))
Vue.component('EditableEmail', () => import('@/components/Utilities/Editable/EditableString/EditableEmail'))

Vue.component('EditableCardinality', () => import('@/components/Utilities/Editable/EditableDropdown/EditableCardinality'))
Vue.component('EditableConformance', () => import('@/components/Utilities/Editable/EditableDropdown/EditableConformance'))
Vue.component('EditableFHIRFlags', () => import('@/components/Utilities/Editable/EditableDropdown/EditableFHIRFlags'))
Vue.component('EditableLanguage', () => import('@/components/Utilities/Editable/EditableDropdown/EditableLanguage'))
Vue.component('EditableQuestionnaireItemType', () => import('@/components/Utilities/Editable/EditableDropdown/EditableQuestionnaireItemType'))

Vue.component('EditableColorLabels', () => import('@/components/Utilities/Editable/EditableColorLabels'))
Vue.component('EditableLanguageButton', () => import('@/components/Utilities/Editable/EditableLanguageButton'))

Vue.component('EditableIssueStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableIssueStatusCode'))
Vue.component('EditableReleaseStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableReleaseStatusCode'))
Vue.component('EditableTemplateStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableTemplateStatusCode'))
Vue.component('EditableProfileStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableProfileStatusCode'))
Vue.component('EditableQuestionnaireResponseStatusCode', () => import('@/components/Utilities/Editable/EditableStatusCode/EditableQuestionnaireResponseStatusCode'))

Vue.component('EditableDateTime', () => import('@/components/Utilities/Editable/EditableDateTime/EditableDateTime'))
Vue.component('EditableDate', () => import('@/components/Utilities/Editable/EditableDateTime/EditableDate'))
Vue.component('EditableTime', () => import('@/components/Utilities/Editable/EditableDateTime/EditableTime'))

Vue.component('TextEditor', () => import('@/components/Utilities/TextEditor/TextEditor'))
Vue.component('TextEditorRenderer', () => import('@/components/Utilities/TextEditor/TextEditorRenderer'))

Vue.component('TreeTable', () => import('@/components/Utilities/TreeTable/TreeTable'))

Vue.component('AdVersionHistoryStepper', () => import('@/components/Utilities/AdVersionHistoryStepper'))

Vue.component('EditableInfoIcon', () => import('@/components/Utilities/Editable/Helper/EditableInfoIcon'))

Vue.use(configPlugin)
Vue.use(FlagIcon)

Vue.config.productionTip = false

Vue.config.devtools = process.env.NODE_ENV === 'development'

Vue.config.errorHandlerUNSUEDYET = (msg, vm, info) => {
  // do something with the error here
  console.log(msg)
  console.log(vm)
  console.log(info)
}

// Register filters.
Vue.prototype.$filters = filters
store.$filters = filters

// Socket IO
const SOCKET_URL = process.env.VUE_APP_SOCKET_URL || ''
const SOCKET_PATH = process.env.VUE_APP_SOCKET_PATH || null
Vue.use(new VueSocketIO({
  debug: false,
  connection: io(SOCKET_URL, {
    path: SOCKET_PATH
  })
}))

Vue.mixin({
  // globally functions accessible throughout the APP
  computed: {
    ...mapGetters({
      isUserLoggedIn: ['authentication/isUserLoggedIn'],
      projectLocales: ['project/projectLocales'],
      projectSelected: ['project/getProjectSelected'],
      projectInFocus: ['project/getProjectInFocus'],
      userIsDBA: ['authentication/userIsDBA'],
      userIsProjectAuthor: ['project/userIsProjectAuthor'],
      userIsIssueEditor: ['project/userIsIssueEditor'],
      userIsEditor: ['project/userIsEditor'],
      userIsDecorAdmin: ['project/userIsDecorAdmin'],
      serverFeature: ['server/getServerFunction'],
      socketId: ['app/socketId']
    })
  },
  methods: {
    formatDate (d) {
      return formatISO9075(new Date(d), 'yyyy-MM-dd HH:mm')
    },
    formatDateSeparator (d) {
      return formatISO9075(new Date(d), 'yyyy-MM-dd – HH:mm')
    },
    formatDateShort (d) {
      return formatISO9075(new Date(d), {
        representation: 'date',
        format: 'extended'
      })
    },
    currentDate () {
      return format(new Date(), "yyyy-MM-dd'T'HH:mm:ss")
    },
    currentYear () {
      return format(new Date(), 'yyyy')
    },
    isExpired (d) {
      // return true if date is before current date (=expired), otherwise false
      return isBefore(new Date(d), new Date())
    },
    shortId (id) {
      return String(
        id
      ).replace(/(\S+\.)+?/g, '')
    },
    shortenTimeStamp (ts) {
      return String(
        ts
      ).replace(/[-T:]/g, '')
    },
    getDecorCheckMessageColor (role) {
      if (role === 'fatal') return 'black lighten-2'
      if (role === 'error' || role === 'Error') return 'red lighten-2'
      if (role === 'warning') return 'orange lighten-2'
      if (role === 'info') return 'blue lighten-2'
    },
    getDecorCheckMessageIcon (role) {
      // return proper sysmbols for fatal error warning info
      if (role === 'fatal') return 'mdi-alert'
      if (role === 'error') return 'mdi-alert'
      if (role === 'warning') return 'mdi-alert-circle'
      return 'mdi-information-variant'
    },
    getUuid () {
      return uuidv4()
    }
    /* other method functions in filters, defined in index.js:
       getStatusColor, getArtefactColor, getUrgentNewsColor, getExampleBackgroundColor
    */
  }
})

window.Vue = new Vue({
  // globally accessible data throughout the APP
  data: {
    apptitle: 'ART-DECOR®',
    appversion: process.env.VUE_APP_VERSION,
    appdescription: process.env.VUE_APP_DESCRIPTION,
    buildNumber: process.env.VUE_APP_BUILDNUMBER,
    supportedThirdPartyTerminologyPackages: supportedtx.supportedthirdpartyterminologypackages || null,
    mailtoPrefix: Buffer.from('bWFpbHRvOg==', 'base64'), // btoa("mailto:")
    appinternetmain: 'https://art-decor.org',
    appinternetdocs: 'https://docs.art-decor.org',
    appinternetterminology: 'https://terminology.art-decor.org',
    appplannedmaintenance: 'https://docs.art-decor.org/administration/maintenance/',
    appdatatypedocs: 'https://docs.art-decor.org/documentation/datatypes/',
    supportAddress: Buffer.from('c3VwcG9ydEBhcnQtZGVjb3Iub3Jn', 'base64'), // btoa("support@art-decor.org")
    feedbackAddress: Buffer.from('ZmVlZGJhY2tAYXJ0LWRlY29yLm9yZw==', 'base64'), // btoa("feedback@art-decor.org")
    suggestionsAddress: Buffer.from('c3VnZ2VzdGlvbnNAYXJ0LWRlY29yLm9yZw==', 'base64'), // btoa("suggestions@art-decor.org")
    ADnewsletterURL: 'http://seu2.cleverreach.com/f/250987-247353/',
    langs: ['en-US', 'nl-NL', 'de-DE', 'fr-FR', 'pl-PL', 'it-IT'],
    availableLocales: ['en-US', 'nl-NL', 'de-DE', 'fr-FR', 'pl-PL', 'it-IT'],
    notify: [], // the list of items for the notifier
    confirm: [], // the list of items for the alert/confirm dialog
    expandAllLimit: 100,
    // all the following data types have a documentation (automagically generated) at
    // https://docs.art-decor.org/documentation/datatypes/DTr1_xxx/, listed xxx here only
    dataTypesWithDocumentationAtDocs: [
      'AD', 'AD.CA', 'AD.CA.BASIC', 'AD.DE', 'AD.EPSOS', 'AD.IPS', 'AD.NL',
      'ADXP', 'ANY', 'BIN', 'BL', 'BN', 'BXIT_IVL_PQ', 'CD', 'CD.EPSOS',
      'CD.IPS', 'CD.SDTC', 'CE', 'CE.EPSOS', 'CE.IPS', 'CO', 'CO.EPSOS',
      'CR', 'CS', 'CS.LANG', 'CV', 'CV.EPSOS', 'CV.IPS', 'ED', 'EIVL_TS',
      'EIVL.event', 'EN', 'ENXP', 'GLIST', 'GLIST_PQ', 'GLIST_TS', 'hl7nl_INT',
      'hl7nl_IVL_QTY', 'hl7nl_IVL_TS', 'hl7nl_PIVL_TS', 'hl7nl_PQ', 'hl7nl_QSET_QTY',
      'hl7nl_RTO', 'hl7nl_TS', 'II', 'II.AT.ATU', 'II.AT.BLZ', 'II.AT.DVR',
      'II.AT.KTONR', 'II.EPSOS', 'II.NL.AGB', 'II.NL.BIG', 'II.NL.BSN', 'II.NL.URA',
      'II.NL.UZI', 'INT', 'INT.NONNEG', 'INT.POS', 'IVL_INT', 'IVL_MO', 'IVL_PQ',
      'IVL_REAL', 'IVL_TS', 'IVL_TS.CH.TZ', 'IVL_TS.EPSOS.TZ', 'IVL_TS.EPSOS.TZ.OPT',
      'IVL_TS.IPS.TZ', 'IVXB_INT', 'IVXB_MO', 'IVXB_PQ', 'IVXB_REAL', 'IVXB_TS',
      'list_int', 'MO', 'ON', 'PIVL_TS', 'PN', 'PN.CA', 'PN.NL', 'PQ', 'PQR', 'QTY',
      'REAL', 'REAL.NONNEG', 'REAL.POS', 'RTO', 'RTO_PQ_PQ', 'RTO_QTY_QTY', 'SC', 'SD.TEXT',
      'SLIST', 'SLIST_PQ', 'SLIST_TS', 'ST', 'SXCM_INT', 'SXCM_MO', 'SXCM_PQ', 'SXCM_REAL',
      'SXCM_TS', 'SXPR_TS', 'TEL', 'TEL.AT', 'TEL.CA.EMAIL', 'TEL.CA.PHONE', 'TEL.EPSOS',
      'TEL.IPS', 'TEL.NL.EXTENDED', 'thumbnail', 'TN', 'TS', 'TS.AT.TZ', 'TS.AT.VAR',
      'TS.CH.TZ', 'TS.DATE', 'TS.DATE.FULL', 'TS.DATE.MIN', 'TS.DATETIME.MIN',
      'TS.DATETIMETZ.MIN', 'TS.EPSOS.TZ', 'TS.EPSOS.TZ.OPT', 'TS.IPS.TZ', 'URL', 'URL.NL.EXTENDED'
    ]
  },
  store,
  router,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
