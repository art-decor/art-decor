export const isLoaded = state => key => state.loaded[key]
export const isLoading = state => key => state.loading[key]

export const setLoaded = (state, { key, status } = {}) => {
  state.loaded[key] = !!status
}
export const setLoading = (state, { key, status } = {}) => {
  state.loading[key] = !!status
}
