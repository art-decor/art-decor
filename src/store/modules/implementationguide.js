import { cloneDeep } from 'lodash'
import ImplementationGuideService from '@/services/ImplementationGuideService'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  activeItem: null,

  implementationGuideMetaData: null,
  implementationGuidePageTree: [],
  implementationGuideResourceList: [],

  implementationGuides: [],
  loading: {
    implementationGuideMetaData: false,
    implementationGuidePageTree: false,
    implementationGuideResourceList: false,
    implementationGuides: false,
    page: false
  },
  loaded: {
    implementationGuides: false
  },
  page: null
}

const initialState = cloneDeep(state)

const getters = {
  activeItem: (state) => state.activeItem,

  implementationGuideMetaData: (state) => state.implementationGuideMetaData,
  implementationGuidePageTree: (state) => state.implementationGuidePageTree,
  implementationGuideResourceList: (state) => state.implementationGuideResourceList,
  implementationGuides: (state) => state.implementationGuides,

  implementationGuidesAreEditable (state, getters, rootState, rootGetters) {
    return (
      rootGetters['server/getServerFunction']({
        path: 'decor.project.project-implementation-guide',
        crudAction: 'write'
      }) && rootGetters['project/userIsEditor'] === true
    )
  },
  implementationGuideIsEditable (state, getters, rootState, rootGetters) {
    return (
      getters.implementationGuidesAreEditable &&
      ['new', 'draft'].includes(getters.implementationGuideMetaData?.statusCode)
    )
  },

  isLoading,
  isLoaded,
  page: (state) => state.page
}

const mutations = {
  incrementImplementationGuideIssueAssociationCount (state) {
    if (state.implementationGuideMetaData?.issueAssociation?.[0]?.count) {
      const newCount = parseInt(state.implementationGuideMetaData.issueAssociation[0].count) + 1
      state.implementationGuideMetaData.issueAssociation[0].count = newCount.toString()
    }
  },
  initImplementationGuide (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  setActiveItem (state, activeItem) {
    state.activeItem = activeItem
  },
  setImplementationGuideMetaData (state, data) {
    state.implementationGuideMetaData = data
  },
  setImplementationGuidePageTree (state, tree) {
    state.implementationGuidePageTree = Array.isArray(tree) ? tree : []
  },
  setImplementationGuideResourceList (state, list) {
    state.implementationGuideResourceList = Array.isArray(list) ? list : []
  },

  setImplementationGuides (state, guides) {
    state.implementationGuides = Array.isArray(guides) ? guides : []
  },

  setLoading,

  setLoaded,

  setPage (state, page) {
    state.page = page
  }

}

const actions = {
  getImplementationGuideList ({ rootGetters, commit }, { params = {} } = {}) {
    return new Promise((resolve, reject) => {
      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      commit('setLoaded', { key: 'implementationGuides', status: false })
      commit('setLoading', { key: 'implementationGuides', status: true })
      return ImplementationGuideService.getImplementationGuideList({ params: { project, ...params } })
        .then(res => {
          const guides = res.data.implementationGuide
          commit('setImplementationGuides', guides)
          commit('setLoaded', { key: 'implementationGuides', status: true })

          resolve(guides)
        })
        .catch(error => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'implementationGuides', status: false })
        })
    })
  },

  getImplementationGuideMetaData ({ rootGetters, commit }, { id, effectiveDate, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id and effectiveDate are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      commit('setLoading', { key: 'implementationGuideMetaData', status: true })
      return ImplementationGuideService.getImplementationGuideMetaData({
        id,
        effectiveDate,
        params: { project, ...params },
        abortController
      })
        .then(res => {
          const data = res.data
          commit('setImplementationGuideMetaData', data)

          resolve(data)
        })
        .catch(error => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'implementationGuideMetaData', status: false })
        })
    })
  },

  getImplementationGuidePage ({ rootGetters, commit }, { id, effectiveDate, pageId, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate || !pageId) {
        return reject(new Error('Id, effectiveDate and pageId are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      commit('setLoading', { key: 'page', status: true })
      return ImplementationGuideService.getImplementationGuidePage({
        id,
        effectiveDate,
        pageId,
        params: { project, ...params },
        abortController
      })
        .then(res => {
          const [page] = res.data
          commit('setPage', page)

          resolve(page)
        })
        .catch(error => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'page', status: false })
        })
    })
  },

  getImplementationGuidePageTree ({ rootGetters, commit }, { id, effectiveDate, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id and effectiveDate are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      commit('setLoading', { key: 'implementationGuidePageTree', status: true })
      return ImplementationGuideService.getImplementationGuidePageTree({
        id,
        effectiveDate,
        params: { project, ...params },
        abortController
      })
        .then(res => {
          const data = res.data
          commit('setImplementationGuidePageTree', [data])

          resolve([data])
        })
        .catch(error => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'implementationGuidePageTree', status: false })
        })
    })
  },

  getImplementationGuideResourceList ({ rootGetters, commit }, { id, effectiveDate, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id and effectiveDate are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      commit('setLoading', { key: 'implementationGuideResourceList', status: true })
      return ImplementationGuideService.getImplementationGuideResourceList({
        id,
        effectiveDate,
        params: { project, ...params },
        abortController
      })
        .then(res => {
          const data = res.data?.resource
          commit('setImplementationGuideResourceList', data)

          resolve(data)
        })
        .catch(error => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'implementationGuideResourceList', status: false })
        })
    })
  },

  patchImplementationGuide ({ rootGetters, commit, dispatch }, { id, effectiveDate, data, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id, and effectiveDate are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      return ImplementationGuideService.patchImplementationGuide({
        id,
        effectiveDate,
        data,
        params: { project, ...params },
        broadcast: `PATCH/${project}/ig`,
        abortController
      })
        .then(res => {
          // commit based on the returnmode
          const { data } = res

          switch (params?.returnmode) {
            case 'metadata':
              commit('setImplementationGuideMetaData', data)
              dispatch('updateMetaDataInList', data)
              break
            case 'page': {
              const [page] = data
              commit('setPage', page)
              dispatch('updatePageDataInList', page)
              break
            }

            case 'pages':
              commit('setImplementationGuidePageTree', [data])
              break
            case 'resources':
              commit('setImplementationGuideResourceList', data?.resource)
              break
            // Will not do anything with the full implementation guide (no returnmode specified)
          }

          resolve(data)
        })
        .catch(error => reject(error))
    })
  },

  postImplementationGuide ({ rootGetters, commit, dispatch }, { data, params = {}, abortController } = {}) {
    return new Promise((resolve, reject) => {
      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      return ImplementationGuideService.postImplementationGuide({
        data,
        params: { project, ...params },
        abortController,
        broadcast: `POST/${project}/ig`
      })
        .then(res => {
          const guides = res.data.implementationGuide
          commit('setImplementationGuides', guides)

          resolve(data)
        })
        .catch(error => reject(error))
    })
  },

  postImplementationGuideRefresh ({ rootGetters, commit }, { id, effectiveDate, data, params = {} } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id, and effectiveDate are required'))
      }

      const project = params.project || rootGetters['project/getProjectInFocus']?.prefix
      if (!project) {
        return reject(new Error('Project is required'))
      }

      ImplementationGuideService.postImplementationGuideRefresh({
        id,
        effectiveDate,
        data,
        params: { project, ...params },
        broadcast: `POST/${project}/igResources`
      })
        .then(res => {
          const compileRequests = res.data.implementationGuide?.reduce(
            (acc, curr) => {
              if (Array.isArray(curr['compile-request'])) {
                return [...acc, ...curr['compile-request']]
              }
              return acc
            },
            []
          )
          if (compileRequests) {
            commit('project/scheduledTasks/setCompileTasks', compileRequests, { root: true })
          }
        })
        .catch(err => reject(err))
    })
  },

  updateMetaDataInList ({ getters, commit }, data) {
    const newList = this.$filters.listHelper.replaceItemInList({
      items: getters.implementationGuides || [],
      item: data,
      handler: match => match.id === data.id && match.effectiveDate === data.effectiveDate
    })
    commit('setImplementationGuides', newList)
    if (
      getters.activeItem &&
      getters.activeItem.id === data.id &&
      getters.activeItem.effectiveDate === data.effectiveDate
    ) {
      commit('setActiveItem', data)
    }
  },

  updatePageDataInList ({ getters, commit }, data) {
    const newList = this.$filters.listHelper.replaceItemInList({
      items: getters.implementationGuidePageTree || [],
      children: 'page',
      item: (original) => ({ ...original, ...data }),
      handler: match => match.id === data.id
    })

    commit('setImplementationGuidePageTree', newList)

    if (getters.activeItem && getters.activeItem.id === data.id) {
      commit('setActiveItem', data)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
