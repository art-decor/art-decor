import QuestionnaireService from '@/services/QuestionnaireService'
import { cloneDeep } from 'lodash'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  questionnaires: null,
  questionnaireActive: null,
  questionnaireItem: null,
  loading: {
    questionnaires: false,
    questionnaireItem: false
  },
  loaded: {
    questionnaires: false,
    questionnaireItem: false
  },
  expandedSections: {
    description: getSessionStorage()?.questionnaire?.expandedSections?.description || false,
    metadata: getSessionStorage()?.questionnaire?.expandedSections?.metadata || false,
    relationshipSection: getSessionStorage()?.questionnaire?.expandedSections?.relationshipSection || false
  },
  preserveQuestionnaireItemOnSelect: false
}

const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  isLoading,
  isLoaded,
  questionnaires (state) {
    return state.questionnaires
  },
  questionnaireActive (state) {
    return state.questionnaireActive
  },
  questionnaireItem (state) {
    return state.questionnaireItem
  },
  questionnaireItemIsEditable (state, getters, rootState, rootGetters) {
    return (
      getters.questionnairesAreEditable &&
            getters.questionnaireActive?.statusCode === 'draft'
    )
  },
  questionnairesAreEditable (state, getters, rootState, rootGetters) {
    return (
      rootGetters['server/getServerFunction']({
        path: 'decor.rules.questionnaires',
        crudAction: 'write'
      }) && rootGetters['project/userIsEditor'] === true
    )
  }
}

const actions = {
  // Get Questionnaire or QuestionnaireResponse (when QR = true)
  getQuestionnaire ({ commit, rootGetters }, { id, effectiveDate, params = {}, QR = false } = {}) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effectiveDate-missing'))
      }
      const prefix =
                params.prefix ||
                rootGetters['project/getProjectSelected']?.prefix
      if (!prefix) {
        reject(new Error('no project selected or prefix provided'))
        return
      }
      commit('setLoading', { key: 'questionnaireItem', status: true })
      commit('setLoaded', { key: 'questionnaireItem', status: false })

      const service = QR ? 'getQuestionnaireResponse' : 'getQuestionnaire'
      return QuestionnaireService[service](id, effectiveDate, {
        prefix,
        ...params
      })
        .then((response) => {
          commit('setLoading', { key: 'questionnaireItem', status: false })
          commit('setLoaded', { key: 'questionnaireItem', status: true })

          const questionnaire = response.data
          commit('setQuestionnaireItem', questionnaire)

          resolve(questionnaire)
        })
        .catch((error) => {
          commit('setLoading', { key: 'templates', status: false })
          reject(error)
        })
    })
  },
  getQuestionnaireList ({ commit, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      const prefix =
                params.prefix ||
                rootGetters['project/getProjectSelected']?.prefix
      if (!prefix) {
        reject(new Error('no project selected or prefix provided'))
        return
      }
      commit('setLoading', { key: 'questionnaires', status: true })
      commit('setLoaded', { key: 'questionnaires', status: false })
      return QuestionnaireService.getQuestionnaireList({
        prefix,
        ...params
      })
        .then((response) => {
          commit('setLoading', { key: 'questionnaires', status: false })
          commit('setLoaded', { key: 'questionnaires', status: true })

          const qq = response.data?.questionnaire || []
          const qr = response.data?.questionnaireresponse || []
          qr.forEach(item => {
            item.qrInvalidRelationship = true
          })
          const questionnaires = [...qq, ...qr]
          commit('setQuestionnaireList', questionnaires.length ? questionnaires : null)

          resolve(questionnaires)
        })
        .catch((error) => {
          commit('setLoading', { key: 'templates', status: false })
          reject(error)
        })
    })
  },
  patchQuestionnaire ({ commit, dispatch, rootGetters }, { item, data, params = {}, QR = false, abortController } = {}) {
    return new Promise((resolve, reject) => {
      const prefix =
                params.prefix ||
                rootGetters['project/getProjectSelected']?.prefix
      const { id, effectiveDate } = item
      if (!prefix || !id || !effectiveDate) {
        return reject(new Error('prefix-id-or-effectiveDate-missing'))
      }

      const service = (QR || this.$filters.isQuestionnaireResponse(item)) ? 'patchQuestionnaireResponse' : 'patchQuestionnaire'

      QuestionnaireService[service]({
        id,
        effectiveDate,
        data,
        params: { prefix, ...params },
        broadcast: `PATCH/${prefix}/questionnaire`,
        abortController
      })
        .then(response => {
          const updatedQuestionnaire = response.data
          const patchPath = data?.parameter?.map(patchObj => patchObj.path?.replace('/', '')) || []
          dispatch('updateQuestionnaireWrapper', { updatedQuestionnaire, patchPath })
          commit('setQuestionnaireItem', updatedQuestionnaire)

          resolve(updatedQuestionnaire)
        })
        .catch(error => reject(error))
    })
  },
  updateQuestionnaireWrapper ({ commit, state }, { updatedQuestionnaire, patchPath = '' }) {
    const propsToUpdate = ['statusCode', 'name', 'versionLabel', 'relationship']
    const isActiveItem = state.questionnaireActive?.id === updatedQuestionnaire.id && state.questionnaireActive?.effectiveDate === updatedQuestionnaire.effectiveDate
    const questionnaireToUpdate = this.$filters.listHelper.findItemInList({
      items: state.questionnaires || [],
      children: 'questionnaireresponse',
      handler: match => match.id === updatedQuestionnaire.id && match.effectiveDate === updatedQuestionnaire.effectiveDate
    })

    if (!questionnaireToUpdate) {
      return
    }

    const newWrapper = { ...questionnaireToUpdate }
    propsToUpdate.forEach(prop => {
      if (updatedQuestionnaire[prop] !== undefined) {
        newWrapper[prop] = updatedQuestionnaire[prop]
      } else {
        delete newWrapper[prop]
      }
    })

    if (isActiveItem) {
      newWrapper.uuid = state.questionnaireActive.uuid
    }

    // Update item in questionnaire list
    const newList = this.$filters.listHelper.replaceItemInList({
      items: state.questionnaires || [],
      item: newWrapper,
      children: 'questionnaireresponse',
      handler: match => match.id === newWrapper.id && match.effectiveDate === newWrapper.effectiveDate
    })
    commit('setQuestionnaireList', newList)

    // Move questionnaireResponse in the list if needed
    if (patchPath.includes('relationship')) {
      commit('moveQuestionnaireResponse', newWrapper)
    }

    // Update questionnaireActive
    if (isActiveItem) {
      propsToUpdate.forEach(prop => {
        if (updatedQuestionnaire[prop] !== undefined) {
          commit('patchQuestionnaireActive', { path: prop, value: updatedQuestionnaire[prop] })
        } else {
          delete state.questionnaireActive[prop]
        }
      })
    }
  },
  refreshListAndKeepFocus ({ commit, getters, dispatch }) {
    dispatch('getQuestionnaireList')
      .then(() => {
        const { id, effectiveDate } = getters.questionnaireActive || {}
        if (!id || !effectiveDate) {
          return
        }
        const newQuestionnaireActive = getters.questionnaires?.find(q => q.id === id && q.effectiveDate === effectiveDate)
        if (!newQuestionnaireActive) {
          commit('setQuestionnaireActive', null)
          commit('setQuestionnaireItem', null)
          return
        }
        commit('patchQuestionnaireItem', { path: 'uuid', value: newQuestionnaireActive.uuid })
        commit('setQuestionnaireActive', newQuestionnaireActive)
      })
      .catch(error => {
        const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
        this.$oops(errorMessage)
      })
  }
}

const mutations = {
  setExpandedSection (state, { section, value }) {
    state.expandedSections[section] = !!value
  },
  initQuestionnaires (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  // Moves a QR from the root level (no relation with a QQ) to QQ.questionnaireresponse
  moveQuestionnaireResponse (state, questionnaire) {
    if (!this.$filters.isQuestionnaireResponse(questionnaire)) {
      console.log('Not a valid action')
      return
    }

    const targetId = questionnaire.relationship[0].ref
    const targetEffectiveDate = questionnaire.relationship[0].flexibility
    if (!targetId || !targetEffectiveDate) {
      console.log('no id or effectiveDate found for target questionnaire')
      return
    }
    const targetQuestionnaire = state.questionnaires?.find(q => q.id === targetId && q.effectiveDate === targetEffectiveDate)
    if (!targetQuestionnaire) {
      console.log('questionnaire not found')
      return
    }

    if (!Array.isArray(targetQuestionnaire.questionnaireresponse)) {
      targetQuestionnaire.questionnaireresponse = []
    }

    // Remove the invalid mark
    delete questionnaire.qrInvalidRelationship

    targetQuestionnaire.questionnaireresponse.push(questionnaire)

    // Construct the updated list
    let updatedList = this.$filters.listHelper.removeItemFromList({
      items: state.questionnaires,
      handler: match => match.id === questionnaire.id && match.effectiveDate === questionnaire.effectiveDate
    })

    updatedList = this.$filters.listHelper.replaceItemInList({
      items: updatedList,
      item: targetQuestionnaire,
      handler: match => match.id === targetQuestionnaire.id && match.effectiveDate === targetQuestionnaire.effectiveDate
    })

    state.questionnaires = updatedList

    // Remove the mark from questionnaireActive
    if (state.questionnaireActive?.id === questionnaire.id && state.questionnaireActive?.effectiveDate === questionnaire.effectiveDate) {
      delete state.questionnaireActive.qrInvalidRelationship
    }
  },
  patchQuestionnaireActive (state, { path, value }) {
    if (state.questionnaireActive) {
      state.questionnaireActive[path] = value
    }
  },
  patchQuestionnaireItem (state, { path, value }) {
    if (state.questionnaireItem) {
      state.questionnaireItem[path] = value
    }
  },
  setLoaded,
  setLoading,
  setQuestionnaireActive (state, questionnaire) {
    state.questionnaireActive = questionnaire
  },
  setQuestionnaireItem (state, questionnaire) {
    state.questionnaireItem = questionnaire
  },
  setQuestionnaireList (state, questionnaires) {
    state.questionnaires = questionnaires
  }

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
