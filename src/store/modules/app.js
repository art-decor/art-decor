import { v4 as uuid } from 'uuid'
const state = {
  // anchorTypes are types that should be displayed as a clickable link
  anchorTypes: ['uri', 'linkedin', 'facebook', 'twitter'],
  debugMode: false,
  menu: {
    menuItemsIfProjectSelected: [
      {
        title: 'project',
        name: 'project',
        serverFuncPath: 'decor.project',
        icon: 'mdi-briefcase',
        restrict: [
          'project/getProjectInFocus'
        ],
        children: [
          {
            title: 'overview',
            name: 'project-overview',
            serverFuncPath: 'decor.project.overview'
          },
          {
            title: 'authors',
            name: 'project-authors',
            serverFuncPath: 'decor.project.authors',
            restrict: [
              'project/userIsProjectAuthor',
              'authentication/userIsDBA'
            ]
          },
          {
            title: 'publications',
            name: 'project-publication',
            serverFuncPath: 'decor.project.publications'
          },
          {
            title: 'implementationguide',
            name: 'project-implementation-guide',
            serverFuncPath: 'decor.project.implementation-guides'
          },
          {
            title: 'history',
            name: 'project-history',
            serverFuncPath: 'decor.project.history'
          },
          {
            title: 'identifiers',
            name: 'project-identifier',
            serverFuncPath: 'decor.project.identifiers',
            restrict: [
              'authentication/isUserLoggedIn'
            ]
          },
          {
            title: 'decor-locks',
            name: 'project-decor-locks-panel',
            serverFuncPath: 'decor.project.locks',
            restrict: [
              'project/userIsEditor'
            ]
          },
          {
            title: 'mycommunity',
            name: 'project-community',
            serverFuncPath: 'decor.project.my-community'

          },
          {
            title: 'governance-groups',
            name: 'project-governance',
            serverFuncPath: 'decor.project.governance-groups',
            restrict: [
              'project/userIsProjectAuthor'
            ]
          },
          {
            title: 'ada',
            name: 'project-ada',
            serverFuncPath: 'decor.project.ada'
          },
          {
            title: 'development',
            name: 'project-development',
            serverFuncPath: 'decor.project.development',
            restrict: [
              'project/userIsDecorAdmin'
            ]
          },
          {
            title: 'project-index',
            name: 'project-index-panel',
            serverFuncPath: 'decor.project.project-index'
          }
        ]
      },
      {
        title: 'datasets',
        name: 'datasets',
        serverFuncPath: 'decor.datasets',
        icon: 'mdi-book-open-blank-variant',
        children: [
          {
            title: 'dataset',
            name: 'dataset',
            serverFuncPath: 'decor.datasets.dataset'
          }
        ]
      },
      {
        title: 'scenarios',
        name: 'scenarios',
        serverFuncPath: 'decor.scenarios',
        icon: 'mdi-movie',
        children: [
          {
            title: 'scenarios',
            name: 'scenarios',
            serverFuncPath: 'decor.scenarios.scenarios'
          },
          {
            title: 'actors',
            name: 'actors',
            serverFuncPath: 'decor.scenarios.actors'
          }
        ]
      },
      {
        title: 'terminology',
        name: 'terminology',
        serverFuncPath: 'decor.terminology',
        icon: 'mdi-web',
        children: [
          {
            title: 'valuesets',
            name: 'value-set',
            serverFuncPath: 'decor.terminology.valuesets'
          },
          {
            title: 'codesystems',
            name: 'codesystems',
            serverFuncPath: 'decor.terminology.codesystems'
          },
          {
            title: 'concept-maps',
            name: 'concept-maps',
            serverFuncPath: 'decor.terminology.concept-maps'
          },
          {
            title: 'associations',
            name: 'terminology-associations',
            serverFuncPath: 'decor.terminology.mappings'
          },
          {
            title: 'identifiers',
            name: 'identifiers',
            serverFuncPath: 'decor.terminology.identifiers'
          },
          {
            title: 'browser',
            name: 'browser-project',
            serverFuncPath: 'decor.terminology.browser'
          }
        ]
      },
      {
        title: 'rules',
        name: 'rules',
        serverFuncPath: 'decor.rules',

        icon: 'mdi-check-circle',
        children: [
          {
            title: 'templates',
            name: 'templates',
            serverFuncPath: 'decor.rules.templates'
          },
          {
            title: 'Profiles',
            name: 'profiles',
            serverFuncPath: 'decor.rules.profiles'
          },
          {
            title: 'questionnaires',
            name: 'questionnaires',
            serverFuncPath: 'decor.rules.questionnaires'
          },
          {
            title: 'associations',
            name: 'templateAssociation',
            serverFuncPath: 'decor.rules.associations'

          },
          {
            title: 'identifiers',
            name: 'rule-identifiers',
            serverFuncPath: 'decor.rules.identifiers'
          }
        ]
      },
      {
        title: 'issues',
        name: 'issues',
        serverFuncPath: 'decor.issues',
        icon: 'mdi-flag',
        children: [
          {
            title: 'issues',
            name: 'issues',
            serverFuncPath: 'decor.issues.issues'
          },
          {
            title: 'labels',
            name: 'labels',
            serverFuncPath: 'decor.issues.labels'
          }
        ]
      }
    ],
    menuItemsNoProjectSelected: [
      {
        title: 'terminology',
        name: 'terminology',
        serverFuncPath: 'decor.terminology',
        icon: 'mdi-web',
        children: [
          {
            title: 'browser',
            name: 'browser',
            serverFuncPath: 'decor.terminology.browser'
          }
        ]
      }
    ],
    menuItemsServerAdmin: [
      {
        title: 'server-admin',
        name: 'server-admin',
        icon: 'mdi-server-network',
        restrict: [
          'authentication/userIsDBA'
        ],
        children: [
          {
            title: 'overview',
            name: 'server-admin-panel'
          },
          {
            title: 'projects',
            name: 'project-admin-panel'
          },
          {
            title: 'decor-locks',
            name: 'decor-locks-panel'
          },
          {
            title: 'server-management',
            name: 'functions-admin-panel'
          },
          {
            title: 'users',
            name: 'user-admin-panel'
          },
          {
            title: 'adawib',
            name: 'adawib-admin-panel'
          },
          {
            title: 'News',
            name: 'news-admin-panel'
          },
          {
            title: 'art-settings',
            name: 'art-settings-admin'
          },
          {
            title: 'governance-groups',
            name: 'server-governancegroup-panel'
          },
          {
            title: 'oid-registry',
            name: 'oid-registry-list-panel'
          }
        ]
      }
    ],
    menuItemsDesign: [
      {
        title: 'Design',
        name: 'design',
        serverFuncPath: 'design',
        icon: 'mdi-lead-pencil',
        restrict: [
          'authentication/isUserLoggedIn'
        ],
        children: [
          {
            title: 'Dialogs',
            name: 'dialogs-design-panel'
          },
          {
            title: 'Buttons',
            name: 'buttons-design-panel'
          },
          {
            title: 'Icons',
            name: 'icons-design-panel'
          },
          {
            title: 'Treetable',
            name: 'treetable-design-panel'
          },
          {
            title: 'Forms',
            name: 'form-design-panel'
          },
          {
            title: 'Graphics',
            name: 'graphics-design-panel'
          }
        ]
      }
    ]
  },
  projectLanguage: null,
  socketId: uuid(),
  compactTableMode: false
}

const getters = {
  anchorTypes (state) {
    return state.anchorTypes
  },
  debugMode (state) {
    return state.debugMode
  },
  menuItemsIfProjectSelectedFiltered (state, getters) {
    return state.menu.menuItemsIfProjectSelected
      .filter((item) => getters.showMenuItem({ item }))
      .map((item) => {
        // show/hide the childeren, based on the permissions in restrict
        item = Object.assign({}, item)
        item.children = item.children.filter((child) => getters.showMenuItem({ item: child }))
        return item
      })
  },
  menuItemsNoProjectSelectedFiltered (state, getters) {
    return state.menu.menuItemsNoProjectSelected
      .filter((item) => getters.showMenuItem({ item }))
      .map((item) => {
        // show/hide the childeren, based on the permissions in restrict
        item = Object.assign({}, item)
        item.children = item.children.filter((child) => getters.showMenuItem({ item: child }))
        return item
      })
  },
  menuItemsServerAdminFiltered (state, getters) {
    return state.menu.menuItemsServerAdmin
      .filter((item) => getters.showMenuItem({ item }))
      .map((item) => {
        // show/hide the childeren, based on the permissions in restrict
        item = Object.assign({}, item)
        item.children = item.children.filter((child) => getters.showMenuItem({ item: child }))
        return item
      })
  },
  menuItemsDesignFiltered (state, getters) {
    return state.menu.menuItemsDesign
      .filter((item) => getters.showMenuItem({ item }))
      .map((item) => {
        // show/hide the childeren, based on the permissions in restrict
        item = Object.assign({}, item)
        item.children = item.children.filter((child) => getters.showMenuItem({ item: child }))
        return item
      })
  },
  projectLanguage (state) {
    return state.projectLanguage
  },
  /**
   * Function to check if a menu item is restricted to a specific 'rule' (access)
   * Rule is the name of the getter, for convenience
   */
  showMenuItem (state, getters, rootState, rootGetters) {
    return ({ root, item, child }) => {
      // const path = `${root}${(item?.name) ? `.${item.name}` : ''}${(child?.name) ? `.${child.name}` : ''}`
      const path = item.serverFuncPath || 'decor'
      return (
        rootGetters['server/getServerFunction']({ path, crudAction: 'read' }) &&
        (Array.isArray(item.restrict)
          ? item.restrict.some((access) => rootGetters[access])
          : true)
      )
    //   return (
    //     // check server functions if a feature should be visible or not
    //     rootGetters['server/getServerFunction']({ path, crudAction: 'read' }) &&
    //     // check the restrict property (if any) if the path needs authorisation
    //     (
    //       (
    //         child == null &&
    //         (
    //           !Object.prototype.hasOwnProperty.call(item, 'restrict') ||
    //           item.restrict.some((access) => rootGetters[access])
    //         )
    //       ) ||
    //       (
    //         child != null &&
    //         (
    //           !Object.prototype.hasOwnProperty.call(child, 'restrict') ||
    //           child.restrict.some((access) => rootGetters[access])
    //         )
    //       )
    //     )
    //   )
    }
  },
  socketId (state) {
    return state.socketId
  }
}

const actions = {
  changeDebugMode ({ commit }, status) {
    commit('changeDebugMode', status)
  }
}
const mutations = {
  changeDebugMode (state, status) {
    state.debugMode = status
  },
  setProjectLanguage (state, language) {
    state.projectLanguage = language
  },
  setCompactTableMode (state, value) {
    if (typeof value !== 'boolean') throw new Error('typeof value should be a boolean')
    state.compactTableMode = value
    // localStorage.setItem('compactTableMode', value)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
