import i18n from '../../plugins/i18n'
import { cloneDeep } from 'lodash'
import TemplateService from '@/services/TemplateService'
import ProfileService from '@/services/ProfileService'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  abortController: null,
  datatypes: null,
  preserveProfileItemOnSelect: false,
  profiles: null,
  profileActive: null,
  profileItem: null,
  profileVersionIndex: 0,
  hideRealtimeUpdateNotifications: {
    profiles: false
  },
  loading: {
    profiles: false,
    profileItem: false
  },
  loaded: {
    profiles: false,
    profileItem: false
  },
  expandedSections: {
    historySection: getSessionStorage()?.rule?.profile?.expandedSections?.historySection || false,
    metadata: getSessionStorage()?.rule?.profile?.expandedSections?.metadata || false,
    relationshipSection: getSessionStorage()?.rule?.profile?.expandedSections?.relationshipSection || false,
    usageSection: getSessionStorage()?.rule?.profile?.expandedSections?.usageSection || false
  }
}
const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  abortController: state => state.abortController,
  datatypes: state => state.datatypes,
  hideRealtimeUpdateNotifications: state => type => state.hideRealtimeUpdateNotifications[type],
  isLoading,
  isLoaded,
  preserveProfileItemOnSelect: state => state.preserveProfileItemOnSelect,
  profiles: state => state.profiles,
  profileActive: state => state.profileActive,
  profileItem: state => state.profileItem,
  profileVersionIndex: state => state.profileVersionIndex,

  profileItemIsEditable (state, getters, rootState, rootGetters) {
    return getters.profilesAreEditable &&
      getters.profileItem?.statusCode === 'draft' && (!getters.profileItem?.ident || getters.profileItem?.ident === rootGetters['project/getProjectInFocus']?.prefix)
  },
  profilesAreEditable (state, getters, rootState, rootGetters) {
    return rootGetters['server/getServerFunction']({ path: 'decor.rules.profiles', crudAction: 'write' }) &&
      rootGetters['project/userIsEditor'] === true
  }
}

const actions = {
  getProfiles ({ commit, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) return reject(new Error('no project selected'))

      commit('setLoading', { key: 'profiles', status: true })
      commit('setLoaded', { key: 'profiles', status: false })
      const prefix = rootGetters['project/getProjectSelected'].prefix
      return ProfileService.getProfileList({ prefix, ...params })
        .then((response) => {
          commit('setLoading', { key: 'profiles', status: false })
          commit('setLoaded', { key: 'profiles', status: true })

          const profiles = response.data?.category?.[0]?.template ?? null
          if (profiles) {
            commit('setProfiles', profiles)
          }
          resolve(profiles)
        })
        .catch((error) => {
          commit('setLoading', { key: 'profiles', status: false })
          reject(error)
        })
    })
  },
  getProfile ({ commit, rootGetters }, { id, effectiveDate, prefix, abortController }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effectiveDate-missing'))
      }
      const prefixToPass = prefix === undefined ? (rootGetters['project/getProjectSelected'].prefix) : prefix
      const language = i18n.locale
      commit('setLoading', { key: 'profileItem', status: true })
      commit('setLoaded', { key: 'profileItem', status: false })

      return TemplateService.getTemplate(id, effectiveDate, prefixToPass, language, abortController)
        .then((response) => {
          commit('setLoading', { key: 'profileItem', status: false })
          if (response.data) {
            commit('setLoaded', { key: 'profileItem', status: true })
            commit('setProfileItem', response.data || null)
          }
          resolve()
        })
        .catch((error) => {
          if (!abortController?.signal?.aborted) {
            commit('setLoading', { key: 'profileItem', status: false })
          }
          commit('setProfileItem', null)
          reject(error)
        })
    })
  },
  patchProfile ({ commit, getters, rootGetters }, { item, data, params = {}, metadata = false }) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      const { id, effectiveDate } = item

      if (!prefix || !id || !effectiveDate) {
        return reject(new Error('prefix-id-or-effectiveDate-missing'))
      }
      TemplateService.patchTemplate(id, effectiveDate, data, { prefix, ...params }, { broadcast: `PATCH/${prefix}/profile` })
        .then(response => {
          const updatedProfile = response.data || null
          commit('setProfileItem', updatedProfile)
          if (metadata) {
            commit('patchProfileListMetaData', { updatedProfile })
          }
          resolve(updatedProfile)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const mutations = {
  setExpandedSection (state, { section, value }) {
    state.expandedSections[section] = !!value
  },
  hideRealtimeUpdateNotifications (state, { type, status }) {
    state.hideRealtimeUpdateNotifications[type] = !!status
  },
  incrementProfileIssueAssociationCount (state) {
    if (state.profileItem?.issueAssociation?.[0]?.count) {
      const newCount = parseInt(state.profileItem.issueAssociation[0].count) + 1
      state.profileItem.issueAssociation[0].count = newCount.toString()
    }
  },
  initProfiles (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  /**
   * Updates the metadata (displayName, statusCode, versionLabel) in the profiles list and in profileActive
   */
  patchProfileListMetaData (state, { updatedProfile, isActiveProfile = true }) {
    const { displayName, statusCode, versionLabel } = updatedProfile

    // Find the item in the profiles list
    const versionToUpdate = this.$filters.listHelper.findItemInList({
      items: state.profiles || [],
      handler: t => t.id === updatedProfile.id
    })

    if (!versionToUpdate) {
      return
    }

    const versionIndex = versionToUpdate.version.findIndex(v => v.effectiveDate === updatedProfile.effectiveDate)
    // Update profileActive.version and profile.version in the list
    if (versionIndex >= 0) {
      if (isActiveProfile) {
        state.profileActive.version[versionIndex].statusCode = statusCode
        state.profileActive.version[versionIndex].displayName = displayName
        state.profileActive.version[versionIndex].versionLabel = versionLabel
      }

      versionToUpdate.version[versionIndex] = {
        ...versionToUpdate.version[versionIndex],
        displayName,
        statusCode,
        versionLabel
      }
    }

    // If updated profile is the most recent version, update properties on 'root level'
    if (versionToUpdate.effectiveDate === updatedProfile.effectiveDate) {
      // Replace in the profiles list
      state.profiles = this.$filters.listHelper.replaceItemInList({
        items: state.profiles || [],
        item: {
          ...versionToUpdate,
          displayName,
          statusCode,
          versionLabel
        },
        handler: t => t.id === updatedProfile.id && t.effectiveDate === updatedProfile.effectiveDate
      })

      // Update profileActive props
      if (isActiveProfile) {
        state.profileActive.statusCode = statusCode
        state.profileActive.displayName = displayName
        state.profileActive.versionLabel = versionLabel
      }
    } else {
      // If updated profile is NOT the most recent version, only update properties on 'profile.version' level
      state.profiles = this.$filters.listHelper.replaceItemInList({
        items: state.profiles || [],
        item: versionToUpdate,
        handler: t => t.id === updatedProfile.id
      })
    }
  },
  setAbortController (state, controller) {
    state.abortController = controller
  },
  setLoaded,
  setLoading,
  setProfiles (state, profiles) {
    state.profiles = profiles
  },
  setProfileActive (state, profile) {
    state.profileActive = profile
  },
  setProfileItem (state, profile) {
    state.profileItem = profile
  },
  setProfileVersionIndex (state, index) {
    state.profileVersionIndex = index
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
