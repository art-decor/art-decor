import ProjectService from '@/services/ProjectService'
import i18n from '../../plugins/i18n'
import Vue from 'vue'
import { sortBy, cloneDeep } from 'lodash'
import scheduledTasks from './scheduled-tasks'
import implementationGuide from './implementationguide'
import myCommunity from './mycommunity'

const state = {
  ADA: [],
  authors: [],
  callApi: false, // only used during development (in case backend is not yet ready)
  compilationFilters: [],
  loadingCompilationFilters: false,
  loadingADA: false,
  loadingProject: false,
  loadingPublications: false,
  loadingPublicationParameters: false,
  loadingPublicationNotes: false,
  projectSelected: null,
  projectInFocus: null,
  // temp solution
  projectProperties: [],
  projects: [],
  publications: [],
  publicationParameters: {}
}

const initialState = cloneDeep(state)

const getters = {
  ADA (state) {
    return state.ADA
  },
  authors (state) {
    return state.authors
  },
  compilationFilters (state) {
    return state.compilationFilters
  },
  compilationActiveFilter (state) {
    if (state.compilationFilterId) {
      return state.compilationFilters.find((filter) => filter?.id === state.compilationFilterId)
    } else {
      return null
    }
  },
  currentProjectPrefix (state) {
    return state.projectInFocus?.prefix
  },
  getFhirURL: state => format => {
    return state.projectInFocus?.restURI
      ?.find(service => service.for === 'FHIR' && service.format === format)?.['#text']
      ?.replace('/' + format, '')
      .replace(/\/$/, '') ||
    `${window.location.origin}/fhir/${format}`
  },
  getProjects: state => {
    return state.projects
  },
  getProjectSelected: state => {
    return state.projectSelected
  },
  getProjectInFocus: state => {
    return state.projectInFocus
  },
  projectLocales (state, getters) {
    return getters.getProjectInFocus?.name instanceof Array
      ? (getters.getProjectInFocus?.name || []).map((item) => item.language)
      : [i18n.locale]
  },
  publications (state) {
    return state.publications
  },
  keywords (state) {
    if (state.getProjectInFocus?.property.some((property) => property.name === 'Keyword')) {
      return state.getProjectInFocus?.property.filter((property) => property.name === 'Keyword')
    } else if (state.projectProperties) {
      return sortBy(state.projectProperties, ['#text'])
    } else {
      return []
    }
  },
  projectProperties (state) {
    return state.projectProperties
  },
  projectHasFhirService: state => format => !!state.projectInFocus?.restURI?.find(service => service.for === 'FHIR' && service.format === format),
  userIsProjectAuthor (state, getters, rootState, rootGetters) {
    const pn = getters.getProjectInFocus?.name
    if (pn instanceof Array) {
      const uau = getters.authors?.find(author => author.username === rootGetters['authentication/getUsername']) || false
      const expired = uau?.expirationDate ? Vue.prototype.$filters.isExpired(uau.expirationDate) : false
      return uau && !expired
    } else {
      return false
    }
  },
  userIsIssueEditor (state, getters, rootState, rootGetters) {
    const loggedIn = rootGetters['authentication/isUserLoggedIn']
    const isInRequiredGroups = rootGetters['authentication/isInGroup']('decor') && (rootGetters['authentication/isInGroup']('editor') || rootGetters['authentication/isInGroup']('issues'))
    return getters.getProjectInFocus?.name instanceof Array
      ? loggedIn && getters.userIsProjectAuthor && isInRequiredGroups
      : false
  },
  userIsEditor (state, getters, rootState, rootGetters) {
    const loggedIn = rootGetters['authentication/isUserLoggedIn']
    const isInRequiredGroups = rootGetters['authentication/isInGroup']('decor') && rootGetters['authentication/isInGroup']('editor')
    return getters.getProjectInFocus?.name instanceof Array
      ? loggedIn && getters.userIsProjectAuthor && isInRequiredGroups
      : false
  },
  userIsDecorAdmin (state, getters, rootState, rootGetters) {
    const loggedIn = rootGetters['authentication/isUserLoggedIn']
    const isInRequiredGroups = rootGetters['authentication/isInGroup']('decor') && rootGetters['authentication/isInGroup']('editor') && rootGetters['authentication/isInGroup']('decor-admin')
    return getters.getProjectInFocus?.name instanceof Array
      ? loggedIn && getters.userIsProjectAuthor && isInRequiredGroups
      : false
  },
  publicationParameters (state) {
    return state.publicationParameters
  },
  projectIsLoading (state) {
    return state.loadingProject
  }
}

const actions = {
  init ({ commit }) {
    commit('initProject')
    commit('project/ig/initImplementationGuide', null, { root: true })
    commit('project/scheduledTasks/initScheduledTasks', null, { root: true })
    commit('project/myCommunity/initMyCommunity', null, { root: true })
  },
  setProjectSelected ({ commit }, projectSelected) {
    commit('setProjectSelected', projectSelected)
  },
  getProjectAuthorList ({ commit }, { prefix }) {
    return new Promise((resolve, reject) => {
      if (!prefix) {
        return reject(new Error('prefix-required'))
      }

      ProjectService.getProjectAuthorList(prefix)
        .then(response => {
          const authors = response.data?.author || []
          commit('setAuthors', authors)
          resolve(authors)
        })
        .catch(error => reject(error))
    })
  },
  getProject ({ commit, dispatch }, { prefix, checkadram }) {
    return new Promise((resolve, reject) => {
      if (!prefix) {
        return reject(new Error('prefix-required'))
      }
      checkadram = checkadram === undefined ? true : checkadram
      commit('setLoadingProject', true)
      ProjectService.getProject(prefix, null, null, checkadram)
        .then(response => {
          const project = response?.data
          dispatch('setProjectInFocus', project)
          dispatch('setProjectSelected', project)
          commit('setLoadingProject', false)
          resolve(project)
        })
        .catch(err => {
          commit('setLoadingProject', false)
          reject(err)
        })
    })
  },
  setProjectInFocus ({ commit, getters, rootGetters }, projectInFocus) {
    commit('setProjectInFocus', projectInFocus)
    const userDefaultLanguage = rootGetters['authentication/getUserDefaultLanguage']
    const lang = userDefaultLanguage && getters.projectLocales?.includes(userDefaultLanguage)
      ? userDefaultLanguage
      : projectInFocus.defaultLanguage
    commit('app/setProjectLanguage', lang, { root: true })
  },
  setProjects ({ commit }) {
    return new Promise((resolve, reject) => {
      return ProjectService.getProjectsList()
        .then((response) => {
          if (response?.data?.project) {
            commit('setProjects', response?.data?.project)
          } else {
            commit('setProjects', [])
          }
          resolve('setProjects')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addProject ({ getters, commit }, project) {
    return new Promise((resolve, reject) => {
      let broadcast
      if (project.private !== 'true') {
        broadcast = 'POST/project'
      }
      return ProjectService.addProject(project, { broadcast })
        .then((response) => {
          if (response?.data != null) {
            resolve('addProject')
            commit('addProject', response?.data)
            if (project.private === 'false') {
              commit('server/addProject', response?.data, { root: true })
            }
          } else {
            reject(new Error(this.$t('no-project-added')))
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  patchProject ({ getters, commit }, { data, path, checkadram = true }) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (prefix) {
        return ProjectService.patchProject(getters.getProjectInFocus.prefix, data, checkadram, { broadcast: `PATCH/${prefix}/project` })
          .then((response) => {
            if (Array.isArray(path)) {
              path.forEach(p => {
                const payload = {
                  value: response.data[p],
                  path: p
                }
                commit('patchProject', payload)
              })
            } else {
              const payload = {
                value: response.data[path],
                path
              }
              commit('patchProject', payload)
            }
            resolve(response.data)
          })
          .catch(error => {
            reject(error)
          })
      } else {
        reject(new Error('no-project-selected'))
      }
    })
  },
  postProjectPublication ({ getters, commit }, data) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      return ProjectService.postProjectPublication(prefix, data, { broadcast: `POST/${prefix}/publication` })
        .then((response) => {
          if (response?.data) {
            commit('setProjectPublicationList', response.data)

            resolve(response.data)
          } else {
            resolve()
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  patchProjectPublication ({ getters, commit }, parameter) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      return ProjectService.patchProjectPublication(prefix, parameter, { broadcast: `PATCH/${prefix}/publication` })
        .then((response) => {
          if (response?.data) {
            commit('setProjectPublicationList', response.data)

            resolve(response.data)
          } else {
            resolve()
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  /**
   * Compilation related functions
   */
  setProjectPublicationList ({ getters, commit }) {
    return new Promise((resolve, reject) => {
      if (!getters.getProjectInFocus?.prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      commit('setLoadingPublications', true)
      return ProjectService.getProjectPublicationList(getters.getProjectInFocus.prefix)
        .then((response) => {
          const publicationList = response.data || null
          if (publicationList) {
            commit('setProjectPublicationList', publicationList)
          }
          commit('setLoadingPublications', false)
          resolve(publicationList)
        })
        .catch((error) => reject(error))
    })
  },
  getCompilationFilters ({ getters, commit }) {
    return new Promise((resolve, reject) => {
      if (!getters.getProjectInFocus?.prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      commit('setLoadingCompilationFilters', true)
      return ProjectService.getCompilationFilters(getters.getProjectInFocus.prefix)
        .then((response) => {
          const filters = response?.data?.filters || []
          commit('setCompilationFilters', filters)
          commit('setLoadingCompilationFilters', false)
          resolve(response)
        })
        .catch((error) => reject(error))
    })
  },
  patchProjectFilters ({ getters, commit }, data) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      commit('setLoadingCompilationFilters', true)
      return ProjectService.patchProjectFilters(prefix, data, { broadcast: `PATCH/${prefix}/filters` })
        .then((response) => {
          const filters = response?.data?.filters || []
          commit('setCompilationFilters', filters)
          commit('setLoadingCompilationFilters', false)
          resolve(response)
        })
        .catch((error) => reject(error))
    })
  },
  setProjectPublicationParameters ({ getters, commit }) {
    return new Promise((resolve, reject) => {
      if (!getters.getProjectInFocus?.prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      commit('setLoadingPublicationParameters', true)
      return ProjectService.getProjectPublicationParameters(getters.getProjectInFocus.prefix)
        .then((response) => {
          if (response?.data) {
            commit('setProjectPublicationParameters', response.data)
          }
          commit('setLoadingPublicationParameters', false)
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  patchProjectPublicationParameters ({ getters, commit }, parameter) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      return ProjectService.patchProjectPublicationParameters(prefix, parameter, { broadcast: `PATCH/${prefix}/publicationParameters` })
        .then((response) => {
          if (response?.data) {
            // returns all parameters
            commit('setProjectPublicationParameters', response.data)
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  getProjectPublicationNotes ({ getters, commit }, release) {
    return new Promise((resolve, reject) => {
      if (!getters.getProjectInFocus?.prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      commit('setLoadingPublicationNotes', true)
      return ProjectService.getProjectPublicationNotes(getters.getProjectInFocus.prefix, release)
        .then((response) => {
          if (response?.data) {
            resolve(response.data)
          } else {
            resolve()
          }
          commit('setLoadingPublicationNotes', false)
        })
        .catch((error) => reject(error))
    })
  },
  // ADA
  getADAList ({ getters, commit }) {
    return new Promise((resolve, reject) => {
      const prefix = getters.getProjectInFocus?.prefix
      if (!prefix) {
        reject(new Error('no-project-selected'))
      }
      commit('setLoadingADA', true)
      ProjectService.getADAList(prefix).then(response => {
        const apps = response.data?.app || []
        commit('setADA', apps)
        commit('setLoadingADA', false)
        resolve(apps)
      })
        .catch(error => {
          commit('setLoadingADA', false)
          reject(error)
        })
    })
  }
}

const mutations = {
  setADA (state, apps) {
    state.ADA = apps
  },
  setAuthors (state, authors) {
    state.authors = authors
  },
  setProjects (state, projects) {
    state.projects = projects
  },
  setProjectSelected (state, projectSelected) {
    state.projectSelected = JSON.parse(JSON.stringify(projectSelected))
  },
  setProjectInFocus (state, projectInFocus) {
    state.projectInFocus = JSON.parse(JSON.stringify(projectInFocus))
  },
  addProject (state, project) {
    state.projects.push(project)
  },
  patchProject (state, payload) {
    const { path, value } = payload
    if (state.projectInFocus == null || !state.projectInFocus[path]) {
      if (value != null) {
        Vue.set(state.projectInFocus, path, value)
      } else {
        Vue.delete(state.projectInFocus, path)
      }
    } else {
      if (value != null) {
        state.projectInFocus[path] = value
      } else {
        Vue.delete(state.projectInFocus, path)
      }
    }
  },
  addKeywordToProject (state, property) {
    if (state.projectInFocus.property == null) {
      Vue.set(state.projectInFocus, 'property', [])
    }
    if (!state.projectInFocus.property.some((_property) => _property['#text'] === property['#text'] && _property.name === property.name)) {
      state.projectInFocus.property.push(property)
      // temp solution
      state.projectProperties.push(property)
    }
  },
  setCompilationFilters (state, filter) {
    state.compilationFilters = filter
  },
  setLoadingADA (state, status) {
    state.loadingADA = status
  },
  setLoadingCompilationFilters (state, status) {
    state.loadingCompilationFilters = status
  },
  setProjectPublicationList (state, publications) {
    state.publications = publications
  },
  setLoadingPublications (state, status) {
    state.loadingPublications = status
  },
  setLoadingPublicationParameters (state, status) {
    state.loadingPublicationParameters = status
  },
  setProjectPublicationParameters (state, parameters) {
    state.publicationParameters = parameters
  },
  setLoadingPublicationNotes (state, status) {
    state.loadingPublicationNotes = status
  },
  setLoadingProject (state, status) {
    state.loadingProject = status
  },
  initProject (state) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (key !== 'projects') {
        state[key] = val
      }
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
  modules: {
    scheduledTasks,
    ig: implementationGuide,
    myCommunity
  }
}
