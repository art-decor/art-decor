import ServerService from '@/services/ServerService'
import ServermgmtService from '@/services/ServermgmtService'
import ValuesetService from '@/services/ValuesetService'
import TerminologyService from '@/services/TerminologyService'
import QuestionnaireService from '@/services/QuestionnaireService'

const state = () => ({
  decorStatusMaps: [],
  decorStatusMapsLoading: true,
  decorTypes: {},
  decorTypesLoading: true,
  governancegroups: [],
  governancegroupsLoading: true,
  governancegroup: {},
  governancegroupLoading: true,
  serverFunctions: {},
  serverFunctionsLoading: true,
  serverInfo: {},
  serverSettings: {},
  serverInfoLoading: true,
  serverGroups: {},
  users: [],
  usersLoading: true,
  valuesets: [],
  valuesetLoading: true,
  severeStartupProblems: false,
  serverSchedulers: [],
  conceptmaps: [],
  conceptmapLoading: true,
  questionnaires: [],
  questionnaireLoading: true
})

const getters = {
  AD2_DEEPLINK_PREFIX (state, getters) {
    return getters.serverSettings?.['url-art-decor-deeplinkprefix']?.[0]?.replace(/\/$/, '') ||
      `${window.location.origin}/art-decor`
  },
  DECOR_SERVICES_BASE_URL (state, getters) {
    return getters.serverSettings?.['url-art-decor-services']?.[0]?.replace(/\/$/, '') ||
      `${window.location.origin}/decor/services`
  },
  URL_ART_DECOR_API (state, getters) {
    return getters.serverSettings?.['url-art-decor-api']?.[0]?.replace(/\/$/, '') ||
      `${window.location.origin}/api`
  },
  serverInfo (state) {
    return state.serverInfo
  },
  serverSettings (state) {
    return state.serverSettings
  },
  artpackageversion (state, getters) {
    return getters.getPackageVersion('art', 'ART v')
  },
  apipackageversion (state, getters) {
    return getters.getPackageVersion('api', 'API v')
  },
  decorpackageversion (state, getters) {
    return getters.getPackageVersion('decor/core', 'DECOR core v')
  },
  getPackageVersion (state) {
    return (packname, prefix) => {
      let x = null
      try {
        x = state?.serverInfo?.packages?.[0]?.package.find(
          (x) => x.collection === packname
        ).version
        return x === null ? prefix : prefix + x
      } catch (err) {
        // return err
        return prefix + '?'
      }
    }
  },
  decorStatusMaps (state) {
    return state.decorStatusMaps
  },
  getDecorTypes: state => {
    return state.decorTypes
  },
  getDecorObjectTypes: state => {
    // get a single decor object type, called f.e. from a computed property in a view component
    return (decorObjectType) => {
      return state.decorTypes?.data?.DecorObjectType?.[0]?.enumeration.find((e) => e.value === decorObjectType) ?? null
    }
  },
  fhirObjectFormats: (state, getters) => getters.getDecorTypes?.data?.FhirObjectFormat?.[0]?.enumeration || [],
  getFhirLabel: (state, getters) => value => {
    const obj = getters.fhirObjectFormats.find(format => format.value === value)?.label || value
    return window.Vue.$filters.getMatchingTranslation({ value: obj })
  },
  getRelationshipTypes: state => {
    // get a single decor relationship type, called f.e. from a computed property in a view component
    return (relationshipType) => {
      return state.decorTypes?.data?.RelationshipTypes?.[0]?.enumeration.find((e) => e.value === relationshipType) ?? null
    }
  },
  getTemplateTypes: state => {
    // get a single template type, called f.e. from a computed property in a view component
    return (templateType) => {
      return state.decorTypes?.data?.TemplateTypes?.[0]?.enumeration.find((e) => e.value === templateType) ?? null
    }
  },
  getExampleTypes: state => {
    // get all example types, called f.e. from a computed property in a view component
    return state.decorTypes?.data?.ExampleType?.[0]?.enumeration
  },
  getCodedConceptStatuses: state => {
    return state.decorTypes?.data?.CodedConceptStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getItemStatuses: state => {
    return state.decorTypes?.data?.ItemStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getIssueStatuses: state => {
    return state.decorTypes?.data?.IssueStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getProfileStatuses: state => {
    return state.decorTypes?.data?.TemplateStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getReleaseStatuses: state => {
    return state.decorTypes?.data?.ReleaseStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getTemplateStatuses: state => {
    return state.decorTypes?.data?.TemplateStatusCodeLifeCycle?.[0]?.enumeration ?? []
  },
  getIssueTypes: state => {
    return state.decorTypes?.data?.IssueType?.[0]?.enumeration ?? []
  },
  getIssuePriorities: state => {
    return state.decorTypes?.data?.IssuePriority?.[0]?.enumeration ?? []
  },
  getScenarioActorTypes: state => {
    return state.decorTypes?.data?.ScenarioActorType?.[0]?.enumeration ?? []
  },
  getTransactionTypes: state => {
    return state.decorTypes?.data?.TransactionType?.[0]?.enumeration ?? []
  },
  getActorTypes: state => {
    return state.decorTypes?.data?.ActorType?.[0]?.enumeration ?? []
  },
  getGovernancegroups (state) {
    return state.governancegroups
  },
  getGovernancegroup (state) {
    return state.governancegroup
  },
  getServerFunction (state, getters, rootState, rootGetters) {
    /**
     * Support for four levels deep.
     * Based on db/apps/art-data/server-info.xml
     */
    return ({ path, crudAction }) => {
      if (rootGetters['app/debugMode']) console.log('getServerFunction:', path)
      if (path != null) {
        const pathArr = path.split('.')
        if (pathArr.length === 1) {
          const specifiedValue = state.serverFunctions?.[pathArr?.[0]]?.[0]?.[crudAction]
          if (crudAction && specifiedValue) {
            return String(specifiedValue) === 'true'
          } else {
            return true
          }
        } else if (pathArr.length === 2) {
          const level1 = pathArr[0]
          const level2 = pathArr[1]
          if (crudAction) {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[crudAction] != null) {
              return String(state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[crudAction]) === 'true'
            } else {
              return true
            }
          } else {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0] != null) {
              return state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]
            } else {
              return {
                create: true,
                read: true,
                update: true,
                delete: true
              }
            }
          }
        } else if (pathArr.length === 3) {
          const level1 = pathArr[0]
          const level2 = pathArr[1]
          const level3 = pathArr[2]
          if (crudAction) {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[crudAction] != null) {
              return String(state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[crudAction]) === 'true'
            } else {
              return true
            }
          } else {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0] != null) {
              return state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]
            } else {
              return {
                create: true,
                read: true,
                update: true,
                delete: true
              }
            }
          }
        } else if (pathArr.length === 4) {
          const level1 = pathArr[0]
          const level2 = pathArr[1]
          const level3 = pathArr[2]
          const level4 = pathArr[3]
          if (crudAction) {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[level4]?.[0]?.[crudAction] != null) {
              return String(state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[level4]?.[0]?.[crudAction]) === 'true'
            } else {
              return true
            }
          } else {
            if (state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[level4]?.[0] != null) {
              return state.serverFunctions?.[level1]?.[0]?.[level2]?.[0]?.[level3]?.[0]?.[level4]?.[0]
            } else {
              return {
                create: true,
                read: true,
                update: true,
                delete: true
              }
            }
          }
        } else {
          // level 5 not support
          if (crudAction) {
            return {
              create: false,
              read: false,
              update: false,
              delete: false
            }
          } else {
            return false
          }
        }
      } else {
        return true
      }
    }
  },
  artefactStatsCards (state, getters) {
    // attribute home is to be able to filter out the entries only needed for the homepage
    // users.length will only be filled after setUsers is called (on the Server Admin page)
    return [
      {
        icon: 'mdi-briefcase',
        title: 'projects',
        href: 'project-admin-panel',
        value: getters.serverInfo.statistics[0].projects[0].count,
        color: 'pink lighten-2',
        home: true
      },
      {
        icon: 'mdi-account-multiple',
        title: 'users',
        href: 'user-admin-panel',
        value: getters.users.length,
        color: 'pink lighten-2'
      },
      {
        icon: 'mdi-book-open-blank-variant',
        title: 'datasets',
        href: '',
        value: getters.serverInfo.statistics[0].datasets[0].count,
        color: 'green lighten-2',
        home: true
      },
      {
        icon: 'mdi-book-open-blank-variant',
        title: 'concepts',
        href: '',
        value: getters.serverInfo.statistics[0].datasets[0].concepts,
        color: 'green lighten-2'
      },
      {
        icon: 'mdi-movie',
        title: 'scenarios',
        href: '',
        value: getters.serverInfo.statistics[0].scenarios[0].count,
        color: 'green lighten-2',
        home: true
      },
      {
        icon: 'mdi-swap-horizontal-bold',
        title: 'transactions',
        href: '',
        value: getters.serverInfo.statistics[0].scenarios[0].transactions,
        color: 'green lighten-2'
      },
      {
        icon: 'mdi-web',
        title: 'terminologies',
        href: '',
        value: getters.serverInfo.statistics[0].terminologies[0].count,
        color: 'blue lighten-2',
        home: true
      },
      {
        icon: 'mdi-heart',
        title: 'valuesets',
        href: '',
        value: getters.serverInfo.statistics[0].terminologies[0].valueSets,
        color: 'blue lighten-2'
      },
      {
        icon: 'mdi-format-align-justify',
        title: 'codesystems',
        href: '',
        value: getters.serverInfo.statistics[0].terminologies[0].codeSystems,
        color: 'blue lighten-2'
      },
      {
        icon: 'mdi-numeric',
        title: 'identifiers',
        href: '',
        value: -1,
        color: 'blue lighten-2'
      },
      {
        icon: 'mdi-map-legend',
        title: 'mapping',
        href: '',
        value: -1,
        color: 'blue lighten-2'
      },
      {
        icon: 'mdi-check-circle',
        title: 'Profiles',
        href: '',
        value: getters.serverInfo.statistics[0].rules[0].count,
        color: 'orange lighten-2',
        home: true
      },
      {
        icon: 'mdi-recycle',
        title: 'templates',
        href: '',
        value: getters.serverInfo.statistics[0].rules[0].template,
        color: 'orange lighten-2'
      },
      {
        icon: 'mdi-fire',
        title: 'fhir-profiles',
        href: '',
        value: getters.serverInfo.statistics[0].rules[0].profile,
        color: 'orange lighten-2'
      },
      {
        icon: 'mdi-flag',
        title: 'issues',
        href: '',
        value: getters.serverInfo.statistics[0].issues[0].count.concat(
          ' / ',
          getters.serverInfo.statistics[0].issues[0].open
        ),
        color: 'red lighten-2',
        home: true
      },
      {
        icon: 'mdi-bookshelf',
        title: 'publications',
        href: '',
        value: getters.serverInfo.statistics[0].releases[0].count,
        color: 'grey',
        home: true
      },
      {
        icon: 'mdi-cloud',
        title: 'Repositories',
        href: '',
        value: -9,
        color: 'grey'
      },
      {
        icon: 'mdi-database-check-outline',
        title: 'Cache',
        href: '',
        value: getters.serverInfo.cache[0].repos,
        status:
          (getters.serverInfo.cache[0].fresh <= 1400
            ? 'active'
            : getters.serverInfo.cache[0].fresh <= 2800 ? 'pending' : 'retired'),
        color: 'grey'
      },
      {
        icon: 'mdi-graph',
        title: 'oid',
        href: '',
        value: -1,
        color: 'blue lighten-2'
      },
      {
        icon: 'mdi-timetable',
        title: 'Scheduled Tasks',
        href: '',
        value: -1,
        color: 'blue lighten-2'
      }
    ]
  },
  installedTerminologyPackages (state) {
    const packages = state.serverInfo?.packages?.[0]?.package || []
    const installedTerminologyPackages = packages.filter(p => p.collection.includes('terminology-data/codesystem-stable-data/'))
    return installedTerminologyPackages
  },
  installedFhirEndpoints (state) {
    const packages = state.serverInfo?.packages?.[0]?.package || []
    const installedFhirPackages = packages.filter(p => p.collection.includes('fhir/'))
    return installedFhirPackages.map(p => ({ ...p, format: p.collection.replace('fhir/', '') }))
  },
  users (state) {
    return state.users
  },
  valuesets (state) {
    return state.valuesets
  },
  getServerSchedulers (state) {
    return state.serverSchedulers
  },
  conceptmaps (state) {
    return state.conceptmaps
  },
  questionnaires (state) {
    return state.questionnaires
  }
}

const actions = {
  setServerInfo ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServerService.getServerInfo()
        .then((response) => {
          if (response && response?.data) {
            commit('setServerInfo', response.data)
          } else {
            reject(new Error('Could not get serverInfo'))
          }
          commit('setServerInfoLoading', false)
          resolve('setServerInfo')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setServerSettings ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServerService.getServerSettings()
        .then((response) => {
          if (response && response?.data) {
            commit('setServerSettings', response.data)
          } else {
            reject(new Error('Could not get server settings'))
          }
          resolve('setServerSettings')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setUsers ({ commit }, payload) {
    const params = payload?.params || null
    return new Promise((resolve, reject) => {
      return ServerService.getUsersList(params)
        .then((response) => {
          if (response && response?.data?.user) {
            commit('setUsers', response.data.user)
          } else {
            reject(new Error('Could not get users'))
          }
          commit('setUsersLoading', false)
          resolve('setUsers')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setDecorStatusMaps ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServerService.getDecorStatusMaps()
        .then((response) => {
          if (response) {
            commit('setDecorStatusMaps', response.data)
          } else {
            reject(new Error('Could not get decorStatusMaps'))
          }
          commit('setDecorStatusMapsLoading', false)
          resolve('setDecorStatusMaps')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setDecorTypes ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServerService.getDecorTypes()
        .then((response) => {
          if (response) {
            // Kai: do we realy need the complete response, or just data in response?
            // why config, headers, request, status and statusText?
            commit('setDecorTypes', response)
          } else {
            reject(new Error('Could not get decorTypes'))
          }
          commit('setDecorTypesLoading', false)
          resolve('setDecorTypes')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setGovernancegroups ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServerService.getGovernancegroupList()
        .then((response) => {
          if (response?.data?.group) {
            commit('setGovernancegroups', response.data.group)
          } else {
            reject(new Error('Could not get Governance Groups'))
          }
          commit('setGovernancegroupsLoading', false)
          resolve('setGovernancegroups')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setGovernancegroup ({ commit }, { id, details = false }) {
    commit('setGovernancegroupLoading', true)
    return new Promise((resolve, reject) => {
      return ServerService.getGovernancegroup(id, details)
        .then((response) => {
          if (response?.data) {
            commit('setGovernancegroup', response.data)
          } else {
            reject(new Error('governancegroup-not-found'))
          }
          commit('setGovernancegroupLoading', false)
          resolve('setGovernancegroup')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setServerFunctions ({ commit }) {
    /**
     * Retrieves CRUD permissions for server functionality
     */
    commit('setServerFunctionsLoading', true)
    return new Promise((resolve, reject) => {
      return ServermgmtService.getServerFunctions()
        .then((response) => {
          if (response?.data) {
            commit('setServerFunctions', response.data)
          } else {
            reject(new Error('server-functions-not-found'))
          }
          commit('setServerFunctionsLoading', false)
          resolve('setServerFunctions')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setValueset ({ commit }, params = {}) {
    commit('setValuesetLoading', true)
    return new Promise((resolve, reject) => {
      return ValuesetService.getValueset(params)
        .then((response) => {
          if (response?.data?.valueSet) {
            commit('setValueset', response.data.valueSet)
          } else {
            reject(new Error('valueset-not-found'))
          }
          commit('setValuesetLoading', false)
          resolve('setValueset')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  addGovernancegroup ({ commit }, governancegroup) {
    return new Promise((resolve, reject) => {
      return ServerService.addGovernancegroup(governancegroup, { broadcast: 'POST/governancegroup' })
        .then((response) => {
          if (response.data) {
            commit('addGovernancegroup', response.data)
            resolve()
          } else {
            reject(new Error('Could not add Governance Group'))
          }
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  patchGovernancegroup ({ commit }, { id, data, path }) {
    return new Promise((resolve, reject) => {
      if (id != null) {
        return ServerService.patchGovernancegroup(id, data, { broadcast: 'PATCH/governancegroup' })
          .then((response) => {
            const payload = {
              id,
              value: response.data[path],
              path
            }
            commit('patchGovernancegroup', payload)
            resolve()
          })
          .catch(error => {
            reject(error)
          })
      } else {
        reject(new Error(', no project selected'))
      }
    })
  },
  deleteGovernancegroup ({ commit }, { id }) {
    return new Promise((resolve, reject) => {
      if (id != null) {
        return ServerService.deleteGovernancegroup(id, { broadcast: 'DELETE/governancegroup' })
          .then((response) => {
            if (response?.status === 204) {
              commit('deleteGovernancegroup', id)
            }
            resolve()
          })
          .catch(error => {
            reject(error)
          })
      } else {
        reject(new Error(', no project selected'))
      }
    })
  },
  patchServerSettings ({ commit }, { data }) {
    return new Promise((resolve, reject) => {
      return ServerService.updateServerSettings(data)
        .then((response) => {
          const payload = response.data
          commit('patchServerSettings', payload)
          resolve(payload)
        })
        .catch(error => {
          reject(error)
        })
    })
  },
  setServerSchedulers ({ commit }) {
    return new Promise((resolve, reject) => {
      return ServermgmtService.getServerSchedulers()
        .then((response) => {
          if (response && response?.data) {
            commit('setServerSchedulers', response.data)
          } else {
            reject(new Error('Could not get server schedulers'))
          }
          resolve('setServerSchedulers')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setConceptmap ({ commit }, params = {}) {
    commit('setConceptmapLoading', true)
    return new Promise((resolve, reject) => {
      return TerminologyService.getConceptMapList(params)
        .then((response) => {
          if (response?.data?.conceptMap) {
            commit('setConceptmap', response?.data?.conceptMap)
          } else {
            reject(new Error('conceptmap-not-found'))
          }
          commit('setConceptmapLoading', false)
          resolve('setConceptmap')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  setQuestionnaire ({ commit }, params = {}) {
    commit('setQuestionnaireLoading', true)
    return new Promise((resolve, reject) => {
      return QuestionnaireService.getQuestionnaireList(params)
        .then((response) => {
          if (response?.data?.questionnaire) {
            commit('setQuestionnaire', response?.data?.questionnaire)
          } else {
            reject(new Error('questionnaire-not-found'))
          }
          commit('setQuestionnaireLoading', false)
          resolve('setQuestionnaire')
        })
        .catch((error) => {
          reject(error)
        })
    })
  }
}

const mutations = {
  setServerFunctions (state, serverFunctions) {
    state.serverFunctions = serverFunctions
  },
  setServerFunctionsLoading (state, status) {
    state.serverFunctionsLoading = status
  },
  setServerInfo (state, serverInfo) {
    state.serverInfo = serverInfo
  },
  setServerSettings (state, serverSettings) {
    state.serverSettings = serverSettings
  },
  setServerStartupProblems (state, status) {
    state.severeStartupProblems = !!status
  },
  setServerInfoLoading (state, status) {
    state.serverInfoLoading = status
  },
  setUsers (state, users) {
    state.users = users
  },
  setUsersLoading (state, status) {
    state.usersLoading = status
  },
  setDecorStatusMaps (state, decorStatusMaps) {
    state.decorStatusMaps = JSON.parse(JSON.stringify(decorStatusMaps))
  },
  setDecorStatusMapsLoading (state, status) {
    state.decorStatusMapsLoading = status
  },
  setDecorTypes (state, decorTypes) {
    state.decorTypes = JSON.parse(JSON.stringify(decorTypes))
  },
  setDecorTypesLoading (state, status) {
    state.decorTypesLoading = status
  },
  setGovernancegroups (state, governancegroups) {
    state.governancegroups = governancegroups
  },
  setGovernancegroupsLoading (state, status) {
    state.governancegroupsLoading = status
  },
  addGovernancegroup (state, governancegroup) {
    state.governancegroups.push(governancegroup)
  },
  deleteGovernancegroup (state, id) {
    state.governancegroups = state.governancegroups.filter(gg => gg.id !== id)
  },
  addProject (state, newProject) {
    if (state.serverInfo?.projects?.[0]?.project) {
      state.serverInfo.projects[0].project.push(newProject)
    }
  },
  patchGovernancegroup (state, payload) {
    const { id, path, value } = payload
    const index = state.governancegroups.findIndex((g) => g.id === id)
    if (index > -1) {
      state.governancegroups[index][path] = value
    }
  },
  patchServerSettings (state, payload) {
    // const { path, value } = payload
    state.serverSettings = payload
  },
  setGovernancegroup (state, governancegroup) {
    state.governancegroup = governancegroup
  },
  setGovernancegroupLoading (state, status) {
    state.governancegroupLoading = status
  },
  setValueset (state, valuesets) {
    state.valuesets = valuesets
  },
  setValuesetLoading (state, status) {
    state.valuesetLoading = status
  },
  setServerSchedulers (state, status) {
    state.serverSchedulers = status
  },
  setConceptmap (state, conceptmaps) {
    state.conceptmaps = conceptmaps
  },
  setConceptmapLoading (state, status) {
    state.conceptmapLoading = status
  },
  setQuestionnaire (state, questionnaires) {
    state.questionnaires = questionnaires
  },
  setQuestionnaireLoading (state, status) {
    state.questionnaireLoading = status
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
