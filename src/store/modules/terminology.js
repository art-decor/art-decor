import TerminologyService from '@/services/TerminologyService'
import ValuesetService from '@/services/ValuesetService'
import LockService from '@/services/LockService'
import i18n from '../../plugins/i18n'
import Vue from 'vue'
import { cloneDeep } from 'lodash'
import { v4 as getUuid } from 'uuid'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  activeItem: null,
  abortController: null,
  backupCompleteCodeSystem: [], // used for edit ValueSet
  backupCompleteCodeSystemPath: null, // used for edit ValueSet, redirect after update/cancel
  backupConceptList: [], // used for edit ValueSet
  cartWithAssociations: [],
  createValuesetFrom: null,
  editValueSetBtn: false,
  openAddValuesetAssociationModalOnEnter: false,
  primaryCodeSystemId: null,
  primaryConceptCode: null,
  secondaryCodeSystemId: null,
  secondaryConceptCode: null,
  shoppingCart: [],
  showNewValueSetDialog: false,
  showNewConceptMapDialog: false,
  searchCode: '',
  searchContext: '',
  loincSearchContext: 'all',
  searchMode: 'term',
  searchTerm: '',
  startShopping: false,
  valueSet: [], // valueSetList
  valueSetActive: null,
  valueSetItem: null,
  valueSetItemBackup: {},
  preserveValueSetItemOnSelect: false,
  preserveConceptMapItemOnSelect: false,
  preserveCodeSystemItemOnSelect: false,
  valueSetVersionIndex: 0,
  conceptMapItems: null,
  conceptMapItem: null,
  conceptMapActive: null,
  conceptMapVersionIndex: 0,
  codeSystemItems: null,
  codeSystemItem: null,
  codeSystemActive: null,
  codeSystemVersionIndex: 0,
  selectedConcept: null,
  /*
   * pre-set of preferred language and selected languages for the browser
   * can be overriden by server settings
   */
  preferredLanguage: 'en-US',
  selectedLanguages: ['en-US', 'nl-NL', 'de-DE'],
  loading: {
    conceptMapItems: false,
    conceptMapItem: false,
    codeSystemItems: false,
    codeSystemItem: false,
    valueSet: false,
    valueSetItem: false
  },
  loaded: {
    conceptMapItems: false,
    conceptMapItem: false,
    codeSystemItems: false,
    codeSystemItem: false,
    valueSet: false,
    valueSetItem: false
  },
  expandedSections: {
    codeSystem: {
      description: getSessionStorage()?.terminology?.expandedSections?.codeSystem?.description || false,
      historySection: getSessionStorage()?.terminology?.expandedSections?.codeSystem?.historySection || false,
      metadata: getSessionStorage()?.terminology?.expandedSections?.codeSystem?.metadata || false,
      usageSection: getSessionStorage()?.terminology?.expandedSections?.codeSystem?.usageSection || false
    },
    valueSet: {
      description: getSessionStorage()?.terminology?.expandedSections?.valueSet?.description || false,
      historySection: getSessionStorage()?.terminology?.expandedSections?.valueSet?.historySection || false,
      metadata: getSessionStorage()?.terminology?.expandedSections?.valueSet?.metadata || false,
      usageSection: getSessionStorage()?.terminology?.expandedSections?.valueSet?.usageSection || false
    }
  },
  hideRealtimeUpdateNotifications: {
    valueSet: false,
    codeSystemItems: false
  }
}

const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  abortController (state) {
    return state.abortController
  },
  hideRealtimeUpdateNotifications: state => type => state.hideRealtimeUpdateNotifications[type],
  cartWithAssociations (state) {
    return state.cartWithAssociations
  },
  codeSystemsAreEditable (state, getters, rootState, rootGetters) {
    return (
      rootGetters['server/getServerFunction']({
        path: 'decor.terminology.codesystems',
        crudAction: 'write'
      }) && rootGetters['project/userIsEditor'] === true
    )
  },
  codeSystemItemIsEditable (state, getters, rootState, rootGetters) {
    return (
      getters.codeSystemsAreEditable &&
      ['new', 'draft'].includes(getters.codeSystemItem?.statusCode)
    )
  },
  shoppingCart (state) {
    return state.shoppingCart
  },
  shoppingCartSelected (state, getters) {
    return getters.shoppingCart.filter((item) => item?.concept?.selected)
  },
  showNewValueSetDialog (state) {
    return state.showNewValueSetDialog
  },
  getCodeSystemName: (state) => (id) => {
    return (
      state.valueSetItem?.sourceCodeSystem?.find(
        (codeSystem) => codeSystem.id === id
      )?.identifierName ||
      state.codeSystemItems?.find(codeSystem => codeSystem.id === id)?.displayName ||
      id
    )
  },
  getPrimaryCodeSystemId: (state) => {
    return state.primaryCodeSystemId
  },
  getPrimaryConceptCode: (state) => {
    return state.primaryConceptCode
  },
  getPreferredLanguage: (state) => {
    return state.preferredLanguage
  },
  getSelectedLanguages: (state) => {
    return state.selectedLanguages
  },
  getValueSet: (state) => {
    return state.valueSet
  },
  selectedConcept: (state) => {
    return state.selectedConcept
  },
  // Returns a list with all versions of codeSystems in the codeSystemItems list.
  // Optionally includes the most recent 'root level' codeSystem, marked with flexiblility: 'dynamic'
  getFullCodeSystemList:
    (state) =>
      ({ includeDynamic = false, noEffectiveDateAllowed = false, initialList } = {}) => {
        const list = []
        const codeSystems = initialList || state.codeSystemItems || []
        codeSystems.forEach((cs) => {
          if (cs.codeSystem?.length) {
            cs.codeSystem.forEach((version) => {
              if (version.effectiveDate || noEffectiveDateAllowed) {
                list.push(version)
              }
            })
          }
          if (includeDynamic) {
            // Add item, marked for dynamic flexibility
            list.push({ ...cs, flexibility: 'dynamic' })
          }
        })
        return list
      },
  // Returns a list with all versions of conceptMaps in the conceptMap list.
  // Optionally includes the most recent 'root level' conceptMap, marked with flexiblility: 'dynamic'
  getFullConceptMapList:
    (state) =>
      ({ includeDynamic = false, noEffectiveDateAllowed = false, initialList } = {}) => {
        const list = []
        const conceptMaps = initialList || state.conceptMapItems || []
        conceptMaps.forEach((cm) => {
          if (Array.isArray(cm?.conceptMap) && cm.conceptMap.length) {
            cm.conceptMap.forEach((version) => {
              if (version.effectiveDate || noEffectiveDateAllowed) {
                list.push(version)
              }
            })
          }
          if (includeDynamic) {
            // Add item, marked for dynamic flexibility
            list.push({ ...cm, flexibility: 'dynamic' })
          }
        })
        return list
      },
  // Returns a list with all versions of valueSets in the valueSet list.
  // Optionally includes the most recent 'root level' valueSet, marked with flexiblility: 'dynamic'
  getFullValueSetList:
    (state) =>
      ({ includeDynamic = false, noEffectiveDateAllowed = false, initialList } = {}) => {
        const list = []
        const valuesets = initialList || state.valueSet || []
        valuesets.forEach((vs) => {
          if (vs.valueSet?.length) {
            vs.valueSet.forEach((version) => {
              if (version.effectiveDate || noEffectiveDateAllowed) {
                list.push(version)
              }
            })
          }
          if (includeDynamic) {
            // Add item, marked for dynamic flexibility
            list.push({ ...vs, flexibility: 'dynamic' })
          }
        })
        return list
      },
  isLoading,
  isLoaded,
  valueSetActive (state) {
    return state.valueSetActive
  },
  valueSetsAreEditable (state, getters, rootState, rootGetters) {
    return (
      rootGetters['server/getServerFunction']({
        path: 'decor.terminology.valuesets',
        crudAction: 'write'
      }) && rootGetters['project/userIsEditor'] === true
    )
  },
  valueSetItemIsEditable (state, getters, rootState, rootGetters) {
    return (
      getters.valueSetsAreEditable &&
      ['new', 'draft'].includes(getters.valueSetItem?.statusCode)
    )
  },
  itemsInValueSetItem (state) {
    if (state.valueSetItem?.items) {
      return state.valueSetItem?.items
    } else {
      return []
    }
  },
  valueSetItem (state) {
    return state.valueSetItem
  },
  valueSetVersionIndex (state) {
    return state.valueSetVersionIndex
  },
  preserveCodeSystemItemOnSelect (state) {
    return state.preserveCodeSystemItemOnSelect
  },
  preserveConceptMapItemOnSelect (state) {
    return state.preserveConceptMapItemOnSelect
  },
  preserveValueSetItemOnSelect (state) {
    return state.preserveValueSetItemOnSelect
  },
  // CodeSystems
  codeSystemItems (state) {
    return state.codeSystemItems
  },
  codeSystemItem (state) {
    return state.codeSystemItem
  },
  codeSystemActive (state) {
    return state.codeSystemActive
  },
  codeSystemVersionIndex (state) {
    return state.codeSystemVersionIndex
  },
  conceptMapVersionIndex (state) {
    return state.conceptMapVersionIndex
  },
  conceptMapItems (state) {
    return state.conceptMapItems
  },
  conceptMapActive (state) {
    return state.conceptMapActive
  },
  conceptMapItem (state) {
    return state.conceptMapItem
  }
}

const actions = {
  setActiveItem ({ commit }, item) {
    commit('setActiveItem', item)
  },
  setPrimaryCodeSystemId ({ commit }, codeSystemId) {
    commit('setPrimaryCodeSystemId', codeSystemId)
  },
  setPrimaryConceptCode ({ commit }, code) {
    commit('setPrimaryConceptCode', code)
  },
  setPreferredLanguage ({ commit }, language) {
    commit('setPreferredLanguage', language)
  },
  setSelectedLanguages ({ commit }, languages) {
    commit('setSelectedLanguages', languages)
  },
  setSelectedConcept ({ commit }, concept) {
    commit('setSelectedConcept', concept)
  },
  getValueSetList (
    { commit, rootGetters },
    { resolveit = false, sort = 'id', release } = {}
  ) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) {
        reject(new Error('no project selected'))
      }
      const prefix = rootGetters['project/getProjectSelected'].prefix
      const language = i18n.locale
      commit('setLoading', { key: 'valueSet', status: true })
      commit('setLoaded', { key: 'valueSet', status: false })
      return TerminologyService.getValueSetList(
        prefix,
        resolveit,
        sort,
        language,
        release
      )
        .then((response) => {
          const valueSetList = response?.data?.valueSet || []
          commit('setValueSetList', valueSetList)
          commit('setLoading', { key: 'valueSet', status: false })
          commit('setLoaded', { key: 'valueSet', status: true })
          // return the valueSets, to handle matchingValueSet in
          // case of direct url or browser refresh
          resolve(valueSetList)
        })
        .catch((error) => {
          commit('setLoading', { key: 'valueSet', status: false })
          reject(error)
        })
    })
  },
  getValueSet ({ commit, getters, rootGetters }, { id, effectiveDate, prefix, abortController }) {
    return new Promise((resolve, reject) => {
      const prefixToPass =
        prefix === undefined
          ? rootGetters['project/getProjectSelected'].prefix
          : prefix
      const language = i18n.locale
      commit('setLoading', { key: 'valueSetItem', status: true })
      commit('setLoaded', { key: 'valueSetItem', status: false })
      return TerminologyService.getValueSet(
        id,
        effectiveDate,
        prefixToPass,
        language,
        abortController
      )
        .then((response) => {
          const valueSetItem = response?.data || {}
          commit('setValueSetItem', valueSetItem)
          commit('setLoading', { key: 'valueSetItem', status: false })
          commit('setLoaded', { key: 'valueSetItem', status: true })
          resolve(valueSetItem)
        })
        .catch((error) => {
          if (!abortController?.signal?.aborted) {
            commit('setLoading', { key: 'valueSetItem', status: false })
          }
          reject(error)
        })
    })
  },
  getValueSetUsage ({ commit }, { id, effectiveDate, params }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        reject(new Error('must-specify-id-and-effectiveDate'))
      }
      TerminologyService.getValueSetUsage(id, effectiveDate, params)
        .then((response) => {
          const usage = response?.data
          resolve(usage)
        })
        .catch((err) => reject(err))
    })
  },
  postValueSet ({ commit, rootGetters, getters }, { data, params = {} }) {
    return new Promise((resolve, reject) => {
      const projectInFocus = rootGetters['project/getProjectInFocus']
      if (!projectInFocus) {
        reject(new Error('no project selected'))
      }
      const prefix = projectInFocus.prefix
      ValuesetService.postValueSet(data, { prefix, ...params }, { broadcast: `POST/${prefix}/valueset` })
        .then((response) => {
          const newValueSet = response.data
          commit('emptyShoppingCart')
          commit('setStartShopping', false)
          commit('addValueSetItemToList', newValueSet)
          commit('setValueSetItem', newValueSet)
          // Find new item in the list and set it as valueSetActive
          const activeItem = getters.getValueSet.find(
            (vs) => vs.id === newValueSet.id
          )
          if (activeItem) {
            commit('setValueSetActive', activeItem)
          }
          // Set versionIndex
          const index = activeItem.valueSet
            ?.filter((vs) => vs.id && vs.effectiveDate)
            .findIndex((vs) => vs.effectiveDate === newValueSet.effectiveDate)
          if (index > -1) {
            commit('changeValueSetVersionIndex', index)
          }
          resolve(newValueSet)
        })
        .catch((err) => {
          reject(err)
        })
    })
  },
  initValueSet ({ commit }) {
    commit('initValueSet')
    commit('changeValueSetVersionIndex', 0)
  },
  changeValueSetVersionIndex ({ commit }, index) {
    commit('changeValueSetVersionIndex', index)
  },
  // start shopping in the terminology browser for a new valueSet
  setStartShopping ({ commit }, status) {
    commit('updateSearchCode', '')
    commit('updateSearchContext', '')
    commit('updateSearchTerm', '')
    commit('setStartShopping', status)
  },
  addToShoppingCart ({ commit }, { item, position }) {
    commit('addToShoppingCart', { item, position })
  },
  updateItemInShoppingCart ({ commit }, { index, item }) {
    commit('updateItemInShoppingCart', { index, item })
  },

  removeFromShoppingCart ({ commit }, item) {
    commit('removeFromShoppingCart', item)
  },
  setShowNewValueSetDialog ({ commit }, status) {
    commit('setShowNewValueSetDialog', status)
  },
  setShowNewConceptMapDialog ({ commit }, status) {
    commit('setShowNewConceptMapDialog', status)
  },
  selectCodeInShoppingCart ({ commit }, code) {
    commit('selectCodeInShoppingCart', code)
  },
  updateSearchCode ({ commit }, searchCode) {
    commit('updateSearchCode', searchCode)
  },
  updateSearchContext ({ commit }, searchContext) {
    commit('updateSearchContext', searchContext)
  },
  updateLoincSearchContext ({ commit }, loincSearchContext) {
    commit('updateLoincSearchContext', loincSearchContext)
  },
  updateSearchTerm ({ commit }, searchTerm) {
    commit('updateSearchTerm', searchTerm)
  },
  /**
   * Associations related functions
   */
  addCartWithAssociations ({ commit }, association) {
    commit('addCartWithAssociations', association)
  },
  removeFromCartWithAssociations ({ getters, commit }, association) {
    return new Promise((resolve, reject) => {
      const index = getters.cartWithAssociations.indexOf(association)
      if (index > -1) {
        commit('removeFromCartWithAssociations', index)
        resolve()
      } else {
        reject(new Error('not-found'))
      }
    })
  },

  /**
   * Codesystems
   */
  deleteCodeSystem ({ commit, rootGetters }, { codesystem, params = {} }) {
    return new Promise((resolve, reject) => {
      const prefix = params.prefix || rootGetters['project/getProjectSelected']?.prefix
      if (!prefix) {
        return reject(new Error('no project prefix found'))
      }

      const id = codesystem?.ref
      if (!id) {
        return reject(new Error('no codesystem reference found'))
      }

      TerminologyService.deleteCodeSystem(id, { project: prefix, ...params }, { broadcast: `DELETE/${prefix}/codesystem` })
        .then(() => {
          commit('setCodeSystemActive', null)
          commit('setCodeSystemItem', null)
          commit('removeCodeSystemFromList', codesystem)
          resolve()
        })
        .catch(error => reject(error))
    })
  },
  getCodeSystemList ({ commit }, params = {}) {
    commit('setLoading', { key: 'codeSystemItems', status: true })
    if (params.sort === undefined) {
      params.sort = 'displayName'
    }
    return new Promise((resolve, reject) => {
      TerminologyService.getCodeSystemList(params)
        .then((response) => {
          const codeSystems = response?.data?.codeSystem
          commit('setCodeSystemItems', codeSystems)
          commit('setLoading', { key: 'codeSystemItems', status: false })
          commit('setLoaded', { key: 'codeSystemItems', status: true })

          resolve(codeSystems)
        })
        .catch((err) => {
          commit('setLoading', { key: 'codeSystemItems', status: false })
          reject(err)
        })
    })
  },
  getCodeSystem ({ commit }, { id, ref, effectiveDate, params, abortController }) {
    return new Promise((resolve, reject) => {
      commit('setLoading', { key: 'codeSystemItem', status: true })
      if (!id && !ref) {
        reject(new Error('must-specify-id-or-ref'))
      }
      const refOrId = ref || id
      TerminologyService.getCodeSystem(refOrId, effectiveDate, params, abortController)
        .then((response) => {
          const codeSystem = response?.data
          commit('setCodeSystemItem', codeSystem)
          resolve(codeSystem)
        })
        .catch((err) => {
          reject(err)
        })
        .finally(() => {
          if (!abortController?.signal?.aborted) {
            commit('setLoading', { key: 'codeSystemItem', status: false })
          }
        })
    })
  },
  getCodeSystemUsage ({ commit }, { id, effectiveDate, params }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        reject(new Error('must-specify-id-and-effectiveDate'))
      }
      TerminologyService.getCodeSystemUsage(id, effectiveDate, params)
        .then((response) => {
          const usage = response?.data
          resolve(usage)
        })
        .catch((err) => reject(err))
    })
  },

  patchCodeSystem (
    { commit, rootGetters },
    { id, effectiveDate, data, params = {}, metaData = false }
  ) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!id || !effectiveDate) {
        return reject(
          new Error('No id or effectiveDate is passed to patchCodeSystem')
        )
      }

      TerminologyService.patchCodeSystem(id, effectiveDate, data, {
        prefix,
        ...params
      }, { broadcast: `PATCH/${prefix}/codesystem` })
        .then((res) => {
          const updatedCodeSystem = res.data
          if (metaData) {
            commit('patchCodeSystemListMetaData', updatedCodeSystem)
          }
          commit('setCodeSystemItem', updatedCodeSystem)
          resolve(updatedCodeSystem)
        })
        .catch((err) => reject(err))
    })
  },

  async cloneCodeSystem ({ commit, getters }, { params, codeSystemForList }) {
    return new Promise((resolve, reject) => {
      // Check if project prefix is passed.
      if (!params?.prefix) {
        // Reject error.
        return reject(new Error('project-prefix-required'))
      }

      return TerminologyService.postCodeSystem(null, params, { broadcast: `POST/${params.prefix}/codesystem` })
        .then((response) => {
          const newCodeSystem = response.data
          const newCodeSystemForList = { ...(codeSystemForList || newCodeSystem) }
          if ([true, 'true'].includes(params?.refOnly)) {
            newCodeSystemForList.ref = newCodeSystemForList.id
          }
          commit('addCodeSystemItemToList', newCodeSystemForList)
          commit('setCodeSystemItem', newCodeSystem)
          // Find new item in the list and set it as codeSystemActive
          const activeItem = getters.codeSystemItems?.find(
            (cs) => cs.id === newCodeSystemForList.id
          )
          if (activeItem) {
            commit('setCodeSystemActive', activeItem)

            // Set versionIndex
            const index = activeItem.codeSystem
              ?.filter((cs) => cs.id && cs.effectiveDate)
              .findIndex((cs) => cs.effectiveDate === newCodeSystem.effectiveDate)
            if (index > -1) {
              commit('setCodeSystemVersionIndex', index)
            }
          }
          resolve(newCodeSystem)
        })
        .catch((error) => reject(error))
    })
  },
  async createCodeSystem ({ commit, getters, rootGetters }, { data, params = {} } = {}) {
    return new Promise((resolve, reject) => {
      // Check if project prefix is passed.
      const prefix = params?.prefix || rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        // Reject error.
        return reject(new Error('project-prefix-required'))
      }

      return TerminologyService.postCodeSystem(data, { prefix, ...params }, { broadcast: `POST/${prefix}/codesystem` })
        .then((response) => {
          const newCodeSystem = response.data
          commit('addCodeSystemItemToList', newCodeSystem)
          commit('setCodeSystemItem', newCodeSystem)
          // Find new item in the list and set it as codeSystemActive
          const activeItem = getters.codeSystemItems?.find(
            (cs) => cs.id === newCodeSystem.id
          )
          if (activeItem) {
            commit('setCodeSystemActive', activeItem)
          }
          // Set versionIndex
          const index = activeItem.codeSystem
            ?.filter((cs) => cs.id && cs.effectiveDate)
            .findIndex((cs) => cs.effectiveDate === newCodeSystem.effectiveDate)
          if (index > -1) {
            commit('setCodeSystemVersionIndex', index)
          }
          resolve(newCodeSystem)
        })
        .catch((error) => reject(error))
    })
  },
  /**
   * Concept list
   */
  async startEditComposition (
    { state, dispatch, commit },
    { start = true, path }
  ) {
    // start is true (start edit) or false (cancel edit)
    if (!start) {
      // if the edit composition is stopped, cancelled or patched
      const objectId = state.valueSetActive.id
      const effectiveDate = state.valueSetActive.effectiveDate
      await LockService.unlockObjectId({ objectId, effectiveDate })
    }
    if (start) {
      // Try to lock object by id and get response.
      // remove the inputs and select in the terminology browser
      commit('updateSearchCode', '')
      commit('updateSearchContext', '')
      commit('updateSearchTerm', '')
    }
    if (state.startShopping) {
      // startShopping is start add ValueSet,
      // and not startEditComposition
      commit('setStartShopping', false)
    }
    if (path) {
      commit('backupCompleteCodeSystemPath', path)
    }
    if (start) {
      // move the items in the cart
      dispatch('moveValueSetItemsToCart', true)
    } else {
      // cancel edit: editValueSetBtn = status
      // empty cart
      dispatch('moveValueSetItemsToCart', false)
    }
    // start/cancel edit
    commit('startEditComposition', start)
  },
  patchValueSetComposition ({ state, commit, dispatch }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = state.valueSetItem
      if (!id || !effectiveDate) {
        reject(new Error('no id or effectiveDate'))
        return
      }
      try {
        const item = state.valueSetItem
        const data = { parameter: [] }
        const path = 'items'
        data.parameter.push({
          op: 'replace',
          path: '/items',
          value: {
            [path]: state.shoppingCart
          }
        })
        dispatch('patchValueSet', { item, data })
          .then(() => {
            commit('startEditComposition', false)
            commit('emptyShoppingCart')
            // When navigating (= next step in the proces) to the valueset,
            // a new call is done with fresh collecttion of ValueSet
            // replace state.valueSetActive with (not needed)
            // if (response.data) commit('setValueSetActive', response.data)
            resolve('putValueSet')
          })
          .catch((error) => reject(error))
      } catch (error) {
        reject(error)
      }
    })
  },
  moveValueSetItemsToCart ({ state, commit }, move = true) {
    if (move) {
      if (state.valueSetItem?.items) {
        const items = cloneDeep(state.valueSetItem.items)
        items.forEach((item) => {
          commit('addToShoppingCart', { item })
        })
      }
    } else {
      commit('emptyShoppingCart')
    }
  },
  /**
   * ValueSet related
   */
  async cloneValueSet ({ commit, getters }, { params, valueSetForList, setItem = true }) {
    return new Promise((resolve, reject) => {
      // Check if project prefix is passed.
      if (!params?.prefix) {
        // Reject error.
        return reject(new Error('project-prefix-required'))
      }

      return ValuesetService.postValueSet(null, params, { broadcast: `POST/${params.prefix}/valueset` })
        .then((response) => {
          const newValueSet = response.data
          const newValueSetForList = { ...(valueSetForList || newValueSet) }
          if ([true, 'true'].includes(params?.refOnly)) {
            newValueSetForList.ref = newValueSetForList.id
          }
          commit('addValueSetItemToList', newValueSetForList)

          if (setItem) {
            commit('setValueSetItem', newValueSet)
            // Find new item in the list and set it as valueSetActive
            const activeItem = getters.getValueSet.find(
              (vs) => vs.id === newValueSet.id
            )
            if (activeItem) {
              commit('setValueSetActive', activeItem)
            }
            // Set versionIndex
            const index = activeItem.valueSet
              ?.filter((vs) => vs.id && vs.effectiveDate)
              .findIndex((vs) => vs.effectiveDate === newValueSet.effectiveDate)
            if (index > -1) {
              commit('changeValueSetVersionIndex', index)
            }
          }
          resolve(newValueSet)
        })
        .catch((error) => reject(error))
    })
  },
  deleteValueSet ({ commit, rootGetters }, { valueset, params = {} }) {
    return new Promise((resolve, reject) => {
      const prefix = params.prefix || rootGetters['project/getProjectSelected']?.prefix
      if (!prefix) {
        return reject(new Error('no project prefix found'))
      }

      const id = valueset?.ref
      if (!id) {
        return reject(new Error('no valueset reference found'))
      }

      TerminologyService.deleteValueSet(id, { project: prefix, ...params }, { broadcast: `DELETE/${prefix}/valueset` })
        .then(() => {
          commit('setValueSetActive', null)
          commit('setValueSetItem', null)
          commit('removeValueSetFromList', valueset)
          resolve()
        })
        .catch(error => reject(error))
    })
  },
  patchValueSet ({ state, commit, rootGetters }, { item, data, metadata = false }) {
    return new Promise((resolve, reject) => {
      if (!item?.id === true || !item?.effectiveDate === true) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }
      if (rootGetters['project/getProjectSelected'] == null) {
        reject(new Error('no project selected'))
      }
      const { id, effectiveDate } = item
      const prefix = rootGetters['project/getProjectSelected'].prefix
      const params = {
        prefix
      }
      return ValuesetService.patchValueSet(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/valueset` })
        .then((response) => {
          const updatedValueSet = response.data
          commit('setValueSetItem', updatedValueSet)
          if (metadata) {
            commit('patchValueSetListMetaData', { updatedValueSet })
          }
          resolve(response.data)
        })
        .catch((error) => reject(error))
    })
  },
  putValueSet ({ state, commit }, data) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = state.valueSetItem
      if (!id || !effectiveDate) {
        reject(new Error('no id or effectiveDate'))
        return
      }
      return ValuesetService.putValueSet(id, effectiveDate, data)
        .then((response) => {
          // update the store with the latest and greatest data
          if (response.data) commit('setValueSetItem', response.data)
          resolve('putValueSet')
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  getConceptMapList ({ commit }, params = {}) {
    commit('setLoading', { key: 'conceptMapItems', status: true })
    return new Promise((resolve, reject) => {
      TerminologyService.getConceptMapList(params)
        .then((response) => {
          const conceptMapItems = response?.data?.conceptMap
          commit('setConceptMapItems', conceptMapItems)
          commit('setLoading', { key: 'conceptMapItems', status: false })
          commit('setLoaded', { key: 'conceptMapItems', status: true })

          resolve(conceptMapItems)
        })
        .catch((err) => {
          commit('setLoading', { key: 'codeSystemItems', status: false })
          reject(err)
        })
    })
  },

  getConceptMapItem ({ commit }, { id, effectiveDate, prefix, abortController }) {
    return new Promise((resolve, reject) => {
      commit('setLoading', { key: 'conceptMapItem', status: true })
      commit('setLoaded', { key: 'conceptMapItem', status: false })
      TerminologyService.getConceptMapItem({ id, effectiveDate, prefix, abortController })
        .then((response) => {
          const conceptMapItem = response?.data
          commit('setConceptMapItem', conceptMapItem)
          commit('setLoaded', { key: 'conceptMapItem', status: true })
          resolve(conceptMapItem)
        })
        .catch((err) => reject(err))
        .finally(() => {
          commit('setLoading', { key: 'conceptMapItem', status: false })
        })
    })
  },

  /**
   * Put scenario item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async putConceptMapItem ({ commit, rootGetters }, { item, data }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item
      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) return reject(new Error('no-project-prefix-found'))

      // Put scenario transactions.
      TerminologyService.putConceptMapItem(
        id,
        effectiveDate,
        data,
        { broadcast: `PUT/${prefix}/conceptMap` }
      ).then((response) => {
        // Get put concept map item.
        const puttedConceptMapItem = response?.data
        // Update concept map item in list.
        commit('patchConceptMapListMetaData', { updatedConceptMap: puttedConceptMapItem })
        // Update concept map item.
        commit('setConceptMapItem', puttedConceptMapItem)
        // Resolve promise.
        resolve(puttedConceptMapItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Patch scenario item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async patchConceptMapItem ({ commit, rootGetters }, { item, data }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }

      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      // Patch scenario transactions.
      TerminologyService.patchConceptMapItem(
        id,
        effectiveDate,
        data,
        { broadcast: `PATCH/${prefix}/conceptMap` }
      ).then((response) => {
        // Get patched concept map item.
        const patchedConceptMapItem = response?.data
        // Update concept map item in list.
        commit('patchConceptMapListMetaData', { updatedConceptMap: patchedConceptMapItem })
        // Update concept map item.
        commit('setConceptMapItem', patchedConceptMapItem)
        // Resolve promise.
        resolve(patchedConceptMapItem)
      }).catch((error) => reject(error))
    })
  },

  async createConceptMapItem ({ commit, dispatch, rootGetters }, newConceptMap) {
    // newConceptMap is used to update the newly created concept map in 'step two'
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      const newConceptMapItem = {
        displayName: newConceptMap.displayName,
        sourceScope: newConceptMap.sourceScope,
        targetScope: newConceptMap.targetScope,
        group: newConceptMap.group
      }

      return TerminologyService.createConceptMapItem(newConceptMapItem, { prefix: projectPrefix }, { broadcast: `POST/${projectPrefix}/conceptMap` }).then((response) => {
        // update newly created concept map with the values from the user
        commit('addConceptMapItemToList', response.data)
        return resolve(response.data)
      }).catch((error) => reject(error))
    })
  },

  async cloneConceptMap ({ commit, getters }, { params, conceptMapForList, setItem = true }) {
    return new Promise((resolve, reject) => {
      // Check if project prefix is passed.
      if (!params?.prefix) { return reject(new Error('project-prefix-required')) }

      return TerminologyService.createConceptMapItem(null, params, { broadcast: `POST/${params.prefix}/conceptMap` })
        .then((response) => {
          const newConceptMap = response.data
          const newConceptMapForList = { ...(conceptMapForList || newConceptMap) }
          if ([true, 'true'].includes(params?.refOnly)) {
            newConceptMapForList.ref = newConceptMapForList.id
          }
          commit('addConceptMapItemToList', newConceptMapForList)

          if (setItem) {
            commit('setConceptMapItem', newConceptMap)

            // Find new item in the list and set it as conceptMapActive
            const activeItem = getters.conceptMapItems.find(
              (cm) => cm.id === newConceptMap.id
            )
            if (activeItem) {
              commit('setConceptMapActive', activeItem)
            }
            // Set versionIndex
            const index = activeItem.conceptMap
              ?.filter((cm) => cm.id && cm.effectiveDate)
              .findIndex((cm) => cm.effectiveDate === newConceptMap.effectiveDate)
            if (index > -1) {
              commit('changeConceptMapVersionIndex', index)
            }
          }

          resolve(newConceptMap)
        })
        .catch((error) => reject(error))
    })
  },

  async deleteValueSetExpansion ({ commit }, { id }) {
    if (!id) {
      throw new Error('Expected id for valueset expansion set')
    }
    await TerminologyService.deleteValueSetExpansionSet({ id })
    commit('decrementValueSetExpansionAssociationCount')
  }
}

const mutations = {
  hideRealtimeUpdateNotifications (state, { type, status }) {
    state.hideRealtimeUpdateNotifications[type] = !!status
  },
  setCreateValuesetFrom (state, route) {
    state.createValuesetFrom = route
  },
  setOpenAddValuesetAssociationModalOnEnter (state, value) {
    state.openAddValuesetAssociationModalOnEnter = value
  },
  setAbortController (state, controller) {
    state.abortController = controller
  },
  setActiveItem (state, item) {
    state.activeItem = item
  },
  initTerminology (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  setExpandedSection (state, { panel, section, value }) {
    state.expandedSections[panel][section] = !!value
  },
  setLoading,
  setLoaded,
  setPrimaryCodeSystemId (state, codeSystemId) {
    state.primaryCodeSystemId = codeSystemId
  },
  setPrimaryConceptCode (state, code) {
    state.primaryConceptCode = code
  },
  setPreferredLanguage (state, language) {
    state.preferredLanguage = language
  },
  setSelectedLanguages (state, languages) {
    state.selectedLanguages = languages
  },
  setSelectedConcept (state, concept) {
    state.selectedConcept = concept
  },
  setValueSetList (state, valueSet) {
    state.valueSet = valueSet
  },
  setValueSetItem (state, item) {
    state.valueSetItem = item
  },
  setValueSetActive (state, valueSetActive) {
    state.valueSetActive = valueSetActive
  },
  setValueSetItemBackup (state) {
    state.valueSetItemBackup = cloneDeep(state.valueSetItem)
  },
  restoreValueSetItemBackup (state) {
    state.valueSetItem = cloneDeep(state.valueSetItemBackup)
    state.valueSetItemBackup = {}
  },
  initValueSet (state) {
    state.valueSetActive = null
    state.valueSetItem = null
  },
  initCodeSystem (state) {
    state.codeSystemActive = null
    state.codeSystemItem = null
    state.codeSystemVersionIndex = 0
  },
  changeConceptMapVersionIndex (state, index) {
    state.conceptMapVersionIndex = index
  },
  changeValueSetVersionIndex (state, index) {
    state.valueSetVersionIndex = index
  },
  addToShoppingCart (state, { item, position }) {
    const preparedItem = { ...item, uuid: getUuid() }
    if (position != null) {
      state.shoppingCart.splice(position, 0, preparedItem)
    } else {
      state.shoppingCart.push(preparedItem)
    }
  },
  updateItemInShoppingCart (state, { index, item }) {
    const preparedItem = { ...item, uuid: getUuid() }

    state.shoppingCart.splice(index, 1, preparedItem)
  },
  removeFromShoppingCart (state, code) {
    const index = state.shoppingCart.indexOf(code)
    if (index > -1) {
      state.shoppingCart.splice(index, 1)
    }
  },
  moveItemInShoppingCart (state, { oldIndex, newIndex, position = 'after' }) {
    const insert = position === 'in'
    const currentItem = state.shoppingCart[oldIndex]
    const beforeItem = position === 'before'
      ? state.shoppingCart[newIndex - 1]
      : state.shoppingCart[newIndex]
    // afterItem not needed yet, maybe for checks later on
    // const afterItem = position === 'before'
    //   ? state.shoppingCart[newIndex]
    //   : state.shoppingCart[newIndex + 1]

    const beforeLevel = +beforeItem?.level || 0
    const currentLevel = +currentItem?.level || 0

    // Update level of the item being moved
    if (insert || beforeLevel < currentLevel) {
      const isInsideAbstractOrSpecializable = ['A', 'S'].includes(beforeItem?.type)
      const isExceptionAfterNonException = currentItem?.is === 'exception' && beforeItem?.is !== 'exception'

      const newLevel = (insert || isInsideAbstractOrSpecializable) && !isExceptionAfterNonException ? beforeLevel + 1 : beforeLevel
      state.shoppingCart[oldIndex].level = `${newLevel}`
    }

    // Adjust newIndex when needed
    if (position === 'before' && oldIndex < newIndex) {
      newIndex -= 1
    }
    if (['in', 'after'].includes(position) && oldIndex > newIndex) {
      newIndex += 1
    }
    state.shoppingCart.splice(newIndex, 0, state.shoppingCart.splice(oldIndex, 1)[0])
  },
  emptyShoppingCart (state) {
    state.shoppingCart = []
  },
  setShowNewValueSetDialog (state, status) {
    state.showNewValueSetDialog = status
  },
  setShowNewConceptMapDialog (state, status) {
    state.showNewConceptMapDialog = status
  },
  setStartShopping (state, status) {
    state.startShopping = status
  },
  selectCodeInShoppingCart (state, code) {
    const index = state.shoppingCart.indexOf(code)
    const selected =
      state.shoppingCart[index].selected === undefined
        ? true
        : !state.shoppingCart[index].selected
    if (selected) {
      Vue.set(state.shoppingCart[index], 'selected', selected)
    } else {
      Vue.delete(state.shoppingCart[index], 'selected')
    }
  },
  updateSearchCode (state, searchCode) {
    state.searchCode = searchCode
  },
  updateSearchMode (state, searchMode) {
    state.searchMode = searchMode
  },
  updateSearchTerm (state, searchTerm) {
    state.searchTerm = searchTerm
  },
  updateSearchContext (state, searchContext) {
    state.searchContext = searchContext
  },
  updateLoincSearchContext (state, loincSearchContext) {
    state.loincSearchContext = loincSearchContext
  },
  addCartWithAssociations (state, association) {
    state.cartWithAssociations.push(association)
  },
  removeFromCartWithAssociations (state, index) {
    state.cartWithAssociations.splice(index, 1)
  },
  emptyCartWithAssociations (state) {
    state.cartWithAssociations = []
  },
  updateItemInCartWithAssociations (state, { index, item }) {
    state.cartWithAssociations.splice(index, 1, item)
  },
  /**
   * CodeSystems
   */
  setCodeSystemItems (state, items) {
    if (Array.isArray(items)) {
      state.codeSystemItems = items
    }
  },
  addCodeSystemItemToList (state, codeSystemItem) {
    const propsToInclude = [
      'displayName',
      'effectiveDate',
      'id',
      'ident',
      'name',
      'statusCode'
    ]

    if (codeSystemItem.ref) {
      propsToInclude.push('ref')
    }
    const uuid = () => getUuid()
    const newList = cloneDeep(state.codeSystemItems) || []

    // Check if there is a codeSystem with the same id
    const existingCodeSystemIndex = newList.findIndex(
      (cs) => cs.id === codeSystemItem.id
    )
    if (existingCodeSystemIndex > -1) {
      // The codeSystem is a new version of an existing codeSystem.
      // It will be added to the codeSystem.codeSystem array of the existing codeSystem.
      const newVersion = { uuid: uuid() }
      propsToInclude.forEach((prop) => {
        newVersion[prop] = codeSystemItem[prop]
      })
      if (newList[existingCodeSystemIndex]?.codeSystem) {
        newList[existingCodeSystemIndex].codeSystem.unshift(newVersion)
        // Sort the list by effectiveDate
        newList[existingCodeSystemIndex].codeSystem.sort((a, b) => {
          if (a.effectiveDate > b.effectiveDate) {
            return -1
          }
          if (a.effectiveDate < b.effectiveDate) {
            return 1
          }
          return 0
        })
      }

      // If the effectiveDate of the new codeSystem is the most recent of all versions, also update on the 'root level'
      if (
        newList[existingCodeSystemIndex]?.effectiveDate &&
        newList[existingCodeSystemIndex].effectiveDate <
        codeSystemItem.effectiveDate
      ) {
        newList[existingCodeSystemIndex] = {
          uuid: newList[existingCodeSystemIndex].uuid,
          codeSystem: newList[existingCodeSystemIndex].codeSystem
        }
        propsToInclude.forEach((prop) => {
          newList[existingCodeSystemIndex][prop] = codeSystemItem[prop]
        })
      }

      // If the new codeSystem is a version of the currently active codeSystem (codeSystemActive)
      if (state.codeSystemActive && state.codeSystemActive.id === codeSystemItem.id) {
        if (Array.isArray(state.codeSystemActive.codeSystem)) {
          state.codeSystemActive.codeSystem.unshift(newVersion)
        }
      }
    } else {
      // This is a codeSystem with a new id. It will be added to the list
      const isReference = !!codeSystemItem.ref
      const newListItem = {
        uuid: uuid(),
        codeSystem: isReference ? codeSystemItem.codeSystem : [{ uuid: uuid() }]
      }
      // Construct the new list item
      propsToInclude.forEach((prop) => {
        newListItem[prop] = codeSystemItem[prop]
        if (!isReference) {
          newListItem.codeSystem[0][prop] = codeSystemItem[prop]
        }
      })
      // Construct the new list
      newList.push(newListItem)
      // Sort the list
      const sortBy = 'id'
      newList.sort((a, b) => {
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) <
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return -1
        }
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) >
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return 1
        }
        return 0
      })
    }

    // Update state
    state.codeSystemItems = newList
  },
  removeCodeSystemFromList (state, codesystem) {
    if (!codesystem?.ref) {
      return console.error('not a codesystem reference')
    }
    if (!Array.isArray(state.codeSystemItems)) {
      return console.error('no list of valuesets found')
    }
    state.codeSystemItems = state.codeSystemItems.filter(vs => vs.ref !== codesystem.ref)
  },
  patchCodeSystemListMetaData (state, updatedCodeSystem) {
    const { displayName, statusCode, versionLabel, name } = updatedCodeSystem

    // Find the item in the codeSystem list
    const versionToUpdate = state.codeSystemItems?.find(
      (cs) => cs.id === updatedCodeSystem.id
    )

    const versionIndex = versionToUpdate.codeSystem.findIndex(
      (v) => v.effectiveDate === updatedCodeSystem.effectiveDate
    )

    const isActiveCodeSystem = state.codeSystemActive?.id === updatedCodeSystem.id
    // && state.codeSystemActive?.effectiveDate === updatedCodeSystem.effectiveDate

    // Update codeSystemActive.codeSystem and codeSystemItems.codeSystem in the list
    if (versionIndex >= 0) {
      if (isActiveCodeSystem) {
        state.codeSystemActive.codeSystem[versionIndex].statusCode = statusCode
        state.codeSystemActive.codeSystem[versionIndex].displayName = displayName
        state.codeSystemActive.codeSystem[versionIndex].versionLabel =
          versionLabel
        state.codeSystemActive.codeSystem[versionIndex].name = name
      }

      versionToUpdate.codeSystem[versionIndex] = {
        ...versionToUpdate.codeSystem[versionIndex],
        displayName,
        statusCode,
        versionLabel,
        name
      }
    }

    // If updated codeSystem is the most recent version, update properties on 'root level'
    if (versionToUpdate?.effectiveDate === updatedCodeSystem.effectiveDate) {
      // Replace in the codeSystem list
      state.codeSystemItems = this.$filters.listHelper.replaceItemInList({
        items: state.codeSystemItems || [],
        item: {
          ...versionToUpdate,
          displayName,
          statusCode,
          versionLabel,
          name
        },
        handler: (t) =>
          t.id === updatedCodeSystem.id &&
          t.effectiveDate === updatedCodeSystem.effectiveDate
      })

      // Update codeSystemActive props
      if (isActiveCodeSystem) {
        state.codeSystemActive.statusCode = statusCode
        state.codeSystemActive.displayName = displayName
        state.codeSystemActive.versionLabel = versionLabel
        state.codeSystemActive.name = name
      }
    } else {
      // If updated codeSystem is NOT the most recent version, only update properties on 'codeSystem.codeSystem' level
      state.codeSystemItems = this.$filters.listHelper.replaceItemInList({
        items: state.codeSystemItems || [],
        item: versionToUpdate,
        handler: (cs) => cs.id === updatedCodeSystem.id
      })
    }
  },
  setCodeSystemItem (state, item) {
    state.codeSystemItem = item
  },
  setCodeSystemActive (state, item) {
    state.codeSystemActive = item
  },
  incrementCodeSystemIssueAssociationCount (state) {
    if (state.codeSystemItem?.issueAssociation?.[0]?.count) {
      const newCount =
        parseInt(state.codeSystemItem.issueAssociation[0].count) + 1
      state.codeSystemItem.issueAssociation[0].count = newCount.toString()
    }
  },
  setCodeSystemVersionIndex (state, index) {
    state.codeSystemVersionIndex = index
  },
  /**
   * Concept list
   */
  backupCompleteCodeSystemPath (state, path) {
    state.backupCompleteCodeSystemPath = path
  },
  backupConceptList (state, items) {
    // called in edit Com
    state.backupConceptList = items
  },
  backupCompleteCodeSystem (state, items) {
    state.backupCompleteCodeSystem = items
  },
  removeConceptListCodeSystem (state, index) {
    state.valueSetItem.completeCodeSystem.splice(index, 1)
  },
  startEditComposition (state, status) {
    state.editValueSetBtn = status
  },
  /**
   * Valueset
   */
  // Add a new valueSetItem (clone, new valueSet) to the list (valueSet) in the correct format
  addValueSetItemToList (state, valueSetItem) {
    const propsToInclude = [
      'displayName',
      'effectiveDate',
      'id',
      'ident',
      'name',
      'statusCode'
    ]

    if (valueSetItem.ref) {
      propsToInclude.push('ref')
    }

    const uuid = () => getUuid()
    const newList = cloneDeep(state.valueSet) || []

    // Check if there is a valueSet with the same id
    const existingValueSetIndex = newList.findIndex(
      (vs) => vs.id === valueSetItem.id
    )
    if (existingValueSetIndex > -1) {
      // The valueSet is a new version of an existing valueSet.
      // It will be added to the valueSet.valueSet array of the existing valueSet.
      const newVersion = { uuid: uuid() }
      propsToInclude.forEach((prop) => {
        newVersion[prop] = valueSetItem[prop]
      })
      if (newList[existingValueSetIndex]?.valueSet) {
        newList[existingValueSetIndex].valueSet.unshift(newVersion)
        // Sort the list by effectiveDate
        newList[existingValueSetIndex].valueSet.sort((a, b) => {
          if (a.effectiveDate > b.effectiveDate) {
            return -1
          }
          if (a.effectiveDate < b.effectiveDate) {
            return 1
          }
          return 0
        })
      }
      // If the effectiveDate of the new valueSet is the most recent of all versions, also update on the 'root level'
      if (
        newList[existingValueSetIndex]?.effectiveDate &&
        newList[existingValueSetIndex].effectiveDate <
        valueSetItem.effectiveDate
      ) {
        newList[existingValueSetIndex] = {
          uuid: newList[existingValueSetIndex].uuid,
          valueSet: newList[existingValueSetIndex].valueSet
        }
        propsToInclude.forEach((prop) => {
          newList[existingValueSetIndex][prop] = valueSetItem[prop]
        })
      }

      // If the new valueSet is a version of the currently active valueSet (valueSetActive)
      if (state.valueSetActive && state.valueSetActive.id === valueSetItem.id) {
        if (Array.isArray(state.valueSetActive.valueSet)) {
          state.valueSetActive.valueSet.unshift(newVersion)
        }
      }
    } else {
      // This is a valueSet with a new id. It will be added to the list
      const isReference = !!valueSetItem.ref
      const newListItem = {
        uuid: uuid(),
        valueSet: isReference ? valueSetItem.valueSet : [{ uuid: uuid() }]
      }
      // Construct the new list item
      propsToInclude.forEach((prop) => {
        newListItem[prop] = valueSetItem[prop]
        if (!isReference) {
          newListItem.valueSet[0][prop] = valueSetItem[prop]
        }
      })
      // Construct the new list
      newList.push(newListItem)
      // Sort the list
      const sortBy = 'displayName'
      newList.sort((a, b) => {
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) <
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return -1
        }
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) >
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return 1
        }
        return 0
      })
    }

    // Update state
    state.valueSet = newList
  },
  removeValueSetFromList (state, valueset) {
    if (!valueset?.ref) {
      return console.error('not a valueset reference')
    }
    if (!Array.isArray(state.valueSet)) {
      return console.error('no list of valuesets found')
    }
    state.valueSet = state.valueSet.filter(vs => vs.ref !== valueset.ref)
  },
  incrementValueSetIssueAssociationCount (state) {
    if (state.valueSetItem?.issueAssociation?.[0]?.count) {
      const newCount =
        parseInt(state.valueSetItem.issueAssociation[0].count) + 1
      state.valueSetItem.issueAssociation[0].count = newCount.toString()
    }
  },
  decrementValueSetExpansionAssociationCount (state) {
    if (state.valueSetItem?.valueSetExpansionAssociation?.[0]?.count) {
      const newCount =
        parseInt(state.valueSetItem.valueSetExpansionAssociation[0].count) - 1
      state.valueSetItem.valueSetExpansionAssociation[0].count = newCount.toString()
    }
  },
  incrementValueSetExpansionAssociationCount (state) {
    if (state.valueSetItem?.valueSetExpansionAssociation?.[0]?.count) {
      const newCount =
        parseInt(state.valueSetItem.valueSetExpansionAssociation[0].count) + 1
      state.valueSetItem.valueSetExpansionAssociation[0].count = newCount.toString()
    }
  },
  /**
   * Updates the metadata (displayName, statusCode, versionLabel) in the valueset list and in valueSetActive
   */
  patchValueSetListMetaData (state, { updatedValueSet, isActiveValueSet = true, isVersionOfValueSetActive = false }) {
    const { displayName, statusCode, versionLabel, name } = updatedValueSet

    // Find the item in the valueSet list
    const versionToUpdate = state.valueSet?.find(
      (vs) => vs.id === updatedValueSet.id
    )

    if (!versionToUpdate) {
      return
    }

    const versionIndex = versionToUpdate.valueSet.findIndex(
      (v) => v.effectiveDate === updatedValueSet.effectiveDate
    )
    // Update valueSetActive.valueSet and valueSet.valueSet in the list
    if (versionIndex >= 0) {
      if (isActiveValueSet || isVersionOfValueSetActive) {
        state.valueSetActive.valueSet[versionIndex].statusCode = statusCode
        state.valueSetActive.valueSet[versionIndex].displayName = displayName
        state.valueSetActive.valueSet[versionIndex].versionLabel = versionLabel
        state.valueSetActive.valueSet[versionIndex].name = name
      }

      versionToUpdate.valueSet[versionIndex] = {
        ...versionToUpdate.valueSet[versionIndex],
        displayName,
        statusCode,
        versionLabel,
        name
      }
    }

    // If updated valueset is the most recent version, update properties on 'root level'
    if (versionToUpdate?.effectiveDate === updatedValueSet.effectiveDate) {
      // Replace in the valueSet list
      state.valueSet = this.$filters.listHelper.replaceItemInList({
        items: state.valueSet || [],
        item: {
          ...versionToUpdate,
          displayName,
          statusCode,
          versionLabel,
          name
        },
        handler: (t) =>
          t.id === updatedValueSet.id &&
          t.effectiveDate === updatedValueSet.effectiveDate
      })

      // Update valueSetActive props
      if (isActiveValueSet) {
        state.valueSetActive.statusCode = statusCode
        state.valueSetActive.displayName = displayName
        state.valueSetActive.versionLabel = versionLabel
        state.valueSetActive.name = name
      }
    } else {
      // If updated valueSet is NOT the most recent version, only update properties on 'valueSet.valueSet' level
      state.valueSet = this.$filters.listHelper.replaceItemInList({
        items: state.valueSet || [],
        item: versionToUpdate,
        handler: (t) => t.id === updatedValueSet.id
      })
    }
  },
  preserveCodeSystemItemOnSelect (state, status) {
    if (typeof status === 'boolean') {
      state.preserveCodeSystemItemOnSelect = status
    }
  },
  preserveConceptMapItemOnSelect (state, status) {
    if (typeof status === 'boolean') {
      state.preserveConceptMapItemOnSelect = status
    }
  },
  preserveValueSetItemOnSelect (state, status) {
    if (typeof status === 'boolean') {
      state.preserveValueSetItemOnSelect = status
    }
  },

  setConceptMapItems (state, conceptMapItems) {
    if (Array.isArray(conceptMapItems)) {
      state.conceptMapItems = conceptMapItems
    }
  },

  setConceptMapActive (state, item) {
    state.conceptMapActive = item
  },
  setConceptMapItem (state, conceptMapItem) {
    state.conceptMapItem = conceptMapItem
  },

  patchConceptMapListMetaData (state, { updatedConceptMap, isActiveConceptMap = true, isVersionOfConceptMapActive = false }) {
    const { displayName, statusCode, versionLabel } = updatedConceptMap

    // Find the item in the conceptMap list
    const versionToUpdate = state.conceptMapItems?.find(
      (vs) => vs.id === updatedConceptMap.id
    )

    if (!versionToUpdate) {
      return
    }

    const versionIndex = versionToUpdate.conceptMap.findIndex(
      (cm) => cm.effectiveDate === updatedConceptMap.effectiveDate
    )
    // Update conceptMapActive.conceptMap and conceptMap.conceptMap in the list
    if (versionIndex >= 0) {
      if (isActiveConceptMap || isVersionOfConceptMapActive) {
        state.conceptMapActive.conceptMap[versionIndex].statusCode = statusCode
        state.conceptMapActive.conceptMap[versionIndex].displayName = displayName
        state.conceptMapActive.conceptMap[versionIndex].versionLabel = versionLabel
      }

      versionToUpdate.conceptMap[versionIndex] = {
        ...versionToUpdate.conceptMap[versionIndex],
        displayName,
        statusCode,
        versionLabel
      }
    }

    // If updated conceptMap is the most recent version, update properties on 'root level'
    if (versionToUpdate?.effectiveDate === updatedConceptMap.effectiveDate) {
      // Replace in the conceptMap list
      state.conceptMapItems = this.$filters.listHelper.replaceItemInList({
        items: state.conceptMapItems || [],
        item: {
          ...versionToUpdate,
          displayName,
          statusCode,
          versionLabel
        },
        handler: (cm) =>
          cm.id === updatedConceptMap.id &&
          cm.effectiveDate === updatedConceptMap.effectiveDate
      })

      // Update conceptMapActive props
      if (isActiveConceptMap) {
        state.conceptMapActive.statusCode = statusCode
        state.conceptMapActive.displayName = displayName
        state.conceptMapActive.versionLabel = versionLabel
      }
    } else {
      // If updated conceptMap is NOT the most recent version, only update properties on 'conceptMap.conceptMap' level
      state.conceptMapItems = this.$filters.listHelper.replaceItemInList({
        items: state.conceptMapItems || [],
        item: versionToUpdate,
        handler: (cm) => cm.id === updatedConceptMap.id
      })
    }
  },

  // Add a new conceptMap item (clone, new valueSet) to the list (conceptMapItems) in the correct format
  addConceptMapItemToList (state, conceptMapItem) {
    const propsToInclude = [
      'displayName',
      'effectiveDate',
      'id',
      'ident',
      'statusCode',
      'versionLabel'
    ]

    if (conceptMapItem.ref) {
      propsToInclude.push('ref')
    }

    const uuid = () => getUuid()
    const newList = cloneDeep(state.conceptMapItems || [])

    // Check if there is a valueSet with the same id
    const existingConceptMapIndex = newList.findIndex(
      (cm) => cm.id === conceptMapItem.id
    )
    if (existingConceptMapIndex > -1) {
      // The conceptMap is a new version of an existing conceptMap.
      // It will be added to the conceptMap.conceptMap array of the existing conceptMap.
      const newVersion = { uuid: uuid() }
      propsToInclude.forEach((prop) => {
        newVersion[prop] = conceptMapItem[prop]
      })
      if (newList[existingConceptMapIndex]?.conceptMap) {
        newList[existingConceptMapIndex].conceptMap.unshift(newVersion)
        // Sort the list by effectiveDate
        newList[existingConceptMapIndex].conceptMap.sort((a, b) => {
          if (a.effectiveDate > b.effectiveDate) {
            return -1
          }
          if (a.effectiveDate < b.effectiveDate) {
            return 1
          }
          return 0
        })
      }
      // If the effectiveDate of the new conceptMap is the most recent of all versions, also update on the 'root level'
      if (
        newList[existingConceptMapIndex]?.effectiveDate &&
        newList[existingConceptMapIndex].effectiveDate <
        conceptMapItem.effectiveDate
      ) {
        newList[existingConceptMapIndex] = {
          uuid: newList[existingConceptMapIndex].uuid,
          conceptMap: newList[existingConceptMapIndex].conceptMap
        }
        propsToInclude.forEach((prop) => {
          newList[existingConceptMapIndex][prop] = conceptMapItem[prop]
        })
      }

      // If the new conceptMap is a version of the currently active conceptMap (conceptMapActive)
      if (state.conceptMapActive && state.conceptMapActive.id === conceptMapItem.id) {
        if (Array.isArray(state.conceptMapActive.conceptMap)) {
          state.conceptMapActive.conceptMap.unshift(newVersion)
        }
      }
    } else {
      // This is a conceptMap with a new id. It will be added to the list
      const isReference = !!conceptMapItem.ref
      const newListItem = {
        uuid: uuid(),
        conceptMap: isReference ? conceptMapItem.conceptMap : [{ uuid: uuid() }]
      }
      // Construct the new list item
      propsToInclude.forEach((prop) => {
        newListItem[prop] = conceptMapItem[prop]
        if (!isReference) {
          newListItem.conceptMap[0][prop] = conceptMapItem[prop]
        }
      })
      // Construct the new list
      newList.push(newListItem)
      // Sort the list
      const sortBy = 'displayName'
      newList.sort((a, b) => {
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) <
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return -1
        }
        if (
          this.$filters.getMatchingTranslation({ value: a[sortBy] }) >
          this.$filters.getMatchingTranslation({ value: b[sortBy] })
        ) {
          return 1
        }
        return 0
      })
    }

    // Update state
    state.conceptMapItems = newList
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
