import MyCommunityService from '@/services/MyCommunityService'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'
import { cloneDeep } from 'lodash'

const state = {
  loaded: {
    environmentItem: false,
    environments: false
  },
  loading: {
    environmentItem: false,
    environments: false
  },
  environmentItem: null,
  environments: []
}

const initialState = cloneDeep(state)

const getters = {
  environmentItem: state => state.environmentItem,
  environments: state => state.environments,
  isLoading,
  isLoaded,
  userCanReadEnvironment: (state, getters) => (envName) => {
    return getters.environments?.find(env => env.name === envName)?.read === 'true'
  },
  userCanWriteEnvironment: (state, getters) => (envName) => {
    return getters.environments?.find(env => env.name === envName)?.write === 'true'
  }
}

const actions = {
  async getMyCommunity ({ rootGetters, commit }, { name }) {
    const prefix = rootGetters['project/getProjectInFocus']?.prefix
    if (!prefix) throw new Error('no-project-selected')
    if (!name) throw new Error('no-name')

    commit('setLoading', { key: 'environmentItem', status: true })
    commit('setLoaded', { key: 'environmentItem', status: false })

    try {
      const res = await MyCommunityService.getMyCommunity({ project: prefix, name })
      const environment = res.data
      if (!environment) throw new Error('unexpected-response')
      commit('setEnvironmentItem', environment)
      commit('setLoaded', { key: 'environmentItem', status: true })
      return environment
    } finally {
      commit('setLoading', { key: 'environmentItem', status: false })
    }
  },
  async getMyCommunityList ({ rootGetters, commit }) {
    const prefix = rootGetters['project/getProjectInFocus']?.prefix
    if (!prefix) throw new Error('no-project-selected')
    commit('setLoaded', { key: 'environments', status: false })
    commit('setLoading', { key: 'environments', status: true })

    try {
      const res = await MyCommunityService.getMyCommunityList({ project: prefix })
      const environments = res.data?.community || []
      commit('setEnvironments', environments)
      commit('setLoaded', { key: 'environments', status: true })
      return environments
    } finally {
      commit('setLoading', { key: 'environments', status: false })
    }
  },
  async postMyCommunity ({ rootGetters, commit }, environment) {
    const prefix = rootGetters['project/getProjectInFocus']?.prefix
    if (!prefix) throw new Error('no-project-selected')
    if (!environment?.name) throw new Error('no-name')

    const res = await MyCommunityService.postMyCommunity({ project: prefix, data: environment, params: { name: environment.name } })
    const newEnvironment = res.data
    if (!newEnvironment) throw new Error('unexpected-response')
    commit('addEnvironmentToList', { environment: newEnvironment, username: rootGetters['authentication/getUsername'] })
    return newEnvironment
  },
  async putMyCommunity ({ getters, rootGetters, commit }, environment) {
    const prefix = rootGetters['project/getProjectInFocus']?.prefix
    if (!prefix) throw new Error('no-project-selected')
    if (!environment?.name) throw new Error('no-name')

    const res = await MyCommunityService.putMyCommunity({ project: prefix, data: environment, name: environment.name })
    const updatedEnvironment = res.data
    if (!updatedEnvironment) throw new Error('unexpected-response')
    commit('updateEnvironmentInList', { environment: updatedEnvironment, username: rootGetters['authentication/getUsername'] })

    if (getters.environmentItem?.name === updatedEnvironment.name) commit('setEnvironmentItem', updatedEnvironment)

    return updatedEnvironment
  }
}

const getEnvironmentForList = (environment, username) => {
  const props = ['name', 'desc', 'displayName', 'projectId', 'private', 'read', 'write']
  const newEnvironment = props.reduce((acc, prop) => {
    if (environment[prop]) acc[prop] = environment[prop]
    return acc
  }, {})

  const currentUserAccess = environment.access?.[0]?.author?.find(access => access.username === username)
  if (currentUserAccess?.rights) {
    newEnvironment.read = currentUserAccess.rights.includes('r').toString()
    newEnvironment.write = currentUserAccess.rights.includes('w').toString()
  }
  return newEnvironment
}

const mutations = {
  addEnvironmentToList (state, { environment, username }) {
    const newEnvironment = getEnvironmentForList(environment, username)
    state.environments.push(newEnvironment)
  },
  initMyCommunity (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  setEnvironmentItem (state, environment) {
    state.environmentItem = environment
  },
  setEnvironments (state, environments) {
    state.environments = environments
  },
  setLoading,
  setLoaded,
  updateEnvironmentInList (state, { environment, username }) {
    const index = state.environments.findIndex(env => env.name === environment.name)
    if (index === -1) return

    const newEnvironment = getEnvironmentForList(environment, username)
    state.environments[index] = newEnvironment
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
