import { cloneDeep } from 'lodash'
import Vue from 'vue'
import ProjectService from '../../services/ProjectService'

const state = {
  scheduledTasks: {
    total: '0'
  }
}

const initialState = cloneDeep(state)

const getters = {
  compileTasks (state) {
    return state.scheduledTasks['compile-request'] || []
  },
  currentTimestamp (state) {
    if (!state.scheduledTasks.lastModifiedDate) {
      return 0
    }
    return new Date(state.scheduledTasks.lastModifiedDate).getTime()
  },
  projectCheckTasks (state) {
    return state.scheduledTasks['projectcheck-request'] || []
  },
  publicationTasks (state) {
    return state.scheduledTasks['publication-request'] || []
  },
  questionnaireTransformTasks (state) {
    return state.scheduledTasks['questionnaire-transform-request'] || []
  },
  terminologyReportTasks (state) {
    return state.scheduledTasks['terminology-report-request'] || []
  },
  genericReportTasks (state) {
    return state.scheduledTasks['generic-report-request'] || []
  },
  taskList (state) {
    let taskList = []
    if (!state.scheduledTasks) {
      return taskList
    }
    for (const property in state.scheduledTasks) {
      if (!Array.isArray(state.scheduledTasks[property])) {
        continue
      }
      const tasks = state.scheduledTasks[property].map(task => ({ ...task, taskType: property }))
      taskList = taskList.concat(tasks)
    }
    return taskList
  }

}

const actions = {
  deleteProjectScheduledTasks ({ dispatch, getters, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      const prefix = params.prefix || rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project prefix'))
      }
      ProjectService.deleteProjectScheduledTasks(prefix, params, { broadcast: `DELETE/${prefix}/scheduled` })
        .then(response => {
          dispatch('setScheduledTasks', response.data)
          resolve(true)
        })
        .catch(err => reject(err))
    })
  },
  getProjectScheduledTasks ({ dispatch, getters, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      const prefix = params.prefix || rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project prefix'))
      }
      ProjectService.getProjectScheduledTasks(prefix, params)
        .then(response => {
          dispatch('setScheduledTasks', response.data)
          resolve(true)
        })
        .catch(err => reject(err))
    })
  },

  setScheduledTasks ({ commit }, responseData) {
    const scheduledTasksData = responseData || { total: '0' }
    // Why is filters included here?
    const propsToRemove = ['all', 'current', 'artefact', 'artifact', 'filters']
    propsToRemove.forEach(prop => {
      delete scheduledTasksData[prop]
    })
    commit('setScheduledTasks', scheduledTasksData)
  }
}

const mutations = {
  initScheduledTasks (state) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      state[key] = val
    }
  },

  setCompileTasks (state, compileTasks) {
    if (!state.scheduledTasks['compile-request']) {
      Vue.set(state.scheduledTasks, 'compile-request', compileTasks)
    } else {
      state.scheduledTasks['compile-request'] = compileTasks
    }
  },
  setProjectCheckTasks (state, projectCheckTasks) {
    if (!state.scheduledTasks['projectcheck-request']) {
      Vue.set(state.scheduledTasks, 'projectcheck-request', projectCheckTasks)
    } else {
      state.scheduledTasks['projectcheck-request'] = projectCheckTasks
    }
  },
  setPublicationTasks (state, publicationTasks) {
    if (!state.scheduledTasks['publication-request']) {
      Vue.set(state.scheduledTasks, 'publication-request', publicationTasks)
    } else {
      state.scheduledTasks['publication-request'] = publicationTasks
    }
  },
  setQuestionnaireTransformTasks (state, questionnaireTransformTasks) {
    if (!state.scheduledTasks['questionnaire-transform-request']) {
      Vue.set(state.scheduledTasks, 'questionnaire-transform-request', questionnaireTransformTasks)
    } else {
      state.scheduledTasks['questionnaire-transform-request'] = questionnaireTransformTasks
    }
  },

  setScheduledTasks (state, scheduledTasksData) {
    state.scheduledTasks = scheduledTasksData
  },
  setTerminologyReportTasks (state, terminologyReportTasks) {
    if (!state.scheduledTasks['terminology-report-request']) {
      Vue.set(state.scheduledTasks, 'terminology-report-request', terminologyReportTasks)
    } else {
      state.scheduledTasks['terminology-report-request'] = terminologyReportTasks
    }
  },
  setGenericReportTasks (state, genericReportTasks) {
    if (!state.scheduledTasks['generic-report-request']) {
      Vue.set(state.scheduledTasks, 'generic-report-request', genericReportTasks)
    } else {
      state.scheduledTasks['generic-report-request'] = genericReportTasks
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
