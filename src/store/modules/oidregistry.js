import OidregistryService from '@/services/OidregistryService'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  loading: {
    oids: false,
    registries: false
  },
  loaded: {
    oids: false,
    registries: false
  },
  oids: [],
  registries: []
}

const getters = {
  oids: (state) => state.oids,
  registries: (state) => state.registries,
  isLoaded,
  isLoading
}

const actions = {
  setOids ({ commit }, parametersFiltered) {
    commit('setLoading', { key: 'oids', status: true })
    const params = {}
    // prepare params for API (only params that have value)
    if (parametersFiltered.length > 0) {
      parametersFiltered.forEach((param) => {
        for (const key in param) {
          params[key] = param[key].toString()
        }
      })
    }
    return OidregistryService.getOIDList(params)
      .then((response) => {
        commit('setOids', response.data.oid ?? [])
        commit('setLoaded', { key: 'oids', status: true })
        return response.data.oid
      })
      .catch((error) => { throw error })
      .finally(() => {
        commit('setLoading', { key: 'oids', status: false })
      })
  },
  setRegistries ({ commit }) {
    commit('setLoading', { key: 'registries', status: true })
    return OidregistryService.getOIDRegistries()
      .then((res) => {
        commit('setRegistries', res.data.registry)
        commit('setLoaded', { key: 'oids', status: true })
        return res.data.registry
      })
      .catch(err => { throw err })
      .finally(() => {
        commit('setLoading', { key: 'registries', status: false })
      })
  }

}

const mutations = {
  changeLoadingOids (state, status) {
    state.loadingOids = status
  },
  setOids (state, oids) {
    state.oids = oids
  },
  setRegistries (state, registries) {
    state.registries = registries
  },
  setLoaded,
  setLoading
}

export default {
  namespaced: true,
  actions,
  mutations,
  state,
  getters
}
