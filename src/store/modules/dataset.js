import DataSetService from '@/services/DataSetService'
import Vue from 'vue'
import { cloneDeep, isEmpty } from 'lodash'
import { v4 as getUuid } from 'uuid'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  abortController: {
    concept: null,
    dataset: null
  },
  activeAssociationDatasetId: null,
  activeAssociationDatasetVersion: null,
  activeAssociationId: null,
  activeAssociationType: null,
  activeAssociationVersion: null,
  activeItem: null,
  datasetItems: [],
  datasetItem: null,
  datasetItemFull: null, // The complete dataset item, including concept list with value domain etc. Used for diagrams
  conceptItems: [],
  conceptItem: null,
  expandedSections: {
    description: getSessionStorage()?.dataset?.expandedSections?.description || false,
    historySection: getSessionStorage()?.dataset?.expandedSections?.historySection || false,
    metadata: getSessionStorage()?.dataset?.expandedSections?.metadata || false,
    relationshipSection: getSessionStorage()?.dataset?.expandedSections?.relationshipSection || false,
    usageSection: getSessionStorage()?.dataset?.expandedSections?.usageSection || false
  },
  hideRealtimeUpdateNotifications: {
    datasetItems: false
  },
  listenerData: {
    deInherit: null
  },
  loading: {
    activeItem: false,
    datasetItems: false,
    conceptItem: false,
    conceptItems: false,
    datasetItemFull: false
  },
  loaded: {
    activeItem: false,
    datasetItems: false,
    conceptItems: false,
    datasetItemFull: false
  },
  loincImportActive: false, // boolean indicating if LOINC import is active, used by BrowserIndex view to display panel
  showNewDatasetDialog: false,
  tree: {
    concepts: {
      property: []
    },
    datasets: {
      property: []
    }
  }
}

const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  abortController: state => selector => {
    return state.abortController?.[selector]
  },
  activeAssociationType (state) {
    return state.activeAssociationType
  },
  activeAssociationDatasetId (state) {
    return state.activeAssociationDatasetId
  },
  activeAssociationDatasetVersion (state) {
    return state.activeAssociationDatasetVersion
  },
  activeAssociationId (state) {
    return state.activeAssociationId
  },
  activeAssociationVersion (state) {
    return state.activeAssociationVersion
  },
  activeItem (state) {
    return state.activeItem
  },
  conceptItem (state) {
    return state.conceptItem
  },
  conceptItems (state) {
    return state.conceptItems
  },
  datasetItem (state) {
    return state.datasetItem
  },
  datasetItems (state) {
    return state.datasetItems
  },
  hideRealtimeUpdateNotifications: state => type => state.hideRealtimeUpdateNotifications[type],
  listenerData: state => type => state.listenerData[type],
  items (state) {
    return state.items
  },
  loincImportActive (state) {
    return state.loincImportActive
  },
  isLoaded,
  isLoading
}

const actions = {
  addKeywordPropertiesToTree ({ state, commit }, { type, properties }) {
    const treeProperties = state.tree[type]?.property || []
    const keywordProperties = properties.filter((property) => property.name === 'Keyword')

    keywordProperties.forEach((property) => {
      if (!treeProperties.includes(property)) {
        commit('addPropertyToTree', { property, type })
      }
    })
  },
  handleReference ({ state, commit, dispatch, rootGetters }, { source, target = {}, mode, associations = false }) {
    return new Promise((resolve, reject) => {
      // Get id and effectiveDate from the source object
      const { id, effectiveDate } = source || {}

      if (!id || !effectiveDate) {
        return reject(new Error('no-id-or-effectiveDate-on-source-object'))
      }
      if (!['designcopy', 'containment', 'to-copy', 'to-designcopy'].includes(mode)) {
        return reject(new Error('invalid-mode-for-target-type'))
      }

      // Broadcast for de-inherit / de-contain
      const broadcast = {}
      if (['to-copy', 'to-designcopy'].includes(mode)) {
        const prefix = rootGetters['project/getProjectInFocus']?.prefix
        broadcast.broadcast = `POST/${prefix}/conceptDeInherit`
      }

      // Get concept in editable state
      DataSetService.getEditableDatasetConcept(id, effectiveDate, null, {
        targetType: mode,
        targetId: target.id || null,
        targetEffectiveDate: target.effectiveDate || null,
        generateConceptListIds: mode === 'to-copy',
        associations
      }, broadcast)
        .then(response => {
          const newConceptItem = response?.data

          // Get a conceptItems object and replace the old concept with the new concept item
          const updatedConceptItems = this.$filters.listHelper.replaceItemInList({
            items: state.conceptItems,
            item: newConceptItem,
            children: 'concept',
            handler: (match) => match?.id === id
          })

          // Save the dataset
          const datasetToUpdate = cloneDeep(state.datasetItem)
          datasetToUpdate.concept = updatedConceptItems

          dispatch('updateDatasetItem', { item: { id: datasetToUpdate.id, effectiveDate: datasetToUpdate.effectiveDate }, data: datasetToUpdate, params: { treeonly: true } })
            .then(updatedDataset => {
            // Get the updated concept item from the updated dataset
              const updatedConceptItem = this.$filters.listHelper.findItemInList({
                items: updatedDataset?.concept,
                children: 'concept',
                handler: (match) => match.id === id && match.effectiveDate === effectiveDate
              })

              commit('setConceptItem', null)
              commit('setActiveItem', updatedConceptItem)
              commit('setConceptItems', updatedDataset.concept)
              dispatch('loadConceptItem', { activeItem: updatedConceptItem, associations: true }).then(() => {
                resolve(updatedConceptItem)
              })
            })
            .catch((error) => reject(error))
        }).catch((error) => reject(error))
    })
  },
  /**
   * Load dataset items.
   * @param commit
   * @param rootGetters
   * @returns {Promise<unknown>}
   */
  loadDatasetItems ({ commit, rootGetters }, params = {}) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix
      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      // Mark datasets as loading.
      commit('setLoading', { key: 'datasetItems', status: true })

      // Load dataset list.
      return DataSetService.getDataSetList({ prefix: projectPrefix, ...params }).then((response) => {
        // Get dataset items.
        const datasetItems = response?.data?.dataset || []
        // Set dataset items.
        commit('setDatasetItems', datasetItems)
        // Mark datasets as not loading.
        commit('setLoading', { key: 'datasetItems', status: false })
        // Mark datasets as loaded.
        commit('setLoaded', { key: 'datasetItems', status: true })
        // Resolve.
        resolve(datasetItems)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Load concept items.
   * @param commit
   * @param rootGetters
   * @param item
   * @param silent
   * @returns {Promise<unknown>}
   */
  loadConceptItems ({ commit, rootGetters }, { item, silent = false, abortController }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Check if silent is false.
      if (silent === false) {
        // Mark concepts as loading.
        commit('setLoading', { key: 'conceptItems', status: true })
      }

      // Get id and effective date from active item
      const { id, effectiveDate } = item

      // Load concept items.
      return DataSetService.getDataSetTree(id, effectiveDate, abortController).then((response) => {
        // Get dataset
        const dataset = response?.data || null
        // Map dataset name
        dataset.name.map((name) => {
          // Set selector name.
          name.selectorName = name?.['#text'] || ''
          if (dataset.versionLabel) {
            name.selectorName += ` (${dataset.versionLabel})`
          }
          // Return name.
          return name
        })
        // Set dataset
        commit('setDatasetItem', dataset)
        // Reset full dataset item
        commit('setDatasetItemFull', null)
        commit('setLoading', { key: 'datasetItemFull', status: false })
        commit('setLoaded', { key: 'datasetItemFull', status: false })

        // Get concept items.
        const conceptItems = response?.data?.concept || []
        // Set concept items.
        commit('setConceptItems', conceptItems)
        // Mark concepts as not loading.
        commit('setLoading', { key: 'conceptItems', status: false })
        // Mark concepts as loaded.
        commit('setLoaded', { key: 'conceptItems', status: true })
        // Resolve.
        resolve(conceptItems)
      }).catch((error) => reject(error))
    })
  },

  getFullDataset ({ commit, rootGetters }, { id, effectiveDate, abortController }) {
    return new Promise((resolve, reject) => {
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      if (!projectPrefix) {
        return reject(new Error('no-project-selected'))
      }
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      commit('setLoading', { key: 'datasetItemFull', status: true })
      commit('setLoaded', { key: 'datasetItemFull', status: false })

      return DataSetService.getDataSetTree(id, effectiveDate, abortController, { treeonly: true, conceptproperty: 'desc valueDomain' })
        .then((response) => {
          const dataset = response.data || null

          if (Array.isArray(dataset.name)) {
            dataset.name.map((name) => {
              name.selectorName = name?.['#text'] || ''
              if (dataset.versionLabel) {
                name.selectorName += ` (${dataset.versionLabel})`
              }
              return name
            })
          }

          commit('setDatasetItemFull', dataset)
          commit('setLoaded', { key: 'datasetItemFull', status: true })
          resolve(dataset)
        })
        .catch((error) => reject(error))
        .finally(() => {
          commit('setLoading', { key: 'datasetItemFull', status: false })
        })
    })
  },

  /**
   * Load specific concept item.
   * @param commit
   * @param rootGetters
   * @param activeItem
   * @param associations
   * @returns {Promise<unknown>}
   */
  loadConceptItem ({ commit, rootGetters }, { activeItem, associations = false, params = {}, abortController }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      // Check if id or effective date are undefined.
      if (!activeItem?.id === true || !activeItem?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }
      // Get id and effective date from active item
      const { id, effectiveDate } = activeItem
      commit('setLoading', { key: 'conceptItem', status: true })
      // Load concept items.
      return DataSetService.getDatasetConcept(id, effectiveDate, associations, params, abortController).then((response) => {
        // Get concept item.
        const conceptItem = response?.data
        // Set concept items.
        commit('setConceptItem', conceptItem)
        // Resolve.
        resolve(conceptItem)
      })
        .catch((error) => reject(error))
        .finally(() => {
          if (!abortController?.signal?.aborted) {
            commit('setLoading', { key: 'conceptItem', status: false })
          }
        })
    })
  },

  getConceptUsage ({ commit }, { id, effectiveDate, params }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      return DataSetService.getDatasetConceptUsage(id, effectiveDate, params).then(response => {
        const usage = response?.data
        resolve(usage)
      }).catch(err => reject(err))
    })
  },
  getDatasetUsage ({ commit }, { id, effectiveDate, params }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      return DataSetService.getDatasetUsage(id, effectiveDate, params).then(response => {
        const usage = response?.data
        resolve(usage)
      }).catch(err => reject(err))
    })
  },

  /**
   * Set active item.
   * @param commit
   * @param item
   */
  setActiveItem ({ commit }, item) {
    // Commit set active item.
    commit('setActiveItem', item)
  },

  /**
   * Update dataset item.
   * @param commit
   * @param dispatch
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async updateDatasetItem ({ commit, rootGetters }, { item, data, params = {} }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate, uuid } = item
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      // Patch dataset concepts.
      DataSetService.updateDataset(
        id,
        effectiveDate,
        data,
        params,
        { broadcast: `PUT/${prefix}/dataset` }
      ).then((response) => {
        // Prepare patched item.
        const updatedItem = response.data
        updatedItem.name.map((name) => {
          // Set selector name.
          name.selectorName = `${name?.['#text'] || ''} ${(updatedItem?.versionLabel != null) ? `(${updatedItem.versionLabel})` : ''}`
          return name
        })

        // Update datasetItems.datasetItem
        const datasetItemForList = cloneDeep(updatedItem)
        delete datasetItemForList.concept
        datasetItemForList.uuid = uuid
        commit('updateDatasetItem', { item, data: datasetItemForList })

        // Update datasetItem
        commit('setDatasetItem', updatedItem)

        // Update conceptItems.
        const conceptItems = updatedItem?.concept || []
        commit('setConceptItems', conceptItems)

        resolve(updatedItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Patch dataset item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async patchDatasetItem ({ commit, rootGetters }, { item, data, params }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      // Patch dataset concepts.
      DataSetService.patchDataset(
        id,
        effectiveDate,
        data,
        params,
        { broadcast: `PATCH/${prefix}/dataset` }
      ).then((response) => {
        // Get patched item.
        const patchedItem = { uuid: item.uuid, ...response?.data, concept: null }
        // Map patched item name.
        patchedItem.name.map((name) => {
          // Set selector name.
          name.selectorName = `${name?.['#text'] || ''} ${(patchedItem?.versionLabel != null) ? `(${patchedItem.versionLabel})` : ''}`
          // Return name.
          return name
        })

        // Commit update dataset in items mutation.
        commit('updateDatasetItem', { item, data: patchedItem })
        commit('setDatasetItem', { ...patchedItem })
        // Resolve response.
        resolve(patchedItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Patch concept item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async patchConceptItem ({ commit, rootGetters, state }, { item, data, params }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      // Patch dataset concepts.
      DataSetService.patchDatasetConcept(
        id,
        effectiveDate,
        data,
        params,
        { broadcast: `PATCH/${prefix}/concept` }
      ).then((response) => {
        // Get patched item.
        const patchedItem = response?.data

        // If type of patchedItem is 'group', the concept array is not comming back in the response
        if (patchedItem.type === 'group' && !patchedItem.concept) {
          const oldItemInList = this.$filters.listHelper.findItemInList({
            items: state.conceptItems,
            children: 'concept',
            handler: (match) => match.id === patchedItem.id && match.effectiveDate === patchedItem.effectiveDate
          })
          patchedItem.concept = oldItemInList.concept
        }

        // Commit update dataset in items mutation.
        commit('updateConceptItem', { item, data: patchedItem })
        // Update current concept item.
        commit('setConceptItem', { ...patchedItem })
        commit('updateActiveConceptItem', patchedItem)
        // Resolve response.
        resolve(patchedItem)
      }).catch((error) => reject(error))
    })
  },

  postConceptStatus ({ commit, rootGetters }, { params, id, effectiveDate }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id or effectiveDate missing'))
      }
      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      DataSetService.postConceptStatus(id, effectiveDate, params, { broadcast: `POST/${prefix}/conceptStatus` })
        .then(response => {
          const newConcept = response.data
          commit('handleRecursiveConceptStatusUpdate', newConcept)
          resolve()
        })
        .catch(error => reject(error))
    })
  },

  postDatasetStatus ({ commit, rootGetters }, { params, id, effectiveDate }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id or effectiveDate missing'))
      }
      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      DataSetService.updateDatasetStatus(id, effectiveDate, params, { broadcast: `POST/${prefix}/datasetStatus` })
        .then(response => {
          const report = response.data
          commit('handleRecursiveDatasetStatusUpdate', report)
          resolve(report)
        })
        .catch(err => reject(err))
    })
  },

  /**
   * Create dataset item.
   * @param commit
   * @param dispatch
   * @param rootGetters
   * @param newDataset
   * @returns {Promise<unknown>}
   */
  async createDatasetItem ({ commit, dispatch, rootGetters }, newDataset) {
    // newDataset is used to update the newly created dataset in 'step two'
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      return DataSetService.createDataset({ prefix: projectPrefix }).then((response) => {
        // update newly created dataset with the values from the user
        return dispatch('updateDatasetAfterCreate', { newDataset, dataset: response.data })
          .then((newDataset) => resolve(newDataset))
          .catch((error) => reject(error))
      }).catch((error) => reject(error))
    })
  },
  async cloneDatasetItem ({ commit, dispatch, rootGetters }, params) {
    return new Promise((resolve, reject) => {
      // Check if project prefix is passed.
      if (!params?.prefix) {
        // Reject error.
        return reject(new Error(this.$t('project-prefix-required')))
      }

      return DataSetService.createDataset(params, { broadcast: `POST/${params.prefix}/dataset` }).then((response) => {
        const clonedDataset = response?.data
        const { id, effectiveDate } = clonedDataset || {}
        if (!id || !effectiveDate) {
          return reject(new Error('no-id-or-effective-date'))
        }
        DataSetService.updateDatasetStatus(id, effectiveDate, { recurse: true, statusCode: 'draft' }, { broadcast: `POST/${params.prefix}/datasetStatus` })
          .then((response) => {
            const statusChangeIsSuccess = !!response.data.success?.find(report => report.id === clonedDataset.id)
            if (statusChangeIsSuccess) {
              clonedDataset.statusCode = 'draft'
            }
            clonedDataset.uuid = getUuid()
            delete clonedDataset.concept
            if (clonedDataset.name) {
              // Set selector name.
              clonedDataset.name.map((name) => {
                name.selectorName = `${name?.['#text'] || ''} (${this.$filters.formatDateShort(clonedDataset.effectiveDate)})`
                return name
              })
            }

            commit('addDatasetItemToList', clonedDataset)
            resolve(clonedDataset)
          })
      }).catch((error) => reject(error))
    })
  },

  // Clear all locks on dataset and delete concepts with status 'new'
  async clearDatasetLocks ({ commit, rootGetters }, dataset) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = dataset || {}

      if (!id || !effectiveDate) {
        return reject(new Error(this.$t('no-dataset-selected')))
      }

      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      return DataSetService.clearDatasetLocks(id, effectiveDate, { params: { treeonly: true }, broadcast: `POST/${prefix}/datasetClearLocks` }).then((response) => {
        const updatedDataset = response.data
        commit('setDatasetItem', updatedDataset)
        commit('setConceptItems', updatedDataset.concept)

        resolve(updatedDataset)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Update dataset after create.
   * @param commit
   * @param rootGetters
   * @param newDataset
   * @param dataset
   * @returns {Promise<unknown>}
   */
  async updateDatasetAfterCreate ({ commit, rootGetters }, { newDataset, dataset }) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      const potentialAttributes = ['name', 'desc']
      const data = {
        parameter: []
      }
      potentialAttributes.forEach((attr) => {
        if (
          newDataset[attr] != null &&
          newDataset[attr] instanceof Array &&
          newDataset[attr].some((a) => a['#text'] !== '' && a['#text'].length > 0)
        ) {
          newDataset[attr].forEach((a) => {
            if (a['#text'] !== '' && a['#text'].length > 0) {
              data.parameter.push({
                op: 'replace',
                path: `/${attr}`,
                value: {
                  [attr]: {
                    language: a.language,
                    '#text': a['#text']
                  }
                }
              })
            }
          })
        }
      })
      const { id, effectiveDate } = dataset
      return DataSetService.patchDataset(id, effectiveDate, data, null, { broadcast: `POST/${prefix}/dataset` })
        .then((response) => {
          const dataset = response.data

          // Prepare the dataset for the list
          dataset.uuid = getUuid()
          delete dataset.concept
          if (dataset.name) {
            // Set selector name.
            dataset.name.map((name) => {
              name.selectorName = `${name?.['#text'] || ''} ${(dataset?.versionLabel != null) ? `(${dataset.versionLabel})` : ''}`
              return name
            })
          }

          commit('addDatasetItemToList', dataset)
          resolve(dataset)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  /**
   * Create concept item.
   * @param state
   * @param commit
   * @param getters
   * @param dispatch
   * @param type
   * @param mode
   * @param item
   * @param target
   * @returns {Promise<unknown>}
   */
  async createConceptItem ({ state, commit, rootGetters }, { type, mode, item, target }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if no dataset item id is defined.
      if (!state.datasetItem?.id || !state.datasetItem?.effectiveDate) {
        // Reject error.
        return reject(new Error(this.$t('no-dataset-selected')))
      }

      // Get id and effective date from dataset item.
      const { id, effectiveDate } = state.datasetItem

      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      // Create new concept item.
      return DataSetService.createDatasetConcept(id, effectiveDate, type, mode, item?.id, { broadcast: `POST/${prefix}/concept` }).then((response) => {
        // Get new concept item.
        const newConceptItem = response?.data
        // Add new concept item.
        commit('addNewConceptItem', { item, newConceptItem, mode })
        resolve(newConceptItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * New dataset
   */
  setShowNewDatasetDialog ({ commit }, status) {
    commit('setShowNewDatasetDialog', status)
  },

  /**
   * Associations
   */
  changeActiveAssociationType ({ state, commit, rootState }, associationType) {
    /**
     * set Association type
     * Comes in handy when navigating to Terminology browser
     * Needed for context.
     */
    commit('changeActiveAssociationType', associationType)
    if (associationType === null) {
      // also reset id and version
      commit('changeActiveAssociationDatasetId', null)
      commit('changeActiveAssociationDatasetVersion', null)
      commit('changeActiveAssociationId', null)
      commit('changeActiveAssociationVersion', null)
    } else if (associationType === 'transactionConceptAssociation') {
      commit('changeActiveAssociationDatasetId', rootState.scenario.scenarioItem?.id)
      commit('changeActiveAssociationDatasetVersion', rootState.scenario.scenarioItem?.effectiveDate)
      commit('changeActiveAssociationId', rootState.scenario.activeItem?.id)
      commit('changeActiveAssociationVersion', rootState.scenario.activeItem?.effectiveDate)
    } else {
      if (
        state.datasetItem?.id != null &&
        state.datasetItem?.effectiveDate != null &&
        state.conceptItem?.id != null &&
        state.conceptItem?.effectiveDate != null
      ) {
        commit('changeActiveAssociationDatasetId', state.datasetItem.id)
        commit('changeActiveAssociationDatasetVersion', state.datasetItem.effectiveDate)
        commit('changeActiveAssociationId', state.conceptItem.id)
        commit('changeActiveAssociationVersion', state.conceptItem.effectiveDate)
      }
    }
  },
  removeConceptAssociation ({ state, commit, rootState, rootGetters }, { association, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate, inherit } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const inheritRef = inherit?.[0]?.ref
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (id == null || effectiveDate == null) {
        reject(new Error('no-concept-selected'))
        return false
      }

      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }

      if (!association) {
        reject(new Error('no-association-passed'))
        return false
      }
      association = cloneDeep(association)
      const isRestoreToDatasetOperation = isEmpty(association)
      association.conceptFlexibility = effectiveDate
      association.conceptId = id
      const data = { parameter: [] }
      data.parameter.push({
        op: 'remove',
        path: '/terminologyAssociation',
        value: { terminologyAssociation: association }
      })

      return DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationConcept` })
        .then((response) => {
          const associationsList = response.data?.terminologyAssociation
          if (Array.isArray(associationsList)) {
            const conceptAssociations = associationsList.filter(item => !!item.conceptId && [id, inheritRef].includes(item.conceptId))
            commit('addAssociationToConcept', conceptAssociations)
          } else if (isRestoreToDatasetOperation) {
            // This is a 'restore to dataset associations' operation, and there are no associations defined on the dataset
            commit('addAssociationToConcept', [])
          } else {
            const index = state.conceptItem?.terminologyAssociation?.indexOf(association)
            if (index > -1) {
              commit('removeConceptAssociation', index)
            }
          }
          resolve()
        })
        .catch((error) => {
          const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
          reject(new Error(errorMessage))
        })
    })
  },
  removeValuesetAssociation ({ state, getters, rootGetters, commit, rootState }, { association, params }) {
    return new Promise((resolve, reject) => {
      if (!association) {
        reject(new Error('data-is-missing'))
      }
      // If the passed association has nothing but a conceptId, it is a 'restore to dataset' operation
      const associationCopy = { ...association }
      delete associationCopy.conceptId
      const isRestoreToDatasetOperation = isEmpty(associationCopy)

      const { id, effectiveDate } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!id || !effectiveDate) {
        reject(new Error('no-concept-selected'))
        return false
      }

      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }

      const data = {
        parameter: [
          {
            op: 'remove',
            path: '/terminologyAssociation',
            value: { terminologyAssociation: association }
          }
        ]
      }

      if (typeof params?.transactionEffectiveDate === 'string') {
        params.transactionEffectiveDate = encodeURIComponent(params.transactionEffectiveDate)
      }

      return DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationValueset` })
        .then(response => {
          const associationsList = response.data?.terminologyAssociation
          if (associationsList && Array.isArray(associationsList)) {
            const valueSetAssociations = associationsList.filter(item => item.valueSet)
            commit('addValueSetAssociationToConcept', valueSetAssociations)
          } else if (isRestoreToDatasetOperation) {
            // This is a 'restore to dataset associations' operation, and there are no associations defined on the dataset
            commit('addValueSetAssociationToConcept', [])
          } else {
            const index = getters.conceptItem?.valueDomain?.[0]?.conceptList?.[0]?.terminologyAssociation?.indexOf(association)
            if (index > -1) {
              commit('removeValuesetAssociation', index)
            }
          }
          resolve()
        })
        .catch((error) => {
          const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
          reject(new Error(errorMessage))
        })
    })
  },
  removeIdentifierAssociation ({ state, getters, rootGetters, commit, rootState }, { association, params }) {
    return new Promise((resolve, reject) => {
      if (!association) {
        return reject(new Error('Missing association'))
      }
      const { id, effectiveDate, inherit } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const inheritRef = inherit?.[0]?.ref
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!id || !effectiveDate) {
        return reject(new Error('No concept selected'))
      }

      if (!prefix) {
        return reject(new Error('no-project-selected'))
      }

      const data = {
        parameter: [
          {
            op: 'remove',
            path: '/identifierAssociation',
            value: { identifierAssociation: association }
          }
        ]
      }

      return DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationIdentifier` })
        .then(response => {
          const associationsList = response.data?.identifierAssociation || []
          const identifierAssociations = associationsList.filter(item => !!item.conceptId && [id, inheritRef].includes(item.conceptId))
          commit('addIdentifierAssociationToConcept', identifierAssociations)
          resolve(associationsList)
        })
        .catch((error) => reject(error))
    })
  },
  removeValuesetCodeAssociation ({ state, rootGetters, rootState, commit }, { association, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!prefix) {
        reject(new Error('Prefix is missing'))
        return false
      }
      if (!id || !effectiveDate) {
        reject(new Error('Id or effectiveDate missing'))
        return
      }
      if (!association) {
        reject(new Error('Association is missing'))
        return
      }
      if (!association.conceptId) {
        reject(new Error('conceptId is missing'))
        return
      }
      const data = {
        parameter: [
          {
            op: 'remove',
            path: '/terminologyAssociation',
            value: {
              terminologyAssociation: association
            }
          }
        ]
      }

      DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationValuesetCode` })
        .then(response => {
          const associationsList = response.data?.terminologyAssociation
          if (Array.isArray(associationsList)) {
            commit('addValueSetCodeAssociationsToConcept', associationsList)
          }

          resolve(associationsList)
        })
        .catch(err => reject(err))
    })
  },
  // conceptAssociation
  addAssociationToConcept ({ state, commit, rootState, rootGetters }, { association, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate, inherit } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const inheritRef = inherit?.[0]?.ref
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (id == null || effectiveDate == null) {
        reject(new Error('no-concept-selected'))
        return false
      }
      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }

      if (!association) {
        reject(new Error('no-association-passed'))
        return false
      }

      association.conceptFlexibility = effectiveDate
      association.conceptId = id
      const data = { parameter: [] }
      data.parameter.push({
        op: 'add',
        path: '/terminologyAssociation',
        value: { terminologyAssociation: association }
      })

      return DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationConcept` })
        .then((response) => {
          const associationsList = response.data?.terminologyAssociation
          if (associationsList) {
            const conceptAssociations = associationsList.filter(item => !!item.conceptId && [id, inheritRef].includes(item.conceptId))
            commit('addAssociationToConcept', conceptAssociations)
            resolve(response)
          } else {
            reject(new Error('no-data'))
          }
        })
        .catch((error) => {
          const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
          reject(new Error(errorMessage))
        })
    })
  },
  // valueSetAssociation
  addValueSetAssociationToConcept ({ state, rootGetters, commit, rootState }, { association, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const conceptId = state.conceptItem?.valueDomain?.[0]?.conceptList?.[0]?.id
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!prefix) {
        reject(new Error('no-project-selected'))
        return false
      }
      if (!id || !effectiveDate) {
        reject(new Error('no-concept-selected'))
        return
      }
      if (!conceptId) {
        reject(new Error('The dataset concept does not have a concept list.'))
        return
      }
      if (!association) {
        reject(new Error('no-association-passed'))
        return
      }
      association.conceptId = conceptId
      const data = {
        parameter: [
          {
            op: 'replace',
            path: '/terminologyAssociation',
            value: {
              terminologyAssociation: association
            }
          }
        ]
      }

      DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationValueset` })
        .then(response => {
          const associationsList = response.data?.terminologyAssociation
          if (associationsList && Array.isArray(associationsList)) {
            const valueSetAssociations = associationsList.filter(item => item.valueSet)
            commit('addValueSetAssociationToConcept', valueSetAssociations)
          }

          resolve(associationsList)
        })
        .catch(err => reject(err))
    })
  },
  // valueSetCodeAssociation
  addvalueSetCodeAssociationToConcept ({ state, rootState, rootGetters, commit }, { association, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (!prefix) {
        reject(new Error('Prefix is missing'))
        return false
      }
      if (!id || !effectiveDate) {
        reject(new Error('Id or effectiveDate missing'))
        return
      }
      if (!association) {
        reject(new Error('Association is missing'))
        return
      }
      if (!association.conceptId) {
        reject(new Error('conceptId is missing'))
        return
      }
      const data = {
        parameter: [
          {
            op: 'add',
            path: '/terminologyAssociation',
            value: {
              terminologyAssociation: association
            }
          }
        ]
      }

      DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationValuesetCode` })
        .then(response => {
          const associationsList = response.data?.terminologyAssociation
          if (Array.isArray(associationsList)) {
            commit('addValueSetCodeAssociationsToConcept', associationsList)
          }

          resolve(associationsList)
        })
        .catch(err => reject(err))
    })
  },

  addIdentifierAssociationToConcept ({ state, commit, rootState, rootGetters }, { associations, params }) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate, inherit } = params?.transactionId ? rootState.scenario.activeItem : state.activeItem
      const inheritRef = inherit?.[0]?.ref
      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      if (id == null || effectiveDate == null) {
        return reject(new Error('No concept selected'))
      }
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      if (!Array.isArray(associations)) {
        return reject(new Error('Expected an array with associations'))
      }

      const data = { parameter: [] }

      associations.forEach(association => {
        association.conceptFlexibility = effectiveDate
        association.conceptId = id

        data.parameter.push({
          op: 'add',
          path: '/identifierAssociation',
          value: { identifierAssociation: association }
        })
      })

      return DataSetService.patchDatasetConceptMap(id, effectiveDate, data, params, { broadcast: `PATCH/${prefix}/conceptAssociationIdentifier` })
        .then((response) => {
          const associationsList = response.data?.identifierAssociation
          if (associationsList) {
            const identifierAssociations = associationsList.filter(item => !!item.conceptId && [id, inheritRef].includes(item.conceptId))
            commit('addIdentifierAssociationToConcept', identifierAssociations)
            resolve(response)
          } else {
            reject(new Error('no-data'))
          }
        })
        .catch((error) => {
          const errorMessage = error?.response?.data?.description || error?.response?.statusText || 'Error'
          reject(new Error(errorMessage))
        })
    })
  },
  setLoincImportActive ({ commit }, status) {
    commit('setLoincImportActive', status)
  }
}

const mutations = {
  setAbortController (state, { type, value }) {
    state.abortController[type] = value
  },
  updateActiveConceptItem (state, updatedItem) {
    if (!state.activeItem) return

    const fieldsToUpdate = ['statusCode', 'versionLabel', 'expirationDate', 'lastModifiedDate', 'name']
    fieldsToUpdate.forEach(prop => {
      if (updatedItem[prop] !== undefined) {
        state.activeItem[prop] = updatedItem[prop]
      }
    })
  },
  /**
   * Update statusCode, versionLabel and expirationDate
   *     state.conceptItems is always updated
   *     state.conceptItem and state.activeItem are updated if affected
   */
  handleRecursiveConceptStatusUpdate (state, conceptItem) {
    // Update the list of conceptItems
    state.conceptItems = this.$filters.listHelper.replaceItemInList({
      items: state.conceptItems,
      children: 'concept',
      item: conceptItem,
      handler: match => conceptItem.id === match.id && conceptItem.effectiveDate === match.effectiveDate
    })

    const fieldsToUpdate = ['statusCode', 'versionLabel', 'expirationDate', 'lastModifiedDate']

    // update conceptItem
    const affectedConceptItem = this.$filters.listHelper.findItemInList({
      items: [conceptItem],
      children: 'concept',
      handler: match => match.id === state.conceptItem.id && match.effectiveDate === state.conceptItem.effectiveDate
    })

    if (affectedConceptItem) {
      const newConceptItem = cloneDeep(state.conceptItem)
      fieldsToUpdate.forEach(prop => {
        newConceptItem[prop] = affectedConceptItem[prop]
      })
      state.conceptItem = newConceptItem
    }

    // update activeItem
    const affectedActiveItem = this.$filters.listHelper.findItemInList({
      items: [conceptItem],
      children: 'concept',
      handler: match => match.id === state.activeItem.id && match.effectiveDate === state.activeItem.effectiveDate
    })

    if (affectedActiveItem) {
      fieldsToUpdate.forEach(prop => {
        state.activeItem[prop] = affectedActiveItem[prop]
        state.activeItem.concept = affectedActiveItem.concept
        state.activeItem.navkey = affectedActiveItem.navkey
      })
    }
  },

  handleRecursiveDatasetStatusUpdate (state, report) {
    const successfulChanges = report.success
    const fieldsToUpdate = ['statusCode', 'versionLabel', 'expirationDate', 'lastModifiedDate']

    if (Array.isArray(successfulChanges)) {
      successfulChanges.forEach(change => {
        const { id, effectiveDate, itemcode, statusCode, versionLabel, expirationDate } = change

        if (itemcode === 'DS') {
          // Update datasetItems.datasetItem
          const itemInList = state.datasetItems?.find(ds => ds.id === id && ds.effectiveDate === effectiveDate)
          if (!itemInList) {
            return
          }
          const newItem = { ...itemInList, statusCode }
          if (versionLabel != null) {
            newItem.versionLabel = versionLabel
          }
          if (expirationDate != null) {
            newItem.expirationDate = expirationDate
          }
          state.datasetItems = this.$filters.listHelper.replaceItemInList({
            items: state.datasetItems,
            item: newItem,
            children: null,
            handler: (match) => match.id === itemInList.id && match.effectiveDate === itemInList.effectiveDate
          })

          // Update activeItem if it is the currently selected item
          if (state.activeItem?.id === id && state.activeItem?.effectiveDate === effectiveDate) {
            fieldsToUpdate.forEach(prop => {
              if (newItem[prop]) {
                state.activeItem[prop] = newItem[prop]
              }
            })
          }

          // Update datasetItem if it is the currently selected item
          if (state.datasetItem?.id === id && state.datasetItem?.effectiveDate === effectiveDate) {
            state.datasetItem = newItem
          }
        } else if (itemcode === 'DE') {
          // Update conceptItems.conceptItem
          const itemInList = this.$filters.listHelper.findItemInList({
            items: state.conceptItems,
            children: 'concept',
            handler: match => match.id === id && match.effectiveDate === effectiveDate
          })
          if (!itemInList) {
            return
          }

          const newItem = { ...itemInList, statusCode }
          if (versionLabel != null) {
            newItem.versionLabel = versionLabel
          }
          if (expirationDate != null) {
            newItem.expirationDate = expirationDate
          }

          state.conceptItems = this.$filters.listHelper.replaceItemInList({
            items: state.conceptItems,
            item: newItem,
            children: 'concept',
            handler: (match) => match?.id === id && match.effectiveDate === effectiveDate
          })

          // Update activeItem if it is the currently selected item
          if (state.activeItem?.id === id && state.activeItem?.effectiveDate === effectiveDate) {
            fieldsToUpdate.forEach(prop => {
              if (newItem[prop]) {
                state.activeItem[prop] = newItem[prop]
              }
            })
          }

          // Update conceptItem if it is the currently selected item
          if (state.conceptItem?.id === id && state.conceptItem?.effectiveDate === effectiveDate) {
            const newConceptItem = { ...state.conceptItem, statusCode }
            if (versionLabel != null) {
              newConceptItem.versionLabel = versionLabel
            }
            if (expirationDate != null) {
              newConceptItem.expirationDate = expirationDate
            }

            state.conceptItem = newConceptItem
          }
        }
      })
    }
  },

  /**
   * Init dataset.
   * @param state
   */
  initDataset (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },

  /**
   * Set dataset items.
   * @param state
   * @param datasetItems
   */
  setDatasetItems (state, datasetItems) {
    state.datasetItems = datasetItems
  },

  /**
   * Set dataset item.
   * @param state
   * @param datasetItem
   */
  setDatasetItem (state, datasetItem) {
    state.datasetItem = datasetItem
  },
  setDatasetItemFull (state, datasetItem) {
    state.datasetItemFull = datasetItem
  },

  /**
   * Set concept items.
   * @param state
   * @param conceptItems
   */
  setConceptItems (state, conceptItems) {
    state.conceptItems = conceptItems
  },

  /**
   * Set concept item.
   * @param state
   * @param conceptItem
   */
  setConceptItem (state, conceptItem) {
    state.conceptItem = conceptItem
  },

  /**
   * Set active item.
   * @param state
   * @param activeItem
   */
  setActiveItem (state, activeItem) {
    state.activeItem = activeItem
  },

  /**
   * Update dataset item.
   * @param state
   * @param item
   * @param data
   */
  updateDatasetItem (state, { item, data }) {
    // Replace dataset with item in list.
    state.datasetItems = this.$filters.listHelper.replaceItemInList({
      items: state.datasetItems,
      item: data,
      children: null,
      handler: (match) => match.id === item.id && match.effectiveDate === item.effectiveDate
    })
  },

  addDatasetItemToList (state, datasetItem) {
    state.datasetItems.push(datasetItem)
  },

  /**
   * Update concept item.
   * @param state
   * @param item
   * @param data
   */
  updateConceptItem (state, { item, data }) {
    // Replace concept with item in list.
    state.conceptItems = this.$filters.listHelper.replaceItemInList({
      items: state.conceptItems,
      item: data,
      children: 'concept',
      handler: (match) => match?.id === item?.id
    })
  },

  removeConceptItem (state, item) {
    state.conceptItems = this.$filters.listHelper.removeItemFromList({
      items: state.conceptItems,
      children: 'concept',
      handler: (match) => match?.id === item?.id
    })
  },

  /**
   * Add new concept item.
   * @param state
   * @param item
   * @param newConceptItem
   * @param mode
   */
  addNewConceptItem (state, { item, newConceptItem, mode }) {
    if (!item) {
      // Add to the root level of the concept list
      if (Array.isArray(state.conceptItems)) {
        state.conceptItems.push(newConceptItem)
      } else {
        state.conceptItems = [newConceptItem]
      }
      if (state.datasetItem) {
        state.datasetItem.concept = state.conceptItems
      }
      return
    }
    // Define mappings.
    const mappings = {
      following: 'addItemAfterListItem',
      preceding: 'addItemBeforeListItem',
      into: 'unshiftItemInsideListItem'
    }

    // Get matching mapping.
    const mapping = mappings?.[mode]
    // Check if mapping is not nil.
    if (!!mapping === true) {
      // Call list helper and set updated concept items.
      const newConceptItems = this.$filters.listHelper[mapping]({
        items: state.conceptItems,
        item: newConceptItem,
        children: 'concept',
        handler: (match) => match.id === item.id
      })

      state.conceptItems = newConceptItems
      if (state.datasetItem) {
        state.datasetItem.concept = newConceptItems
      }
    }
  },

  setExpandedSection (state, { section, value }) {
    state.expandedSections[section] = !!value
  },

  /**
   * Set loading state.
   * @param state
   * @param type
   * @param status
   */
  setLoading (state, { key, status }) {
    setLoading(state, { key: 'activeItem', status })
    setLoading(state, { key, status })
  },

  /**
   * Set loaded state.
   * @param state
   * @param type
   * @param status
   */
  setLoaded (state, { key, status }) {
    setLoaded(state, { key: 'activeItem', status })
    setLoaded(state, { key, status })
  },
  setListenerData (state, { type, value }) {
    state.listenerData[type] = value
  },
  hideRealtimeUpdateNotifications (state, { type, status }) {
    state.hideRealtimeUpdateNotifications[type] = !!status
  },

  addPropertyToTree (state, { type, property }) {
    if (type !== null && property !== null) {
      state.tree[type].property.push(property)
    }
  },
  resetPropertyInTree (state, type) {
    for (const activeTreeKey in state.tree) {
      state.tree[activeTreeKey].property = []
    }
  },

  /**
   * Set show new dataset dialog.
   * @param state
   * @param status
   */
  setShowNewDatasetDialog (state, status) {
    state.showNewDatasetDialog = status
  },
  removeConceptAssociation (state, index) {
    state.conceptItem.terminologyAssociation.splice(index, 1)
  },
  removeValuesetAssociation (state, index) {
    state.conceptItem.valueDomain[0].conceptList[0].terminologyAssociation.splice(index, 1)
  },
  removeValuesetCodeAssociation (state, index) {
    state.conceptItem.valueDomain[0].conceptList[0].concept.splice(index, 1)
  },
  addAssociationToConcept (state, association) {
    // association array of associations
    if (state.conceptItem?.terminologyAssociation == null) {
      Vue.set(state.conceptItem, 'terminologyAssociation', [])
    }
    if (association instanceof Array) {
      state.conceptItem.terminologyAssociation = association
    } else {
      state.conceptItem.terminologyAssociation.push(association)
    }
  },
  addIdentifierAssociationToConcept (state, association) {
    if (state.conceptItem?.identifierAssociation == null) {
      Vue.set(state.conceptItem, 'identifierAssociation', [])
    }
    if (association instanceof Array) {
      state.conceptItem.identifierAssociation = association
    } else {
      state.conceptItem.identifierAssociation.push(association)
    }
  },
  addValueSetAssociationToConcept (state, associations) {
    if (!state.conceptItem?.valueDomain?.[0]) {
      Vue.set(state.conceptItem, 'valueDomain', [{ conceptList: [{ terminologyAssociation: [] }] }])
    }
    if (state.conceptItem.valueDomain[0].conceptList?.[0] && !state.conceptItem.valueDomain[0].conceptList[0].terminologyAssociation) {
      const newValueDomain = cloneDeep(state.conceptItem.valueDomain)
      newValueDomain[0].conceptList[0].terminologyAssociation = []
      Vue.set(state.conceptItem, 'valueDomain', newValueDomain)
    }
    state.conceptItem.valueDomain[0].conceptList[0].terminologyAssociation = associations
  },
  addValueSetCodeAssociationsToConcept (state, associations = []) {
    if (state.conceptItem.valueDomain == null) {
      Vue.set(state.conceptItem, 'valueDomain', [])
    }
    if (state.conceptItem.valueDomain[0] == null) {
      Vue.set(state.conceptItem.valueDomain, 0, [])
    }
    if (state.conceptItem.valueDomain[0].conceptList == null) {
      Vue.set(state.conceptItem.valueDomain[0], 'conceptList', [])
    }
    if (state.conceptItem.valueDomain[0].conceptList[0] == null) {
      Vue.set(state.conceptItem.valueDomain[0].conceptList, 0, [])
    }
    if (state.conceptItem.valueDomain[0].conceptList[0].concept == null) {
      Vue.set(state.conceptItem.valueDomain[0].conceptList[0], 'concept', [])
    }

    const newConceptList = state.conceptItem.valueDomain[0].conceptList[0].concept.map(concept => ({ ...concept, terminologyAssociation: [] }))
    associations.forEach(association => {
      const concept = newConceptList.find(con => con.id === association.conceptId)
      if (concept) {
        concept.terminologyAssociation.push(association)
      }
    })
    state.conceptItem.valueDomain[0].conceptList[0].concept = newConceptList
  },
  changeActiveAssociationType (state, associationType) {
    state.activeAssociationType = associationType
  },
  changeActiveAssociationDatasetId (state, id) {
    state.activeAssociationDatasetId = id
  },
  changeActiveAssociationDatasetVersion (state, version) {
    state.activeAssociationDatasetVersion = version
  },
  changeActiveAssociationId (state, id) {
    state.activeAssociationId = id
  },
  changeActiveAssociationVersion (state, version) {
    state.activeAssociationVersion = version
  },
  incrementIssueAssociation (state, type) {
    if (state[type]?.issueAssociation?.[0]?.count) {
      const newCount = parseInt(state[type].issueAssociation[0].count) + 1
      state[type].issueAssociation[0].count = newCount.toString()
    }
  },
  setLoincImportActive (state, status) {
    state.loincImportActive = status
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
