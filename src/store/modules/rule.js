import template from './template'
import profile from './profile'

export default {
  namespaced: true,
  modules: {
    profile,
    template
  }
}
