const state = {
  activeRequests: 0
}

const getters = {
  hasActiveRequests (state) {
    return state.activeRequests > 0
  }
}

const actions = {
  async decrementActiveRequests ({ commit }) {
    await setTimeout(async () => {
      commit('decrementActiveRequests')
    }, 100)
  }
}
const mutations = {
  incrementActiveRequests (state) {
    state.activeRequests++
  },

  async decrementActiveRequests (state) {
    if (--state.activeRequests < 0) {
      state.activeRequests = 0
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
