import ServerService from '@/services/ServerService'
import AuthenticationService from '@/services/AuthenticationService'

const state = () => ({
  loginDialog: false,
  to: null, // temp 'to-object' dispatched from router and app (successfull login)
  token: null,
  tokenIssued: null,
  tokenExpires: null,
  user: null,
  userInfo: null,
  isUserLoggedIn: false
})

const getters = {
  getUser: state => {
    return state.user
  },
  isInGroup: state => group => {
    try {
      const payload = JSON.parse(atob(String(state.token).split('.')?.[1]))
      // Return true if payload contains group
      return (payload?.groups || []).indexOf(group) >= 0
    } catch (exception) {
      // Do nothing.
    }

    // Return false.
    return false
  },
  getUsername: state => {
    try {
      // Try to get payload from token.
      const payload = JSON.parse(atob(String(state.token).split('.')?.[1]))
      return payload?.name || null
    } catch (exception) {
      // Do nothing.
    }

    // Return false.
    return null
  },
  getFullName (state) {
    return state.user?.fullName || null
  },
  getUserDefaultLanguage (state) {
    return state.user?.defaultLanguage
  },
  isUserLoggedIn (state) {
    return state.isUserLoggedIn
  },
  loginDialog (state) {
    return state.loginDialog
  },
  userIsDBA (state, getters) {
    return getters.isInGroup('dba')
  },

  userInfo (state) {
    return state.userInfo
  }
}

const actions = {
  login ({ dispatch }, { username, password }) {
    return new Promise((resolve, reject) => {
      AuthenticationService.login({ username, password })
        .then(res => {
          const { user, token, issued, expires } = res.data
          dispatch('setTokenData', { user, token, issued, expires })
          resolve(res.data)
        })
        .catch(error => {
          dispatch('clearTokenData')
          reject(error)
        })
    })
  },
  saveToForRedirect ({ commit }, to) {
    /**
     * function for storing the current page the user visits,
     * that needs authentication
     * @param {Object} to - To object from router (or null to reset)
     * @param {null} to - To object from router (or null to reset)
     */
    commit('saveToForRedirect', to)
  },
  setLoginDialog ({ commit }, status) {
    commit('setLoginDialog', status)
  },
  setTokenData ({ commit }, { user, token, issued, expires }) {
    commit('setUser', user)
    commit('setToken', token)
    commit('setTokenIssued', issued)
    commit('setTokenExpires', expires)
  },
  clearTokenData ({ commit }) {
    commit('setUser', null)
    commit('setToken', null)
    commit('setTokenIssued', null)
    commit('setTokenExpires', null)
  },

  getUserInfo ({ commit }, name) {
    return new Promise((resolve, reject) => {
      if (!name) {
        return reject(new Error('No name provided'))
      }

      ServerService.getUser(name)
        .then(res => {
          const userInfo = res.data
          commit('setUserInfo', userInfo)
          resolve(userInfo)
        })
        .catch(err => reject(err))
    })
  }
}

const mutations = {
  saveToForRedirect (state, to) {
    state.to = to
  },
  setLoginDialog (state, status) {
    state.loginDialog = status
  },
  setToken (state, token) {
    state.token = token
    state.isUserLoggedIn = !!(token)
  },
  setTokenIssued (state, issued) {
    state.tokenIssued = issued
  },
  setTokenExpires (state, expires) {
    state.tokenExpires = expires
  },
  setUser (state, user) {
    state.user = user
    state.isUserLoggedIn = user !== null
  },
  setUserInfo (state, userInfo) {
    state.userInfo = userInfo
  },
  setUserDefaultLanguage (state, language) {
    state.user.defaultLanguage = language
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
