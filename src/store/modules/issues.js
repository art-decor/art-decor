import IssueService from '@/services/IssueService'
import { cloneDeep } from 'lodash'

const state = {
  issue: null,
  issues: [],
  issuesTotal: 0,
  labels: [],
  loadingIssue: false,
  loadingIssues: true,
  loadingLabels: true
}

const initialState = cloneDeep(state)

const getters = {
  issue: (state) => state.issue,
  issues (state) {
    return state.issues
  },
  issuesTotal (state) {
    return state.issuesTotal
  },
  labels (state) {
    return state.labels
  },
  loadingIssue (state) {
    return state.loadingIssue
  },
  loadingIssues (state) {
    return state.loadingIssues
  },
  getIssueLabelByCode: (state) => (code) => {
    return state.labels.find(label => label.code === code)
  }
}
const actions = {
  changeLoadingIssues ({ commit }, status) {
    commit('changeLoadingIssues', status)
  },
  getIssue ({ commit }, { id }) {
    return new Promise((resolve, reject) => {
      if (!id) {
        return reject(new Error('id required'))
      }
      commit('changeLoadingIssue', true)

      IssueService.getIssue(id)
        .then(res => {
          const issue = res.data || null
          commit('setIssue', issue)
          resolve(issue)
        })
        .catch(err => reject(err))
        .finally(() => {
          commit('changeLoadingIssue', false)
        })
    })
  },
  setIssues ({ state, commit }, { filters, abortController }) {
    commit('changeLoadingIssues', true)
    return new Promise((resolve, reject) => {
      return IssueService.getIssueList(filters, abortController)
        .then((response) => {
          if (state.issues.length > 0) commit('setIssues', [])
          commit('changeIssueTotal', parseInt(response?.data?.total ?? 0))
          if (response?.data?.issue != null) {
            commit('setIssues', response?.data?.issue)
          }
          commit('changeLoadingIssues', false)
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  setLabels ({ commit, rootGetters }) {
    commit('changeLoadingLabels', true)
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected']?.prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.getIssueLabelList(rootGetters['project/getProjectSelected'].prefix)
        .then((response) => {
          commit('setLabels', [])
          if (response?.data?.label != null) {
            commit('setLabels', response?.data?.label)
          }
          commit('changeLoadingLabels', false)
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  createIssue ({ commit, rootGetters }, issue) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.createIssue(prefix, issue, { broadcast: `POST/${prefix}/issue` })
        .then((response) => {
          if (response?.data) {
            commit('addIssue', response?.data)
          }
          resolve(response)
        })
        .catch((error) => reject(error))
    })
  },
  updateIssueAttribute ({ commit, rootGetters }, { value, data }) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      const id = value.id
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      if (id == null) {
        reject(new Error('id-is-missing'))
        return
      }
      return IssueService.patchIssue(prefix, id, data, { broadcast: `PATCH/${prefix}/issue` })
        .then((response) => {
          if (response.data) {
            commit('updateIssueAttribute', response.data)
            commit('setIssue', response.data)
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  updateIssueSubscribtion ({ commit, rootGetters }, { id, value }) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected']?.prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.subscribeIssue(rootGetters['project/getProjectSelected'].prefix, id, value)
        .then(() => {
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  createIssueLabel ({ commit, rootGetters }, label) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.createIssueLabel(prefix, label, { broadcast: `POST/${prefix}/issue/label` })
        .then((response) => {
          if (response.data) {
            commit('addIssueLabel', response.data)
            resolve()
          }
        })
        .catch((error) => reject(error))
    })
  },
  updateIssueLabel ({ commit, rootGetters }, { initialItem, label }) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.putIssueLabel(prefix, initialItem.code, label, { broadcast: `PUT/${prefix}/issue/label` })
        .then((response) => {
          if (response.data) {
            commit('updateIssueLabel', { initialItem, label: response.data })
            resolve()
          }
        })
        .catch((error) => reject(error))
    })
  },
  deleteIssueLabel ({ commit, rootGetters }, label) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (prefix == null) {
        reject(new Error('no-project-selected'))
        return false
      }
      return IssueService.deleteIssueLabel(prefix, label.code, { broadcast: `DELETE/${prefix}/issue/label` })
        .then((response) => {
          if (response.status === 204) {
            commit('deleteIssueLabel', label)
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  }
}
const mutations = {
  changeLoadingIssue (state, status) {
    state.loadingIssue = status
  },
  changeLoadingIssues (state, status) {
    state.loadingIssues = status
  },
  changeLoadingLabels (state, status) {
    state.loadingLabels = status
  },
  changeIssueTotal (state, issuesTotal) {
    state.issuesTotal = issuesTotal
  },
  setIssue (state, issue) {
    state.issue = issue
  },
  setIssues (state, issues) {
    state.issues = issues
  },
  setLabels (state, labels) {
    state.labels = labels
  },
  addIssue (state, issue) {
    state.issues.push(issue)
    state.issuesTotal = state.issues.length
  },
  updateIssueAttribute (state, issue) {
    const index = state.issues.findIndex((i) => i.id === issue.id)
    if (index !== -1) {
      state.issues.splice(index, 1, issue)
    }
  },
  addIssueLabel (state, label) {
    state.labels.push(label)
  },
  updateIssueLabel (state, { initialItem, label }) {
    const index = state.labels.indexOf(initialItem)
    if (index !== -1) {
      state.labels.splice(index, 1, label)
    }
  },
  deleteIssueLabel (state, label) {
    const index = state.labels.indexOf(label)
    if (index !== -1) {
      state.labels.splice(index, 1)
    }
  },
  initIssues (state) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      state[key] = val
    }
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
