import i18n from '../../plugins/i18n'
import { cloneDeep } from 'lodash'
import { v4 as getUuid } from 'uuid'
import TemplateService from '@/services/TemplateService'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  abortController: null,
  datatypes: null,
  templates: null,
  templateActive: null,
  templateItem: null,
  templateAssociations: [],
  templateVersionIndex: 0,
  hideRealtimeUpdateNotifications: {
    templates: false
  },
  loading: {
    templates: false,
    templateItem: false,
    templateAssociations: false
  },
  loaded: {
    templates: false,
    templateItem: false,
    templateAssociations: false
  },
  expandedSections: {
    description: getSessionStorage()?.rule?.template?.expandedSections?.description || false,
    historySection: getSessionStorage()?.rule?.template?.expandedSections?.historySection || false,
    metadata: getSessionStorage()?.rule?.template?.expandedSections?.metadata || false,
    relationshipSection: getSessionStorage()?.rule?.template?.expandedSections?.relationshipSection || false,
    usageSection: getSessionStorage()?.rule?.template?.expandedSections?.usageSection || false
  },
  preserveTemplateItemOnSelect: false
}
const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  abortController: state => state.abortController,
  datatypes: state => {
    return state.datatypes
  },
  fullTemplateList: state => ({ includeDynamic = false, sort = true, initialList = state.templates } = {}) => {
    const list = []
    const templates = initialList || []
    templates.forEach(cat => {
      if (cat.template?.length) {
        cat.template.forEach(tm => {
          if (tm.version?.length) {
            tm.version.forEach(v => {
              list.push(v)
            })
          }
          if (includeDynamic) {
            // Add item, marked for dynamic flexibility
            list.push({ ...tm, flexibility: 'dynamic' })
          }
        })
      }
    })
    if (sort) {
      return list.sort((a, b) => {
        if (a.name > b.name) {
          return 1
        }
        if (a.name < b.name) {
          return -1
        }
        return 0
      })
    }
    return list
  },
  hideRealtimeUpdateNotifications: state => type => state.hideRealtimeUpdateNotifications[type],
  isLoading,
  isLoaded,
  templates: state => {
    if (Array.isArray(state.templates)) {
      // This is a temporary filter, needed until the backend has a different endpoint for fhir profiles
      return state.templates.filter(cat => cat.type !== 'fhirprofile')
    }
    return state.templates
  },
  templateActive (state) {
    return state.templateActive
  },
  templateItem (state) {
    return state.templateItem
  },
  templateVersionIndex (state) {
    return state.templateVersionIndex
  },
  templateItemIsEditable (state, getters, rootState, rootGetters) {
    return getters.templatesAreEditable &&
      getters.templateItem?.statusCode === 'draft' && (!getters.templateItem?.ident || getters.templateItem?.ident === rootGetters['project/getProjectInFocus']?.prefix)
  },
  templatesAreEditable (state, getters, rootState, rootGetters) {
    return rootGetters['server/getServerFunction']({ path: 'decor.rules.templates', crudAction: 'write' }) &&
      rootGetters['project/userIsEditor'] === true
  }
}

const actions = {
  cloneTemplate ({ commit, getters, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      if (!params.sourceId) {
        reject(new Error('no sourceId specified'))
        return
      }
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (!params.prefix) {
        if (!prefix) {
          reject(new Error('no project selected'))
        }
        params.prefix = prefix
      }

      TemplateService.postTemplate(null, params, { broadcast: `POST/${prefix}/template` })
        .then(res => {
          const newTemplate = res.data
          commit('addTemplateItemToList', { templateItem: newTemplate, projectPrefix: prefix })
          commit('setTemplateItem', newTemplate)

          // Find new item in the list and set it as templateActive
          const activeCat = getters.templates.find(cat => cat.type === newTemplate?.classification?.[0]?.type?.[0])

          const activeItem = activeCat.template?.find(tm => tm.id === newTemplate.id)
          if (activeItem) {
            commit('setTemplateActive', activeItem)
          }
          // Set versionIndex
          const index = activeItem.version
            ?.filter(tm => tm.id && tm.effectiveDate) // or should be and
            .findIndex(tm => tm.effectiveDate === newTemplate.effectiveDate)
          if (index > -1) {
            setTimeout(() => {
              commit('setTemplateVersionIndex', index)
            }, 0)
          }

          resolve(newTemplate)
        })
        .catch(err => reject(err))
    })
  },
  createTemplate ({ commit, dispatch, getters, rootGetters }, { data, params = {}, templateForList } = {}) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      if (!params.prefix) {
        if (!prefix) {
          reject(new Error('no project selected'))
        }
        params.prefix = prefix
      }
      TemplateService.postTemplate(data || null, params, { broadcast: `POST/${prefix}/template` })
        .then(res => {
          const newTemplate = res.data

          // Check if category exists, if not, create it
          const category = newTemplate?.classification?.[0]?.type?.[0]

          if (!getters.templates?.find(cat => cat.type === category)) {
            dispatch('createCategory', category)
          }

          const newTemplateForList = { ...(templateForList || newTemplate) }
          if ([true, 'true'].includes(params?.refOnly)) {
            newTemplateForList.ref = newTemplateForList.id
          }

          commit('addTemplateItemToList', { templateItem: newTemplateForList, projectPrefix: prefix })
          commit('setTemplateItem', newTemplate)
          const activeCat = getters.templates.find(cat => cat.type === category)

          const activeItem = activeCat.template?.find(tm => tm.id === newTemplate.id)
          if (activeItem) {
            commit('setTemplateActive', activeItem)
          }

          resolve(newTemplate)
        })
        .catch(err => reject(err))
    })
  },
  createCategory ({ commit, rootGetters }, type) {
    const categoryModel = rootGetters['server/getTemplateTypes'](type)
    if (!categoryModel?.value || !categoryModel.label) {
      console.error('no category data')
      return
    }
    const newCategory = {
      name: categoryModel.label,
      type: categoryModel.value,
      template: [],
      uuid: getUuid()
    }
    commit('addCategoryToList', newCategory)
    return newCategory
  },
  deleteTemplate ({ commit, rootGetters }, { template, params = {} }) {
    return new Promise((resolve, reject) => {
      const prefix = params.prefix || rootGetters['project/getProjectSelected']?.prefix
      if (!prefix) {
        return reject(new Error('no project prefix found'))
      }

      const id = template?.ref
      if (!id) {
        return reject(new Error('no template reference found'))
      }

      TemplateService.deleteTemplate(id, { project: prefix, ...params }, { broadcast: `DELETE/${prefix}/template` })
        .then(() => {
          commit('setTemplateActive', null)
          commit('setTemplateItem', null)
          commit('removeTemplateFromList', template)
          resolve()
        })
        .catch(error => reject(error))
    })
  },
  setTemplateList ({ commit, rootGetters }, params = {}) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) {
        return reject(new Error('no project selected'))
      }
      commit('setLoading', { key: 'templates', status: true })
      commit('setLoaded', { key: 'templates', status: false })
      const prefix = rootGetters['project/getProjectSelected'].prefix
      return TemplateService.getTemplateList({ prefix, ...params })
        .then((response) => {
          commit('setLoading', { key: 'templates', status: false })
          commit('setLoaded', { key: 'templates', status: true })

          if (response?.data?.category != null) {
            commit('setTemplateList', response?.data?.category)
          }
          resolve(response?.data?.category)
        })
        .catch((error) => {
          commit('setLoading', { key: 'templates', status: false })
          reject(error)
        })
    })
  },

  getTemplate ({ commit, rootGetters }, { id, effectiveDate, prefix, abortController }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effectiveDate-missing'))
      }
      const prefixToPass = prefix === undefined ? (rootGetters['project/getProjectSelected'].prefix) : prefix
      const language = i18n.locale
      commit('setLoading', { key: 'templateItem', status: true })
      commit('setLoaded', { key: 'templateItem', status: false })

      return TemplateService.getTemplate(id, effectiveDate, prefixToPass, language, abortController)
        .then((response) => {
          commit('setLoading', { key: 'templateItem', status: false })
          if (response.data) {
            commit('setLoaded', { key: 'templateItem', status: true })
            commit('setTemplateItem', response.data || null)
          }
          resolve()
        })
        .catch((error) => {
          if (!abortController?.signal?.aborted) {
            commit('setLoading', { key: 'templateItem', status: false })
          }
          commit('setTemplateItem', null)
          reject(error)
        })
    })
  },

  getTemplateAssociations ({ commit, rootGetters }) {
    return new Promise((resolve, reject) => {
      const prefixToPass = rootGetters['project/getProjectSelected'].prefix
      commit('setLoading', { key: 'templateAssociations', status: true })
      commit('setLoaded', { key: 'templateAssociations', status: false })

      return TemplateService.getTemplateAssociations(prefixToPass)
        .then((response) => {
          // Get template association.
          const templateAssociation = response?.data?.templateAssociation || []
          commit('setLoading', { key: 'templateAssociations', status: false })
          commit('setLoaded', { key: 'templateAssociations', status: true })
          commit('setTemplateAssociations', templateAssociation)
          resolve(templateAssociation)
        })
        .catch((error) => {
          commit('setLoading', { key: 'templateAssociations', status: false })
          commit('setTemplateAssociations', null)
          reject(error)
        })
    })
  },

  getTemplateForEdit ({ commit }, { id, effectiveDate, params = {} }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }
      TemplateService.getTemplateForEdit(id, effectiveDate, params)
        .then(response => {
          const enhancedTemplate = response.data

          commit('setTemplateItem', enhancedTemplate)
          resolve(enhancedTemplate)
        })
        .catch(err => reject(err))
    })
  },

  getTemplateDatatypes ({ commit }, params) {
    return new Promise((resolve, reject) => {
      TemplateService.getTemplateDatatypes(params)
        .then(res => {
          const datatypes = res.data
          commit('setDatatypes', datatypes)
          resolve(datatypes)
        })
        .catch(err => reject(err))
    })
  },

  getTemplateUsage ({ commit }, { id, effectiveDate, params }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      return TemplateService.getTemplateUsage(id, effectiveDate, params).then(response => {
        const usage = response?.data
        resolve(usage)
      }).catch(err => reject(err))
    })
  },

  patchTemplate ({ commit, getters, rootGetters }, { item, data, params = {}, metadata = false }) {
    return new Promise((resolve, reject) => {
      const prefix = rootGetters['project/getProjectSelected']?.prefix
      const { id, effectiveDate } = item

      if (!prefix || !id || !effectiveDate) {
        return reject(new Error('prefix-id-or-effectiveDate-missing'))
      }
      TemplateService.patchTemplate(id, effectiveDate, data, { prefix, ...params }, { broadcast: `PATCH/${prefix}/template` })
        .then(response => {
          const updatedTemplate = response.data || null
          commit('setTemplateItem', updatedTemplate)
          if (metadata) {
            commit('patchTemplateListMetaData', { updatedTemplate })
          }
          resolve(updatedTemplate)
        })
        .catch((error) => {
          reject(error)
        })
    })
  },

  putTemplate ({ commit, rootGetters }, { data, params = { deletelock: true } }) {
    return new Promise((resolve, reject) => {
      if (!data) {
        return reject(new Error('no data'))
      }
      const id = data?.id || data?.ref
      const effectiveDate = data?.effectiveDate
      if (!id || !effectiveDate) {
        return reject(new Error('no id or effectiveDate'))
      }

      const prefix = rootGetters['project/getProjectInFocus']?.prefix

      TemplateService.putTemplate(id, effectiveDate, data, params, { broadcast: `PUT/${prefix}/template` })
        .then(res => {
          const updatedTemplate = res.data
          commit('setTemplateItem', updatedTemplate)

          resolve(updatedTemplate)
        })
        .catch(err => reject(err))
    })
  }
}

const mutations = {
  setAbortController (state, controller) {
    state.abortController = controller
  },
  setExpandedSection (state, { section, value }) {
    state.expandedSections[section] = !!value
  },
  // Add a new template (clone, new template) to the list (templates) in the correct format
  addTemplateItemToList (state, { templateItem, projectPrefix }) {
    // Find the category
    const category = templateItem.classification?.[0]?.type?.[0] || templateItem.class
    if (!category) {
      console.error('could not determine the template category')
      return
    }
    const categoryIndex = state.templates.findIndex(cat => cat.type === category)

    const propsToInclude = ['displayName', 'effectiveDate', 'id', 'name', 'statusCode', 'ident']

    if (templateItem.ref) {
      propsToInclude.push('ref')
    }
    if (templateItem.class) {
      propsToInclude.push('class')
    }

    const uuid = () => getUuid()
    const newCategoryList = cloneDeep(state.templates.find(cat => cat.type === category))

    // Check if there is a template with the same id
    const existingTemplateIndex = newCategoryList?.template?.findIndex(tm => tm.id === templateItem.id)
    if (existingTemplateIndex > -1) {
      // The template is a new version of an existing template.
      // It will be added to the template.version array of the existing template.
      const newVersion = { uuid: uuid() }
      propsToInclude.forEach(prop => {
        newVersion[prop] = templateItem[prop]
      })
      if (newCategoryList.template[existingTemplateIndex]?.version) {
        newCategoryList.template[existingTemplateIndex].version.unshift(newVersion)
        // Sort the list by effectiveDate
        newCategoryList.template[existingTemplateIndex].version.sort((a, b) => {
          if (a.effectiveDate > b.effectiveDate) {
            return -1
          }
          if (a.effectiveDate < b.effectiveDate) {
            return 1
          }
          return 0
        })
      }
      // If the effectiveDate of the new template is the most recent of all versions, also update on the 'root level / dynamic version'
      if (newCategoryList.template[existingTemplateIndex]?.effectiveDate && newCategoryList.template[existingTemplateIndex].effectiveDate < templateItem.effectiveDate) {
        newCategoryList.template[existingTemplateIndex] = {
          uuid: newCategoryList.template[existingTemplateIndex].uuid,
          version: newCategoryList.template[existingTemplateIndex].version
        }
        propsToInclude.forEach(prop => {
          newCategoryList.template[existingTemplateIndex][prop] = templateItem[prop]
        })
      }

      // If the new template is a version of the currently active template (templateActive)
      if (state.templateActive && state.templateActive.id === templateItem.id) {
        if (Array.isArray(state.templateActive.version)) {
          state.templateActive.version.unshift(newVersion)
        }
      }
    } else {
      if (!newCategoryList.template) {
        newCategoryList.template = []
      }
      const isReference = !!templateItem.ref

      // This is a template with a new id. It will be added to the list
      const newListItem = {
        uuid: uuid(),
        version: isReference ? templateItem.version : [{ uuid: uuid() }]
      }
      // Construct the new list item
      propsToInclude.forEach(prop => {
        if (templateItem[prop]) {
          newListItem[prop] = templateItem[prop]
          if (!isReference) {
            newListItem.version[0][prop] = templateItem[prop]
          }
        }
        // if (templateItem.ident && templateItem.ident !== projectPrefix) {
        //   newListItem.ref = templateItem.id
        //   newListItem.version[0].ref = templateItem.id
        // }
      })
      // Construct the new list
      newCategoryList.template.push(newListItem)
      // Sort the list
      const sortBy = 'displayName'
      newCategoryList.template.sort((a, b) => {
        if (this.$filters.getMatchingTranslation({ value: a[sortBy] }) < this.$filters.getMatchingTranslation({ value: b[sortBy] })) {
          return -1
        }
        if (this.$filters.getMatchingTranslation({ value: a[sortBy] }) > this.$filters.getMatchingTranslation({ value: b[sortBy] })) {
          return 1
        }
        return 0
      })
    }
    // Update state
    if (!state.templates) {
      state.templates = [newCategoryList]
    } else {
      state.templates.splice(categoryIndex, 1, newCategoryList)
    }
  },
  addCategoryToList (state, category) {
    if (!Array.isArray(state.templates)) {
      state.templates = [category]
    } else {
      state.templates.push(category)
    }
  },
  hideRealtimeUpdateNotifications (state, { type, status }) {
    state.hideRealtimeUpdateNotifications[type] = !!status
  },
  incrementTemplateIssueAssociationCount (state) {
    if (state.templateItem?.issueAssociation?.[0]?.count) {
      const newCount = parseInt(state.templateItem.issueAssociation[0].count) + 1
      state.templateItem.issueAssociation[0].count = newCount.toString()
    }
  },
  initProfiles (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },
  initTemplate (state) {
    state.templateActive = null
    state.templateItem = null
  },
  /**
   * Updates the metadata (displayName, statusCode, versionLabel) in the templates list and in templateActive
   */
  patchTemplateListMetaData (state, { updatedTemplate, isActiveTemplate = true }) {
    const { displayName, statusCode, versionLabel } = updatedTemplate

    // Find the item in the templates list
    const versionToUpdate = this.$filters.listHelper.findItemInList({
      items: state.templates || [],
      children: 'template',
      handler: t => t.id === updatedTemplate.id
    })

    if (!versionToUpdate) {
      return
    }

    const versionIndex = versionToUpdate.version.findIndex(v => v.effectiveDate === updatedTemplate.effectiveDate)
    // Update templateActive.version and template.version in the list
    if (versionIndex >= 0) {
      if (isActiveTemplate) {
        state.templateActive.version[versionIndex].statusCode = statusCode
        state.templateActive.version[versionIndex].displayName = displayName
        state.templateActive.version[versionIndex].versionLabel = versionLabel
      }

      versionToUpdate.version[versionIndex] = {
        ...versionToUpdate.version[versionIndex],
        displayName,
        statusCode,
        versionLabel
      }
    }

    // If updated template is the most recent version, update properties on 'root level'
    if (versionToUpdate.effectiveDate === updatedTemplate.effectiveDate) {
      // Replace in the templates list
      state.templates = this.$filters.listHelper.replaceItemInList({
        items: state.templates || [],
        item: {
          ...versionToUpdate,
          displayName,
          statusCode,
          versionLabel
        },
        children: 'template',
        handler: t => t.id === updatedTemplate.id && t.effectiveDate === updatedTemplate.effectiveDate
      })

      // Update templateActive props
      if (isActiveTemplate) {
        state.templateActive.statusCode = statusCode
        state.templateActive.displayName = displayName
        state.templateActive.versionLabel = versionLabel
      }
    } else {
      // If updated template is NOT the most recent version, only update properties on 'template.version' level
      state.templates = this.$filters.listHelper.replaceItemInList({
        items: state.templates || [],
        item: versionToUpdate,
        children: 'template',
        handler: t => t.id === updatedTemplate.id
      })
    }
  },

  preserveTemplateItemOnSelect (state, value) {
    state.preserveTemplateItemOnSelect = value
  },
  removeTemplateFromList (state, template) {
    if (!template?.ref) {
      return console.error('not a template reference')
    }
    if (!Array.isArray(state.templates)) {
      return console.error('no list of templates found')
    }
    const templateCategory = template.class

    const newList = cloneDeep(state.templates)
    const category = newList.find(cat => cat.type === templateCategory)
    if (!Array.isArray(category?.template)) {
      return
    }

    category.template = category.template.filter(tm => tm.ref !== template.ref)

    state.templates = newList
  },
  setEditableTemplateBody (state, templateBody) {
    state.editableTemplateBody = templateBody
  },
  setLoaded,
  setLoading,
  setTemplateActive (state, templateActive) {
    state.templateActive = templateActive
  },
  setDatatypes (state, datatypes) {
    state.datatypes = datatypes
  },
  setTemplateItem (state, item) {
    state.templateItem = item
  },
  setTemplateAssociations (state, item) {
    state.templateAssociations = item
  },
  setTemplateList (state, templates) {
    state.templates = templates
  },
  setTemplateVersionIndex (state, index) {
    state.templateVersionIndex = index
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
