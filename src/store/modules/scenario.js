import ScenarioService from '@/services/ScenarioService'
import { cloneDeep } from 'lodash'
import Vue from 'vue'
import getSessionStorage from '@/filters/getSessionStorage'
import { isLoaded, isLoading, setLoaded, setLoading } from '../utils'

const state = {
  abortController: null,
  actors: [],
  callApi: true, // only used during development (in case backend is not yet ready)
  loadingActors: false,

  activeItem: null,
  activeConceptItem: null,
  scenarioItems: [],
  scenarioItem: null,
  transactionItems: [],
  transactionItem: null,
  transactionItemConcepts: null,
  expandedSections: {
    description: getSessionStorage()?.scenario?.expandedSections?.description || false,
    historySection: getSessionStorage()?.scenario?.expandedSections?.historySection || false,
    metadata: getSessionStorage()?.scenario?.expandedSections?.metadata || false
    // relationshipSection: getSessionStorage()?.scenario?.expandedSections?.relationshipSection || false,
    // usageSection: getSessionStorage()?.scenario?.expandedSections?.usageSection || false
  },
  hideRealtimeUpdateNotifications: {
    scenarioItems: false,
    actors: false
  },
  loading: {
    activeItem: false,
    scenarioItems: false,
    transactionItems: false,
    transactionItem: false
  },
  loaded: {
    actors: false,
    activeItem: false,
    scenarioItems: false,
    transactionItems: false,
    transactionItem: false
  },
  showNewScenarioDialog: false
}

const initialState = cloneDeep(state)
delete initialState.expandedSections

const getters = {
  abortController (state) {
    return state.abortController
  },
  activeItem (state) {
    return state.activeItem
  },
  actors (state) {
    return state.actors
  },
  loadingActors (state) {
    return state.loadingActors
  },
  scenarioItem (state) {
    return state.scenarioItem
  },
  scenarioItems (state) {
    return state.scenarioItems
  },
  transactionItem (state) {
    return state.transactionItem
  },
  fullTransactionList: state => ({ initialList = state.scenarioItems } = {}) => {
    const transactionList = []
    const scenarioItems = initialList || []
    scenarioItems.forEach(scenario => {
      if (Array.isArray(scenario.transaction)) {
        scenario.transaction.forEach((group) => {
          transactionList.push(group)
          if (Array.isArray(group.transaction)) {
            group.transaction.forEach((transaction) => {
              transactionList.push(transaction)
            })
          }
        })
      }
    })
    return transactionList
  },
  isLoaded,
  isLoading,
  hideRealtimeUpdateNotifications: state => type => state.hideRealtimeUpdateNotifications[type]
}

const actions = {
  setActors ({ commit, rootGetters }) {
    commit('setActorsLoading', true)
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) {
        reject(new Error('no project selected'))
      }
      const prefix = rootGetters['project/getProjectSelected'].prefix
      return ScenarioService.getActorList(prefix)
        .then((response) => {
          if (response?.data?.actor != null) {
            commit('setActors', response.data.actor)
          } else {
            commit('setActors', [])
          }
          commit('setActorsLoading', false)
          commit('setLoaded', { key: 'actors', status: true })

          resolve('setActors')
        })
        .catch((error) => {
          commit('setActorsLoading', false)
          reject(error)
        })
    })
  },
  saveActor ({ state, commit, rootGetters }, actor) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) {
        reject(new Error('no project selected'))
      }
      const prefix = rootGetters['project/getProjectSelected'].prefix

      if (actor.id == null) {
        if (state.callApi) {
          return ScenarioService.postScenarioActor(prefix, actor, { broadcast: `POST/${prefix}/scenarioActor` })
            .then((response) => {
              if (response?.data) {
                commit('addActor', response.data)
                resolve(response)
              } else {
                reject(new Error('no-actor-found'))
              }
            })
            .catch((error) => reject(error))
        } else {
          actor.id = Date.now()
          commit('addActor', actor)
          resolve()
        }
      } else {
        if (state.callApi) {
          return ScenarioService.patchScenarioActor(prefix, actor, { broadcast: `PATCH/${prefix}/scenarioActor` })
            .then((response) => {
              if (response?.data?.id === actor.id) {
                const index = state.actors.findIndex((a) => a.id === actor.id)
                if (index > -1) {
                  commit('updateActor', { index, actor })
                  resolve()
                } else {
                  reject(new Error('actor-not-found'))
                }
              } else {
                reject(new Error('no-data'))
              }
            })
            .catch((error) => reject(error))
        } else {
          const index = state.actors.findIndex((a) => a.id === actor.id)
          if (index > -1) {
            commit('updateActor', { index, actor })
            resolve()
          } else {
            reject(new Error('actor-not-found'))
          }
        }
      }
    })
  },
  deleteActor ({ commit, rootGetters }, actor) {
    return new Promise((resolve, reject) => {
      if (rootGetters['project/getProjectSelected'] == null) {
        reject(new Error('no project selected'))
      }
      const prefix = rootGetters['project/getProjectSelected'].prefix
      return ScenarioService.deleteActor(prefix, actor, { broadcast: `DELETE/${prefix}/scenarioActor` })
        .then((response) => {
          if (response.status === 204) {
            const index = state.actors.indexOf(actor)
            if (index > -1) {
              commit('deleteActor', { index })
              resolve()
            } else {
              reject(new Error('actor-not-found'))
            }
          }
        })
        .catch((error) => reject(error))
    })
  },

  /**
   * Load scenario items.
   * @param commit
   * @param rootGetters
   * @returns {Promise<unknown>}
   */
  loadScenarioItems ({ commit, rootGetters }, params = {}) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix
      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      // Mark scenarios as loading.
      commit('setLoading', { key: 'scenarioItems', status: true })

      // Load scenario list.
      return ScenarioService.getScenarioList({ prefix: projectPrefix, ...params }).then((response) => {
        // Get scenario items.
        const scenarioItems = response?.data?.scenario || []
        // Set scenario items.
        commit('setScenarioItems', scenarioItems)
        // Mark scenarios as not loading.
        commit('setLoading', { key: 'scenarioItems', status: false })
        // Mark scenarios as loaded.
        commit('setLoaded', { key: 'scenarioItems', status: true })
        // Resolve.
        resolve(scenarioItems)
      }).catch((error) => {
        commit('setLoading', { key: 'scenarioItems', status: false })
        reject(error)
      })
    })
  },

  /**
   * Set active item.
   * @param commit
   * @param item
   */
  setActiveItem ({ commit }, item) {
    // Commit set active item.
    commit('setActiveItem', item)
  },

  /**
   * Update scenario item.
   * @param commit
   * @param dispatch
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async updateScenarioItem ({ commit, rootGetters }, { item, data }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      // Patch scenario transactions.
      ScenarioService.updateScenarioItem(
        id,
        effectiveDate,
        data,
        { broadcast: `PUT/${prefix}/scenario` }
      ).then((response) => {
        // Get patched item.
        const updatedItem = { ...item, ...response?.data }
        // Commit update scenario in items mutation.
        commit('updateScenarioItem', { item, data: updatedItem })
        commit('setActiveItem', updatedItem)
        commit('setScenarioItem', updatedItem)
        // Resolve response.
        resolve(updatedItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Patch scenario item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async patchScenarioItem ({ commit, rootGetters }, { item, data }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No selected project'))
      }

      // Patch scenario transactions.
      ScenarioService.patchScenarioItem(
        id,
        effectiveDate,
        data,
        { broadcast: `PATCH/${prefix}/scenario` }
      ).then((response) => {
        // Get patched item.
        const patchedItem = response?.data
        // Commit update scenario in items mutation.
        commit('updateScenarioItem', { item, data: patchedItem })
        // Update activeItem and scenarioItem
        commit('setActiveItem', patchedItem)
        commit('setScenarioItem', patchedItem)
        // Resolve response.
        resolve(patchedItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Patch transaction item.
   * @param commit
   * @param item
   * @param data
   * @returns {Promise<unknown>}
   */
  async patchTransactionItem ({ commit, rootGetters }, { item, data }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Check if id or effective date are undefined.
      if (!item?.id === true || !item?.effectiveDate === true) {
        // Reject error.
        return reject(new Error('id-or-effective-date-is-missing'))
      }

      // Get id and effective date from item
      const { id, effectiveDate } = item

      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No selected project'))
      }

      // Patch scenario transactions.
      ScenarioService.patchTransactionItem(
        id,
        effectiveDate,
        data,
        { broadcast: `PATCH/${prefix}/transaction` }
      ).then((response) => {
        // Get patched item.
        const patchedItem = { ...item, ...response?.data }
        // Loop each key.
        for (const key of Object.keys(patchedItem)) {
          // Check if key does not exist in response data.
          if (!(key in response?.data)) {
            // Delete property on patched item.
            delete patchedItem[key]
          }
        }

        // Commit update scenario in items mutation.
        commit('updateScenarioItem', { item, data: patchedItem })
        // Commit set scenario item mutation.
        commit('setScenarioItem', patchedItem)
        commit('setActiveItem', patchedItem)
        // Resolve response.
        resolve(patchedItem)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Create scenario item.
   * @param commit
   * @param dispatch
   * @param rootGetters
   * @param newScenarioItem
   * @returns {Promise<unknown>}
   */
  async createScenarioItem ({ commit, dispatch, rootGetters }, newScenarioItem) {
    // newScenarioItem is used to update the newly created scenario in 'step two'
    return new Promise((resolve, reject) => {
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      return ScenarioService.createScenarioItem(projectPrefix, newScenarioItem, { broadcast: `POST/${projectPrefix}/scenario` }).then(async (response) => {
        await commit('addNewScenarioItem', { item: response.data })
        return resolve(response)
      }).catch((error) => reject(error))
    })
  },

  /**
   * Clone scenario item.
   * @param commit
   * @param dispatch
   * @param rootGetters
   * @param scenarioItem
   * @returns {Promise<unknown>}
   */
  async cloneScenarioItem ({ commit, dispatch, rootGetters }, scenarioItem) {
    // newScenarioItem is used to update the newly created scenario in 'step two'
    return new Promise((resolve, reject) => {
      // Get id and effective date from scenario item.
      const { id, effectiveDate } = scenarioItem
      // Get project prefix.
      const projectPrefix = rootGetters['project/getProjectSelected']?.prefix

      // Check if project prefix is not defined.
      if (!projectPrefix) {
        // Reject error.
        return reject(new Error('no-project-selected'))
      }

      return ScenarioService.cloneScenarioItem(projectPrefix, id, effectiveDate, { broadcast: `POST/${projectPrefix}/scenario` }).then(async (response) => {
        await commit('addNewScenarioItem', { item: response.data })
        return resolve(response)
      }).catch((error) => reject(error))
    })
  },

  postScenarioStatus ({ commit, rootGetters }, { params, id, effectiveDate }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id or effectiveDate missing'))
      }
      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      ScenarioService.postScenarioStatus(id, effectiveDate, params, { broadcast: `POST/${prefix}/scenarioStatus` })
        .then(response => {
          const report = response.data
          commit('handleRecursiveScenarioStatusUpdate', report)
          resolve(report)
        })
        .catch(err => reject(err))
    })
  },

  postTransactionStatus ({ commit, rootGetters }, { params, id, effectiveDate }) {
    return new Promise((resolve, reject) => {
      if (!id || !effectiveDate) {
        return reject(new Error('Id or effectiveDate missing'))
      }
      const prefix = rootGetters['project/getProjectInFocus']?.prefix
      if (!prefix) {
        return reject(new Error('No project selected'))
      }

      ScenarioService.postTransactionStatus(id, effectiveDate, params, { broadcast: `POST/${prefix}/scenarioStatus` })
        .then(response => {
          const report = response.data
          commit('handleRecursiveScenarioStatusUpdate', report)
          resolve(report)
        })
        .catch(err => reject(err))
    })
  },

  postTransactionQuestionnaire ({ commit, rootGetters }, { params = {}, data } = {}) {
    return new Promise((resolve, reject) => {
      const project = rootGetters['project/getProjectInFocus']?.prefix
      if (!params.project && !project) {
        return reject(new Error('No project selected'))
      }

      ScenarioService.postTransactionQuestionnaire({
        params: { project, ...params },
        data,
        broadcast: `POST/${params.project || project}/transactionQuestionnaire`
      })
        .then(res => {
          resolve(res.data)
        })
        .catch(err => reject(err))
    })
  },

  /**
   * Clone scenario transaction item.
   * @param state
   * @param commit
   * @param getters
   * @param dispatch
   * @param type
   * @param mode
   * @param ref
   * @param item
   * @param target
   * @returns {Promise<unknown>}
   */
  async cloneScenarioTransactionItem ({ state, commit, dispatch }, { type, mode, ref, item, target }) {
    // Return new promise.
    return new Promise((resolve, reject) => {
      // Get id and effective date from item and target.
      const id = target?.id
      const effectiveDate = target?.effectiveDate
      const sourceId = item?.id
      const sourceEffectiveDate = item?.effectiveDate

      // Create new transaction item.
      return ScenarioService.cloneScenarioTransactionItem(id, effectiveDate, sourceId, sourceEffectiveDate, type, mode, ref).then((response) => {
        // Apply new item to scenario structure.
        const newScenario = this.$filters.listHelper?.pushItemInsideListItem({
          items: [{ ...this.$filters.clone(target) }],
          item: response.data,
          children: 'transaction',
          handler: (match) => match.id === (ref || target.id)
        })[0]

        // Commit update scenario in items mutation.
        commit('updateScenarioItem', { item: newScenario, data: newScenario })

        return resolve(response)
      }).catch((error) => reject(error))
    })
  },

  initItems ({ commit }) {
    commit('initItems')
  },

  /**
   * New scenario
   */
  setShowNewScenarioDialog ({ commit }, status) {
    commit('setShowNewScenarioDialog', status)
  },

  getTransaction ({ commit }, { params, item, fulltree, setTransactionItem = true, abortController = null } = {}) {
    return new Promise((resolve, reject) => {
      const { id, effectiveDate } = item
      if (!id || !effectiveDate) {
        return reject(new Error('No id or effectiveDate passed'))
      }

      commit('setLoading', { key: 'transactionItem', status: true })

      ScenarioService.getRepresentingTemplate(id, effectiveDate, !!fulltree, params, abortController)
        .then(res => {
          const transactionItem = res.data
          if (setTransactionItem) {
            commit('setTransactionItem', transactionItem)
          }
          commit('setTransactionItemConcepts', transactionItem?.concept)
          commit('setLoaded', { key: 'transactionItem', status: true })

          resolve()
        })
        .catch(err => reject(err))
        .finally(() => {
          commit('setLoading', { key: 'transactionItem', status: false })
        })
    })
  }
}

const mutations = {
  setAbortController (state, controller) {
    state.abortController = controller
  },
  setActors (state, actors) {
    state.actors = actors
  },
  addActor (state, actor) {
    state.actors.push(actor)
  },
  updateActor (state, { index, actor }) {
    state.actors.splice(index, 1, actor)
  },
  deleteActor (state, { index }) {
    state.actors.splice(index, 1)
  },
  setActorsLoading (state, status) {
    state.loadingActors = status
  },
  handleRecursiveScenarioStatusUpdate (state, report) {
    const successfulChanges = report.success
    const fieldsToUpdate = ['statusCode', 'versionLabel', 'expirationDate', 'lastModifiedDate']

    if (Array.isArray(successfulChanges)) {
      successfulChanges.forEach(change => {
        const { id, effectiveDate, itemcode, statusCode, versionLabel, expirationDate } = change

        // SC + TR
        if (itemcode === 'SC') {
          // Update scenarioItems.scenarioItem
          const itemInList = state.scenarioItems?.find(sc => sc.id === id && sc.effectiveDate === effectiveDate)
          if (!itemInList) {
            return
          }
          const newItem = { ...itemInList, statusCode }
          if (versionLabel != null) {
            newItem.versionLabel = versionLabel
          }
          if (expirationDate != null) {
            newItem.expirationDate = expirationDate
          }
          state.scenarioItems = this.$filters.listHelper.replaceItemInList({
            items: state.scenarioItems,
            item: newItem,
            children: null,
            handler: (match) => match.id === itemInList.id && match.effectiveDate === itemInList.effectiveDate
          })

          // Update activeItem if it is the currently selected item
          if (state.activeItem?.id === id && state.activeItem?.effectiveDate === effectiveDate) {
            fieldsToUpdate.forEach(prop => {
              if (newItem[prop]) {
                state.activeItem[prop] = newItem[prop]
              }
            })
          }

          // Update scenarioItem if it is the currently selected item
          if (state.scenarioItem?.id === id && state.scenarioItem?.effectiveDate === effectiveDate) {
            state.scenarioItem = newItem
          }
        } else if (itemcode === 'TR') {
          // Update scenarioItems.scenarioItem.transaction
          const itemInList = this.$filters.listHelper.findItemInList({
            items: state.scenarioItems,
            children: 'transaction',
            handler: match => match.id === id && match.effectiveDate === effectiveDate
          })
          if (!itemInList) {
            return
          }

          const newItem = { ...itemInList, statusCode }
          if (versionLabel != null) {
            newItem.versionLabel = versionLabel
          }
          if (expirationDate != null) {
            newItem.expirationDate = expirationDate
          }

          state.scenarioItems = this.$filters.listHelper.replaceItemInList({
            items: state.scenarioItems,
            item: newItem,
            children: 'transaction',
            handler: (match) => match?.id === id && match.effectiveDate === effectiveDate
          })

          // Update activeItem if it is the currently selected item
          if (state.activeItem?.id === id && state.activeItem?.effectiveDate === effectiveDate) {
            fieldsToUpdate.forEach(prop => {
              if (newItem[prop]) {
                state.activeItem[prop] = newItem[prop]
              }
            })
          }

          // Update scenarioItem if it is the currently selected item
          if (state.scenarioItem?.id === id && state.scenarioItem?.effectiveDate === effectiveDate) {
            const newScenarioItem = { ...state.scenarioItem, statusCode }
            if (versionLabel != null) {
              newScenarioItem.versionLabel = versionLabel
            }
            if (expirationDate != null) {
              newScenarioItem.expirationDate = expirationDate
            }

            state.scenarioItem = newScenarioItem
          }
        }
      })
    }
  },
  hideRealtimeUpdateNotifications (state, { type, status }) {
    state.hideRealtimeUpdateNotifications[type] = !!status
  },

  /**
   * Init scenario.
   * @param state
   */
  initScenario (state, exceptions) {
    for (const [key, val] of Object.entries(cloneDeep(initialState))) {
      if (!exceptions?.includes(key)) {
        state[key] = val
      }
    }
  },

  /**
   * Set scenario items.
   * @param state
   * @param scenarioItems
   */
  setScenarioItems (state, scenarioItems) {
    state.scenarioItems = scenarioItems
  },

  /**
   * Set scenario item.
   * @param state
   * @param scenarioItem
   */
  setScenarioItem (state, scenarioItem) {
    state.scenarioItem = scenarioItem
  },

  /**
   * Set transaction items.
   * @param state
   * @param transactionItems
   */
  setTransactionItems (state, transactionItems) {
    state.transactionItems = transactionItems
  },

  /**
   * Set transaction item.
   * @param state
   * @param transactionItem
   */
  setTransactionItem (state, transactionItem) {
    state.transactionItem = transactionItem
  },

  setTransactionItemConcepts (state, transactionItem) {
    const prepare = (item) => {
      // Get minimum multiplicity.
      const minimumMultiplicity = item?.minimumMultiplicity ?? (item?.absent ? '' : '0')
      // Get maximum multiplicity.
      const maximumMultiplicity = item?.maximumMultiplicity ?? (item?.absent ? '' : '*')
      // Get a optionally correct conformance.
      let conformance = item?.conformance

      if (conformance === '') {
        conformance = item?.absent ? '' : minimumMultiplicity === '0' ? 'O' : ''
      }

      if (maximumMultiplicity === '0') {
        conformance = 'NP'
      }

      if (Array.isArray(item.concept)) {
        item.concept = item.concept.map(prepare)
      }

      // Return prepared item object.
      return {
        ...item,
        minimumMultiplicity,
        maximumMultiplicity,
        conformance
      }
    }

    state.transactionItemConcepts = transactionItem?.map(prepare) || null
  },

  /**
   * Set active item.
   * @param state
   * @param activeItem
   */
  setActiveItem (state, activeItem) {
    state.activeItem = activeItem
  },

  /**
   * Set active concept item.
   * @param state
   * @param conceptItem
   */
  setActiveConceptItem (state, conceptItem) {
    state.activeConceptItem = conceptItem
  },

  setActiveItemKeepReference (state, updatedItem) {
    for (const key in state.activeItem) {
      delete state.activeItem[key]
    }
    for (const key in updatedItem) {
      Vue.set(state.activeItem, key, updatedItem[key])
    }
  },

  /**
   * Update scenario item.
   * @param state
   * @param item
   * @param data
   */
  updateScenarioItem (state, { item, data }) {
    const { uuid } = this.$filters.listHelper.findItemInList({
      items: state.scenarioItems,
      children: 'transaction',
      handler: (match) => match.id === item.id && match.effectiveDate === item.effectiveDate
    }) || {}

    // Replace scenario with item in list.
    state.scenarioItems = this.$filters.listHelper.replaceItemInList({
      items: state.scenarioItems,
      item: { ...data, uuid },
      children: 'transaction',
      handler: (match) => match.id === item.id && match.effectiveDate === item.effectiveDate
    })
  },

  /**
   * Update transaction item.
   * @param state
   * @param item
   * @param data
   */
  updateTransactionItem (state, { item, data }) {
    // Replace transaction with item in list.
    state.transactionItems = this.$filters.listHelper.replaceItemInList({
      items: state.transactionItems,
      item: data,
      children: 'transaction',
      handler: (match) => match?.id === item?.id
    })
  },

  /**
   * Add new scenario item.
   * @param state
   * @param item
   */
  addNewScenarioItem (state, { item }) {
    // Push new scenario item to array.
    state.scenarioItems.push(item)
  },

  /**
   * Add new transaction item.
   * @param state
   * @param item
   * @param newTransactionItem
   * @param mode
   */
  addNewTransactionItem (state, { item, newTransactionItem, mode }) {
    // Define mappings.
    const mappings = {
      following: 'addItemAfterListItem',
      preceding: 'addItemBeforeListItem',
      into: 'unshiftItemInsideListItem'
    }

    // Get matching mapping.
    const mapping = mappings?.[mode]
    // Check if mapping is not nil.
    if (!!mapping === true) {
      // Call list helper and set updated transaction items.
      state.transactionItems = this.$filters.listHelper[mapping]({
        items: state.transactionItems,
        item: newTransactionItem,
        children: 'transaction',
        handler: (match) => match.id === item.id
      })
    }
  },

  setExpandedSection (state, { section, value }) {
    state.expandedSections[section] = !!value
  },

  /**
   * Set loading state.
   * @param state
   * @param type
   * @param status
   */
  setLoading (state, { key, status }) {
    setLoading(state, { key: 'activeItem', status })
    setLoading(state, { key, status })
  },

  /**
   * Set loaded state.
   * @param state
   * @param type
   * @param status
   */
  setLoaded (state, { key, status }) {
    setLoaded(state, { key: 'activeItem', status })
    setLoaded(state, { key, status })
  },

  /**
   * Set show new scenario dialog.
   * @param state
   * @param status
   */
  setShowNewScenarioDialog (state, status) {
    state.showNewScenarioDialog = status
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
