import NewsService from '@/services/NewsService'

const state = {
  loadingNews: true,
  news: []
}

const getters = {
  getNews (state) {
    return state.news
  },
  loadingNews (state) {
    return state.loadingNews
  }
}

const actions = {
  setNews ({ commit }) {
    commit('changeLoadingNews', true)
    return new Promise((resolve, reject) => {
      return NewsService.getNews()
        .then((response) => {
          if (response?.data?.news) {
            commit('setNews', response.data.news)
          }
          commit('changeLoadingNews', false)
          resolve()
        })
        .catch((error) => {
          reject(error)
        })
    })
  },
  postNewsItem ({ commit }, newsItem) {
    return new Promise((resolve, reject) => {
      return NewsService.postNewsItem(newsItem, { broadcast: 'POST/newsItem' })
        .then((repsonse) => {
          if (repsonse?.data) {
            commit('postNewsItem', repsonse.data)
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  putNewsItem ({ commit }, newsItem) {
    return new Promise((resolve, reject) => {
      return NewsService.putNewsItem(newsItem.id, newsItem, { broadcast: 'PUT/newsItem' })
        .then((repsonse) => {
          if (repsonse?.data) {
            commit('putNewsItem', repsonse.data)
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  deleteNewsItem ({ commit }, newsItem) {
    return new Promise((resolve, reject) => {
      return NewsService.deleteNewsItem(newsItem.id)
        .then((response) => {
          if (response.status === 204) {
            commit('deleteNewsItem', newsItem)
          } else {
            reject(new Error(this.$t('unknown-error')))
          }
          resolve()
        })
        .catch((error) => reject(error))
    })
  },
  changeLoadingNews ({ commit }, status) {
    commit('changeLoadingNews', status)
  }
}

const mutations = {
  setNews (state, news) {
    state.news = news
  },
  postNewsItem (state, newsItem) {
    state.news.push(newsItem)
  },
  putNewsItem (state, newsItem) {
    const index = state.news.findIndex((news) => news.id === newsItem.id)
    if (index > -1) {
      state.news.splice(index, 1, newsItem)
    }
  },
  deleteNewsItem (state, newsItem) {
    const index = state.news.indexOf(newsItem)
    if (index > -1) {
      state.news.splice(index, 1)
    }
  },
  changeLoadingNews (state, status) {
    state.loadingNews = status
  }
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
}
