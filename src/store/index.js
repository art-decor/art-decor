import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import app from './modules/app'
import axios from './modules/axios'
import dataset from './modules/dataset'
import oidregistry from './modules/oidregistry'
import news from './modules/news'
import authentication from './modules/authentication'
import project from './modules/project'
import issues from './modules/issues'
import scenario from './modules/scenario'
import server from './modules/server'
import terminology from './modules/terminology'
import rule from './modules/rule'
import questionnaire from './modules/questionnaire'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    authentication,
    app,
    axios,
    dataset,
    news,
    oidregistry,
    project,
    issues,
    scenario,
    server,
    terminology,
    rule,
    questionnaire
  },
  strict: debug,
  plugins: [
    createPersistedState({
      paths: [
        'app.compactTableMode',
        'authentication.token',
        'authentication.tokenExpires',
        'authentication.tokenIssued',
        'authentication.user',
        'authentication.isUserLoggedIn'
      ]
    }),
    createPersistedState({
      key: 'adstate',
      storage: window.sessionStorage,
      paths: [
        'dataset.expandedSections',
        'scenario.expandedSections',
        'terminology.expandedSections',
        'rule.template.expandedSections',
        'rule.profile.expandedSections',
        'questionnaire.expandedSections'
      ]
    })
  ]
})
