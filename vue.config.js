process.env.VUE_APP_VERSION = require('./package.json').version
process.env.VUE_APP_DESCRIPTION = require('./package.json').description
process.env.VUE_APP_BUILDNUMBER = require('./artdecor.json').buildNumber

module.exports = {
  devServer: {
    proxy: {
      '/image-proxy': {
        target: process.env.VUE_APP_DEV_SERVER_API_PROXY_TARGET || 'http://localhost:3000'
      },
      '/api': {
        target: process.env.VUE_APP_DEV_SERVER_API_PROXY_TARGET || 'http://localhost:3000',
        changeOrigin: true,
        pathRewrite: {
          '^/api': 'exist'
        }
      }
    }
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? '/ad/'
    : '/',
  transpileDependencies: [
    'vuetify'
  ],
  pluginOptions: {
    i18n: {
      locale: 'en-US',
      fallbackLocale: 'en-US',
      localeDir: 'locales',
      enableInSFC: true
    }
  }
}
